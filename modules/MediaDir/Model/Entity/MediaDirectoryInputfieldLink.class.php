<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Media Directory Inputfield Link Class
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      Cloudrexx Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_mediadir
 * @todo        Edit PHP DocBlocks!
 */
namespace Cx\Modules\MediaDir\Model\Entity;
/**
 * Media Directory Inputfield Link Class
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  module_mediadir
 */
class MediaDirectoryInputfieldLink extends \Cx\Modules\MediaDir\Controller\MediaDirectoryLibrary implements Inputfield
{
    public $arrPlaceholders = array('TXT_MEDIADIR_INPUTFIELD_NAME','MEDIADIR_INPUTFIELD_VALUE','MEDIADIR_INPUTFIELD_VALUE_HREF','MEDIADIR_INPUTFIELD_VALUE_NAME');



    /**
     * Constructor
     */
    function __construct($name)
    {
        parent::__construct('.', $name);
        parent::getFrontendLanguages();
        parent::getSettings();
    }



    function getInputfield($intView, $arrInputfield, $intEntryId=null)
    {
        global $objDatabase, $_ARRAYLANG;

        $langId = static::getOutputLocale()->getId();

        switch ($intView) {
            default:
            case 1:
                //modify (add/edit) View
                $intId = intval($arrInputfield['id']);
                $arrValue = array();
                if (!empty($intEntryId)) {
                    $objInputfieldValue = $objDatabase->Execute("
                        SELECT
                            `value`,
                            `lang_id`
                        FROM
                            ".DBPREFIX."module_".$this->moduleTablePrefix."_rel_entry_inputfields
                        WHERE
                            field_id=".$intId."
                        AND
                            entry_id=".$intEntryId."
                    ");

                    if ($objInputfieldValue !== false) {
                        while (!$objInputfieldValue->EOF) {
                            $arrValue[intval($objInputfieldValue->fields['lang_id'])] = $objInputfieldValue->fields['value'];
                            $objInputfieldValue->MoveNext();
                        }
                        $arrValue[0] = isset($arrValue[$langId]) ? $arrValue[$langId] : null;
                    }
                }

                if (empty($arrValue)) {
                    foreach ($arrInputfield['default_value'] as $intLangKey => $strDefaultValue) {
                        $strDefaultValue = empty($strDefaultValue) ? $arrInputfield['default_value'][0] : $strDefaultValue;
                        if (substr($strDefaultValue,0,2) == '[[') {
                            $objPlaceholder = new \Cx\Modules\MediaDir\Controller\MediaDirectoryPlaceholder($this->moduleName);
                            $arrValue[$intLangKey] = $objPlaceholder->getPlaceholder($strDefaultValue);
                        } else {
                            $arrValue[$intLangKey] = $strDefaultValue;
                        }
                    }
                }

                $cx = \Cx\Core\Core\Controller\Cx::instanciate();

                $minimize  = '';
                if ($cx->isBackendMode() || $this->arrSettings['settingsFrontendUseMultilang']) {
                    $minimize  = "<a href=\"javascript:ExpandMinimize('$intId');\">{$_ARRAYLANG['TXT_MEDIADIR_MORE']}&nbsp;&raquo;</a>";
                }

                $strDefaultValue = isset($arrValue[0]) ? $arrValue[0] : '';
                $strDefaultInput = $this->getInput($intId, $strDefaultValue, 0, $arrInputfield);
                $strInputfield = <<<INPUT
                    <div id="{$this->moduleNameLC}Inputfield_{$intId}_Minimized" class="{$this->moduleNameLC}GroupMultilang" style="display: block; float:left;">
                        $strDefaultInput
                        $minimize
                    </div>
INPUT;
                if ($cx->isBackendMode() || $this->arrSettings['settingsFrontendUseMultilang']) {
                    $countFrontendLang = count($this->arrFrontendLanguages);
                    $strInputfield .= '<div id="' . $this->moduleNameLC . 'Inputfield_' .$intId . '_Expanded" class="' . $this->moduleNameLC . 'GroupMultilang" style="display: none; float:left;">';

                    foreach ($this->arrFrontendLanguages as $key => $arrLang) {
                        $intLangId = $arrLang['id'];
                        $minimize  = '';
                        if(($key+1) == $countFrontendLang) {
                            $minimize = "&nbsp;<a href=\"javascript:ExpandMinimize('".$intId."');\">&laquo;&nbsp;".$_ARRAYLANG['TXT_MEDIADIR_MINIMIZE']."</a>";
                        }

                        $value    = isset($arrValue[$intLangId]) ? $arrValue[$intLangId] : '';
                        $strInput = $this->getInput($intId, $value, $intLangId);
                        $strInputfield .= <<<INPUT
                            <div>
                                $strInput
                                $minimize
                            </div>
INPUT;
                    }
                    $strInputfield .= '</div>';
                }
                return $strInputfield;
                break;
            case 2:
                //search View
                break;
        }
    }

    /**
     * Get input field based on locale id and value as HTML-code
     *
     * @param integer $id            Input field id
     * @param string  $data          Input field value from db
     * @param integer $langId        Locale id
     * @param array   $arrInputfield Metadata of inputfield
     *
     * @return string
     */
    protected function getInput($id = 0, $data = '', $langId = 0, $arrInputfield = array())
    {
        global $_ARRAYLANG;

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();

        $arrValue = $this->parseData($data);
        $value = $arrValue['href'];
        $displayName = $arrValue['name'];

        $flagPath   = $cx->getCodeBaseOffsetPath() . $cx->getCoreFolderName().'/Country/View/Media/Flag';
        $inputStyle = '';
        $inputDefaultClass = array();
        if (!empty($langId)) {
            $inputStyle = 'background: #ffffff url(\''. $flagPath .'/flag_'. \FWLanguage::getLanguageParameter($langId, 'iso1') .'.gif\') no-repeat 3px 3px;';
            $inputDefaultClass[] = $this->moduleNameLC . 'LangInputfield';
        } else {
            $inputDefaultClass[] = $this->moduleNameLC . 'InputfieldDefault';
        }

        $infoValue = '';
        if (!empty($arrInputfield['info'][0])) {
            $infoValue = empty($arrInputfield['info'][$langId]) ? 'title="'.$arrInputfield['info'][0].'"' : 'title="'.$arrInputfield['info'][$langId].'"';
            if ($cx->isFrontendMode()) {
                $inputDefaultClass[] = $this->moduleNameLC . 'InputfieldHint';
            }
        }

        if ($cx->isFrontendMode()) {
            $inputDefaultClass[] = $this->moduleNameLC . 'InputfieldLink';
        }

        $strInputfield = '<input type="text" data-id="' . $id . '" class="' . implode(' ', $inputDefaultClass) . '" name="' . $this->moduleNameLC . 'Inputfield[' . $id . '][href][' . $langId . ']" id="' . $this->moduleNameLC . 'Inputfield_' . $id . '_' . $langId . '" value="' . $value . '" style="' . $inputStyle .'" ' . $infoValue . ' onfocus="this.select();" />';

        if ($cx->isBackendMode()) {
            $strInputfield .= ' &nbsp; <input type="button"
                onClick="getMediaBrowser($J(this));"
                data-input-id="' . $this->moduleNameLC . 'Inputfield_' . $id . '_' . $langId . '"
                data-views="filebrowser,sitestructure"
                data-startmediatype="' . $this->moduleNameLC . '"
                value="' . $_ARRAYLANG['TXT_BROWSE'] . '"
            />';
        }

        $inputDefaultClass[] = $this->moduleNameLC . 'InputfieldLinkDisplayName';
        $strInputfield .= '<br />
            <input type="text" name="' . $this->moduleNameLC . 'Inputfield[' . $id . '][name][' . $langId . ']"
                value="' . $displayName . '"
                data-id="' . $id . '"
                data-related-field-prefix="' . $this->moduleNameLC . 'InputfieldLinkDisplayName"
                class="' . implode(' ', $inputDefaultClass) . '"
                id="' . $this->moduleNameLC . 'InputfieldLinkDisplayName_' . $id . '_' . $langId . '"
                onfocus="this.select();" />
            &nbsp;<i>' . $_ARRAYLANG['TXT_MEDIADIR_DISPLAYNAME'] . '</i>';
        return $strInputfield;
    }

    public function saveInputfield($intInputfieldId, $strValue, $langId = 0)
    {
        $data = $_POST[$this->moduleNameLC.'Inputfield'][$intInputfieldId];

        // disable localization in case localized submission has been disabled
        // for frontend
        if (
            \Cx\Core\Core\Controller\Cx::instanciate()->getMode() ==
            \Cx\Core\Core\Controller\Cx::MODE_FRONTEND &&
            !$this->arrSettings['settingsFrontendUseMultilang']
        ) {
            $langId = 0;
        }

        // reset field value as we have to fetch it directly from POST (if set)
        if (isset($data['href'][$langId])) {
            $strValue = contrexx_input2raw(
                $data['href'][$langId]
            );
        }

        // fetch the display name to be used for the associated link
        $strName = '';
        if (!empty($data['name'][$langId])) {
            $strName = contrexx_input2raw($data['name'][$langId]);
        }

        if (empty($strValue) && empty($strName)) {
            return '';
        }

        return json_encode(
            array(
                'href' => $strValue,
                'name'=> $strName,
            )
        );
    }


    function deleteContent($intEntryId, $intIputfieldId)
    {
        global $objDatabase;

        $objDeleteInputfield = $objDatabase->Execute("DELETE FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_rel_entry_inputfields WHERE `entry_id`='".intval($intEntryId)."' AND  `field_id`='".intval($intIputfieldId)."'");

        if($objDeleteInputfield !== false) {
            return true;
        } else {
            return false;
        }
    }



    function getContent($intEntryId, $arrInputfield, $arrTranslationStatus)
    {
        $data = static::getRawData($intEntryId, $arrInputfield, $arrTranslationStatus);
        $arrValue = $this->parseData($data);
        $strValue = $arrValue['href'];

        // replace the links
        $strValue = preg_replace('/\\[\\[([A-Z0-9_-]+)\\]\\]/', '{\\1}', $strValue);
        \LinkGenerator::parseTemplate($strValue, true);

        //make link name without protocol
        if ($arrValue['name']) {
            $strValueName = contrexx_raw2xhtml($arrValue['name']);
        } else {
            $strValueName = preg_replace('#^.*://#', '', $strValue);
            if (strlen($strValueName) >= 55 ) {
                $strValueName = substr($strValueName, 0, 55)." [...]";
            }
        }

        //make link href with "http://"
        $strValueHref = $strValue;
        if (substr($strValueHref, 0, 1) !== '/' && !preg_match('#^.*://#', $strValueHref)) {
            $strValueHref = 'http://' . $strValueHref;
        }

        //make hyperlink with <a> tag
        $strValueLink = '<a href="'.$strValueHref.'" class="'.$this->moduleNameLC.'InputfieldLink" target="_blank">'.$strValueName.'</a>';

        if(!empty($strValue)) {
            $arrContent['TXT_'.$this->moduleLangVar.'_INPUTFIELD_NAME'] = htmlspecialchars($arrInputfield['name'][0], ENT_QUOTES, CONTREXX_CHARSET);
            $arrContent[$this->moduleLangVar.'_INPUTFIELD_VALUE'] = $strValueLink;
            $arrContent[$this->moduleLangVar.'_INPUTFIELD_VALUE_HREF'] = $strValueHref;
            $arrContent[$this->moduleLangVar.'_INPUTFIELD_VALUE_NAME'] = $strValueName;
        } else {
            $arrContent = null;
        }

        return $arrContent;
    }

    /**
     * Convert stored data into associative array of following form:
     * [
     *     'href' => $url,
     *     'name' => $name,
     * ]
     * Note: In case $data is not a valid JSON-string, then $data will be
     * assumed to be the url ($url).
     *
     * @param   string  $data   JSON-encoded string from database.
     * @return  array
     */
    protected function parseData($data) {
        $linkData = json_decode($data, true);
        if (!$linkData && empty($data)) {
            return array(
                'href' => '',
                'name'=> '',
            );
        }
        // backwards compatibility
        elseif (!$linkData) {
            $linkData = array(
                'href' => strip_tags(htmlspecialchars($data, ENT_QUOTES, CONTREXX_CHARSET)),
                'name'=> '',
            );
        }
        return $linkData;
    }

    function getRawData($intEntryId, $arrInputfield, $arrTranslationStatus) {
        global $objDatabase;

        $intId = intval($arrInputfield['id']);
        $objEntryDefaultLang = $objDatabase->Execute("SELECT `lang_id` FROM ".DBPREFIX."module_".$this->moduleTablePrefix."_entries WHERE id=".intval($intEntryId)." LIMIT 1");
        $intEntryDefaultLang = intval($objEntryDefaultLang->fields['lang_id']);
        $langId = static::getOutputLocale()->getId();

        $intLangId = $langId;
        if ($this->arrSettings['settingsTranslationStatus'] == 1) {
            $intLangId = (in_array($langId, $arrTranslationStatus)) ? $langId : $intEntryDefaultLang;
        }

        $objInputfieldValue = $objDatabase->Execute("
            SELECT
                `value`
            FROM
                ".DBPREFIX."module_".$this->moduleTablePrefix."_rel_entry_inputfields
            WHERE
                field_id=".$intId."
            AND
                entry_id=".intval($intEntryId)."
            AND
                lang_id=".$intLangId."

            LIMIT 1
        ");

        if (empty($objInputfieldValue->fields['value'])) {
            $objInputfieldValue = $objDatabase->Execute("
                SELECT
                    `value`
                FROM
                    ".DBPREFIX."module_".$this->moduleTablePrefix."_rel_entry_inputfields
                WHERE
                    field_id=".$intId."
                AND
                    entry_id=".intval($intEntryId)."
                AND
                    lang_id=".intval($intEntryDefaultLang)."
                LIMIT 1
            ");
        }

        return $objInputfieldValue->fields['value'];
    }


    function getJavascriptCheck()
    {
        $fieldName = $this->moduleNameLC."Inputfield_";
        $strJavascriptCheck = <<<EOF

            case 'link':
                value = document.getElementById('$fieldName' + field + '_0').value;
                if (value == "" && isRequiredGlobal(inputFields[field][1], value)) {
                    isOk = false;
                    document.getElementById('$fieldName' + field + '_0').style.border = "#ff0000 1px solid";
                } else if (value != "" && !matchType(inputFields[field][2], value)) {
                    isOk = false;
                    document.getElementById('$fieldName' + field + '_0').style.border = "#ff0000 1px solid";
                } else {
                    document.getElementById('$fieldName' + field + '_0').style.borderColor = '';
                }
                break;

EOF;
        return $strJavascriptCheck;
    }


    function getFormOnSubmit($intInputfieldId)
    {
        return null;
    }
}
