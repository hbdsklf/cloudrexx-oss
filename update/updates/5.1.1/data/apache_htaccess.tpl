# Set the path, relative from the document root, to Cloudrexx.
# Technical note: This is ASCMS_PATH_OFFSET
# I.e.: /
RewriteBase   %PATH_ROOT_OFFSET%

# Denied the access to the files from the downloads (DAM) component
# Exceptions are the thumbnail files
RewriteCond %{REQUEST_URI} images\/Downloads\/
RewriteCond %{REQUEST_URI} !\.thumb$
RewriteCond %{REQUEST_URI} !\.thumb_thumbnail\.[^\/]*$
RewriteCond %{REQUEST_URI} !\.thumb_medium\.[^\/]*$
RewriteCond %{REQUEST_URI} !\.thumb_large\.[^\/]*$
RewriteRule . - [F]

# Check if virtual language directory had been stripped off of request.
# If so, do check if the file does actually exists.
# If the file does not exists, do re-add the virtual language directory.
RewriteCond %{ENV:REDIRECT_LOCALE} !^$
RewriteCond %{REQUEST_URI} ^/(core|core_modules|lib|modules|feed|media|images|themes)/
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule .* %{ENV:REDIRECT_LOCALE}%{REQUEST_URI} [L,E=LOCALE:1]

# Strip virtual language directory for MEDIA RESSOURCES, CONTENT DATA and THEME
# IMPORTANT: When running Contrexx in a subdirectory (relativ to the webserver's DocumentRoot)
#            then you'll have to comment-out the following ruleset
RewriteCond %{ENV:REDIRECT_LOCALE} ^$
RewriteCond %{REQUEST_URI} ^/[a-z]{1,2}(?:-[A-Za-z]{2,4})?/(core|core_modules|lib|modules|feed|media|images|themes)/
RewriteRule ^([a-z]{1,2}(?:-[A-Za-z]{2,4})?)\/(.*)$ /$2 [L,E=LOCALE:$1]

# Deny direct access to directories containing sensitive data
RewriteCond %{ENV:REDIRECT_END} !1
RewriteCond %{REQUEST_URI} ^/(config|tmp(?!/public)|websites|core\/.*/Data|core_modules\/.*\/Data|modules\/.*\/Data)/
RewriteRule . - [F]

# Deny access to debug log
RewriteCond %{REQUEST_URI} ^/dbg\.log
RewriteRule . - [F]

# Resolve language specific sitemap.xml
RewriteCond %{ENV:REDIRECT_END} !1
RewriteRule ^([a-z]{1,2}(?:-[A-Za-z]{2,4})?)\/sitemap(?:_[a-z]{1,2}(?:-[A-Za-z]{2,4})?)?.xml$ /sitemap_$1.xml [L,NC,E=END:1]

# Allow directory index files
RewriteCond %{REQUEST_FILENAME}/index.php -f
RewriteRule   .  %{REQUEST_URI}/index.php [L,QSA]

# Redirect all requests to non-existing files to Contrexx
RewriteCond   %{REQUEST_FILENAME}  !-f
RewriteRule   .  index.php?__cap=%{REQUEST_URI} [L,QSA]

# Add captured request to index files
RewriteRule ^index.php index.php?__cap=%{REQUEST_URI} [L,QSA]
