<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 * 
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */
 
/**
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_mediabrowser
 */

namespace Cx\Core\MediaSource\Model\Entity;

use Cx\Model\Base\EntityBase;

/**
 * Class MediaSource
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_mediabrowser
 */
class MediaSource extends EntityBase {

    /**
     * Name of the mediatype e.g. files, shop, media1
     * @var string
     */
    protected $name;

    /**
     * @var int
     */
    protected $position;

    /**
     * Human readable name
     * @var string
     */
    protected $humanName;

    /**
     * Array with the web and normal path to the directory.
     *
     * e.g:
     * array(
     *      $this->cx->getWebsiteImagesContentPath(),
     *      $this->cx->getWebsiteImagesContentWebPath(),
     * )
     *
     * @var array
     */
    protected $directory = array();

    /**
     * Array with access ids to use with \Permission::checkAccess($id, 'static', true)
     *
     * Accepts various formats. Most basic is as follows:
     * <code>array(
     *     <id>[,<id>,...]
     * )</code>
     *
     * This will set the access as follows:
     * - for both, read and write access, the user must have access to every
     *   listed id (as specified by <id>).
     *
     * Use the following format instead, to change the requirement of having
     * access to all listed IDs, but instead only one of the listed IDs:
     * <code>array(
     * 	   'any' => array(
     *         <id>[,<id>,...]
     *     ),
     * )</code>
     *
     * To set different read and write access permissions, do specify as
     * follows:
     * <code>array(
     *     'read' => array(
     *         <id>[,<id>,...]
     *     ),
     *     'write' => array(
     *         <id>[,<id>,...]
     *     ),
     * )</code>
     *
     * Again, to make the user only require having access to one of the listed
     * IDs, do specify as follows:
     * <code>array(
     *     'read' => array(
     *         'any' => array(
     *             <id>[,<id>,...]
     *         ),
     *     ),
     *     'write' => array(
     *         'any' => array(
     *             <id>[,<id>,...]
     *         ),
     *     ),
     * )</code>
     *
     * @var array
     */
    protected $accessIds = array();

    /**
     * @var FileSystem
     */
    protected $fileSystem;

    /**
     * @var bool if indexer is activated
     */
    protected $isIndexingActivated;

    /**
     * @var \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController
     */
    protected $systemComponentController;


    public function __construct($name,$humanName, $directory, $accessIds = array(), $position = '',FileSystem $fileSystem = null, \Cx\Core\Core\Model\Entity\SystemComponentController $systemComponentController = null, $isIndexingActivated = true) {
        $this->fileSystem = $fileSystem ? $fileSystem : LocalFileSystem::createFromPath($directory[0]);
        $this->name      = $name;
        $this->position  = $position;
        $this->humanName = $humanName;
        $this->directory = $directory;
        $this->accessIds = $accessIds;
        $this->setIndexingActivated($isIndexingActivated);

        // Sets provided SystemComponentController
        $this->systemComponentController = $systemComponentController;
        if (!$this->systemComponentController) {
            // Searches a SystemComponentController intelligently by RegEx on backtrace stack frame
            $traces = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 2);
            $trace = end($traces);
            if (empty($trace['class'])) {
                throw new MediaBrowserException('No SystemComponentController for ' . __CLASS__ . ' can be found');
            }
            $matches = array();
            preg_match(
                '/Cx\\\\(?:Core|Core_Modules|Modules)\\\\([^\\\\]*)\\\\/',
                $trace['class'],
                $matches
            );
            $this->systemComponentController = $this->getComponent($matches[1]);
        }
    }

    /**
     * Define if indexer is activated
     *
     * @param $activated
     *
     * @return void
     */
    public function setIndexingActivated($activated)
    {
        $this->isIndexingActivated = $activated;
    }

    /**
     * Get information if indexer is activated
     *
     * @return bool
     */
    public function isIndexingActivated()
    {
        return $this->isIndexingActivated;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getDirectory()
    {
        return $this->directory;
    }


    /**
     * @return array
     */
    public function getAccessIds()
    {
        return $this->accessIds;
    }

    /**
     * @param array $accessIds See DocBlock of {link $this->accessIds}
     */
    public function setAccessIds($accessIds)
    {
        $this->accessIds = $accessIds;
    }

    /**
     * @param   string  $scope  Set to 'read' or 'write' to check for specific
     *                          access permissions.
     * @return bool
     */
    public function checkAccess($scope = 'read'){
        // fetch set access IDs on MediaSource
        if (
            !empty($scope) &&
            isset($this->accessIds[$scope])
        ) {
            $accessIds = $this->accessIds[$scope];
        } else {
            $accessIds = $this->accessIds;
        }

        // check if the user must have permission to all access IDs
        $requiresAll = true;
        $modifier = 'all';
        if (is_array(current($accessIds))) {
            $modifier = current(array_keys($accessIds));
            $accessIds = current($accessIds);
        }
        if ($modifier == 'any') {
            $requiresAll = false;
        }

        // finally, perform access check
        foreach ($accessIds as $id){
            // in case the user requires having access to only one access ID,
            // then we can stop now
            if (
                \Permission::checkAccess($id, 'static', true) &&
                !$requiresAll
            ) {
                return true;
            }

            // in case the user requires having access to all specified access
            // IDs, then we have to abort here in case the user is missing
            // access to any IDs
            if (
                !\Permission::checkAccess($id, 'static', true) &&
                $requiresAll
            ) {
                return false;
            }
        }

        // in case the user only requires access to one access ID, but this
        // line of code is reached, then this means that the user did not
        // have access to any of the specified access IDs. Therefore, the
        // user does not have access permission to this MediaSource.
        if (!$requiresAll) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getHumanName()
    {
        return $this->humanName;
    }

    /**
     * @param string $humanName
     */
    public function setHumanName($humanName)
    {
        $this->humanName = $humanName;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return FileSystem
     */
    public function getFileSystem() {
        return $this->fileSystem;
    }

    /**
     * @return \Cx\Core\Core\Model\Entity\SystemComponentController
     */
    public function getSystemComponentController() {
        return $this->systemComponentController;
    }
}
