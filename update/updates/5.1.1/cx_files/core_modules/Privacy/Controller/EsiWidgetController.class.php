<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class EsiWidgetController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 * @version     1.0.0
 */

namespace Cx\Core_Modules\Privacy\Controller;

/**
 * JsonAdapter Controller to handle EsiWidgets
 * Usage:
 * - Create a subclass that implements parseWidget()
 * - Register it as a Controller in your ComponentController
 *
 * @copyright   CLOUDREXX CMS - Cloudrexx AG Thun
 * @author      Michael Ritter <michael.ritter@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  coremodules_privacy
 * @version     1.0.0
 */

class EsiWidgetController extends \Cx\Core_Modules\Widget\Controller\EsiWidgetController {

    /**
     * Parses a widget
     * @param string $name Widget name
     * @param \Cx\Core\Html\Sigma Widget template
     * @param \Cx\Core\Routing\Model\Entity\Response $response Current response
     * @param array $params Array of params
     */
    public function parseWidget($name, $template, $response, $params) {
        if ($name === 'COOKIE_NOTE') {
            $widgetTemplate = new \Cx\Core_Modules\Widget\Model\Entity\Sigma();
            // TODO: this is a workaround as the ClassLoader does not load
            // the template-file from the correct theme.
            // The related feature is in core_modules/Widget/Controller/EsiWidgetController.class.php in method objectifyParams() for the param 'theme'
            // which should set $theme through $this->cx->getResponse()->setTheme($theme)
            $theme = $params['theme'];
            if (
                $theme instanceof \Cx\Core\View\Model\Entity\Theme &&
                file_exists($this->cx->getWebsiteThemesPath() . '/' . $theme->getFolderName() . '/core_modules/Privacy/Template/Frontend/CookieNote.html')
            ) {
                $widgetTemplate->setRoot($this->cx->getWebsiteThemesPath() . '/' . $theme->getFolderName() . '/core_modules/Privacy/Template/Frontend');
            } else {
                // note: this would be the regular code being used, once
                // the bug has been fixed
                $widgetTemplate->setRoot($this->getDirectory(false) . '/View/Template/Frontend');
            }
            
            $widgetTemplate->loadTemplateFile('CookieNote.html');
            \LinkGenerator::parseTemplate($widgetTemplate->_blocks['__global__']);
            $widgetTemplate->setVariable(
                \Env::get('init')->getComponentSpecificLanguageData(
                    $this->getSystemComponentController()->getName(),
                    true,
                    FRONTEND_LANG_ID
                )
            );
            $template->setTemplate($widgetTemplate->get());
            return;
        }
    }
}
