<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Page
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_contentmanager
 */

namespace Cx\Modules\MediaDir\Model\Entity;

/**
 * Page
 *
 * @copyright   CLOUDREXX CMS - CLOUDREXX AG
 * @author      CLOUDREXX Development Team <info@cloudrexx.com>
 * @package     cloudrexx
 * @subpackage  core_contentmanager
 */
class InputfieldParseTarget extends \Cx\Core_Modules\Widget\Model\Entity\WidgetParseTarget
{
    protected $entryId;
    protected $fieldId;
    protected $content = '';

    /**
     * Returns the name of the attribute used to parse Widget named $widgetName
     * @return string Attribute name used as getter name
     */
    public function getWidgetContentAttributeName($widgetName) {
        return 'content';
    }

    public function getContent() {
        // TODO: properly initialize a mediadir inputfield
        // to take the set translation status into account
        $db = $this->cx->getDb()->getAdodb();

        $result = $db->Execute("
            SELECT
                `value`
            FROM
                ".DBPREFIX."module_mediadir_rel_entry_inputfields
            WHERE
                field_id=".intval($this->fieldId)."
            AND
                entry_id=".intval($this->entryId)."
            AND
                lang_id=".FRONTEND_LANG_ID."
            LIMIT 1
        ");

        $value = $result->fields['value'];
        $value = preg_replace('/\\[\\[([A-Z0-9_-]+)\\]\\]/', '{\\1}', $value);
        return $value;
    }
            
    public function setEntryId($id) {
        $this->entryId = $id;
    }

    public function setFieldId($id) {
        $this->fieldId = $id;
    }
}
