<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _dataaccessUpdate()
{
    global $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            // IMPORTANT: same table statement in access.php
            // but as dataaccess.php is executed beforehand, we do execute it
            // here too
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_modules_access_permission',
                array(
                    'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'allowed_protocols'      => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'id'),
                    'allowed_methods'        => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'allowed_protocols'),
                    'requires_login'         => array('type' => 'TINYINT(1)', 'notnull' => false, 'after' => 'allowed_methods'),
                    'valid_user_groups'      => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'requires_login'),
                    'valid_access_ids'       => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'valid_user_groups'),
                    'callback'               => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'valid_access_ids'),
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_module_data_access',
                array(
                    'id' => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'read_permission' => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'id'),
                    'write_permission' => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'read_permission'),
                    'data_source_id' => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'write_permission'),
                    'name' => array('type' => 'VARCHAR(255)', 'after' => 'data_source_id'),
                    'field_list' => array('type' => 'longtext', 'after' => 'name'),
                    'access_condition' => array('type' => 'longtext', 'after' => 'field_list'),
                    'allowed_output_methods' => array('type' => 'longtext', 'after' => 'access_condition'),
                ),
                array(
                    'name' => array('fields' => array('name'), 'type' => 'UNIQUE'),
                ),
                'InnoDB',
                '',
                array(
                    'read_permission' => array(
                        'table' => DBPREFIX.'core_modules_access_permission',
                        'column' => 'id',
                        //'onDelete' => 'NO ACTION',
                        //'onUpdate' => 'NO ACTION',
                    ),
                    'write_permission' => array(
                        'table' => DBPREFIX.'core_modules_access_permission',
                        'column' => 'id',
                        //'onDelete' => 'NO ACTION',
                        //'onUpdate' => 'NO ACTION',
                    ),
                    'data_source_id' => array(
                        'table' => DBPREFIX.'core_data_source',
                        'column' => 'id',
                        //'onDelete' => 'NO ACTION',
                        //'onUpdate' => 'NO ACTION',
                    ),
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_module_data_access_apikey',
                array(
                    'id' => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'api_key' => array('type' => 'VARCHAR(32)', 'after' => 'id')
                ),
                array(
                    'api_key' => array('fields' => array('api_key'), 'type' => 'UNIQUE')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_module_data_access_data_access_apikey',
                array(
                    'id' => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'api_key_id' => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'id'),
                    'data_access_id' => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'api_key_id'),
                    'read_only' => array('type' => 'TINYINT(1)', 'notnull' => false, 'after' => 'data_access_id'),
                ),
                array(),
                'InnoDB',
                '',
                array(
                    'api_key_id' => array(
                        'table' => DBPREFIX.'core_module_data_access_apikey',
                        'column' => 'id',
                        //'onDelete' => 'NO ACTION',
                        //'onUpdate' => 'NO ACTION',
                    ),
                    'data_access_id' => array(
                        'table' => DBPREFIX.'core_module_data_access',
                        'column' => 'id',
                        //'onDelete' => 'NO ACTION',
                        //'onUpdate' => 'NO ACTION',
                    ),
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _dataaccessInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'core_module_data_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `read_permission` int(11) DEFAULT NULL,
  `write_permission` int(11) DEFAULT NULL,
  `data_source_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `field_list` longtext NOT NULL,
  `access_condition` longtext NOT NULL,
  `allowed_output_methods` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `read_permission` (`read_permission`),
  KEY `write_permission` (`write_permission`),
  KEY `data_source_id` (`data_source_id`),
  CONSTRAINT `'.DBPREFIX.'core_module_data_access_ibfk_data_source_id` FOREIGN KEY (`data_source_id`) REFERENCES `'.DBPREFIX.'core_data_source` (`id`),
  CONSTRAINT `'.DBPREFIX.'core_module_data_access_ibfk_read_permission` FOREIGN KEY (`read_permission`) REFERENCES `'.DBPREFIX.'core_modules_access_permission` (`id`),
  CONSTRAINT `'.DBPREFIX.'core_module_data_access_ibfk_write_permission` FOREIGN KEY (`write_permission`) REFERENCES `'.DBPREFIX.'core_modules_access_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'core_module_data_access_apikey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_key` (`api_key`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'core_module_data_access_data_access_apikey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `api_key_id` int(11) DEFAULT NULL,
  `data_access_id` int(11) DEFAULT NULL,
  `read_only` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `api_key_id` (`api_key_id`),
  KEY `data_access_id` (`data_access_id`),
  CONSTRAINT `'.DBPREFIX.'core_module_data_access_apikey_ibfk_api_key_id` FOREIGN KEY (`api_key_id`) REFERENCES `'.DBPREFIX.'core_module_data_access_apikey` (`id`),
  CONSTRAINT `'.DBPREFIX.'core_module_data_access_apikey_ibfk_data_access_id` FOREIGN KEY (`data_access_id`) REFERENCES `'.DBPREFIX.'core_module_data_access` (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
