<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


////////////////////////////////////////////////////
//BEGIN OF NEWS CONVERTING STUFF
/*
    this was c&ped together from news/admin.class.php and news/lib/newsLib.class.php 
*/

class HackyFeedRepublisher {

    protected $arrSettings = array();

    public function runRepublishing() {
        $this->initRepublishing();
    
        FWLanguage::init();

        $langIds = array_keys(FWLanguage::getLanguageArray());
        
        foreach($langIds as $id) {
            $this->createRSS($id);
        }
    }

    protected function initRepublishing()
    {
        global  $_ARRAYLANG, $objInit, $objTemplate, $_CONFIG;

        //getSettings
        global $objDatabase;
        $query = "SELECT name, value FROM ".DBPREFIX."module_news_settings";
        $objResult = $objDatabase->Execute($query);
        while (!$objResult->EOF) {
            $this->arrSettings[$objResult->fields['name']] = $objResult->fields['value'];
            $objResult->MoveNext();
        }
    }

    protected function createRSS($langId){
        global $_CONFIG, $objDatabase; 
        $_FRONTEND_LANGID = $langId;


        if (intval($this->arrSettings['news_feed_status']) == 1) {
            $arrNews = array();
            require_once(ASCMS_FRAMEWORK_PATH.'/RSSWriter.class.php');
            $objRSSWriter = new RSSWriter();

            $objRSSWriter->characterEncoding = CONTREXX_CHARSET;
            $objRSSWriter->channelTitle = $this->arrSettings['news_feed_title'];
            $objRSSWriter->channelLink = 'http://'.$_CONFIG['domainUrl'].($_SERVER['SERVER_PORT'] == 80 ? "" : ":".intval($_SERVER['SERVER_PORT'])).ASCMS_PATH_OFFSET.'/'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'/'.CONTREXX_DIRECTORY_INDEX.'?section=news';
            $objRSSWriter->channelDescription = $this->arrSettings['news_feed_description'];
            $objRSSWriter->channelLanguage = FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang');
            $objRSSWriter->channelCopyright = 'Copyright '.date('Y').', http://'.$_CONFIG['domainUrl'];

            if (!empty($this->arrSettings['news_feed_image'])) {
                $objRSSWriter->channelImageUrl = 'http://'.$_CONFIG['domainUrl'].($_SERVER['SERVER_PORT'] == 80 ? "" : ":".intval($_SERVER['SERVER_PORT'])).$this->arrSettings['news_feed_image'];
                $objRSSWriter->channelImageTitle = $objRSSWriter->channelTitle;
                $objRSSWriter->channelImageLink = $objRSSWriter->channelLink;
            }
            $objRSSWriter->channelWebMaster = $_CONFIG['coreAdminEmail'];

            $itemLink = "http://".$_CONFIG['domainUrl'].($_SERVER['SERVER_PORT'] == 80 ? "" : ":".intval($_SERVER['SERVER_PORT'])).ASCMS_PATH_OFFSET.'/'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'/'.CONTREXX_DIRECTORY_INDEX.'?section=news&amp;cmd=details&amp;newsid=';

            $query = "
                SELECT      tblNews.id,
                            tblNews.date,
                            tblNews.title,
                            tblNews.text,
                            tblNews.redirect,
                            tblNews.source,
                            tblNews.catid AS categoryId,
                            tblNews.teaser_frames AS teaser_frames,
                            tblNews.teaser_text,
                            tblCategory.name AS category
                FROM        ".DBPREFIX."module_news AS tblNews
                INNER JOIN  ".DBPREFIX."module_news_categories AS tblCategory
                USING       (catid)
                WHERE       tblNews.status=1
                    AND     tblNews.lang = ".$_FRONTEND_LANGID."
                    AND     (tblNews.startdate <= CURDATE() OR tblNews.startdate = '0000-00-00 00:00:00')
                    AND     (tblNews.enddate >= CURDATE() OR tblNews.enddate = '0000-00-00 00:00:00')"
                    .($this->arrSettings['news_message_protection'] == '1' ? " AND tblNews.frontend_access_id=0 " : '')
                            ."ORDER BY tblNews.date DESC";

            if (($objResult = $objDatabase->SelectLimit($query, 20)) !== false && $objResult->RecordCount() > 0) {
                while (!$objResult->EOF) {
                    if (empty($objRSSWriter->channelLastBuildDate)) {
                        $objRSSWriter->channelLastBuildDate = date('r', $objResult->fields['date']);
                    }
                    $arrNews[$objResult->fields['id']] = array(
                        'date'          => $objResult->fields['date'],
                        'title'         => $objResult->fields['title'],
                        'text'          => empty($objResult->fields['redirect']) ? (!empty($objResult->fields['teaser_text']) ? nl2br($objResult->fields['teaser_text']).'<br /><br />' : '').$objResult->fields['text'] : (!empty($objResult->fields['teaser_text']) ? nl2br($objResult->fields['teaser_text']) : ''),
                        'redirect'      => $objResult->fields['redirect'],
                        'source'        => $objResult->fields['source'],
                        'category'      => $objResult->fields['category'],
                        'teaser_frames' => explode(';', $objResult->fields['teaser_frames']),
                        'categoryId'    => $objResult->fields['categoryId']
                    );
                    $objResult->MoveNext();
                }
            }

            // create rss feed
            $objRSSWriter->xmlDocumentPath = ASCMS_FEED_PATH.'/news_'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'.xml';
            foreach ($arrNews as $newsId => $arrNewsItem) {
                $objRSSWriter->addItem(
                    contrexx_raw2xml($arrNewsItem['title']),
                    (empty($arrNewsItem['redirect'])) ? ($itemLink.$newsId.(isset($arrNewsItem['teaser_frames'][0]) ? '&amp;teaserId='.$arrNewsItem['teaser_frames'][0] : '')) : htmlspecialchars($arrNewsItem['redirect'], ENT_QUOTES, CONTREXX_CHARSET),
                    contrexx_raw2xml($arrNewsItem['text']),
                    '',
                    array('domain' => "http://".$_CONFIG['domainUrl'].($_SERVER['SERVER_PORT'] == 80 ? "" : ":".intval($_SERVER['SERVER_PORT'])).ASCMS_PATH_OFFSET.'/'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'/'.CONTREXX_DIRECTORY_INDEX.'?section=news&amp;category='.$arrNewsItem['categoryId'], 'title' => $arrNewsItem['category']),
                    '',
                    '',
                    '',
                    $arrNewsItem['date'],
                    array('url' => htmlspecialchars($arrNewsItem['source'], ENT_QUOTES, CONTREXX_CHARSET), 'title' => contrexx_raw2xml($arrNewsItem['title']))
               );
            }
            $status = $objRSSWriter->write();

            // create headlines rss feed
            $objRSSWriter->removeItems();
            $objRSSWriter->xmlDocumentPath = ASCMS_FEED_PATH.'/news_headlines_'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'.xml';
            foreach ($arrNews as $newsId => $arrNewsItem) {
                $objRSSWriter->addItem(
                    contrexx_raw2xml($arrNewsItem['title']),
                    $itemLink.$newsId.(isset($arrNewsItem['teaser_frames'][0]) ? "&amp;teaserId=".$arrNewsItem['teaser_frames'][0] : ""),
                    '',
                    '',
                    array('domain' => 'http://'.$_CONFIG['domainUrl'].($_SERVER['SERVER_PORT'] == 80 ? '' : ':'.intval($_SERVER['SERVER_PORT'])).ASCMS_PATH_OFFSET.'/'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'/'.CONTREXX_DIRECTORY_INDEX.'?section=news&amp;category='.$arrNewsItem['categoryId'], 'title' => $arrNewsItem['category']),
                    '',
                    '',
                    '',
                    $arrNewsItem['date']
                );
            }
            $statusHeadlines = $objRSSWriter->write();

            $objRSSWriter->feedType = 'js';
            $objRSSWriter->xmlDocumentPath = ASCMS_FEED_PATH.'/news_'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'.js';
            $objRSSWriter->write();

            /*
            if (count($objRSSWriter->arrErrorMsg) > 0) {
                $this->strErrMessage .= implode('<br />', $objRSSWriter->arrErrorMsg);
            }
            if (count($objRSSWriter->arrWarningMsg) > 0) {
                $this->strErrMessage .= implode('<br />', $objRSSWriter->arrWarningMsg);
            }
            */
        } else {
            @unlink(ASCMS_FEED_PATH.'/news_'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'.xml');
            @unlink(ASCMS_FEED_PATH.'/news_headlines_'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'.xml');
            @unlink(ASCMS_FEED_PATH.'/news_'.FWLanguage::getLanguageParameter($_FRONTEND_LANGID, 'lang').'.js');
        }
    }
}
//END OF NEWS CONVERTING STUFF

function _newsUpdate() {
    global $objDatabase, $_CONFIG, $objUpdate, $_ARRAYLANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        /************************************************
        * EXTENSION:	Placeholder NEWS_LINK replaced	*
        *				by NEWS_LINK_TITLE				*
        * ADDED:		Contrexx v2.1.0					*
        ************************************************/
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '2.1.0')) {
            try {
                \Cx\Lib\UpdateUtil::migrateContentPage('news', null, '{NEWS_LINK}', '{NEWS_LINK_TITLE}', '2.1.0');
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            $objResult= \Cx\Lib\UpdateUtil::sql("SELECT 1 FROM `".DBPREFIX."module_news_settings` WHERE `name` = 'recent_news_message_limit'");
            if ($objResult->RecordCount() == 0) {
                \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_news_settings` (`name`, `value`) VALUES ('recent_news_message_limit', '5')");
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    /************************************************
    * EXTENSION:	Front- and backend permissions  *
    * ADDED:		Contrexx v2.1.0					*
    ************************************************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        $query = "SELECT 1 FROM `".DBPREFIX."module_news_settings` WHERE `name` = 'news_message_protection'";
        $objResult = $objDatabase->SelectLimit($query, 1);
        if ($objResult) {
            if ($objResult->RecordCount() == 0) {
                $query = "INSERT INTO `".DBPREFIX."module_news_settings` (`name`, `value`) VALUES ('news_message_protection', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "SELECT 1 FROM `".DBPREFIX."module_news_settings` WHERE `name` = 'news_message_protection_restricted'";
        $objResult = $objDatabase->SelectLimit($query, 1);
        if ($objResult) {
            if ($objResult->RecordCount() == 0) {
                $query = "INSERT INTO `".DBPREFIX."module_news_settings` (`name`, `value`) VALUES ('news_message_protection_restricted', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $arrColumns = $objDatabase->MetaColumnNames(DBPREFIX.'module_news');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_news'));
            return false;
        }

        if (!in_array('frontend_access_id', $arrColumns)) {
            $query = "ALTER TABLE `".DBPREFIX."module_news` ADD `frontend_access_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `validated`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }
        if (!in_array('backend_access_id', $arrColumns)) {
            $query = "ALTER TABLE `".DBPREFIX."module_news` ADD `backend_access_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' AFTER `frontend_access_id`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }



        /************************************************
        * EXTENSION:	Thunbmail Image                 *
        * ADDED:		Contrexx v2.1.0					*
        ************************************************/
        $arrColumns = $objDatabase->MetaColumnNames(DBPREFIX.'module_news');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_news'));
            return false;
        }

        if (!in_array('teaser_image_thumbnail_path', $arrColumns)) {
            $query = "ALTER TABLE `".DBPREFIX."module_news` ADD `teaser_image_thumbnail_path` TEXT NOT NULL AFTER `teaser_image_path`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }



        try{
            // delete obsolete table  contrexx_module_news_access
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX.'module_news_access');
            # fix some ugly NOT NULL without defaults
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_news',
                array(
                    'id'                         => array('type'=>'INT(6) UNSIGNED','notnull'=>true,  'primary'     =>true,   'auto_increment' => true),
                    'date'                       => array('type'=>'INT(14)',            'notnull'=>false, 'default_expr'=>'NULL'),
                    'title'                      => array('type'=>'VARCHAR(250)',       'notnull'=>true,  'default'     =>''),
                    'text'                       => array('type'=>'MEDIUMTEXT',         'notnull'=>true),
                    'redirect'                   => array('type'=>'VARCHAR(250)',       'notnull'=>true,  'default'     =>''),
                    'source'                     => array('type'=>'VARCHAR(250)',       'notnull'=>true,  'default'     =>''),
                    'url1'                       => array('type'=>'VARCHAR(250)',       'notnull'=>true,  'default'     =>''),
                    'url2'                       => array('type'=>'VARCHAR(250)',       'notnull'=>true,  'default'     =>''),
                    'catid'                      => array('type'=>'INT(2) UNSIGNED',    'notnull'=>true,  'default'     =>0),
                    'lang'                       => array('type'=>'INT(2) UNSIGNED',    'notnull'=>true,  'default'     =>0),
                    'userid'                     => array('type'=>'INT(6) UNSIGNED',    'notnull'=>true,  'default'     =>0),
                    'startdate'                  => array('type'=>'DATETIME',           'notnull'=>true,  'default'     =>'0000-00-00 00:00:00'),
                    'enddate'                    => array('type'=>'DATETIME',           'notnull'=>true,  'default'     =>'0000-00-00 00:00:00'),
                    'status'                     => array('type'=>'TINYINT(4)',         'notnull'=>true,  'default'     =>1),
                    'validated'                  => array('type'=>"ENUM('0','1')",      'notnull'=>true,  'default'     =>0),
                    'frontend_access_id'         => array('type'=>'INT(10) UNSIGNED',   'notnull'=>true,  'default'     =>0),
                    'backend_access_id'          => array('type'=>'INT(10) UNSIGNED',   'notnull'=>true,  'default'     =>0),
                    'teaser_only'                => array('type'=>"ENUM('0','1')",      'notnull'=>true,  'default'     =>0),
                    'teaser_frames'              => array('type'=>'TEXT',               'notnull'=>true),
                    'teaser_text'                => array('type'=>'TEXT',               'notnull'=>true),
                    'teaser_show_link'           => array('type'=>'TINYINT(1) UNSIGNED','notnull'=>true,  'default'     =>1),
                    'teaser_image_path'          => array('type'=>'TEXT',               'notnull'=>true),
                    'teaser_image_thumbnail_path'=> array('type'=>'TEXT',               'notnull'=>true),
                    'changelog'                  => array('type'=>'INT(14)',            'notnull'=>true,  'default'     =>0),
                ),
                array(#indexes
                    'newsindex' =>array ('type' => 'FULLTEXT', 'fields' => array('text','title','teaser_text'))
                )
            );

        }
        catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        //encoding was a little messy in 2.1.4. convert titles and teasers to their raw representation
        if($_CONFIG['coreCmsVersion'] == "2.1.4") {
            try{
                $res = \Cx\Lib\UpdateUtil::sql('SELECT `id`, `title`, `teaser_text` FROM `'.DBPREFIX.'module_news` WHERE `changelog` > '.mktime(0,0,0,12,15,2010));
                while($res->MoveNext()) {
                    $title = $res->fields['title'];
                    $teaserText = $res->fields['teaser_text'];
                    $id = $res->fields['id'];

                    //title is html entity style
                    $title = html_entity_decode($title, ENT_QUOTES, CONTREXX_CHARSET);
                    //teaserText is html entity style, but no cloudrexx was specified on encoding
                    $teaserText = html_entity_decode($teaserText);

                    \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_news` SET `title`="'.addslashes($title).'", `teaser_text`="'.addslashes($teaserText).'" where `id`='.$id);
                }

                $hfr = new HackyFeedRepublisher();
                $hfr->runRepublishing();
            }
            catch (\Cx\Lib\UpdateException $e) {
                DBG::trace();
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
    }

    /****************************
    * ADDED:    Contrexx v3.0.0 *
    *****************************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_locale',
                array(
                    'news_id'        => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'lang_id'        => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'news_id'),
                    'is_active'      => array('type' => 'INT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'after' => 'lang_id'),
                    'title'          => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'is_active'),
                    'text'           => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'title'),
                    'teaser_text'    => array('type' => 'text', 'notnull' => true, 'after' => 'text')
                ),
                array(
                    'newsindex'      => array('fields' => array('text', 'title', 'teaser_text'), 'type' => 'FULLTEXT')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_categories_locale',
                array(
                    'category_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'lang_id'        => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'category_id'),
                    'name'           => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => '', 'after' => 'lang_id')
                ),
                array(
                    'name'           => array('fields' => array('name'), 'type' => 'FULLTEXT')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_types',
                array(
                    'typeid'     => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'primary' => true, 'auto_increment' => true)
                )
            );


            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_types_locale',
                array(
                    'lang_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'type_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'lang_id'),
                    'name'       => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => '', 'after' => 'type_id')
                ),
                array(
                    'name'       => array('fields' => array('name'), 'type' => 'FULLTEXT')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_settings_locale',
                array(
                    'name'       => array('type' => 'VARCHAR(50)', 'notnull' => true, 'default' => '', 'primary' => true),
                    'lang_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'name'),
                    'value'      => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'lang_id')
                ),
                array(
                    'name'       => array('fields' => array('name'), 'type' => 'FULLTEXT')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_comments',
                array(
                    'id'             => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                    'title'          => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'text'           => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'title'),
                    'newsid'         => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'text'),
                    'date'           => array('type' => 'INT(14)', 'notnull' => false, 'default' => NULL,'after' => 'newsid'),
                    'poster_name'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'date'),
                    'userid'         => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'poster_name'),
                    'ip_address'     => array('type' => 'VARCHAR(32)', 'notnull' => true, 'default' => '', 'after' => 'userid'),
                    'is_active'      => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '1', 'after' => 'ip_address')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_stats_view',
                array(
                    'user_sid'       => array('type' => 'CHAR(32)', 'notnull' => true),
                    'news_id'        => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'after' => 'user_sid'),
                    'time'           => array('type' => 'timestamp', 'notnull' => true, 'default_expr' => 'CURRENT_TIMESTAMP', 'on_update' => 'CURRENT_TIMESTAMP', 'after' => 'news_id')
                ),
                array(
                    'idx_user_sid'   => array('fields' => array('user_sid')),
                    'idx_news_id'    => array('fields' => array('news_id'))
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_rel_news',
                array(
                    'news_id'         => array('type' => 'INT(11)', 'notnull' => true),
                    'related_news_id' => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'news_id'),
                ),
                array(
                    'related_news' => array('fields' => array('news_id', 'related_news_id'), 'type' => 'UNIQUE'),
                ),
                'InnoDB'
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_rel_tags',
                array(
                    'news_id' => array('type' => 'INT(11)', 'notnull' => true),
                    'tag_id'  => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'news_id'),
                ),
                array(
                    'NewsTagsRelation' => array('fields' => array('news_id', 'tag_id'), 'type' => 'UNIQUE'),
                ),
                'InnoDB'
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_tags',
                array(
                    'id'           => array('type' => 'INT(11)', 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                    'tag'          => array('type' => 'VARCHAR(255)', 'notnull' => true, 'after' => 'id', 'binary' => true),
                    'viewed_count' => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'tag'),
                ),
                array(
                    'tag' => array('fields' => array('tag'), 'type' => 'UNIQUE'),
                ),
                'InnoDB'
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_rel_categories',
                array(
                    'news_id'     => array('type' => 'INT(11)', 'notnull' => true),
                    'category_id' => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'news_id'),
                ),
                array(
                    'NewsTagsRelation'   => array('fields' => array('news_id', 'category_id'), 'type' => 'UNIQUE'),
                ),
                'InnoDB'
            );

            $arrColumnsNews = $objDatabase->MetaColumnNames(DBPREFIX.'module_news');
            if ($arrColumnsNews === false) {
                setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_news'));
                return false;
            }

            if (isset($arrColumnsNews['CATID'])) {
                \Cx\Lib\UpdateUtil::sql('INSERT INTO `'. DBPREFIX .'module_news_rel_categories` (`news_id`,`category_id`) SELECT `id`, `catid` FROM `'. DBPREFIX .'module_news`');
            }
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news',
                array(
                    'id'                             => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                    'date'                           => array('type' => 'INT(14)', 'notnull' => false, 'default' => NULL, 'after' => 'id'),
                    'title'                          => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'date'),
                    'text'                           => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'title'),
                    'redirect'                       => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'text'),
                    'source'                         => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'redirect'),
                    'url1'                           => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'source'),
                    'url2'                           => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'url1'),
                    'lang'                           => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'url2'),
                    'typeid'                         => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'lang'),
                    'publisher'                      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'typeid'),
                    'publisher_id'                   => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'publisher'),
                    'author'                         => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'publisher_id'),
                    'author_id'                      => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'author'),
                    'userid'                         => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'author_id'),
                    'startdate'                      => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'userid'),
                    'enddate'                        => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'startdate'),
                    'status'                         => array('type' => 'TINYINT(4)', 'notnull' => true, 'default' => '1', 'after' => 'enddate'),
                    'validated'                      => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'status'),
                    'frontend_access_id'             => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'validated'),
                    'backend_access_id'              => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'frontend_access_id'),
                    'teaser_only'                    => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'backend_access_id'),
                    'teaser_frames'                  => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_only'),
                    'teaser_text'                    => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_frames'),
                    'teaser_show_link'               => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'after' => 'teaser_text'),
                    'teaser_image_path'              => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_show_link'),
                    'teaser_image_thumbnail_path'    => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_image_path'),
                    'changelog'                      => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0', 'after' => 'teaser_image_thumbnail_path'),
                    'allow_comments'                 => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'changelog'),
                    'enable_related_news'            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'allow_comments'),
                    'enable_tags'                    => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'enable_related_news'),
                    'redirect_new_window'            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'enable_tags'),
                ),
                array(
                    'newsindex'                      => array('fields' => array('text','title','teaser_text'), 'type' => 'FULLTEXT')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        try {
            if (isset($arrColumnsNews['TITLE']) && isset($arrColumnsNews['TEXT']) && isset($arrColumnsNews['TEASER_TEXT']) && isset($arrColumnsNews['LANG'])) {
                \Cx\Lib\UpdateUtil::sql('
                    INSERT INTO `'.DBPREFIX.'module_news_locale` (`news_id`, `lang_id`, `title`, `text`, `teaser_text`)
                    SELECT `id`, `lang`, `title`, `text`, `teaser_text` FROM `'.DBPREFIX.'module_news`
                    ON DUPLICATE KEY UPDATE `news_id` = `news_id`
                ');
            }
            if (isset($arrColumnsNews['TITLE'])) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news` DROP `title`');
            }
            if (isset($arrColumnsNews['TEXT'])) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news` DROP `text`');
            }
            if (isset($arrColumnsNews['TEASER_TEXT'])) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news` DROP `teaser_text`');
            }
            if (isset($arrColumnsNews['LANG'])) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news` DROP `lang`');
            }

            $arrColumnsNewsCategories = $objDatabase->MetaColumnNames(DBPREFIX.'module_news_categories');
            if ($arrColumnsNewsCategories === false) {
                setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_news_categories'));
                return false;
            }
            if (isset($arrColumnsNewsCategories['NAME'])) {
                \Cx\Lib\UpdateUtil::sql('
                    INSERT INTO '.DBPREFIX.'module_news_categories_locale (`category_id`, `lang_id`, `name`)
                    SELECT c.catid, l.id, c.name
                    FROM '.DBPREFIX.'module_news_categories AS c, '.DBPREFIX.'languages AS l
                    ORDER BY c.catid, l.id
                    ON DUPLICATE KEY UPDATE `category_id` = `category_id`
                ');
                \Cx\Lib\UpdateUtil::sql('
                    INSERT INTO '.DBPREFIX.'module_news_categories_locale (`category_id`, `lang_id`, `name`)
                    SELECT c.catid, l.id, c.name
                    FROM '.DBPREFIX.'module_news_categories AS c, '.DBPREFIX.'languages AS l
                    ORDER BY c.catid, l.id
                    ON DUPLICATE KEY UPDATE `category_id` = `category_id`
                ');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news_categories` DROP `name`');
            }
            if (isset($arrColumnsNewsCategories['LANG'])) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news_categories` DROP `lang`');
            }

            \Cx\Lib\UpdateUtil::sql('
                INSERT INTO `'.DBPREFIX.'module_news_settings_locale` (`name`, `lang_id`, `value`)
                SELECT n.`name`, l.`id`, n.`value`
                FROM `'.DBPREFIX.'module_news_settings` AS n, `'.DBPREFIX.'languages` AS l
                WHERE n.`name` IN ("news_feed_description", "news_feed_title")
                ORDER BY n.`name`, l.`id`
                ON DUPLICATE KEY UPDATE `'.DBPREFIX.'module_news_settings_locale`.`name` = `'.DBPREFIX.'module_news_settings_locale`.`name`
            ');

            \Cx\Lib\UpdateUtil::sql('DELETE FROM `'.DBPREFIX.'module_news_settings` WHERE `name` IN ("news_feed_title", "news_feed_description")');

            \Cx\Lib\UpdateUtil::sql('
                INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`)
                VALUES  ("news_comments_activated", "0"),
                        ("news_comments_anonymous", "0"),
                        ("news_comments_autoactivate", "0"),
                        ("news_comments_notification", "1"),
                        ("news_comments_timeout", "30"),
                        ("news_default_teasers", ""),
                        ("news_use_types","0"),
                        ("news_use_top","0"),
                        ("news_top_days","10"),
                        ("news_top_limit","10"),
                        ("news_assigned_author_groups", "0"),
                        ("news_assigned_publisher_groups", "0")
                ON DUPLICATE KEY UPDATE `name` = `name`
            ');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::migrateContentPage('news', 'details', array('{NEWS_DATE}','{NEWS_COMMENTS_DATE}'), array('{NEWS_LONG_DATE}', '{NEWS_COMMENTS_LONG_DATE}'), '3.0.1');

            // this adds the block news_redirect
            $search = array(
                '/.*\{NEWS_TEXT\}.*/ms',
            );
            $callback = function($matches) {
                if (   !preg_match('/<!--\s+BEGIN\s+news_redirect\s+-->/ms', $matches[0])) {
                    $newsContent = <<<NEWS
    <!-- BEGIN news_text -->{NEWS_TEXT}<!-- END news_text -->
        <!-- BEGIN news_redirect -->{TXT_NEWS_REDIRECT_INSTRUCTION} <a href="{NEWS_REDIRECT_URL}" target="_blank">{NEWS_REDIRECT_URL}</a><!-- END news_redirect -->
NEWS;
                    return str_replace('{NEWS_TEXT}', $newsContent, $matches[0]);
                } else {
                    return $matches[0];
                }
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'news', 'cmd' => 'details'), $search, $callback, array('content'), '3.0.1');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.3')) {
        try {
            // migrate content page to version 3.0.1
            $search = array(
            '/(.*)/ms',
            );
            $callback = function($matches) {
                $content = $matches[1];
                if (empty($content)) {
                    return $content;
                }

                // migrate to ckeditor
                $content = str_replace('FCKeditorAPI.GetInstance(\'newsText\').SetData(\'\')', 'CKEDITOR.instances[\'newsText\'].setData()', $content);

                if (!preg_match('/<!--\s+BEGIN\s+news_submit_form_captcha\s+-->.*<!--\s+END\s+news_submit_form_captcha\s+-->/ms', $content)) {
                    // check if captcha code is already present
                    if (preg_match('/\{IMAGE_URL\}/ms', $content)) {
                        // add missing template block news_submit_form_captcha
                        $content = preg_replace('/(.*)(<p[^>]*>.*?<label[^>]*>.*?\{IMAGE_URL\}.*?<\/p>)/ms', '$1<!-- BEGIN news_submit_form_captcha -->$2<!-- END news_submit_form_captcha -->', $content);
                    } else {
                        // add whole captcha code incl. template block
                        $content = preg_replace('/(.*)(<tr[^>]*>.*?<td([^>]*)>.*?\{NEWS_TEXT\}.*?(\s*)<\/tr>)/ms', '$1$2$4<!-- BEGIN news_submit_form_captcha -->$4<tr>$4    <td$3>{NEWS_CAPTCHA_CODE}</td>$4</tr>$4<!-- END news_submit_form_captcha -->', $content);
                    }
                }

                // add text variable
                $content = str_replace('Captcha', '{TXT_NEWS_CAPTCHA}', $content);

                // replace image with {NEWS_CAPTCHA_CODE}
                $content = preg_replace('/<img[^>]+\{IMAGE_URL\}[^>]+>(?:<br\s*\/?>)?/ms', '{NEWS_CAPTCHA_CODE}', $content);

                // remove {TXT_CAPTCHA}
                $content = str_replace('{TXT_CAPTCHA}', '', $content);

                // remove <input type="text" name="captcha" id="captcha" />
                $content = preg_replace('/<input[^>]+name\s*=\s*[\'"]captcha[\'"][^>]*>/ms', '', $content);

                return $content;
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'news', 'cmd' => 'submit'), $search, $callback, array('content'), '3.0.1');


            // replace comments placeholder with a sigma block , news module
            $search = array(
                '/.*\{NEWS_COUNT_COMMENTS\}.*/ms',
            );
            $callback = function($matches) {
                $placeholder = '{NEWS_COUNT_COMMENTS}';
                $htmlCode = '<!-- BEGIN news_comments_count -->'.$placeholder.'<!-- END news_comments_count -->';
                if (!preg_match('/<!--\s+BEGIN\s+news_comments_count\s+-->.*<!--\s+END\s+news_comments_count\s+-->/ms', $matches[0])) {
                    return str_replace($placeholder, $htmlCode, $matches[0]);
                } else {
                    return $matches[0];
                }
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'news', 'cmd' => ''), $search, $callback, array('content'), '3.0.3');
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'news', 'cmd' => 'details'), $search, $callback, array('content'), '3.0.3');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    /************************************************
    * EXTENSION:    Categories as NestedSet         *
    * ADDED:        Contrexx v3.1.0                 *
    ************************************************/
    if (!isset($_SESSION['contrexx_update']['news'])) {
        $_SESSION['contrexx_update']['news'] = array();
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0') && !isset($_SESSION['contrexx_update']['news']['nestedSet'])) {
        try {
            $nestedSetRootId = null;
            $count = null;
            $leftAndRight = 2;
            $sorting = 1;
            $level = 2;

            // add nested set columns
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_categories',
                array(
                    'catid'          => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'parent_id'      => array('type' => 'INT(11)', 'after' => 'catid'),
                    'left_id'        => array('type' => 'INT(11)', 'after' => 'parent_id'),
                    'right_id'       => array('type' => 'INT(11)', 'after' => 'left_id'),
                    'sorting'        => array('type' => 'INT(11)', 'after' => 'right_id'),
                    'level'          => array('type' => 'INT(11)', 'after' => 'sorting'),
                    'display'        => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'level'),
                )
            );

            // add nested set root node and select its id
            $objResultRoot = \Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories` (`parent_id`, `left_id`, `right_id`, `sorting`, `level`) VALUES (0, 0, 0, 0, 0)');
            if ($objResultRoot) {
                $nestedSetRootId = $objDatabase->Insert_ID();
            }

            // count categories
            $objResultCount = \Cx\Lib\UpdateUtil::sql('SELECT count(`catid`) AS count FROM `'.DBPREFIX.'module_news_categories`');
            if ($objResultCount && !$objResultCount->EOF) {
                $count = $objResultCount->fields['count'];
            }

            // add nested set information to root node
            \Cx\Lib\UpdateUtil::sql('
                UPDATE `'.DBPREFIX.'module_news_categories` SET
                `parent_id` = '.$nestedSetRootId.',
                `left_id` = 1,
                `right_id` = '.($count*2).',
                `sorting` = 1,
                `level` = 1
                WHERE `catid` = '.$nestedSetRootId.'
            ');

            // add nested set information to all categories
            $objResultCatSelect = \Cx\Lib\UpdateUtil::sql('SELECT `catid` FROM `'.DBPREFIX.'module_news_categories` ORDER BY `catid` ASC');
            if ($objResultCatSelect) {
                while (!$objResultCatSelect->EOF) {
                    $catId = $objResultCatSelect->fields['catid'];
                    if ($catId != $nestedSetRootId) {
                        \Cx\Lib\UpdateUtil::sql('
                            UPDATE `'.DBPREFIX.'module_news_categories` SET
                            `parent_id` = '.$nestedSetRootId.',
                            `left_id` = '.$leftAndRight++.',
                            `right_id` = '.$leftAndRight++.',
                            `sorting` = '.$sorting++.',
                            `level` = '.$level.'
                            WHERE `catid` = '.$catId.'
                        ');
                    }
                    $objResultCatSelect->MoveNext();
                }
            }

            // add new tables
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_categories_locks',
                array(
                    'lockId'         => array('type' => 'VARCHAR(32)'),
                    'lockTable'      => array('type' => 'VARCHAR(32)', 'after' => 'lockId'),
                    'lockStamp'      => array('type' => 'BIGINT(11)', 'notnull' => true, 'after' => 'lockTable')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_categories_catid',
                array(
                    'id'     => array('type' => 'INT(11)', 'notnull' => true)
                )
            );


            // insert id of last added category
            \Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories_catid` (`id`) SELECT MAX(`catid`) FROM `'.DBPREFIX.'module_news_categories`');
            $_SESSION['contrexx_update']['news']['nestedSet'] = true;
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    /************************************
    * EXTENSION:    Module page changes *
    * ADDED:        Contrexx v3.1.0     *
    ************************************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
        try {
            $result = \Cx\Lib\UpdateUtil::sql('SELECT `id` FROM `'.DBPREFIX.'content_page` WHERE `module` = "news" AND `cmd` RLIKE "^[0-9]*$"');
            if ($result && ($result->RecordCount() > 0)) {
                while (!$result->EOF) {

                    \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('id' => $result->fields['id']), '/(.*)/ms', function($matches) {
                        $page = $matches[0];

                        if (!empty($page) &&
                            !preg_match('/<!--\s+BEGIN\s+news_status_message\s+-->.*<!--\s+END\s+news_status_message\s+-->/ms', $page) &&
                            !preg_match('/<!--\s+BEGIN\s+news_menu\s+-->.*<!--\s+END\s+news_menu\s+-->/ms', $page) &&
                            !preg_match('/<!--\s+BEGIN\s+news_list\s+-->.*<!--\s+END\s+news_list\s+-->/ms', $page)
                        ) {
                            $page = preg_replace_callback('/<form[^>]*>[^<]*\\{NEWS_CAT_DROPDOWNMENU\\}[^>]*<\/form>/ims', function($matches) {
                                $menu = $matches[0];

                                $menu = preg_replace_callback('/(action\s*=\s*([\'"])[^\2]+section=news)\2/i', function($matches) {
                                    return $matches[1].'&cmd=[[NEWS_CMD]]'.$matches[2];
                                }, $menu);

                                return '
                                    <!-- BEGIN news_status_message -->
                                    {TXT_NEWS_NO_NEWS_FOUND}
                                    <!-- END news_status_message -->

                                    <!-- BEGIN news_menu -->
                                    '.$menu.'
                                    <!-- END news_menu -->
                                ';

                            }, $page);

                            $page = preg_replace_callback('/<ul[^>]*>[^<]*<!--\s+BEGIN\s+newsrow\s+-->.*<!--\s+END\s+newsrow\s+-->[^>]*<\/ul>/ims', function($matches) {
                                return '
                                    <!-- BEGIN news_list -->
                                    '.$matches[0].'
                                    <!-- END news_list -->
                                ';
                            }, $page);

                            if (!preg_match('/<!--\s+BEGIN\s+news_status_message\s+-->.*<!--\s+END\s+news_status_message\s+-->/ms', $page)) {
                                $page = '
                                    <!-- BEGIN news_status_message -->
                                    {TXT_NEWS_NO_NEWS_FOUND}
                                    <!-- END news_status_message -->
                                '.$page;
                            }
                        }

                        return $page;

                    }, array('content'), '3.1.0');

                    $result->MoveNext();
                }
            }

            $result = \Cx\Lib\UpdateUtil::sql('SELECT `id` FROM `'.DBPREFIX.'content_page` WHERE `module` = "news" AND `cmd` RLIKE "^details[0-9]*$"');
            if ($result && ($result->RecordCount() > 0)) {
                while (!$result->EOF) {

                    \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('id' => $result->fields['id']), '/(.*)/ms', function($matches) {
                        $page = $matches[0];

                        if (!empty($page) && !preg_match('/<!--\s+BEGIN\s+news_use_teaser_text\s+-->.*<!--\s+END\s+news_use_teaser_text\s+-->/ms', $page)) {
                            $page = preg_replace('/\\{NEWS_TEASER_TEXT\\}/', '<!-- BEGIN news_use_teaser_text -->\0<!-- END news_use_teaser_text -->', $page);
                        }

                        return $page;
                    }, array('content'), '3.1.0');


                    $result->MoveNext();
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    /***********************************
    * EXTENSION:    new settings added *
    * ADDED:        Contrexx v3.1.0    *
    ***********************************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
        try {
            $result = \Cx\Lib\UpdateUtil::sql('SELECT `name` FROM `'.DBPREFIX.'module_news_settings` WHERE `name` = "news_use_teaser_text"');
            if ($result && ($result->RecordCount() == 0)) {
                \Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES ("news_use_teaser_text", 1)');
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }



    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news',
                array(
                    'id'                             => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                    'date'                           => array('type' => 'INT(14)', 'notnull' => false, 'default' => NULL, 'after' => 'id'),
                    'redirect'                       => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'date'),
                    'source'                         => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'redirect'),
                    'url1'                           => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'source'),
                    'url2'                           => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'url1'),
                    'typeid'                         => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'url2'),
                    'publisher'                      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'typeid'),
                    'publisher_id'                   => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'publisher'),
                    'author'                         => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'publisher_id'),
                    'author_id'                      => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'author'),
                    'userid'                         => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'author_id'),
                    'startdate'                      => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'userid'),
                    'enddate'                        => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'startdate'),
                    'status'                         => array('type' => 'TINYINT(4)', 'notnull' => true, 'default' => '1', 'after' => 'enddate'),
                    'validated'                      => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'status'),
                    'frontend_access_id'             => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'validated'),
                    'backend_access_id'              => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'frontend_access_id'),
                    'teaser_only'                    => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'backend_access_id'),
                    'teaser_frames'                  => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_only'),
                    'teaser_show_link'               => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'after' => 'teaser_frames'),
                    'teaser_image_path'              => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_show_link'),
                    'teaser_image_thumbnail_path'    => array('type' => 'text', 'notnull' => true, 'after' => 'teaser_image_path'),
                    'changelog'                      => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0', 'after' => 'teaser_image_thumbnail_path'),
                    'allow_comments'                 => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'changelog'),
                    'enable_related_news'            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'allow_comments'),
                    'enable_tags'                    => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'enable_related_news'),
                    'redirect_new_window'            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'enable_tags'),
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_comments',
                array(
                    'id'             => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                    'title'          => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'text'           => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'title'),
                    'newsid'         => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'text'),
                    'date'           => array('type' => 'INT(14)', 'notnull' => false, 'default' => NULL,'after' => 'newsid'),
                    'poster_name'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'date'),
                    'userid'         => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'poster_name'),
                    'ip_address'     => array('type' => 'VARCHAR(32)', 'notnull' => true, 'default' => '', 'after' => 'userid'),
                    'is_active'      => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '1', 'after' => 'ip_address')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
        try {
            // fetch index infos for dropping duplicate settings below
            $structure = \Cx\Lib\UpdateUtil::sql('SHOW INDEX FROM `'.DBPREFIX.'module_news_settings`');
            if ($structure === false) {
                setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_news_settings'));
                return false;
            }

            $indexIsOk = false;
            $indexExists = false;
            while (!$structure->EOF) {
                // fetch unique keys
                if (
                    empty($structure->fields['Key_name']) ||
                    $structure->fields['Key_name'] != 'name'
                ) {
                    $structure->MoveNext();
                    continue;
                }

                // index by name 'name' already exists
                $indexExists = true;

                // check if index 'name' is correct
                if (
                    $structure->fields['Non_unique'] ||
                    $structure->fields['Index_type'] != 'BTREE'
                ) {
                    break;
                }

                // index is correct
                $indexIsOk = true;
                break;
            }

            // drop duplicate settings
            if (
                !$indexExists ||
                !$indexIsOk
            ) {
                // drop invalid index
                if ($indexExists) {
                    \Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'module_news_settings` DROP INDEX `name`');
                }
                if (!\Cx\Lib\UpdateUtil::table_exist(DBPREFIX.'module_news_settings_clean')) {
                    \Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_settings_clean` SELECT * FROM `'.DBPREFIX.'module_news_settings`');
                }
                \Cx\Lib\UpdateUtil::sql('TRUNCATE `'.DBPREFIX.'module_news_settings`');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_news_settings` ADD UNIQUE KEY `name` (`name`) USING BTREE');
                \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `'.DBPREFIX.'module_news_settings` SELECT * FROM `'.DBPREFIX.'module_news_settings_clean`');
                \Cx\Lib\UpdateUtil::sql('DROP TABLE `'.DBPREFIX.'module_news_settings_clean`');
            }

            // add missing settings
            \Cx\Lib\UpdateUtil::sql(
                'INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`)
                VALUES  ("use_related_news", "0"),
                        ("news_use_tags", "0"),
                        ("use_previous_next_news_link", "0"),
                        ("use_thumbnails", "1")
                ON DUPLICATE KEY UPDATE `name` = `name`'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            // migrate path to images and media
            $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
            $attributes = array(
                'title'         => 'module_news_locale',
                'text'          => 'module_news_locale',
                'teaser_text'   => 'module_news_locale',
                'value'         => 'module_news_settings',
                'html'          => 'module_news_teaser_frame_templates',
                'teaser_image_path'=> 'module_news',
                'teaser_image_thumbnail_path'=> 'module_news',
            );
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'News (News)'
            ));
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.2')) {
        try {
            \Cx\Lib\UpdateUtil::sql(
                'INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`)
                VALUES  ("login_redirect", "0")
                ON DUPLICATE KEY UPDATE `name` = `name`'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            // add display column
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_news_categories',
                array(
                    'catid'          => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'parent_id'      => array('type' => 'INT(11)', 'after' => 'catid'),
                    'left_id'        => array('type' => 'INT(11)', 'after' => 'parent_id'),
                    'right_id'       => array('type' => 'INT(11)', 'after' => 'left_id'),
                    'sorting'        => array('type' => 'INT(11)', 'after' => 'right_id'),
                    'level'          => array('type' => 'INT(11)', 'after' => 'sorting'),
                    'display'        => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'level'),
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _newsInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(14) DEFAULT NULL,
  `redirect` varchar(250) NOT NULL DEFAULT \'\',
  `source` varchar(250) NOT NULL DEFAULT \'\',
  `url1` varchar(250) NOT NULL DEFAULT \'\',
  `url2` varchar(250) NOT NULL DEFAULT \'\',
  `typeid` int(2) unsigned NOT NULL DEFAULT \'0\',
  `publisher` varchar(255) NOT NULL DEFAULT \'\',
  `publisher_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `author` varchar(255) NOT NULL DEFAULT \'\',
  `author_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `userid` int(6) unsigned NOT NULL DEFAULT \'0\',
  `startdate` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  `enddate` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  `status` tinyint(4) NOT NULL DEFAULT \'1\',
  `validated` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `frontend_access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `backend_access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `teaser_only` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `teaser_frames` text NOT NULL,
  `teaser_show_link` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `teaser_image_path` text NOT NULL,
  `teaser_image_thumbnail_path` text NOT NULL,
  `changelog` int(14) NOT NULL DEFAULT \'0\',
  `allow_comments` tinyint(1) NOT NULL DEFAULT \'0\',
  `enable_related_news` tinyint(1) NOT NULL DEFAULT \'0\',
  `enable_tags` tinyint(1) NOT NULL DEFAULT \'0\',
  `redirect_new_window` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news` (`id`, `date`, `redirect`, `source`, `url1`, `url2`, `typeid`, `publisher`, `publisher_id`, `author`, `author_id`, `userid`, `startdate`, `enddate`, `status`, `validated`, `frontend_access_id`, `backend_access_id`, `teaser_only`, `teaser_frames`, `teaser_show_link`, `teaser_image_path`, `teaser_image_thumbnail_path`, `changelog`, `allow_comments`, `enable_related_news`, `enable_tags`, `redirect_new_window`) VALUES (\'4\', \'1412001328\', \'\', \'\', \'\', \'\', \'0\', \'MaxMuster AG\', \'0\', \'Max Muster\', \'0\', \'1\', \'0000-00-00 00:00:00\', \'0000-00-00 00:00:00\', \'1\', \'1\', \'0\', \'0\', \'0\', \'\', \'1\', \'\', \'\', \'1412001331\', \'1\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news` (`id`, `date`, `redirect`, `source`, `url1`, `url2`, `typeid`, `publisher`, `publisher_id`, `author`, `author_id`, `userid`, `startdate`, `enddate`, `status`, `validated`, `frontend_access_id`, `backend_access_id`, `teaser_only`, `teaser_frames`, `teaser_show_link`, `teaser_image_path`, `teaser_image_thumbnail_path`, `changelog`, `allow_comments`, `enable_related_news`, `enable_tags`, `redirect_new_window`) VALUES (\'6\', \'1412001303\', \'\', \'\', \'\', \'\', \'0\', \'\', \'1\', \'\', \'1\', \'1\', \'0000-00-00 00:00:00\', \'0000-00-00 00:00:00\', \'1\', \'1\', \'0\', \'0\', \'0\', \'\', \'1\', \'\', \'\', \'1412001303\', \'1\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_categories` (
  `catid` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `left_id` int(11) NOT NULL,
  `right_id` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `display tinyint(1) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories` (`catid`, `parent_id`, `left_id`, `right_id`, `sorting`, `level`) VALUES (\'1\', \'1\', \'1\', \'4\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories` (`catid`, `parent_id`, `left_id`, `right_id`, `sorting`, `level`) VALUES (\'2\', \'1\', \'2\', \'3\', \'2\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_categories_catid` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories_catid` (`id`) VALUES (\'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_categories_locale` (
  `category_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(100) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`category_id`,`lang_id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories_locale` (`category_id`, `lang_id`, `name`) VALUES (\'2\', \'1\', \'Kundennews\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_categories_locale` (`category_id`, `lang_id`, `name`) VALUES (\'2\', \'2\', \'Customers\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_categories_locks` (
  `lockId` varchar(32) NOT NULL,
  `lockTable` varchar(32) NOT NULL,
  `lockStamp` bigint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL DEFAULT \'\',
  `text` mediumtext NOT NULL,
  `newsid` int(6) unsigned NOT NULL DEFAULT \'0\',
  `date` int(14) DEFAULT NULL,
  `poster_name` varchar(255) NOT NULL DEFAULT \'\',
  `userid` int(5) unsigned NOT NULL DEFAULT \'0\',
  `ip_address` varchar(32) NOT NULL DEFAULT \'\',
  `is_active` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_comments` (`id`, `title`, `text`, `newsid`, `date`, `poster_name`, `userid`, `ip_address`, `is_active`) VALUES (\'2\', \'Richtige Wahl getroffen\', \'Da habt ihr eine gute Wahl getroffen, wir benutzen schon lange das Cloudrexx CMS und waren immer äusserst zufrieden damit!\\r\\nLiebe Grüsse Peter\', \'4\', \'1347535130\', \'Peter\', \'0\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_locale` (
  `news_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `is_active` int(1) unsigned NOT NULL DEFAULT \'1\',
  `title` varchar(250) NOT NULL DEFAULT \'\',
  `text` mediumtext NOT NULL,
  `teaser_text` text NOT NULL,
  PRIMARY KEY (`news_id`,`lang_id`),
  FULLTEXT KEY `newsindex` (`text`,`title`,`teaser_text`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_locale` (`news_id`, `lang_id`, `is_active`, `title`, `text`, `teaser_text`) VALUES (\'4\', \'1\', \'1\', \'Was ist Cloudrexx?\', \'<h2>Was ist <a href=\\\"http://www.cloudrexx.com\\\" target=\\\"_blank\\\">Cloudrexx</a> ?</h2>\\r\\nCloudrexx ist eine f&uuml;hrende Schweizer Customer-Experience-Management-L&ouml;sung (CXM). Der neue Cloud-Service auf&nbsp;Basis der Website Management L&ouml;sung Conrexx ist&nbsp;optimiert f&uuml;r Unternehmen aus der Schweiz, Deutschland und &Ouml;sterreich.&nbsp;<br />\\r\\n&nbsp;\\r\\n<h2>Was ist mit&nbsp;<a href=\\\"http://www.cloudrexx.com\\\" target=\\\"_blank\\\">Cloudrexx</a> m&ouml;glich?</h2>\\r\\n\\r\\n<ul>\\r\\n	<li><strong>Web Content Management:&nbsp;</strong>Mit Cloudrexx erstellen und verwalten Sie Ihrer Website schnell und einfach. In der WYSIWYG-Umgebung funktioniert das Gestalten von Inhaltsseiten mit Texten, Tabellen und Bildern &auml;hnlich wie bei Word. Alle Inhaltsseiten sind in der &uuml;bersichtlichen Seitenverwaltung abgelegt. Dort besteht ausserdem die M&ouml;glichkeit, Benutzergruppen zu erstellen und Benutzerrechte zu definieren.</li>\\r\\n	<li><strong>Online Marketing:&nbsp;</strong>Cloudrexx bietet ein umfassendes Set an Funktionen, um ohne zus&auml;tzliche Spezialsoftware Online-Marketing &nbsp;zu betreiben. Optimieren Sie mit <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>Ihre Website dank ausgefeilter Search Engine Optimization (SEO) oder machen Sie Ihre Kunden mit regelm&auml;ssigen Newslettern gezielt &nbsp;auf neue Produkte oder Aktionen aufmerksam. Die Anbindung an die Social-Media-Kan&auml;le ist ebenfalls gew&auml;hrleistet.</li>\\r\\n	<li><strong>E-Commerce:</strong>&nbsp;Bauen Sie Ihre Website mit <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>zum Verkaufskanal aus. Die Software enth&auml;lt ein voll funktionst&uuml;chtiges Shop-Modul, in das Sie Produkteinfos und Fotos einpflegen sowie die Bezahlung abwickeln k&ouml;nnen. Per Knopfdruck k&ouml;nnen Sie die Produkte ver&ouml;ffentlichen und ab sofort zum Verkauf anbieten. Dank der Anbindung verschiedener Zahlungsanbieter k&ouml;nnen Sie praktisch alle verf&uuml;gbaren Zahlungsarten anbieten.</li>\\r\\n	<li><strong>CRM:</strong>&nbsp;Erh&ouml;hen Sie die Zufriedenheit Ihrer Kunden durch ein professionelles Kundenbeziehungsmanagement. Bearbeiten Sie Kundenanfragen ohne Umweg &uuml;ber ein zus&auml;tzliches Tool direkt in <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>und profitieren von den Synergien zu den anderen Modulen. Bedienen Sie Ihre Zielgruppen bed&uuml;rfnisgerecht und individuell.</li>\\r\\n	<li><strong>Analytics:&nbsp;</strong>Wer die besten Informationen rechtzeitig, komplett und am richtigen Ort zur Hand hat, setzt sich durch. Messen Sie zum Beispiel, wer Ihre Website besucht, welches Ihre bestbesuchten Seiten sind und von wo die Besucher herkommen. Nutzen Sie diese Informationen, um Ihren Auftritt und Ihr Angebot zu optimieren. Erkennen Sie dank den &uuml;bersichtlichen Charts in <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>die neusten Trends.</li>\\r\\n	<li><strong>Media Asset Management:</strong>&nbsp;<span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>beinhaltet ein System zum Verwalten aller g&auml;ngigen Dateiformate. Medienmanager und Bildmanager erlauben das nachtr&auml;gliche Bearbeiten von Bildern, das Bereitstellen von Downloads sowie ein intelligentes System zum Verschicken von Downloadlinks. Zudem laden Sie mit dem Multi-File-Uploader beliebig viele Dateien gleichzeitig in k&uuml;rzester Zeit hoch..</li>\\r\\n</ul>\\r\\n<br />\\r\\n<span style=\\\"line-height: 20.7999992370605px;\\\">Dank den umfangreichen Funktionalit&auml;ten bietet Cloudrexx f&uuml;r praktisch s&auml;mtliche Bed&uuml;rfnisse, die heute an ein Online-Business&nbsp;gestellt werden, eine L&ouml;sung an.</span>\', \'Wir haben für unsere Website ein Cloudrexx CMS gewählt.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_locale` (`news_id`, `lang_id`, `is_active`, `title`, `text`, `teaser_text`) VALUES (\'4\', \'2\', \'1\', \'Was ist Cloudrexx?\', \'<h2>Was ist <a href=\\\"http://www.cloudrexx.com\\\" target=\\\"_blank\\\">Cloudrexx</a> ?</h2>\\r\\nCloudrexx ist eine f&uuml;hrende Schweizer Customer-Experience-Management-L&ouml;sung (CXM). Der neue Cloud-Service auf&nbsp;Basis der Website Management L&ouml;sung Conrexx ist&nbsp;optimiert f&uuml;r Unternehmen aus der Schweiz, Deutschland und &Ouml;sterreich.&nbsp;<br />\\r\\n&nbsp;\\r\\n<h2>Was ist mit&nbsp;<a href=\\\"http://www.cloudrexx.com\\\" target=\\\"_blank\\\">Cloudrexx</a> m&ouml;glich?</h2>\\r\\n\\r\\n<ul>\\r\\n	<li><strong>Web Content Management:&nbsp;</strong>Mit Cloudrexx erstellen und verwalten Sie Ihrer Website schnell und einfach. In der WYSIWYG-Umgebung funktioniert das Gestalten von Inhaltsseiten mit Texten, Tabellen und Bildern &auml;hnlich wie bei Word. Alle Inhaltsseiten sind in der &uuml;bersichtlichen Seitenverwaltung abgelegt. Dort besteht ausserdem die M&ouml;glichkeit, Benutzergruppen zu erstellen und Benutzerrechte zu definieren.</li>\\r\\n	<li><strong>Online Marketing:&nbsp;</strong>Cloudrexx bietet ein umfassendes Set an Funktionen, um ohne zus&auml;tzliche Spezialsoftware Online-Marketing &nbsp;zu betreiben. Optimieren Sie mit <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>Ihre Website dank ausgefeilter Search Engine Optimization (SEO) oder machen Sie Ihre Kunden mit regelm&auml;ssigen Newslettern gezielt &nbsp;auf neue Produkte oder Aktionen aufmerksam. Die Anbindung an die Social-Media-Kan&auml;le ist ebenfalls gew&auml;hrleistet.</li>\\r\\n	<li><strong>E-Commerce:</strong>&nbsp;Bauen Sie Ihre Website mit <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>zum Verkaufskanal aus. Die Software enth&auml;lt ein voll funktionst&uuml;chtiges Shop-Modul, in das Sie Produkteinfos und Fotos einpflegen sowie die Bezahlung abwickeln k&ouml;nnen. Per Knopfdruck k&ouml;nnen Sie die Produkte ver&ouml;ffentlichen und ab sofort zum Verkauf anbieten. Dank der Anbindung verschiedener Zahlungsanbieter k&ouml;nnen Sie praktisch alle verf&uuml;gbaren Zahlungsarten anbieten.</li>\\r\\n	<li><strong>CRM:</strong>&nbsp;Erh&ouml;hen Sie die Zufriedenheit Ihrer Kunden durch ein professionelles Kundenbeziehungsmanagement. Bearbeiten Sie Kundenanfragen ohne Umweg &uuml;ber ein zus&auml;tzliches Tool direkt in <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>und profitieren von den Synergien zu den anderen Modulen. Bedienen Sie Ihre Zielgruppen bed&uuml;rfnisgerecht und individuell.</li>\\r\\n	<li><strong>Analytics:&nbsp;</strong>Wer die besten Informationen rechtzeitig, komplett und am richtigen Ort zur Hand hat, setzt sich durch. Messen Sie zum Beispiel, wer Ihre Website besucht, welches Ihre bestbesuchten Seiten sind und von wo die Besucher herkommen. Nutzen Sie diese Informationen, um Ihren Auftritt und Ihr Angebot zu optimieren. Erkennen Sie dank den &uuml;bersichtlichen Charts in <span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>die neusten Trends.</li>\\r\\n	<li><strong>Media Asset Management:</strong>&nbsp;<span style=\\\"line-height: 20.7999992370605px;\\\">Cloudrexx&nbsp;</span>beinhaltet ein System zum Verwalten aller g&auml;ngigen Dateiformate. Medienmanager und Bildmanager erlauben das nachtr&auml;gliche Bearbeiten von Bildern, das Bereitstellen von Downloads sowie ein intelligentes System zum Verschicken von Downloadlinks. Zudem laden Sie mit dem Multi-File-Uploader beliebig viele Dateien gleichzeitig in k&uuml;rzester Zeit hoch..</li>\\r\\n</ul>\\r\\n<br />\\r\\n<span style=\\\"line-height: 20.7999992370605px;\\\">Dank den umfangreichen Funktionalit&auml;ten bietet Cloudrexx f&uuml;r praktisch s&auml;mtliche Bed&uuml;rfnisse, die heute an ein Online-Business&nbsp;gestellt werden, eine L&ouml;sung an.</span>\', \'Wir haben für unsere Website ein Cloudrexx CMS gewählt.\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_rel_categories` (
  `news_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  UNIQUE KEY `NewsTagsRelation` (`news_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_rel_categories` (`news_id`, `category_id`) VALUES (\'4\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_rel_news` (
  `news_id` int(11) NOT NULL,
  `related_news_id` int(11) NOT NULL,
  UNIQUE KEY `related_news` (`news_id`,`related_news_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_rel_tags` (
  `news_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  UNIQUE KEY `NewsTagsRelation` (`news_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_settings` (
  `name` varchar(50) NOT NULL DEFAULT \'\',
  `value` varchar(250) NOT NULL DEFAULT \'\',
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_activate_submitted_news\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_assigned_author_groups\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_assigned_publisher_groups\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_comments_activated\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_comments_anonymous\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_comments_autoactivate\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_comments_notification\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_comments_timeout\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_default_teasers\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_feed_image\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_feed_status\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_headlines_limit\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_message_protection\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_message_protection_restricted\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_notify_group\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_notify_user\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_settings_activated\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_submit_news\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_submit_only_community\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_ticker_filename\', \'newsticker.txt\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_top_days\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_top_limit\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_use_tags\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_use_teaser_text\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_use_top\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'news_use_types\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'recent_news_message_limit\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'use_previous_next_news_link\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'use_related_news\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings` (`name`, `value`) VALUES (\'use_thumbnails\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_settings_locale` (
  `name` varchar(50) NOT NULL DEFAULT \'\',
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `value` varchar(250) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`name`,`lang_id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings_locale` (`name`, `lang_id`, `value`) VALUES (\'news_feed_description\', \'1\', \'Informationen rund um Cloudrexx\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings_locale` (`name`, `lang_id`, `value`) VALUES (\'news_feed_description\', \'2\', \'Informationen rund um Cloudrexx\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings_locale` (`name`, `lang_id`, `value`) VALUES (\'news_feed_title\', \'1\', \'Cloudrexx\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_settings_locale` (`name`, `lang_id`, `value`) VALUES (\'news_feed_title\', \'2\', \'Cloudrexx\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_stats_view` (
  `user_sid` char(32) NOT NULL,
  `news_id` int(6) unsigned NOT NULL,
  `time` timestamp NOT NULL ,
  KEY `idx_user_sid` (`user_sid`),
  KEY `idx_news_id` (`news_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) BINARY NOT NULL,
  `viewed_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_teaser_frame` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lang_id` int(3) unsigned NOT NULL DEFAULT \'0\',
  `frame_template_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(50) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_teaser_frame` (`id`, `lang_id`, `frame_template_id`, `name`) VALUES (\'1\', \'1\', \'1\', \'Beispiel1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_teaser_frame` (`id`, `lang_id`, `frame_template_id`, `name`) VALUES (\'2\', \'1\', \'2\', \'Beispiel2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_teaser_frame` (`id`, `lang_id`, `frame_template_id`, `name`) VALUES (\'4\', \'1\', \'3\', \'Beispiel3\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_teaser_frame_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(100) NOT NULL DEFAULT \'\',
  `html` text NOT NULL,
  `source_code_mode` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_teaser_frame_templates` (`id`, `description`, `html`, `source_code_mode`) VALUES (\'1\', \'3 Teaserboxen (1. Zeile: 1 Teaser - 2. Zeile: 2 Teaser)\', \'<table cellspacing=\\\"5\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n    <tbody>\\r\\n        <tr>\\r\\n            <td colspan=\\\"2\\\"><!-- BEGIN teaser_1 -->\\r\\n            <table width=\\\"480\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"25%\\\"><img src=\\\"{TEASER_IMAGE_PATH}\\\" width=\\\"80\\\" height=\\\"120\\\" alt=\\\"\\\" /></td>\\r\\n                        <td width=\\\"75%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_1 --></td>\\r\\n        </tr>\\r\\n        <tr>\\r\\n            <td width=\\\"50%\\\"><!-- BEGIN teaser_2 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img src=\\\"{TEASER_IMAGE_PATH}\\\" width=\\\"80\\\" height=\\\"120\\\" alt=\\\"\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_2 --></td>\\r\\n            <td width=\\\"50%\\\"><!-- BEGIN teaser_3 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th>{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td>{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td><img src=\\\"{TEASER_IMAGE_PATH}\\\" width=\\\"80\\\" height=\\\"120\\\" alt=\\\"\\\" /></td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_3 --></td>\\r\\n        </tr>\\r\\n    </tbody>\\r\\n</table>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_teaser_frame_templates` (`id`, `description`, `html`, `source_code_mode`) VALUES (\'2\', \'4 Teaserboxen (2 Teaser pro Zeile)\', \'<table cellspacing=\\\"5\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n    <tbody>\\r\\n        <tr>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_1 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_1 --></td>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_2 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th>{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td>{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_2 --></td>\\r\\n        </tr>\\r\\n        <tr>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_3 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th>{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td>{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_3 --></td>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_4 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_4 --></td>\\r\\n        </tr>\\r\\n    </tbody>\\r\\n</table>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_news_teaser_frame_templates` (`id`, `description`, `html`, `source_code_mode`) VALUES (\'3\', \'6 Teaserboxen (2 Teaser pro Zeile)\', \'<table cellspacing=\\\"5\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n    <tbody>\\r\\n        <tr>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_1 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_1 --></td>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_2 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_2 --></td>\\r\\n        </tr>\\r\\n        <tr>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_3 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_3 --></td>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_4 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_4 --></td>\\r\\n        </tr>\\r\\n        <tr>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_5 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_5 --></td>\\r\\n            <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\"><!-- BEGIN teaser_6 -->\\r\\n            <table width=\\\"235\\\" cellspacing=\\\"0\\\" cellpadding=\\\"0\\\" border=\\\"0\\\">\\r\\n                <tbody>\\r\\n                    <tr>\\r\\n                        <th colspan=\\\"2\\\">{TEASER_CATEGORY}</th>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td colspan=\\\"2\\\">{TEASER_DATE}<br />{TEASER_TITLE} <!-- BEGIN teaser_link --><a href=\\\"{TEASER_URL}\\\" target=\\\"{TEASER_URL_TARGET}\\\">» Zur Meldung</a><!-- END teaser_link --></td>\\r\\n                    </tr>\\r\\n                    <tr>\\r\\n                        <td width=\\\"50%\\\"><img alt=\\\"\\\" src=\\\"{TEASER_IMAGE_PATH}\\\" /></td>\\r\\n                        <td width=\\\"50%\\\" style=\\\"vertical-align: top;\\\">{TEASER_TEXT}</td>\\r\\n                    </tr>\\r\\n                </tbody>\\r\\n            </table>\\r\\n            <!-- END teaser_6 --></td>\\r\\n        </tr>\\r\\n    </tbody>\\r\\n</table>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_ticker` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `charset` enum(\'ISO-8859-1\',\'UTF-8\') NOT NULL DEFAULT \'ISO-8859-1\',
  `urlencode` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `prefix` varchar(250) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_types` (
  `typeid` int(2) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`typeid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_news_types_locale` (
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `type_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(100) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`lang_id`,`type_id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
