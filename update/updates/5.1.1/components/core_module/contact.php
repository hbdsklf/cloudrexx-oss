<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _contactUpdate()
{
    global $objUpdate, $_CONFIG;
    try {
        if (!in_array('contact_phase_1', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_recipient',
                    array(
                        'id' => array('type' => 'INT', 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                        'id_form' => array('type' => 'INT(11)', 'notnull' => true, 'default' => 0),
                        'name' => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => ''),
                        'email' => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => ''),
                        'sort' => array('type' => 'INT(11)', 'notnull' => true, 'default' => 0),
                    )
                );

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form_field',
                    array(
                        'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'id_form' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                        'name' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                        'type' => array('type' => 'ENUM(\'text\',\'label\',\'checkbox\',\'checkboxGroup\',\'date\',\'file\',\'multi_file\',\'hidden\',\'password\',\'radio\',\'select\',\'textarea\',\'recipient\')', 'notnull' => true, 'default' => 'text', 'binary' => true),
                        'attributes' => array('type' => 'TEXT'),
                        'is_required' => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0'),
                        'check_type' => array('type' => 'INT(3)', 'notnull' => true, 'default' => '1'),
                        'order_id' => array('type' => 'SMALLINT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0')
                    ),
                    array(
                        'id_form' => array('fields' => array('id_form')),
                    )
                );


                /****************************
                 * ADDED:    Contrexx v3.0.0 *
                 ****************************/
                /*
                 * Create new table 'module_contact_form_field_lang'
                 * to store language patameters of each field
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form_field_lang',
                    array(
                        'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'fieldID' => array('type' => 'INT(10)', 'unsigned' => true, 'after' => 'id'),
                        'langID' => array('type' => 'INT(10)', 'unsigned' => true, 'after' => 'fieldID'),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'langID'),
                        'attributes' => array('type' => 'text', 'after' => 'name')
                    ),
                    array(
                        'fieldID' => array('fields' => array('fieldID', 'langID'), 'type' => 'UNIQUE')
                    )
                );

                /*
                 * Migrate name and attributes fields from 'module_contact_form_field' table
                 * to 'module_contact_form_field_lang' table for active frontend language.
                 * For other languages empty string
                 */
                if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form_field', 'name') && \Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form', 'langId')) {
                    $query = "SELECT `field`.`id`, `field`.`id_form`, `field`.`name`, `field`.`attributes`, `form`.`langId`
                              FROM `" . DBPREFIX . "module_contact_form_field` AS `field`
                              JOIN `" . DBPREFIX . "module_contact_form` AS `form`
                              ON `form`.`id` = `field`.`id_form`";
                    $objResult = \Cx\Lib\UpdateUtil::sql($query);
                    if ($objResult) {
                        while (!$objResult->EOF) {
                            $rowCountResult = \Cx\Lib\UpdateUtil::sql("SELECT 1
                                                            FROM `" . DBPREFIX . "module_contact_form_field_lang`
                                                            WHERE `fieldID` = " . $objResult->fields['id'] . "
                                                            AND `langID` = " . $objResult->fields['langId'] . "
                                                            LIMIT 1");

                            if ($rowCountResult->RecordCount() == 0) {
                                $query = "INSERT INTO `" . DBPREFIX . "module_contact_form_field_lang` (
                                     `fieldID`, `langID`, `name`, `attributes`
                                     ) VALUES (
                                     " . $objResult->fields['id'] . ",
                                     " . $objResult->fields['langId'] . ",
                                     '" . addslashes($objResult->fields['name']) . "',
                                     '" . addslashes($objResult->fields['attributes']) . "')";
                                \Cx\Lib\UpdateUtil::sql($query);
                            }

                            $objResult->MoveNext();
                        }
                    }
                }

                /*
                 * Create table 'module_contact_recipient_lang'
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_recipient_lang',
                    array(
                        'id' => array('type' => 'INT(10)', 'notnull' => true, 'unsigned' => true, 'primary' => true, 'auto_increment' => true),
                        'recipient_id' => array('type' => 'INT(10)', 'notnull' => true, 'unsigned' => true, 'after' => 'id'),
                        'langID' => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'recipient_id'),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'langID')
                    ),
                    array(
                        'recipient_id' => array('fields' => array('recipient_id', 'langID'), 'type' => 'UNIQUE')
                    )
                );

                /*
                 * Transfer recipientId and name from 'module_contact_recipient'
                 * to 'module_contact_recipient_lang'
                 */
                if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_recipient', 'name')) {
                    $query = "SELECT `id`, `id_form`, `name` FROM `" . DBPREFIX . "module_contact_recipient`";
                    $objResult = \Cx\Lib\UpdateUtil::sql($query);
                    while (!$objResult->EOF) {
                        $langId = 1;
                        if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form', 'langId')) {
                            $query = "SELECT `langId`
                                       FROM `" . DBPREFIX . "module_contact_form`
                                       WHERE `id` = " . $objResult->fields['id_form'];
                            $formLangId = \Cx\Lib\UpdateUtil::sql($query);
                            $langId = ($formLangId->fields['langId'] != null) ? $formLangId->fields['langId'] : 1;
                        } else {
                            $langId = 1;
                        }

                        /*
                         * Check for row already exsist
                         */
                        $rowCountResult = \Cx\Lib\UpdateUtil::sql("SELECT 1 as count
                                                        FROM `" . DBPREFIX . "module_contact_recipient_lang`
                                                        WHERE `recipient_id` = " . $objResult->fields['id'] . "
                                                        AND `langID` = " . $langId . "
                                                        LIMIT 1");
                        if ($rowCountResult->RecordCount() == 0) {
                            $query = "INSERT INTO `" . DBPREFIX . "module_contact_recipient_lang` (
                                  `recipient_id`, `langID`, `name`
                                  ) VALUES (
                                  " . $objResult->fields['id'] . ",
                                  $langId,
                                  '" . addslashes($objResult->fields['name']) . "')";
                            \Cx\Lib\UpdateUtil::sql($query);
                        }

                        $objResult->MoveNext();
                    }
                }

                /*
                 * Drop column 'recipient name' from 'module_contact_recipient'
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_recipient',
                    array(
                        'id' => array('type' => 'INT(11)', 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                        'id_form' => array('type' => 'INT(11)', 'notnull' => true, 'default' => 0, 'after' => 'id'),
                        'email' => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'id_form'),
                        'sort' => array('type' => 'INT(11)', 'notnull' => true, 'default' => 0, 'after' => 'email')
                    )
                );
            }

            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
                /*
                 * Create new table 'module_contact_form_submit_data'
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form_submit_data',
                    array(
                        'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'id_entry' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'after' => 'id'),
                        'id_field' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'after' => 'id_entry'),
                        'formlabel' => array('type' => 'TEXT', 'after' => 'id_field'),
                        'formvalue' => array('type' => 'TEXT', 'after' => 'formlabel')
                    ),
                    array(
                        'id_entry' => array('fields' => array('id_entry')),
                    )
                );
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'contact_phase_1';
            return 'timeout';
        }

        if (!in_array('contact_phase_2', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
                /*
                 * Transfer 'data' field of 'module_contact_form_data' table to 'field_label' and 'field_value'
                 * in 'module_contact_form_submit_data' after base64 decoding
                 * Fetch fieldId from 'module_contact_form_field_lang' table by matching fieldLabel
                 */
                if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form_data', 'data')) {
                    /*
                     * Execute migrate script for every 30 dataset
                     */
                    $query = "SELECT `id`, `id_form`, `data`
                              FROM `" . DBPREFIX . "module_contact_form_data`
                              WHERE `data` != ''
                              LIMIT 10";
                    while (($objResult = \Cx\Lib\UpdateUtil::sql($query)) && $objResult->RecordCount()) {
                        while (!$objResult->EOF) {
                            $fields_attr = explode(";", $objResult->fields['data']);

                            foreach ($fields_attr as $key => $value) {
                                $field_attr = explode(",", $value);
                                $field_label = base64_decode($field_attr[0]);
                                $field_value = base64_decode($field_attr[1]);

                                /*
                                 * In the contrexx 2.1.4, the recipient fields were stored as 'contactFormField_recipient' in the submitted data.
                                 */
                                if ($field_label == "contactFormField_recipient" &&
                                    \Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form_field', 'name')
                                ) {
                                    $form_recipient_query = "SELECT `name`
                                                        FROM `" . DBPREFIX . "module_contact_form_field`
                                                        WHERE `type` = 'recipient'
                                                        AND `id_form` = " . $objResult->fields['id_form'] . "
                                                        LIMIT 1";
                                    $formRecipientResult = \Cx\Lib\UpdateUtil::sql($form_recipient_query);
                                    $field_label = $formRecipientResult->fields['name'];
                                }

                                $form_label_query = '
                                SELECT `lang`.`fieldID`
                                FROM `' . DBPREFIX . 'module_contact_form_field` AS `field`
                                LEFT JOIN `' . DBPREFIX . 'module_contact_form_field_lang` AS `lang` ON `lang`.`fieldID` = `field`.`id`
                                WHERE (`field`.`id_form` = ' . $objResult->fields['id_form'] . ') AND (`lang`.`name` = "' . contrexx_raw2db($field_label) . '")
                            ';
                                $formLabelResult = \Cx\Lib\UpdateUtil::sql($form_label_query);
                                $fieldId = ($formLabelResult->fields['fieldID'] != null) ? $formLabelResult->fields['fieldID'] : 0;

                                $submitCount = \Cx\Lib\UpdateUtil::sql("SELECT 1
                                                            FROM `" . DBPREFIX . "module_contact_form_submit_data`
                                                            WHERE `id_entry` = " . $objResult->fields['id'] . "
                                                            AND `id_field` = " . $fieldId . "
                                                            LIMIT 1");

                                if ($submitCount->RecordCount() == 0) {
                                    $submitQuery = "INSERT INTO `" . DBPREFIX . "module_contact_form_submit_data` (
                                          `id_entry`, `id_field`, `formlabel`, `formvalue`
                                          ) VALUES (
                                          " . $objResult->fields['id'] . ",
                                          " . $fieldId . ",
                                          '" . addslashes($field_label) . "',
                                          '" . addslashes($field_value) . "')";
                                    \Cx\Lib\UpdateUtil::sql($submitQuery);
                                }
                            }

                            /*
                             * Empty column 'data' of the current dataset
                             */
                            $updateQuery = "UPDATE `" . DBPREFIX . "module_contact_form_data`
                                  SET `data` = ''
                                  WHERE `id` = " . $objResult->fields['id'];
                            \Cx\Lib\UpdateUtil::sql($updateQuery);

                            // check timeout
                            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                                return 'timeout';
                            }

                            $objResult->MoveNext();
                        }
                    }
                }
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'contact_phase_2';
            return 'timeout';
        }

        if (!in_array('contact_phase_3', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
                /*
                 * Alter table 'module_contact_form_data' by dropping 'data' field and adding 'id_lang'
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form_data',
                    array(
                        'id' => array('type' => 'INT(10)', 'notnull' => true, 'unsigned' => true, 'primary' => true, 'auto_increment' => true),
                        'id_form' => array('type' => 'INT(10)', 'notnull' => true, 'unsigned' => true, 'default' => 0, 'after' => 'id'),
                        'id_lang' => array('type' => 'INT(10)', 'notnull' => true, 'unsigned' => true, 'default' => 1, 'after' => 'id_form'),
                        'time' => array('type' => 'INT(14)', 'notnull' => true, 'unsigned' => true, 'default' => 0, 'after' => 'id_lang'),
                        'host' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'after' => 'time'),
                        'lang' => array('type' => 'VARCHAR(64)', 'notnull' => true, 'after' => 'host'),
                        'browser' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'after' => 'lang'),
                        'ipaddress' => array('type' => 'VARCHAR(15)', 'notnull' => true, 'after' => 'browser')
                    ),
                    array(
                        'id_form' => array('fields' => array('id_form')),
                    )
                );
            }

            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
                /*
                 * Alter table 'module_contact_form_field' by dropping name and attributes column
                 * Add 'special_type' column
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form_field',
                    array(
                        'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'id_form' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => 0, 'after' => 'id'),
                        'type' => array('type' => 'ENUM(\'text\',\'label\',\'checkbox\',\'checkboxGroup\',\'country\',\'date\',\'file\',\'multi_file\',\'fieldset\',\'hidden\',\'horizontalLine\',\'password\',\'radio\',\'select\',\'textarea\',\'recipient\',\'special\',\'datetime\')', 'notnull' => true, 'default' => 'text', 'after' => 'id_form', 'binary' => true),
                        'special_type' => array('type' => 'VARCHAR(20)', 'notnull' => true, 'after' => 'type'),
                        'is_required' => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'special_type'),
                        'check_type' => array('type' => 'INT(3)', 'notnull' => true, 'default' => 1, 'after' => 'is_required'),
                        'order_id' => array('type' => 'SMALLINT(5)', 'notnull' => true, 'unsigned' => true, 'default' => 0, 'after' => 'check_type')
                    ),
                    array(
                        'id_form' => array('fields' => array('id_form')),
                    )
                );
            }

            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
                /*
                 * Update 'id_lang' column with form language id
                 */
                if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form', 'langId')) {
                    $query = "SELECT `id`, `id_form` FROM `" . DBPREFIX . "module_contact_form_data`";
                    $objResult = \Cx\Lib\UpdateUtil::sql($query);

                    if ($objResult) {
                        while (!$objResult->EOF) {
                            $query = "UPDATE `" . DBPREFIX . "module_contact_form_data`
                                    SET `id_lang` = (
                                        SELECT `langId` from `" . DBPREFIX . "module_contact_form`
                                        WHERE `id` = " . $objResult->fields['id_form'] . "
                                    ) WHERE `id` = " . $objResult->fields['id'];
                            \Cx\Lib\UpdateUtil::sql($query);
                            $objResult->MoveNext();
                        }
                    }
                }

                /*
                 * Create table 'module_contact_form_lang'
                 */
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form_lang',
                    array(
                        'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'formID' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'after' => 'id'),
                        'langID' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'after' => 'formID'),
                        'is_active' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => 1, 'after' => 'langID'),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'is_active'),
                        'text' => array('type' => 'TEXT', 'after' => 'name'),
                        'feedback' => array('type' => 'TEXT', 'after' => 'text'),
                        'mailTemplate' => array('type' => 'TEXT', 'after' => 'feedback'),
                        'subject' => array('type' => 'VARCHAR(255)', 'after' => 'mailTemplate')
                    ),
                    array(
                        'formID' => array('fields' => array('formID', 'langID'), 'type' => 'UNIQUE')
                    )
                );

                /*
                 * Migrate few fields from module_contact_form to module_contact_form_lang
                 * and remaining fields to module_contact_form of new version
                 */
                if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form', 'name')) {
                    $query = "SELECT `id`, `name`, `subject`, `text`, `feedback`, `langId`
                              FROM `" . DBPREFIX . "module_contact_form`";
                    $objResult = \Cx\Lib\UpdateUtil::sql($query);
                    while (!$objResult->EOF) {
                        /*
                         * Check for row already exsist
                         */
                        $rowCountResult = \Cx\Lib\UpdateUtil::sql("SELECT 1
                                                        FROM `" . DBPREFIX . "module_contact_form_lang`
                                                        WHERE `formID` = " . $objResult->fields['id'] . "
                                                        AND `langID` = " . $objResult->fields['langId'] . "
                                                        LIMIT 1");

                        if ($rowCountResult->RecordCount() == 0) {
                            $formLangQuery = "INSERT INTO `" . DBPREFIX . "module_contact_form_lang` (
                                `formID`,
                                `langID`,
                                `is_active`,
                                `name`,
                                `text`,
                                `feedback`,
                                `mailTemplate`,
                                `subject`
                            ) VALUES (
                                " . $objResult->fields['id'] . ",
                                " . $objResult->fields['langId'] . ",
                                1,
                                '" . addslashes($objResult->fields['name']) . "',
                                '" . addslashes($objResult->fields['text']) . "',
                                '" . addslashes($objResult->fields['feedback']) . "',
                                '" . addslashes('<table>
                                     <tbody>
                                     <!-- BEGIN form_field -->
                                        <tr>
                                            <td>
                                                [[FIELD_LABEL]]
                                            </td>
                                            <td>
                                                [[FIELD_VALUE]]
                                            </td>
                                        </tr>
                                     <!-- END form_field -->
                                     </tbody>
                                 </table>') . "',
                                '" . addslashes($objResult->fields['subject']) . "'
                            )";
                            \Cx\Lib\UpdateUtil::sql($formLangQuery);
                        }

                        $objResult->MoveNext();
                    }
                }

                /*
                 * Alter table 'module_contact_form' by dropping name, subject, feedback and form text
                 * Add new column 'html_mail'
                 */
                if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_contact_form', 'html_mail')) {
                    $htmlMailIsNew = false;
                } else {
                    $htmlMailIsNew = true;
                }

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_contact_form',
                    array(
                        'id' => array('type' => 'INT(10)', 'notnull' => true, 'unsigned' => true, 'primary' => true, 'auto_increment' => true),
                        'mails' => array('type' => 'TEXT', 'after' => 'id'),
                        'showForm' => array('type' => 'TINYINT(1)', 'notnull' => true, 'unsigned' => true, 'default' => 0, 'after' => 'mails'),
                        'use_captcha' => array('type' => 'TINYINT(1)', 'notnull' => true, 'unsigned' => true, 'default' => 1, 'after' => 'showForm'),
                        'use_custom_style' => array('type' => 'TINYINT(1)', 'notnull' => true, 'unsigned' => true, 'default' => 0, 'after' => 'use_captcha'),
                        'send_copy' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => 0, 'after' => 'use_custom_style'),
                        'use_email_of_sender' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => 0, 'after' => 'send_copy'),
                        'html_mail' => array('type' => 'TINYINT(1)', 'notnull' => true, 'unsigned' => true, 'default' => 1, 'after' => 'use_email_of_sender'),
                        'send_attachment' => array('type' => 'TINYINT(1)', 'notnull' => true, 'unsigned' => true, 'default' => '0'),
                    )
                );

                if ($htmlMailIsNew) {
                    \Cx\Lib\UpdateUtil::sql('UPDATE ' . DBPREFIX . 'module_contact_form SET html_mail = 0');
                }
            }

            if (   !$objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '2.1.5')
                && $objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
                // for all versions >= 2.2.0 & < 3.1.0
                // change all fields currently set to 'file' to 'multi_file' ('multi_file' is same as former 'file' in previous versions)
                \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_contact_form_field` SET `type` = 'multi_file' WHERE `type` = 'file'");
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'contact_phase_3';
            return 'timeout';
        }

        if (!in_array('contact_phase_4', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            /**
             * Update the content pages
             */
            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
                $em = \Env::get('em');
                $pageRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');
                $Contact = new \Cx\Core_Modules\Contact\Controller\ContactLib();
                $Contact->initContactForms();

                foreach ($Contact->arrForms as $id => $form) {
                    foreach ($form['lang'] as $langId => $lang) {
                        if ($lang['is_active'] == true) {
                            $page = $pageRepo->findOneByModuleCmdLang('contact', $id, $langId);
                            if ($page) {
                                $page->setContent('{APPLICATION_DATA}');
                                $page->setUpdatedAtToNow();
                                $em->persist($page);
                            }
                        }
                    }
                }
                $em->flush();
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'contact_phase_4';
            return 'timeout';
        }

        if (!in_array('contact_phase_5', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
                /*******************************************
                * EXTENSION:    Database structure changes *
                *******************************************/
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_contact_form',
                    array(
                        'id'                     => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'mails'                  => array('type' => 'text', 'after' => 'id'),
                        'showForm'               => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'mails'),
                        'use_captcha'            => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'after' => 'showForm'),
                        'use_custom_style'       => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'use_captcha'),
                        'save_data_in_crm'       => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'use_custom_style'),
                        'send_copy'              => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'save_data_in_crm'),
                        'use_email_of_sender'    => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'send_copy'),
                        'html_mail'              => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'after' => 'use_email_of_sender'),
                        'send_attachment'        => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'html_mail'),
                        'crm_customer_groups' => array('type' => 'text', 'notnull' => false, 'after' => 'send_attachment'),
                        'send_multiple_reply' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'crm_customer_groups')
                    )
                );
                \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `' . DBPREFIX . 'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'7\', \'fieldMetaBrowser\', \'1\', \'1\')');
                \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `' . DBPREFIX . 'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'8\', \'storeFormSubmissions\', \'1\', \'1\')');

                // migrate path to images and media
                $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
                $attributes = array(
                    'text'          => 'module_contact_form_lang',
                    'feedback'      => 'module_contact_form_lang',
                    'mailTemplate'  => 'module_contact_form_lang',
                    'formvalue'     => 'module_contact_form_submit_data',

                );
                foreach ($attributes as $attribute => $table) {
                    foreach ($pathsToMigrate as $oldPath => $newPath) {
                        $success = \Cx\Lib\UpdateUtil::migratePath(
                            '`' . DBPREFIX . $table . '`',
                            '`' . $attribute .  '`',
                            $oldPath,
                            $newPath
                        );
                        if (!$success) {

                        }
                    }
                }
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'contact_phase_5';
            return 'timeout';
        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    return true;
}
function _contactInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_form` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mails` text NOT NULL,
  `showForm` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `use_captcha` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `use_custom_style` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `save_data_in_crm` tinyint(1) NOT NULL DEFAULT \'0\',
  `send_copy` tinyint(1) NOT NULL DEFAULT \'0\',
  `use_email_of_sender` tinyint(1) NOT NULL DEFAULT \'0\',
  `html_mail` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `send_attachment` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `crm_customer_groups` text ,
  `send_multiple_reply` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form` (`id`, `mails`, `showForm`, `use_captcha`, `use_custom_style`, `save_data_in_crm`, `send_copy`, `use_email_of_sender`, `html_mail`, `send_attachment`, `crm_customer_groups`, `send_multiple_reply`) VALUES (\'1\', \'\', \'0\', \'1\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\', NULL, \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form` (`id`, `mails`, `showForm`, `use_captcha`, `use_custom_style`, `save_data_in_crm`, `send_copy`, `use_email_of_sender`, `html_mail`, `send_attachment`, `crm_customer_groups`, `send_multiple_reply`) VALUES (\'2\', \'\', \'0\', \'1\', \'0\', \'1\', \'0\', \'0\', \'1\', \'0\', NULL, \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_form_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_form` int(10) unsigned NOT NULL DEFAULT \'0\',
  `id_lang` int(10) unsigned NOT NULL DEFAULT \'1\',
  `time` int(14) unsigned NOT NULL DEFAULT \'0\',
  `host` varchar(255) NOT NULL DEFAULT \'\',
  `lang` varchar(64) NOT NULL DEFAULT \'\',
  `browser` varchar(255) NOT NULL DEFAULT \'\',
  `ipaddress` varchar(15) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  INDEX `id_form` (`id_form`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_form_field` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_form` int(10) unsigned NOT NULL DEFAULT \'0\',
  `type` enum(\'text\',\'label\',\'checkbox\',\'checkboxGroup\',\'country\',\'date\',\'file\',\'multi_file\',\'fieldset\',\'hidden\',\'horizontalLine\',\'password\',\'radio\',\'select\',\'textarea\',\'recipient\',\'special\',\'datetime\') BINARY NOT NULL DEFAULT \'text\',
  `special_type` varchar(20) NOT NULL,
  `is_required` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `check_type` int(3) NOT NULL DEFAULT \'1\',
  `order_id` smallint(5) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  INDEX `id_form` (`id_form`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'71\', \'1\', \'special\', \'access_firstname\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'72\', \'1\', \'special\', \'access_company\', \'\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'73\', \'1\', \'special\', \'access_email\', \'1\', \'2\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'74\', \'1\', \'textarea\', \'\', \'1\', \'1\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'76\', \'1\', \'special\', \'access_lastname\', \'\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'77\', \'2\', \'special\', \'access_firstname\', \'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'78\', \'2\', \'special\', \'access_lastname\', \'1\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'79\', \'2\', \'special\', \'access_company\', \'\', \'1\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'80\', \'2\', \'special\', \'access_email\', \'1\', \'2\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'81\', \'2\', \'textarea\', \'\', \'\', \'1\', \'11\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'82\', \'2\', \'special\', \'access_title\', \'\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'83\', \'2\', \'special\', \'access_address\', \'\', \'1\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'84\', \'2\', \'special\', \'access_zip\', \'\', \'1\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'85\', \'2\', \'special\', \'access_city\', \'\', \'1\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'86\', \'2\', \'special\', \'access_country\', \'\', \'0\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'87\', \'2\', \'special\', \'access_phone_office\', \'\', \'1\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field` (`id`, `id_form`, `type`, `special_type`, `is_required`, `check_type`, `order_id`) VALUES (\'88\', \'2\', \'special\', \'access_website\', \'\', \'1\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_form_field_lang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldID` int(10) unsigned NOT NULL,
  `langID` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `attributes` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `fieldID` (`fieldID`,`langID`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'40\', \'71\', \'1\', \'Name\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'41\', \'72\', \'1\', \'Firma\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'42\', \'73\', \'1\', \'E-Mail\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'43\', \'74\', \'1\', \'Bemerkung\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'44\', \'71\', \'2\', \'Name\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'45\', \'72\', \'2\', \'Company name\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'46\', \'73\', \'2\', \'E-Mail\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'47\', \'74\', \'2\', \'Message\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'50\', \'76\', \'1\', \'Nachname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'51\', \'76\', \'2\', \'Surname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'52\', \'77\', \'1\', \'Vorname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'53\', \'77\', \'2\', \'Name\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'54\', \'78\', \'1\', \'Nachname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'55\', \'78\', \'2\', \'Surname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'56\', \'79\', \'1\', \'Firma\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'57\', \'79\', \'2\', \'Company name\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'58\', \'80\', \'1\', \'E-Mail\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'59\', \'80\', \'2\', \'E-Mail\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'60\', \'81\', \'1\', \'Bemerkung\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'61\', \'81\', \'2\', \'Message\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'62\', \'82\', \'1\', \'Anrede\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'63\', \'82\', \'2\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'64\', \'83\', \'1\', \'Adresse\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'65\', \'83\', \'2\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'66\', \'84\', \'1\', \'Postleitzahl\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'67\', \'84\', \'2\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'68\', \'85\', \'1\', \'Ort\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'69\', \'85\', \'2\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'70\', \'86\', \'1\', \'Land\', \'Bitte wählen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'71\', \'86\', \'2\', \'\', \'Bitte wählen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'72\', \'87\', \'1\', \'Telefon\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'73\', \'87\', \'2\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'74\', \'88\', \'1\', \'Webseite\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_field_lang` (`id`, `fieldID`, `langID`, `name`, `attributes`) VALUES (\'75\', \'88\', \'2\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_form_lang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `formID` int(10) unsigned NOT NULL,
  `langID` int(10) unsigned NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `feedback` text NOT NULL,
  `mailTemplate` text NOT NULL,
  `subject` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `formID` (`formID`,`langID`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_lang` (`id`, `formID`, `langID`, `is_active`, `name`, `text`, `feedback`, `mailTemplate`, `subject`) VALUES (\'7\', \'1\', \'1\', \'1\', \'Kontaktformular\', \'Sie haben eine Frage zu unserer Unternehmung, unseren Produkten oder Dienstleistungen?<br />\\r\\nFüllen Sie bitte das untenstehende Formular aus und wir beantworten Ihre Anfrage schnell und unverbindlich.\', \'Vielen Dank für Ihre Anfrage. Wir werden uns so schnell wie möglich mit Ihnen in Verbindung setzen.\', \'<table>\\r\\n	<tbody><!-- BEGIN form_field -->\\r\\n		<tr>\\r\\n			<td>{FIELD_LABEL}</td>\\r\\n			<td>{FIELD_VALUE}</td>\\r\\n		</tr>\\r\\n		<!-- END form_field -->\\r\\n	</tbody>\\r\\n</table>\\r\\n\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_lang` (`id`, `formID`, `langID`, `is_active`, `name`, `text`, `feedback`, `mailTemplate`, `subject`) VALUES (\'8\', \'1\', \'2\', \'1\', \'Contact\', \'Sie haben eine Frage zu unserer Unternehmung, unseren Produkten oder Dienstleistungen?<br />\\r\\nFüllen Sie bitte das untenstehende Formular aus und wir beantworten Ihre Anfrage schnell und unverbindlich.\', \'Thank you for your interest. We will contact you as fast as possible.\', \'<table>\\r\\n	<tbody><!-- BEGIN form_field -->\\r\\n		<tr>\\r\\n			<td>{FIELD_LABEL}</td>\\r\\n			<td>{FIELD_VALUE}</td>\\r\\n		</tr>\\r\\n		<!-- END form_field -->\\r\\n	</tbody>\\r\\n</table>\\r\\n\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_lang` (`id`, `formID`, `langID`, `is_active`, `name`, `text`, `feedback`, `mailTemplate`, `subject`) VALUES (\'9\', \'2\', \'1\', \'1\', \'Beispiel-CRM-Formular\', \'Aus Ihrer Anfrage wird automatisch ein neuer Kontakt im CRM angelegt.<br />\\r\\n \', \'Vielen Dank für Ihre Anfrage. Wir werden uns so schnell wie möglich mit Ihnen in Verbindung setzen.\', \'<table>\\r\\n	<tbody><!-- BEGIN form_field -->\\r\\n		<tr>\\r\\n			<td>{FIELD_LABEL}</td>\\r\\n			<td>{FIELD_VALUE}</td>\\r\\n		</tr>\\r\\n		<!-- END form_field -->\\r\\n	</tbody>\\r\\n</table>\\r\\n\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_form_lang` (`id`, `formID`, `langID`, `is_active`, `name`, `text`, `feedback`, `mailTemplate`, `subject`) VALUES (\'10\', \'2\', \'2\', \'1\', \'CRM Contact\', \'<h2>Pflegen Sie Ihre Kundendaten mit dem integrierten CRM-System von Cloudrexx</h2>\\r\\nKundenbeziehungen müssen gepflegt werden, das weiss jeder. Am besten geht das mit einem WebCRM-System, das einfach zu bedienen ist und kein Schulungsaufwand erfordert. <br />\\r\\n<br />\\r\\nCloudrexx enthält neu ab der Version 3.1 ein umfangreiches und auf KMU optimiertes CRM-System, mit dem Sie Ihre Kontakte, Aufträge und Verkaufschancen verwalten und direkt aus Ihrer Website bearbeiten können.\\r\\n<h2>Leistungsumfang</h2>\\r\\n\\r\\n<ul>\\r\\n	<li><strong>100% INTEGRIERT IN IHRE WEBSITE</strong> Alle Verkaufschancen, die auf Ihrer Website entstehen (z. B. wenn der Interessent ein Formular ausfüllt), werden automatisch in das CRM-System integriert und können dort bearbeitet werden.</li>\\r\\n	<li><strong>KUNDENPROFILE</strong> Erfassen Sie die Kontaktdaten Ihrer Kunden und kategorisieren Sie diese nach Unternehmenstyp und Branche. Kontaktpersonen werden Unternehmungen zugewiesen, um den persönlichen Kontakt sicherzustellen. Optional kann dem Profil ein Foto beigefügt werden. Unterscheiden Sie zwischen privaten Kunden und Unternehmungen. Beim Speichern von Privatpersonen kann automatisch ein Userprofil in der Benutzerverwaltung der Website für die angegebene E-Mailadresse generiert werden.</li>\\r\\n	<li><strong>KUNDENGRUPPEN</strong> Teilen Sie Ihre Kunden in verschiedene Kategorien ein und erstellen Sie selber definierte Kundengruppen. Behalten Sie den Überblick über die Grösse Ihrer Zielgruppen und betreiben Sie dadruch spezifischeres Marketing.</li>\\r\\n	<li><strong>AUFGABEN</strong> Erfassen Sie Ihre Projekte direkt im CRM und weisen Sie diese dem Kunden zu. So gehen keine Aufträge vergessen und jeder bereits ausgeführte Auftrag wird in der Historie des entsprechenden Kunden angezeigt. Jedem Auftrag kann eine ausführliche Beschreibung beigefügt werden. Auch ersichtlich ist der Mitarbeiter, welcher den Eintrag erstellt hat.</li>\\r\\n	<li><strong>VERKAUFSCHANCEN</strong> Sichern Sie sich Ihre Aufträge. Offerierte Projekte werden eingetragen und dem Profil des Interessenten zugeteilt. Notieren Sie den Angebotspreis, wie hoch die Verkaufschancen sind und den Offertenverlauf. Verbessern Sie so Ihre Kundenkommunikation grundlegend und verrechnen Sie Ende Monat mehr Aufträge.</li>\\r\\n	<li><strong>SCHNITTSTELLEN WIZARD FÜR IMPORT & EXPORT</strong> Durch CSV Dateien können tausende von Kundendaten aus dem CRM exportiert oder ins CRM importiert werden. Die Schnittstelle ist ein wichtiges Werkzeug, die Kontaktangaben nicht manuell zu erfassen.</li>\\r\\n</ul>\\r\\n\\r\\n<h2>Definition CRM</h2>\\r\\nCustomer-Relationship-Management, kurz CRM genannt, steht für Kundenpflege, beispielsweise gezieltes Dokumentieren und Verwalten der Kundendaten. Mittels einer CRM Software werden die Kundenbeziehungen gepflegt, Projekte notiert und alle Informationen zu den einzelnen Kunden gesammelt, was sich massgeblich auf den Unternehmenserfolg auswirkt.\', \'Thank you for your interest. We will contact you as fast as possible.\', \'<table>\\r\\n	<tbody><!-- BEGIN form_field -->\\r\\n		<tr>\\r\\n			<td>{FIELD_LABEL}</td>\\r\\n			<td>{FIELD_VALUE}</td>\\r\\n		</tr>\\r\\n		<!-- END form_field -->\\r\\n	</tbody>\\r\\n</table>\\r\\n\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_form_submit_data` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_entry` int(10) unsigned NOT NULL,
  `id_field` int(10) unsigned NOT NULL,
  `formlabel` text NOT NULL,
  `formvalue` text NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_entry` (`id_entry`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_recipient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_form` int(11) NOT NULL DEFAULT \'0\',
  `email` varchar(250) NOT NULL DEFAULT \'\',
  `sort` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_recipient_lang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `recipient_id` int(10) unsigned NOT NULL,
  `langID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `recipient_id` (`recipient_id`,`langID`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_contact_settings` (
  `setid` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `setname` varchar(250) NOT NULL DEFAULT \'\',
  `setvalue` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`setid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'1\', \'fileUploadDepositionPath\', \'/images/attach\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'2\', \'spamProtectionWordList\', \'poker,casino,viagra,sex,porn\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'3\', \'fieldMetaDate\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'4\', \'fieldMetaHost\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'5\', \'fieldMetaLang\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'6\', \'fieldMetaIP\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'7\', \'fieldMetaBrowser\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_contact_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'8\', \'storeFormSubmissions\', \'0\', \'1\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
