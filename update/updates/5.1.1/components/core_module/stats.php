<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _statsUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        // remove redundancies
        if (!isset($_SESSION['contrexx_update']['update']['update_stats'])) {
            $_SESSION['contrexx_update']['update']['update_stats'] = array();
        }

        foreach (array(
            'stats_browser' => array(
                'obsoleteIndex'    => 'name',
                'unique' => array('name'),
                'change' => "`name` `name` VARCHAR(255) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_colourdepth' => array(
                'obsoleteIndex'    => 'depth',
                'unique' => array('depth')
            ),
            'stats_country' => array(
                'obsoleteIndex'    => 'country',
                'unique' => array('country'),
                'change' => "`country` `country` VARCHAR(100) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_hostname' => array(
                'obsoleteIndex'    => 'hostname',
                'unique' => array('hostname'),
                'change' => "`hostname` `hostname` VARCHAR(255) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_operatingsystem' => array(
                'obsoleteIndex'    => 'name',
                'unique' => array('name'),
                'change' => "`name` `name` VARCHAR(255) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_referer' => array(
                'obsoleteIndex'    => 'uri',
                'unique' => array('uri'),
                'change' => "`uri` `uri` VARCHAR(255) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_requests' => array(
                'obsoleteIndex'    => array('page', 'unique'),
                'unique' => array('pageId'),
                'unique_key' => 'pageId', 
                'count' => 'visits',
                'change' => "`page` `page` VARCHAR(255) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_requests_summary' => array(
                'obsoleteIndex'    => 'type',
                'unique' => array('type', 'timestamp')
            ),
            'stats_screenresolution' => array(
                'obsoleteIndex'    => 'resolution',
                'unique' => array('resolution')
            ),
            'stats_search' => array(
                'change' => "`name` `name` VARCHAR(100) BINARY NOT NULL DEFAULT ''",
                'unique' => array('name')
            ),
            'stats_spiders' => array(
                'obsoleteIndex'    => 'page',
                'unique' => array('page'),
                'change' => "`page` `page` VARCHAR(100) BINARY DEFAULT NULL"
            ),
            'stats_spiders_summary'    => array(
                'obsoleteIndex'    => 'unqiue',
                'unique' => array('name'),
                'change' => "`name` `name` VARCHAR(255) BINARY NOT NULL DEFAULT ''"
            ),
            'stats_visitors_summary' => array(
                'obsoleteIndex'    => 'type',
                'unique' => array('type', 'timestamp')
            ),
            /************************************************
            * EXTENSION:    Unique key on sid attribte of   *
            *               table contrexx_statis_visitors  *
            * ADDED:        Contrexx v2.1.0                    *
            ************************************************/
            'stats_visitors' => array(
                'obsoleteIndex'    => 'sid',
                'unique' => array('sid'),
                'count'  => 'timestamp'
            )

        ) as $table => $arrUnique) {
            do {
                if (in_array($table, ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['update_stats']))) {
                    break;
                } elseif (!checkTimeoutLimit()) {
                    return 'timeout';
                }

                if (isset($arrUnique['change'])) {
                    $query = 'ALTER TABLE `'.DBPREFIX.$table.'` CHANGE '.$arrUnique['change'];
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }

                try {
                    $structure = \Cx\Lib\UpdateUtil::sql('SHOW INDEX FROM `'.DBPREFIX.$table.'`');
                } catch (\Cx\Lib\UpdateException $e) {
                    return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                }
                if ($structure === false) {
                    setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.$table));
                    return false;
                }

                $keys = array();
                while (!$structure->EOF) {
                    if (empty($structure->fields['Key_name'])) {
                        $structure->MoveNext();
                        continue;
                    }

                    if ((
                        isset($arrUnique['unique_key']) &&
                        $structure->fields['Key_name'] == $arrUnique['unique_key']
                    ) || (
                        !isset($arrUnique['unique_key']) &&
                        $structure->fields['Key_name'] == 'unique'
                    )) {
                        $_SESSION['contrexx_update']['update']['update_stats'][] = $table;
                        break 2;
                    }

                    $keys[] = $structure->fields['Key_name'];
                    $structure->MoveNext();
                }

                if (isset($arrUnique['obsoleteIndex'])) {
                    if (is_array($arrUnique['obsoleteIndex'])) {
                        foreach ($arrUnique['obsoleteIndex'] as $obsoleteIndex) {
                            if (in_array($obsoleteIndex, $keys)) {
                                $query = 'ALTER TABLE `'.DBPREFIX.$table.'` DROP INDEX `'.$obsoleteIndex.'`';
                                if ($objDatabase->Execute($query) === false) {
                                    return _databaseError($query, $objDatabase->ErrorMsg());
                                }
                            }
                        }
                    } elseif (in_array($arrUnique['obsoleteIndex'], $keys)) {
                        $query = 'ALTER TABLE `'.DBPREFIX.$table.'` DROP INDEX `'.$arrUnique['obsoleteIndex'].'`';
                        if ($objDatabase->Execute($query) === false) {
                            return _databaseError($query, $objDatabase->ErrorMsg());
                        }
                    }
                }

                #DBG::msg("table = $table");
                #DBG::dump($arrUnique);
                if (isset($arrUnique['unique'])) {
                    $query = 'SELECT `'.implode('`,`', $arrUnique['unique']).'`, COUNT(`id`) AS redundancy FROM `'.DBPREFIX.$table.'` GROUP BY `'.implode('`,`', $arrUnique['unique']).'` ORDER BY redundancy DESC';
                    $objEntry = $objDatabase->SelectLimit($query, 10);
                    if ($objEntry !== false) {
                        while (!$objEntry->EOF) {
                            if (!checkTimeoutLimit()) {
                                return 'timeout';
                            }
                            $lastRedundancyCount = $objEntry->fields['redundancy'];
                            if ($objEntry->fields['redundancy'] > 1) {
                                $where = array();
                                foreach ($arrUnique['unique'] as $unique) {
                                    $where[] = "`".$unique."` = '".addslashes($objEntry->fields[$unique])."'";
                                }
                                $query = 'DELETE FROM `'.DBPREFIX.$table.'` WHERE '.implode(' AND ', $where).' ORDER BY `'.(isset($arrUnique['count']) ? $arrUnique['count'] : 'count').'` LIMIT '.($objEntry->fields['redundancy']-1);
                                if ($objDatabase->Execute($query) === false) {
                                    return _databaseError($query, $objDatabase->ErrorMsg());
                                }
                            } else {
                                break;
                            }
                            $objEntry->MoveNext();
                        }
                    } else {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }

                    if ($objEntry->RecordCount() == 0 || $lastRedundancyCount < 2) {
                        $uniqueKey = 'unique';
                        if (isset($arrUnique['unique_key'])) {
                            $uniqueKey = $arrUnique['unique_key'];
                        }
                        $query = 'ALTER TABLE `'.DBPREFIX.$table.'` ADD UNIQUE `' . $uniqueKey . '` (`'.implode('`,`', $arrUnique['unique']).'`)';
                        if ($objDatabase->Execute($query) == false) {
                            return _databaseError($query, $objDatabase->ErrorMsg());
                        }
                        $_SESSION['contrexx_update']['update']['update_stats'][] = $table;
                        break;
                    }
                }
            } while ($objEntry->RecordCount() > 1);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            $structure = \Cx\Lib\UpdateUtil::sql('SHOW INDEX FROM `'.DBPREFIX.'stats_search`');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        if ($structure === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'stats_search'));
            return false;
        }

        while (!$structure->EOF) {
            if (empty($structure->fields['Key_name'])) {
                $structure->MoveNext();
                continue;
            }

            if ($structure->fields['Key_name'] == 'unique') {
                $query = 'ALTER TABLE `'.DBPREFIX.'stats_search` DROP INDEX `unique`';
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
                break;
            }

            $structure->MoveNext();
        }

        if(empty($_SESSION['contrexx_update']['update']['update_stats']['utf8'])){
            $query = "ALTER TABLE `".DBPREFIX."stats_search` CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL";
            if($objDatabase->Execute($query)){
                $_SESSION['contrexx_update']['update']['update_stats']['utf8'] = 1;
                $query = "ALTER TABLE `".DBPREFIX."stats_search` CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET binary NOT NULL";
                if($_SESSION['contrexx_update']['update']['update_stats']['utf8'] == 1 && $objDatabase->Execute($query)){
                    $_SESSION['contrexx_update']['update']['update_stats']['utf8'] = 2;
                    $query = "ALTER TABLE `".DBPREFIX."stats_search` CHANGE `name` `name` VARCHAR( 100 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL";
                    if($_SESSION['contrexx_update']['update']['update_stats']['utf8'] == 2 && $objDatabase->Execute($query)){
                        $_SESSION['contrexx_update']['update']['update_stats']['utf8'] = 3;
                        try{
                            \Cx\Lib\UpdateUtil::sql('CREATE TABLE `' . DBPREFIX . 'stats_search_unique` LIKE `' . DBPREFIX . 'stats_search`');
                            \Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'stats_search_unique` ADD UNIQUE `unique` (`name`, `external`)');
                            \Cx\Lib\UpdateUtil::sql('INSERT IGNORE `' . DBPREFIX . 'stats_search_unique` SELECT * FROM `' . DBPREFIX . 'stats_search`');
                            \Cx\Lib\UpdateUtil::sql('DROP TABLE `' . DBPREFIX . 'stats_search`');
                            \Cx\Lib\UpdateUtil::sql('RENAME TABLE `' . DBPREFIX . 'stats_search_unique` TO `' . DBPREFIX . 'stats_search`');
                        }
                        catch (\Cx\Lib\UpdateException $e) {
                            // we COULD do something else here..
                            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
                    }else{
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }else{
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }else{
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'stats_search',
                array(
                    'id'         => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(100)', 'binary' => true, 'default' => ''),
                    'count'      => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'sid'        => array('type' => 'VARCHAR(32)', 'notnull' => true, 'default' => ''),
                    'external'   => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0')
                ),
                array(
                    'unique'     => array('fields' => array('name','external'), 'type' => 'UNIQUE')
                )
            );
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        try {
            //2.2.0: new config option 'exclude_identifying_info'
            \Cx\Lib\UpdateUtil::sql('
                INSERT INTO '.DBPREFIX.'stats_config (id, name, value, status)
                VALUES (20, "exclude_identifying_info", 1, 0)
                ON DUPLICATE KEY UPDATE `id` = `id`
            ');

            \Cx\Lib\UpdateUtil::sql('UPDATE `' . DBPREFIX. 'stats_config` SET `value` = 0 WHERE `name` LIKE \'count_referer\'');
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'stats_requests',
            array(
                  'id'             => array('type' => 'INT(9)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                  'timestamp'      => array('type' => 'INT(11)', 'default' => '0', 'notnull' => false, 'after' => 'id'),
                  'pageId'         => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'timestamp'),
                  'page'           => array('type' => 'VARCHAR(255)', 'after' => 'pageId', 'default' => '', 'binary' => true),
                  'visits'         => array('type' => 'INT(9)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'page'),
                  'sid'            => array('type' => 'VARCHAR(32)', 'after' => 'visits', 'default' => ''),
                  'pageTitle'      => array('type' => 'VARCHAR(250)', 'after' => 'sid') //this field is added
                  ),
            array(
                  'pageId'         => array('fields' => array('pageId'), 'type' => 'UNIQUE')
                  )
        );
    }


    return true;
}
function _statsInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_browser` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) BINARY NOT NULL DEFAULT \'\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_colourdepth` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `depth` tinyint(3) unsigned NOT NULL DEFAULT \'0\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`depth`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL DEFAULT \'\',
  `value` varchar(255) NOT NULL DEFAULT \'\',
  `status` int(1) DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'1\', \'reload_block_time\', \'1800\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'2\', \'online_timeout\', \'3000\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'3\', \'paging_limit\', \'100\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'4\', \'count_browser\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'5\', \'count_operating_system\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'6\', \'make_statistics\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'7\', \'count_spiders\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'9\', \'count_requests\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'10\', \'remove_requests\', \'86400\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'11\', \'count_search_terms\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'12\', \'count_screen_resolution\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'13\', \'count_colour_depth\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'14\', \'count_javascript\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'15\', \'count_referer\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'16\', \'count_hostname\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'17\', \'count_country\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'18\', \'paging_limit_visitor_details\', \'100\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'19\', \'count_visitor_number\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'stats_config` (`id`, `name`, `value`, `status`) VALUES (\'20\', \'exclude_identifying_info\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country` varchar(100) BINARY NOT NULL DEFAULT \'\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`country`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_hostname` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) BINARY NOT NULL DEFAULT \'\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`hostname`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_javascript` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `support` enum(\'0\',\'1\') DEFAULT \'0\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_operatingsystem` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) BINARY NOT NULL DEFAULT \'\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_referer` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) BINARY NOT NULL DEFAULT \'\',
  `timestamp` int(11) unsigned NOT NULL DEFAULT \'0\',
  `count` mediumint(8) unsigned NOT NULL DEFAULT \'0\',
  `sid` varchar(32) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`uri`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_requests` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) DEFAULT \'0\',
  `pageId` int(6) unsigned NOT NULL DEFAULT \'0\',
  `page` varchar(255) BINARY NOT NULL DEFAULT \'\',
  `visits` int(9) unsigned NOT NULL DEFAULT \'0\',
  `sid` varchar(32) NOT NULL DEFAULT \'\',
  `pageTitle` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pageId` (`pageId`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_requests_summary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT \'\',
  `timestamp` int(11) NOT NULL DEFAULT \'0\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`type`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_screenresolution` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `resolution` varchar(11) NOT NULL DEFAULT \'\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`resolution`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_search` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) BINARY NOT NULL DEFAULT \'\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  `sid` varchar(32) NOT NULL DEFAULT \'\',
  `external` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`name`,`external`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_spiders` (
  `id` int(9) unsigned NOT NULL AUTO_INCREMENT,
  `last_indexed` int(14) DEFAULT NULL,
  `page` varchar(100) BINARY DEFAULT NULL,
  `pageId` mediumint(6) unsigned NOT NULL DEFAULT \'0\',
  `count` int(11) NOT NULL DEFAULT \'0\',
  `spider_useragent` varchar(255) DEFAULT NULL,
  `spider_ip` varchar(100) DEFAULT NULL,
  `spider_host` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`page`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_spiders_summary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) BINARY NOT NULL DEFAULT \'\',
  `timestamp` int(11) NOT NULL DEFAULT \'0\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_visitors` (
  `id` int(8) unsigned NOT NULL AUTO_INCREMENT,
  `sid` varchar(32) NOT NULL DEFAULT \'\',
  `timestamp` int(11) NOT NULL DEFAULT \'0\',
  `client_ip` varchar(100) DEFAULT NULL,
  `client_host` varchar(255) DEFAULT NULL,
  `client_useragent` varchar(255) DEFAULT NULL,
  `proxy_ip` varchar(100) DEFAULT NULL,
  `proxy_host` varchar(255) DEFAULT NULL,
  `proxy_useragent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`sid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'stats_visitors_summary` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(10) NOT NULL DEFAULT \'\',
  `timestamp` int(11) NOT NULL DEFAULT \'0\',
  `count` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`type`,`timestamp`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
