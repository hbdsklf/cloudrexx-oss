<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _accessUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG, $_CORELANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        $arrTables = $objDatabase->MetaTables('TABLES');
        if (!$arrTables) {
            setUpdateMsg($_ARRAYLANG['TXT_UNABLE_DETERMINE_DATABASE_STRUCTURE']);
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        /****************************
         *
         * ADD NOTIFICATION E-MAILS
         *
         ***************************/
        try{
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'access_user_mail',
                array(
                    'type'           => array('type' => 'ENUM(\'reg_confirm\',\'reset_pw\',\'user_activated\',\'user_deactivated\',\'new_user\',\'user_account_invitation\',\'signup_notification\',\'user_profile_modification\')', 'notnull' => true, 'default' => 'reg_confirm'),
                    'lang_id'        => array('type' => 'TINYINT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'type'),
                    'sender_mail'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'lang_id'),
                    'sender_name'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'sender_mail'),
                    'subject'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'sender_name'),
                    'format'         => array('type' => 'ENUM(\'text\',\'html\',\'multipart\')', 'notnull' => true, 'default' => 'text', 'after' => 'subject'),
                    'body_text'      => array('type' => 'TEXT', 'after' => 'format'),
                    'body_html'      => array('type' => 'TEXT', 'after' => 'body_text')
                ),
                array(
                    'mail'           => array('fields' => array('type','lang_id'), 'type' => 'UNIQUE')
                ),
                'InnoDB'
            );
            $result = \Cx\Lib\UpdateUtil::sql('SHOW KEYS FROM `' . DBPREFIX . 'access_group_dynamic_ids`');
            if ($result->EOF) {
                // ALTER IGNORE TABLE has been removed in MySQL 5.7.4
                //\Cx\Lib\UpdateUtil::sql('ALTER IGNORE TABLE `' . DBPREFIX . 'access_group_dynamic_ids` ADD PRIMARY KEY ( `access_id` , `group_id` )');
                \Cx\Lib\UpdateUtil::sql('CREATE TABLE `' . DBPREFIX . 'access_group_dynamic_ids_unique` LIKE `' . DBPREFIX . 'access_group_dynamic_ids`');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'access_group_dynamic_ids_unique` ADD PRIMARY KEY ( `access_id` , `group_id` )');
                \Cx\Lib\UpdateUtil::sql('INSERT IGNORE `' . DBPREFIX . 'access_group_dynamic_ids_unique` SELECT * FROM `' . DBPREFIX . 'access_group_dynamic_ids`');
                \Cx\Lib\UpdateUtil::sql('DROP TABLE `' . DBPREFIX . 'access_group_dynamic_ids`');
                \Cx\Lib\UpdateUtil::sql('RENAME TABLE `' . DBPREFIX . 'access_group_dynamic_ids_unique` TO `' . DBPREFIX . 'access_group_dynamic_ids`');
            }
            $result = \Cx\Lib\UpdateUtil::sql('SHOW KEYS FROM `' . DBPREFIX . 'access_group_static_ids`');
            if ($result->EOF) {
                // ALTER IGNORE TABLE has been removed in MySQL 5.7.4
                //\Cx\Lib\UpdateUtil::sql('ALTER IGNORE TABLE `' . DBPREFIX . 'access_group_static_ids` ADD PRIMARY KEY ( `access_id` , `group_id` )');
                //\Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'access_group_static_ids` ADD PRIMARY KEY ( `access_id` , `group_id` )');
                \Cx\Lib\UpdateUtil::sql('CREATE TABLE `' . DBPREFIX . 'access_group_static_ids_unique` LIKE `' . DBPREFIX . 'access_group_static_ids`');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'access_group_static_ids_unique` ADD PRIMARY KEY ( `access_id` , `group_id` )');
                \Cx\Lib\UpdateUtil::sql('INSERT IGNORE `' . DBPREFIX . 'access_group_static_ids_unique` SELECT * FROM `' . DBPREFIX . 'access_group_static_ids`');
                \Cx\Lib\UpdateUtil::sql('DROP TABLE `' . DBPREFIX . 'access_group_static_ids`');
                \Cx\Lib\UpdateUtil::sql('RENAME TABLE `' . DBPREFIX . 'access_group_static_ids_unique` TO `' . DBPREFIX . 'access_group_static_ids`');
            }
        }
        catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        \DBG::msg('001');
        $arrMails = array(
            array(
                'type' => 'reg_confirm',
                'subject' => 'Benutzerregistrierung bestätigen',
                'body_text' => 'Hallo [[USERNAME]],\r\n\r\nVielen Dank für Ihre Anmeldung bei [[HOST]].\r\nBitte klicken Sie auf den folgenden Link, um Ihre E-Mail-Adresse zu bestätigen:\r\n[[ACTIVATION_LINK]]\r\n\r\nUm sich später einzuloggen, geben Sie bitte Ihren Benutzernamen \"[[USERNAME]]\" und das Passwort ein, das Sie bei der Registrierung festgelegt haben.\r\n\r\n\r\n--\r\nIhr [[SENDER]]'
            ),
            array(
                'type' => 'reset_pw',
                'subject' => 'Kennwort zurücksetzen',
                'body_text' => 'Hallo [[USERNAME]],\r\n\r\nUm ein neues Passwort zu wählen, müssen Sie auf die unten aufgeführte URL gehen und dort Ihr neues Passwort eingeben.\r\n\r\nWICHTIG: Die Gültigkeit der URL wird nach 60 Minuten verfallen, nachdem diese E-Mail abgeschickt wurde.\r\nFalls Sie mehr Zeit benötigen, geben Sie Ihre E-Mail Adresse einfach ein weiteres Mal ein.\r\n\r\nIhre URL:\r\n[[URL]]\r\n\r\n\r\n--\r\n[[SENDER]]'
            ),
            array(
                'type' => 'user_activated',
                'subject' => 'Ihr Benutzerkonto wurde aktiviert',
                'body_text' => 'Hallo [[USERNAME]],\r\n\r\nIhr Benutzerkonto auf [[HOST]] wurde soeben aktiviert und kann von nun an verwendet werden.\r\n\r\n\r\n--\r\n[[SENDER]]'
            ),
            array(
                'type' => 'user_deactivated',
                'subject' => 'Ihr Benutzerkonto wurde deaktiviert',
                'body_text' => 'Hallo [[USERNAME]],\r\n\r\nIhr Benutzerkonto auf [[HOST]] wurde soeben deaktiviert.\r\n\r\n\r\n--\r\n[[SENDER]]'
            ),
            array(
                'type' => 'new_user',
                'subject' => 'Ein neuer Benutzer hat sich registriert',
                'body_text' => 'Der Benutzer [[USERNAME]] hat sich soeben registriert und muss nun frei geschaltet werden.\r\n\r\nÜber die folgende Adresse kann das Benutzerkonto von [[USERNAME]] verwaltet werden:\r\n[[LINK]]\r\n\r\n\r\n--\r\n[[SENDER]]'
            )
        );

        foreach ($arrMails as $arrMail) {
            $query = "SELECT 1 FROM `" . DBPREFIX . "access_user_mail` WHERE `type` = '" . $arrMail['type'] . "'";
            $objMail = $objDatabase->SelectLimit($query, 1);
            if ($objMail !== false) {
                if ($objMail->RecordCount() == 0) {
                    $query = "INSERT INTO `" . DBPREFIX . "access_user_mail` (
                        `type`,
                        `lang_id`,
                        `sender_mail`,
                        `sender_name`,
                        `subject`,
                        `body_text`,
                        `body_html`
                    ) VALUES (
                        '" . $arrMail['type'] . "',
                        0,
                        '" . addslashes($_CONFIG['coreAdminEmail']) . "',
                        '" . addslashes($_CONFIG['coreAdminName']) . "',
                        '" . $arrMail['subject'] . "',
                        '" . $arrMail['body_text'] . "',
                        ''
                    )";
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        /****************
         *
         * ADD SETTINGS
         *
         ***************/
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_settings',
                array(
                    'key' => array('type' => 'VARCHAR(32)', 'notnull' => true, 'default' => ''),
                    'value' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'key'),
                    'status' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'value')
                ),
                array(
                    'key' => array('fields' => array('key'), 'type' => 'UNIQUE')
                ),
                'InnoDB'
            );

            if (in_array(DBPREFIX . "communit_config", $arrTables)) {
                $objResult = \Cx\Lib\UpdateUtil::sql('SELECT `name`, `value`, `status` FROM `' . DBPREFIX . 'community_config`');
                while (!$objResult->EOF) {
                    $arrCommunityConfig[$objResult->fields['name']] = array(
                        'value' => $objResult->fields['value'],
                        'status' => $objResult->fields['status']
                    );
                    $objResult->MoveNext();
                }
            }

            $arrSettings = array(
                'user_activation' => array('value' => '', 'status' => isset($arrCommunityConfig['user_activation']['status']) ? $arrCommunityConfig['user_activation']['status'] : 0),
                'user_activation_timeout' => array('value' => isset($arrCommunityConfig['user_activation_timeout']['value']) ? $arrCommunityConfig['user_activation_timeout']['value'] : 0, 'status' => isset($arrCommunityConfig['user_activation_timeout']['status']) ? $arrCommunityConfig['user_activation_timeout']['status'] : 0),
                'assigne_to_groups' => array('value' => isset($arrCommunityConfig['community_groups']['value']) ? $arrCommunityConfig['community_groups']['value'] : '', 'status' => 1),
                'max_profile_pic_width' => array('value' => '160', 'status' => 1),
                'max_profile_pic_height' => array('value' => '160', 'status' => 1),
                'profile_thumbnail_pic_width' => array('value' => '50', 'status' => 1),
                'profile_thumbnail_pic_height' => array('value' => '50', 'status' => 1),
                'max_profile_pic_size' => array('value' => '30000', 'status' => 1),
                'max_pic_width' => array('value' => '600', 'status' => 1),
                'max_pic_height' => array('value' => '600', 'status' => 1),
                'max_thumbnail_pic_width' => array('value' => '130', 'status' => 1),
                'max_thumbnail_pic_height' => array('value' => '130', 'status' => 1),
                'max_pic_size' => array('value' => '200000', 'status' => 1),
                'notification_address' => array('value' => addslashes($_CONFIG['coreAdminEmail']), 'status' => 1),
                'user_config_email_access' => array('value' => '', 'status' => 1),
                'user_config_profile_access' => array('value' => '', 'status' => 1),
                'default_email_access' => array('value' => 'members_only', 'status' => 1),
                'default_profile_access' => array('value' => 'members_only', 'status' => 1),
                'user_delete_account' => array('value' => '', 'status' => 1),
                'block_currently_online_users' => array('value' => '10', 'status' => 0),
                'block_currently_online_users_pic' => array('value' => '', 'status' => 0),
                'block_last_active_users' => array('value' => '10', 'status' => 0),
                'block_last_active_users_pic' => array('value' => '', 'status' => 0),
                'block_latest_reg_users' => array('value' => '10', 'status' => 0),
                'block_latest_reg_users_pic' => array('value' => '', 'status' => 0),
                'block_birthday_users' => array('value' => '10', 'status' => 0),
                'block_birthday_users_pic' => array('value' => '', 'status' => 0),
                'session_user_interval' => array('value' => '0', 'status' => 1),
                'user_accept_tos_on_signup' => array('value' => '', 'status' => 0),
                'user_captcha' => array('value' => '', 'status' => 0),
                'profile_thumbnail_method' => array('value' => 'crop', 'status' => 1),
                'profile_thumbnail_scale_color' => array('value' => '#FFFFFF', 'status' => 1),
            );

            foreach ($arrSettings as $key => $arrSetting) {
                if (!\Cx\Lib\UpdateUtil::sql("SELECT 1 FROM `" . DBPREFIX . "access_settings` WHERE `key` = '" . $key . "'")->RecordCount()) {
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `" . DBPREFIX . "access_settings`
                        SET `key`       = '" . $key . "',
                            `value`     = '" . $arrSetting['value'] . "',
                            `status`    = '" . $arrSetting['status'] . "'
                    ");
                }
            }

            // delete obsolete table community_config
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX . 'community_config');

            // delete obsolete table user_validity
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX . 'user_validity');
        } catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    /********************
     *
     * ADD USER PROFILE
     *
     *******************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_profile',
                array(
                    'user_id' => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'gender' => array('type' => 'ENUM(\'gender_undefined\',\'gender_female\',\'gender_male\')', 'notnull' => true, 'default' => 'gender_undefined', 'after' => 'user_id'),
                    'title' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'gender'),
                    'designation' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'title'),
                    'firstname' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'designation'),
                    'lastname' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'firstname'),
                    'company' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'lastname'),
                    'address' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'company'),
                    'city' => array('type' => 'VARCHAR(50)', 'notnull' => true, 'default' => '', 'after' => 'address'),
                    'zip' => array('type' => 'VARCHAR(10)', 'notnull' => true, 'default' => '', 'after' => 'city'),
                    'country' => array('type' => 'SMALLINT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'zip'),
                    'phone_office' => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'country'),
                    'phone_private' => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'phone_office'),
                    'phone_mobile' => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'phone_private'),
                    'phone_fax' => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'phone_mobile'),
                    'birthday' => array('type' => 'VARCHAR(11)', 'notnull' => false, 'after' => 'phone_fax'),
                    'website' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'birthday'),
                    'profession' => array('type' => 'VARCHAR(150)', 'notnull' => true, 'default' => '', 'after' => 'website'),
                    'interests' => array('type' => 'text', 'after' => 'profession', 'notnull' => false),
                    'signature' => array('type' => 'text', 'after' => 'interests', 'notnull' => false),
                    'picture' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'signature')
                ),
                array(
                    'profile' => array('fields' => array('firstname' => 100, 'lastname' => 100, 'company' => 50))
                ),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        /***************************
         *
         * MIGRATE GROUP RELATIONS
         *
         **************************/
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_rel_user_group',
                array(
                    'user_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'group_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'user_id')
                ),
                array(),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        $arrColumns = $objDatabase->MetaColumnNames(DBPREFIX . 'access_users');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX . 'access_users'));
            return false;
        }

        if (in_array('groups', $arrColumns)) {
            $query = "SELECT `id`, `groups` FROM " . DBPREFIX . "access_users WHERE `groups` != ''";
            $objUser = $objDatabase->Execute($query);
            if ($objUser) {
                while (!$objUser->EOF) {
                    $arrGroups = explode(',', $objUser->fields['groups']);
                    foreach ($arrGroups as $groupId) {
                        $query = "SELECT 1 FROM " . DBPREFIX . "access_rel_user_group WHERE `user_id` = " . $objUser->fields['id'] . " AND `group_id` = " . intval($groupId);
                        $objRel = $objDatabase->SelectLimit($query, 1);
                        if ($objRel) {
                            if ($objRel->RecordCount() == 0) {
                                $query = "INSERT INTO " . DBPREFIX . "access_rel_user_group (`user_id`, `group_id`) VALUES (" . $objUser->fields['id'] . ", " . intval($groupId) . ")";
                                if ($objDatabase->Execute($query) === false) {
                                    return _databaseError($query, $objDatabase->ErrorMsg());
                                }
                            }
                        } else {
                            return _databaseError($query, $objDatabase->ErrorMsg());
                        }
                    }

                    $objUser->MoveNext();
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }

            $query = "ALTER TABLE `" . DBPREFIX . "access_users` DROP `groups`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        \DBG::msg('002');

        /*********************
         *
         * ADD USER VALIDITY
         *
         ********************/
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_validity',
                array(
                    'validity' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true)
                ),
                array(),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        $query = "SELECT 1 FROM `" . DBPREFIX . "access_user_validity`";
        $objResult = $objDatabase->SelectLimit($query, 1);
        if ($objResult) {
            if ($objResult->RecordCount() == 0) {
                $query = "
                    INSERT INTO `" . DBPREFIX . "access_user_validity` (`validity`) VALUES
                        ('0'), ('1'), ('15'), ('31'), ('62'),
                        ('92'), ('123'), ('184'), ('366'), ('731')
                    ";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        /********************
         *
         * MIGRATE PROFILES
         *
         *******************/
        if (in_array('firstname', $arrColumns)) {
            $query = "SELECT `id`, `firstname`, `lastname`, `residence`, `profession`, `interests`, `webpage`, `company`, `zip`, `phone`, `mobile`, `street` FROM `" . DBPREFIX . "access_users`";
            $objUser = $objDatabase->Execute($query);
            if ($objUser) {
                while (!$objUser->EOF) {
                    $query = "SELECT 1 FROM `" . DBPREFIX . "access_user_profile` WHERE `user_id` = " . $objUser->fields['id'];
                    $objProfile = $objDatabase->SelectLimit($query, 1);
                    if ($objProfile) {
                        if ($objProfile->RecordCount() == 0) {
                            $query = "INSERT INTO `" . DBPREFIX . "access_user_profile` (
                                `user_id`,
                                `gender`,
                                `firstname`,
                                `lastname`,
                                `company`,
                                `address`,
                                `city`,
                                `zip`,
                                `country`,
                                `phone_office`,
                                `phone_private`,
                                `phone_mobile`,
                                `phone_fax`,
                                `website`,
                                `profession`,
                                `interests`,
                                `picture`
                            ) VALUES (
                                " . $objUser->fields['id'] . ",
                                'gender_undefined',
                                '" . addslashes($objUser->fields['firstname']) . "',
                                '" . addslashes($objUser->fields['lastname']) . "',
                                '" . addslashes($objUser->fields['company']) . "',
                                '" . addslashes($objUser->fields['street']) . "',
                                '" . addslashes($objUser->fields['residence']) . "',
                                '" . addslashes($objUser->fields['zip']) . "',
                                0,
                                '',
                                '" . addslashes($objUser->fields['phone']) . "',
                                '" . addslashes($objUser->fields['mobile']) . "',
                                '',
                                '" . addslashes($objUser->fields['webpage']) . "',
                                '" . addslashes($objUser->fields['profession']) . "',
                                '" . addslashes($objUser->fields['interests']) . "',
                                ''
                            )";
                            if ($objDatabase->Execute($query) === false) {
                                return _databaseError($query, $objDatabase->ErrorMsg());
                            }
                        }
                    } else {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }

                    $objUser->MoveNext();
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        $arrRemoveColumns = array(
            'firstname',
            'lastname',
            'residence',
            'profession',
            'interests',
            'webpage',
            'company',
            'zip',
            'phone',
            'mobile',
            'street',
            'levelid'
        );

        foreach ($arrRemoveColumns as $column) {
            if (in_array($column, $arrColumns)) {
                $query = "ALTER TABLE " . DBPREFIX . "access_users DROP `" . $column . "`";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }

        $arrColumnDetails = $objDatabase->MetaColumns(DBPREFIX . 'access_users');
        if ($arrColumnDetails === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX . 'access_users'));
            return false;
        }

        if (in_array('regdate', $arrColumns)) {
            if ($arrColumnDetails['REGDATE']->type == 'date') {
                if (!in_array('regdate_new', $arrColumns)) {
                    $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `regdate_new` INT( 14 ) UNSIGNED NULL DEFAULT '0' AFTER `regdate`";
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }

                $query = "UPDATE `" . DBPREFIX . "access_users` SET `regdate_new` = UNIX_TIMESTAMP(`regdate`), `regdate` = '0000-00-00' WHERE `regdate` != '0000-00-00'";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }

                $query = "ALTER TABLE `" . DBPREFIX . "access_users` DROP `regdate`";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }

        $arrColumns = $objDatabase->MetaColumnNames(DBPREFIX . 'access_users');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX . 'access_users'));
            return false;
        }

        if (in_array('regdate_new', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` CHANGE `regdate_new` `regdate` INT( 14 ) UNSIGNED NOT NULL DEFAULT '0'";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        $query = "ALTER TABLE `" . DBPREFIX . "access_users` CHANGE `is_admin` `is_admin` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '0'";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        if (!in_array('email_access', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `email_access` ENUM( 'everyone', 'members_only', 'nobody' ) NOT NULL DEFAULT 'nobody' AFTER `email`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!in_array('profile_access', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `profile_access` ENUM( 'everyone', 'members_only', 'nobody' ) NOT NULL DEFAULT 'members_only' AFTER `active`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!in_array('frontend_lang_id', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` CHANGE `langId` `frontend_lang_id` INT( 2 ) UNSIGNED NOT NULL DEFAULT '0'";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!in_array('backend_lang_id', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `backend_lang_id` INT( 2 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `frontend_lang_id`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            } else {
                $query = "UPDATE `" . DBPREFIX . "access_users` SET `backend_lang_id` = `frontend_lang_id`";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }

        if (!in_array('last_auth', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `last_auth` INT( 14 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `regdate`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!in_array('last_activity', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `last_activity` INT( 14 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `last_auth`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!in_array('expiration', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `expiration` INT( 14 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `regdate`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!in_array('validity', $arrColumns)) {
            $query = "ALTER TABLE `" . DBPREFIX . "access_users` ADD `validity` INT UNSIGNED NOT NULL DEFAULT '0' AFTER `expiration`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        } else {
            try {
                \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "access_users` SET `expiration` = `validity`*60*60*24+`regdate` WHERE `expiration` = 0 AND `validity` > 0");
            } catch (\Cx\Lib\UpdateException $e) {
                // we COULD do something else here..
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        \DBG::msg('003');

        /***********************************
         *
         * MIGRATE COMMUNITY CONTENT PAGES
         *
         **********************************/
        // only execute this part for versions < 2.0.0
        $pattern = array(
            '/section=community&(amp;)?cmd=profile/',
            '/section=community&(amp;)?cmd=register/',
            '/section=community/',
        );
        $replacement = array(
            'section=access&$1cmd=settings',
            'section=access&$1cmd=signup',
            'section=access',
        );
        try {
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array(), $pattern, $replacement, array('content', 'target'), '2.0.0');
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array('module' => 'community'), array('/community/', '/profile/', '/register/'), array('access', 'settings', 'signup'), array('module', 'cmd'), '2.0.0');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

    }

    /***********************************
     *
     * CREATE PROFILE ATTRIBUTE TABLES
     *
     **********************************/
    try {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_attribute',
                array(
                    'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'parent_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => false, 'default' => null, 'after' => 'id'),
                    'type' => array('type' => 'ENUM(\'text\',\'textarea\',\'mail\',\'uri\',\'date\',\'image\',\'checkbox\',\'menu\',\'menu_option\',\'group\',\'frame\',\'history\')', 'notnull' => true, 'default' => 'text', 'after' => 'parent_id'),
                    'mandatory' => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'type'),
                    'sort_type' => array('type' => 'ENUM(\'asc\',\'desc\',\'custom\')', 'notnull' => true, 'default' => 'asc', 'after' => 'mandatory'),
                    'order_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'sort_type'),
                    'access_special' => array('type' => 'ENUM(\'\',\'menu_select_higher\',\'menu_select_lower\')', 'notnull' => true, 'default' => '', 'after' => 'order_id'),
                    'access_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'access_special'),
                    'read_access_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'access_id'),
                ),
                array(
                    'tree' => array('fields' => array('id', 'order_id', 'parent_id')),
                ),
                'InnoDB'
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_core_attribute',
                array(
                    'id' => array('type' => 'VARCHAR(25)', 'primary' => true),
                    'mandatory' => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                    'sort_type' => array('type' => 'ENUM(\'asc\',\'desc\',\'custom\')', 'notnull' => true, 'default' => 'asc', 'after' => 'mandatory'),
                    'order_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'sort_type'),
                    'access_special' => array('type' => 'ENUM(\'\',\'menu_select_higher\',\'menu_select_lower\')', 'notnull' => true, 'default' => '', 'after' => 'order_id'),
                    'access_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'access_special'),
                    'read_access_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'access_id'),
                ),
                array(),
                'InnoDB'
            );
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_attribute_name',
                array(
                    'attribute_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'lang_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'attribute_id'),
                    'name' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'lang_id')
                ), array(
                    'id_name' => array('fields' => array('attribute_id', 'name')),
                ),
                'InnoDB'
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_attribute_value',
                array(
                    'attribute_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'user_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'attribute_id'),
                    'history_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'user_id'),
                    'value' => array('type' => 'text', 'after' => 'history_id')
                ),
                array(
                    'value' => array('fields' => array('value'), 'type' => 'FULLTEXT'),
                    'contrexx_access_user_attribute_value_user_id_ibfk' => array('fields' => array('user_id')),
                    'attribute_user_idx' => array('fields' => array('attribute_id', 'user_id')),
                )
            );


            /************************
             *
             * ADD USER TITLE TABLE
             *
             ***********************/
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_title',
                array(
                    'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'title' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'order_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'title')
                ),
                array(
                    'title' => array('fields' => array('title'), 'type' => 'UNIQUE')
                ),
                'InnoDB'
            );

            $arrDefaultTitle = array(
                'Sehr geehrte Frau',
                'Sehr geehrter Herr',
                'Dear Ms',
                'Dear Mr',
                'Madame',
                'Monsieur'
            );

            foreach ($arrDefaultTitle as $title) {
                \Cx\Lib\UpdateUtil::sql("INSERT INTO `" . DBPREFIX . "access_user_title` SET `title` = '" . $title . "' ON DUPLICATE KEY UPDATE `id` = `id`");
            }
        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {

        /******************************
         *
         * REMOVE OBSOLETE ACCESS IDS
         *
         *****************************/
        $query = 'DELETE FROM `' . DBPREFIX . 'access_group_static_ids` WHERE `access_id` IN (28, 29, 30, 33, 34, 36)';
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }


        /*******************
         *
         * MIGRATE SESSION
         *
         ******************/
        $arrColumns = $objDatabase->MetaColumnNames(DBPREFIX . 'sessions');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX . 'sessions'));
            return false;
        }

        if (!in_array('user_id', $arrColumns)) {
            $query = "
                ALTER TABLE `" . DBPREFIX . "sessions`
                 DROP `username`,
                  ADD `user_id` INT UNSIGNED NOT NULL DEFAULT 0 AFTER `status`";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        /***************************************
         *
         * ADD CHECKBOX PROFILE ATTRIBUTE TYPE
         *
         **************************************/
        $query = "ALTER TABLE `" . DBPREFIX . "access_user_attribute` CHANGE `type` `type` enum('text','textarea','mail','uri','date','image','checkbox','menu','menu_option','group','frame','history') NOT NULL DEFAULT 'text'";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

    }

    try{
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.0')) {
            // Enforce unique and non-empty email addresses
            // *before* changing the structure and index
            if (!_accessForceUniqueEmail()) {
                return false; // Interrupt with message about udpated emails
            }
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_users',
                array(
                    'id' => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'is_admin' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'id'),
                    'username' => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'is_admin'),
                    'password' => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'username'),
                    'auth_token' => array('type' => 'VARCHAR(32)', 'notnull' => true, 'after' => 'password'),
                    'auth_token_timeout' => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'auth_token'),
                    'regdate' => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'auth_token_timeout'),
                    'expiration' => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'regdate'),
                    'validity' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'expiration'),
                    'last_auth' => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'validity'),
                    'last_auth_status' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '1', 'after' => 'last_auth'),
                    'last_activity' => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'last_auth_status'),
                    'email' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'after' => 'last_activity'),
                    'email_access' => array('type' => 'ENUM(\'everyone\',\'members_only\',\'nobody\')', 'notnull' => true, 'default' => 'nobody', 'after' => 'email'),
                    'frontend_lang_id' => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'email_access'),
                    'backend_lang_id' => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'frontend_lang_id'),
                    'active' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'backend_lang_id'),
                    'verified' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'after' => 'active'),
                    'primary_group' => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'verified'),
                    'profile_access' => array('type' => 'ENUM(\'everyone\',\'members_only\',\'nobody\')', 'notnull' => true, 'default' => 'members_only', 'after' => 'primary_group'),
                    'restore_key' => array('type' => 'VARCHAR(32)', 'notnull' => true, 'default' => '', 'after' => 'profile_access'),
                    'restore_key_time' => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'restore_key'),
                    'u2u_active' => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '1', 'after' => 'restore_key_time')
                ),
                array(
                    'username' => array('fields' => array('username')),
                    'UNIQ_7CD32875E7927C74' => array('fields' => array('email'), 'type' => 'UNIQUE'),
                )
            );
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_groups',
                array(
                    'group_id' => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'group_name' => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => '', 'after' => 'group_id'),
                    'group_description' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'group_name'),
                    'is_active' => array('type' => 'TINYINT(4)', 'notnull' => true, 'default' => '1', 'after' => 'group_description'),
                    'type' => array('type' => 'ENUM(\'frontend\',\'backend\')', 'notnull' => true, 'default' => 'frontend', 'after' => 'is_active'),
                    'homepage' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'type'),
                    'toolbar' => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'homepage'),
                )
            );
        }
    }
    catch (\Cx\Lib\UpdateException $e) {
        // we COULD do something else here..
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        \DBG::msg('004');
        // only update if installed version is at least a version 2.0.0
        // older versions < 2.0 have a complete other structure of the content page and must therefore completely be reinstalled
        if (!$objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '2.0.0')) {
            try {
                // migrate content page to version 3.0.1
                $search = array(
                '/(.*)/ms',
                );
                $callback = function($matches) {
                    $content = $matches[1];
                    if (empty($content)) {
                        return $content;
                    }

                    // add missing access_captcha template block
                    if (!preg_match('/<!--\s+BEGIN\s+access_captcha\s+-->.*<!--\s+END\s+access_captcha\s+-->/ms', $content)) {
                        $content = preg_replace('/(<\/fieldset>|)(\s*)(<p[^>]*>|)(\{ACCESS_SIGNUP_BUTTON\})(<\/p>|)/ms', '$2<!-- BEGIN access_captcha -->$2$3<label>{TXT_ACCESS_CAPTCHA}</label>{ACCESS_CAPTCHA_CODE}$5$2<!-- END access_captcha -->$2$1$2$3$4$5', $content);
                    }

                    // add missing access_newsletter template block
                    if (!preg_match('/<!--\s+BEGIN\s+access_newsletter\s+-->.*<!--\s+END\s+access_newsletter\s+-->/ms', $content)) {
                        $content = preg_replace('/(\s*)(<p[^>]*>|)(\{ACCESS_SIGNUP_BUTTON\})(<\/p>|)/ms', '$1<!-- BEGIN access_newsletter -->$1<fieldset><legend>Newsletter abonnieren</legend>$1    <!-- BEGIN access_newsletter_list -->$1    <p>$1        <label for="access_user_newsletters-{ACCESS_NEWSLETTER_ID}">&nbsp;{ACCESS_NEWSLETTER_NAME}</label>$1        <input type="checkbox" name="access_user_newsletters[]" id="access_user_newsletters-{ACCESS_NEWSLETTER_ID}" value="{ACCESS_NEWSLETTER_ID}"{ACCESS_NEWSLETTER_SELECTED} />$1    </p>$1    <!-- END access_newsletter_list -->$1</fieldset>$1<!-- END access_newsletter -->$1$2$3$4', $content);
                    }

                    return $content;
                };

                \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'access', 'cmd' => 'signup'), $search, $callback, array('content'), '3.0.1');
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
    }



    /***************************************
     *
     * ADD NETWORK TABLE FOR SOCIAL LOGIN
     *
     **************************************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'access_user_network',
                array(
                    'id'                 => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'oauth_provider'     => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'oauth_id'           => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => '', 'after' => 'oauth_provider'),
                    'user_id'            => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'oauth_id')
                ),
                array(),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        /***************************************
         *
         * ADD NEW VALUES FOR SOCIAL LOGIN
         *
         **************************************/
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('sociallogin', '', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('sociallogin_show_signup', '', 0) ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('use_usernames', '0', '1') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('sociallogin_assign_to_groups', '3', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('sociallogin_active_automatically', '', '1') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('sociallogin_activation_timeout', '10', '0') ON DUPLICATE KEY UPDATE `key` = `key`");

            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES ('access', 'providers', 'sociallogin', 'text', '{\"facebook\":{\"active\":\"0\",\"settings\":[\"\",\"\"]},\"twitter\":{\"active\":\"0\",\"settings\":[\"\",\"\"]},\"google\":{\"active\":\"0\",\"settings\":[\"\",\"\",\"\"]}}', '', '0') ON DUPLICATE KEY UPDATE `section` = `section`");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        /**
         * Content page
         * access signup
         */
        try {
            // migrate content page to version 3.0.1
            $search = array(
                '/(.*)/ms',
            );
            $callback = function($matches) {
                $content = $matches[1];
                if (empty($content)) {
                    return $content;
                }

                // fix duplicated social networks blocks
                if (preg_match('/<!--\s+BEGIN\s+access_social_networks\s+-->.*<!--\s+BEGIN\s+access_social_networks\s+-->/ms', $content)) {
                    $content = preg_replace('/<br\s+\/><br\s+\/><!--\s+BEGIN\s+access_social_networks\s+-->.*?<!--\s+END\s+access_social_networks\s+-->/ms', '', $content);
                }

                // add missing access_social_networks template block
                if (!preg_match('/<!--\s+BEGIN\s+access_social_networks\s+-->.*<!--\s+END\s+access_social_networks\s+-->/ms', $content)) {
                    $content = preg_replace('/(<!--\s+BEGIN\s+access_signup_form\s+-->.*?)(<div[^>]*>|)(.*?\{ACCESS_SIGNUP_MESSAGE\}.*?)(<\/div>|)/ms', '$1<br /><br /><!-- BEGIN access_social_networks --><fieldset><legend>oder Login mit Social Media</legend><!-- BEGIN access_social_networks_facebook -->        <a class="facebook loginbutton" href="{ACCESS_SOCIALLOGIN_FACEBOOK}">Facebook</a>        <!-- END access_social_networks_facebook -->        <!-- BEGIN access_social_networks_google -->        <a class="google loginbutton" href="{ACCESS_SOCIALLOGIN_GOOGLE}">Google</a>        <!-- END access_social_networks_google -->        <!-- BEGIN access_social_networks_twitter -->        <a class="twitter loginbutton" href="{ACCESS_SOCIALLOGIN_TWITTER}">Twitter</a>        <!-- END access_social_networks_twitter -->    </fieldset>    <!-- END access_social_networks -->$2$3$4', $content);
                }

                return $content;
            };

            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'access', 'cmd' => 'signup'), $search, $callback, array('content'), '3.0.3');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
        try {
            // add access to filesharing for existing groups
            $result = \Cx\Lib\UpdateUtil::sql("SELECT `group_id` FROM `" . DBPREFIX . "access_group_static_ids` WHERE access_id = 7 GROUP BY group_id");
            if ($result !== false) {
                while (!$result->EOF) {
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `" . DBPREFIX . "access_group_static_ids` (`access_id`, `group_id`)
                                                VALUES (8, " . intval($result->fields['group_id']) . ")");
                    $result->MoveNext();
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            //update module name
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."core_setting` SET `section` = 'Access' WHERE `section` = 'access' AND `name` = 'providers' AND `group` = 'sociallogin'");
        }  catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    
    /************************************************
	* BUGFIX:	Set write access to the upload dir  *
	************************************************/
    // This is obsolete due to the new \Cx\Lib\FileSystem
    /*
	require_once ASCMS_FRAMEWORK_PATH.'/File.class.php';
	$objFile = new File();
	if (is_writeable(ASCMS_ACCESS_PROFILE_IMG_PATH) || $objFile->setChmod(ASCMS_ACCESS_PROFILE_IMG_PATH, ASCMS_ACCESS_PROFILE_IMG_WEB_PATH, '')) {
    	if ($mediaDir = @opendir(ASCMS_ACCESS_PROFILE_IMG_PATH)) {
    		while($file = readdir($mediaDir)) {
    			if ($file != '.' && $file != '..') {
    				if (!is_writeable(ASCMS_ACCESS_PROFILE_IMG_PATH.'/'.$file) && !$objFile->setChmod(ASCMS_ACCESS_PROFILE_IMG_PATH.'/', ASCMS_ACCESS_PROFILE_IMG_WEB_PATH.'/', $file)) {
    					setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_FILE'], ASCMS_ACCESS_PROFILE_IMG_PATH.'/'.$file, $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
    					return false;
    				}
    			}
			}
    	} else {
    		setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_ACCESS_PROFILE_IMG_PATH.'/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
    		return false;
		}
    } else {
    	setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_ACCESS_PROFILE_IMG_PATH.'/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
    	return false;
    }

	require_once ASCMS_FRAMEWORK_PATH.'/File.class.php';
	$objFile = new File();
	if (is_writeable(ASCMS_ACCESS_PHOTO_IMG_PATH) || $objFile->setChmod(ASCMS_ACCESS_PHOTO_IMG_PATH, ASCMS_ACCESS_PHOTO_IMG_WEB_PATH, '')) {
    	if ($mediaDir = @opendir(ASCMS_ACCESS_PHOTO_IMG_PATH)) {
    		while($file = readdir($mediaDir)) {
    			if ($file != '.' && $file != '..') {
    				if (!is_writeable(ASCMS_ACCESS_PHOTO_IMG_PATH.'/'.$file) && !$objFile->setChmod(ASCMS_ACCESS_PHOTO_IMG_PATH.'/', ASCMS_ACCESS_PHOTO_IMG_WEB_PATH.'/', $file)) {
    					setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_FILE'], ASCMS_ACCESS_PHOTO_IMG_PATH.'/'.$file, $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
    					return false;
    				}
    			}
			}
    	} else {
    		setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_ACCESS_PHOTO_IMG_PATH.'/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
    		return false;
		}
    } else {
    	setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_ACCESS_PHOTO_IMG_PATH.'/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
    	return false;
    }*/

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('user_account_verification', '1', '1') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('block_next_birthday_users', '5', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('block_next_birthday_users_pic', '', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('block_random_access_users', '5', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('signup_notification_address', '" . $_CONFIG['coreAdminEmail'] . "', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."access_settings` (`key`, `value`, `status`) VALUES ('user_change_notification_address', '" . $_CONFIG['coreAdminEmail'] . "', '0') ON DUPLICATE KEY UPDATE `key` = `key`");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            // IMPORTANT: this table statement is also executed in dataaccess.php
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_modules_access_permission',
                array(
                    'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'allowed_protocols'      => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'id'),
                    'allowed_methods'        => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'allowed_protocols'),
                    'requires_login'         => array('type' => 'TINYINT(1)', 'notnull' => true, 'after' => 'allowed_methods'),
                    'valid_user_groups'      => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'requires_login'),
                    'valid_access_ids'       => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'valid_user_groups'),
                    'callback'               => array('type' => 'longtext', 'notnull' => true, 'comment' => '(DC2Type:array)', 'after' => 'valid_access_ids'),
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES ('user_account_invitation',0,'".$_CONFIG['coreAdminEmail']."','Cloudrexx','Willkommen bei Cloudrexx','multipart','Hallo [[FIRSTNAME]] [[LASTNAME]]\r\n\r\nFür Sie wurde ein persönlicher Zugang bei [[WEBSITE]] eingerichtet.\r\nSie können sich unter dem Link [[LINK]] mit den folgenden Angaben anmelden:\r\nBenutzername: [[EMAIL]]\r\nPasswort: [[PASSWORD]]\r\n\r\n\r\nSupport\r\n--------------------------------------\r\nHaben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter support@cloudrexx.com. Wir sind gerne für Sie da.\r\n\r\n\r\nFreundliche Grüsse\r\nIhr Cloudrexx-Team','<html>\r\n<head>\r\n	<title></title>\r\n</head>\r\n<body style=\"cursor: auto;\">\r\n<style type=\"text/css\">*, html, body, table {padding: 0;margin: 0;font-size: 14px;font-family: arial;line-height: 1.5;color: #000000;}\r\n  body {background-color: #fff;}\r\n  a {color: #0071bc;text-decoration: none;}\r\n</style>\r\n<table bgcolor=\"#fff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td bgcolor=\"#0071bc\" style=\"height:75px;\">\r\n			<div align=\"center\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"596\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" style=\"padding-top: 15px; padding-bottom: 12px;\"><img alt=\"logo\" src=\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\" /></td>\r\n						<td align=\"right\" style=\"padding-top: 12px; padding-bottom: 12px;\"><span style=\"color:#fff; font-size: 18px;font-family: arial;\">Ihr pers&ouml;nlicher Zugang</span></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td>&nbsp;</td>\r\n		</tr>\r\n		<tr>\r\n			<td>\r\n			<div align=\"center\">\r\n			<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"480\">\r\n				<tbody>\r\n					<tr>\r\n						<td style=\"padding-bottom: 7px;\">\r\n						<h1 style=\"font-size: 25px;font-family: arial;\">Willkommen bei Cloudrexx!</h1>\r\n						</td>\r\n					</tr>\r\n					<tr>\r\n						<td style=\"padding-bottom: 13px;  font-family: arial; font-size:14px\"><br />\r\n						Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\r\n						<br />\r\n						F&uuml;r Sie wurde ein pers&ouml;nlicher Zugang bei [[WEBSITE]] eingerichtet.<br />\r\n						Sie k&ouml;nnen sich unter dem Link [[LINK]] mit den folgenden Angaben anmelden:<br />\r\n						<br />\r\n						Benutzername: [[EMAIL]]<br />\r\n						Passwort: [[PASSWORD]]\r\n						<h2 style=\"font-size: 19px;\">Support</h2>\r\n						<span style=\"line-height: 21px;\">Haben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter </span><a href=\"mailto:support@cloudrexx.com\" style=\"line-height: 21px;\">support@cloudrexx.com</a><span style=\"line-height: 21px;\">. Wir sind gerne f&uuml;r Sie da.<br />\r\n						<br />\r\n						Freundliche Gr&uuml;sse<br />\r\n						Ihr Cloudrexx-Team</span><br />\r\n						&nbsp;</td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</div>\r\n			</td>\r\n		</tr>\r\n		<tr>\r\n			<td style=\"border-top: 1px solid #B9B9B9\">\r\n			<div align=\"center\">\r\n			<table cellspacing=\"0\" width=\"596\">\r\n				<tbody>\r\n					<tr>\r\n						<td align=\"left\" style=\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\" width=\"50%\">&copy; 2017 Cloudrexx AG</td>\r\n						<td align=\"right\" style=\"padding-top: 12px;\" width=\"33%\">&nbsp;</td>\r\n						<td align=\"right\" style=\"padding-top: 12px;\"><a href=\"https://www.cloudrexx.com/\" style=\"font-family: arial; color: #0071bc; font-size: 11px;\">www.cloudrexx.com</a></td>\r\n					</tr>\r\n				</tbody>\r\n			</table>\r\n			</div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</body>\r\n</html>\r\n')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'access_user_mail',
                array(
                    'type'           => array('type' => 'ENUM(\'reg_confirm\',\'reset_pw\',\'user_activated\',\'user_deactivated\',\'new_user\',\'user_account_invitation\',\'signup_notification\',\'user_profile_modification\')', 'notnull' => true, 'default' => 'reg_confirm'),
                    'lang_id'        => array('type' => 'TINYINT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'type'),
                    'sender_mail'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'lang_id'),
                    'sender_name'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'sender_mail'),
                    'subject'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'sender_name'),
                    'format'         => array('type' => 'ENUM(\'text\',\'html\',\'multipart\')', 'notnull' => true, 'default' => 'text', 'after' => 'subject'),
                    'body_text'      => array('type' => 'TEXT', 'after' => 'format'),
                    'body_html'      => array('type' => 'TEXT', 'after' => 'body_text')
                ),
                array(
                    'mail'           => array('fields' => array('type','lang_id'), 'type' => 'UNIQUE')
                ),
                'InnoDB'
            );

            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES ('signup_notification','0','".$_CONFIG['coreAdminEmail']."', 'Cloudrexx','Neue Benutzeranmeldung','multipart','Auf [[HOST]] hat sich ein neuer Benutzer angemeldet:\r\n\r\nBenutzer: [[PROFILE_NAME]]\r\n\r\nProfil-Daten:\r\n[[PROFILE_DATA]]\r\n\r\n\r\nLink zum Profil: [[NODE_ACCESS_USER]]?id=[[USER_ID]]', '<html>\r\n<head>\r\n  <title></title>\r\n</head>\r\n<body style=\"cursor: auto;\" tcap-name=\"framey0\">\r\n<style type=\"text/css\">*, html, body, table {padding: 0;margin: 0;font-size: 14px;font-family: arial;line-height: 1.5;color: #000000;}\r\n  body {background-color: #fff;}\r\n  a {color: #0071bc;text-decoration: none;}\r\n  div.profile thead th {text-align:left;}\r\n  div.profile table {width:100%;}\r\n</style>\r\n<table bgcolor=\"#fff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n <tbody>\r\n     <tr>\r\n            <td bgcolor=\"#0071bc\" style=\"height:75px;\">\r\n         <div align=\"center\">\r\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"596\">\r\n              <tbody>\r\n                 <tr>\r\n                        <td align=\"left\" style=\"padding-top: 15px; padding-bottom: 12px;\"><img alt=\"logo\" src=\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\" /></td>\r\n                     <td align=\"right\" style=\"padding-top: 12px; padding-bottom: 12px;\"><span style=\"color:#fff; font-size: 18px;font-family: arial;\">Neue Benutzeranmeldung</span></td>\r\n                  </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n       <tr>\r\n            <td>&nbsp;</td>\r\n     </tr>\r\n       <tr>\r\n            <td>\r\n            <div align=\"center\">\r\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"480\">\r\n              <tbody>\r\n                 <tr>\r\n                        <td style=\"padding-bottom: 13px;  font-family: arial; font-size:14px\"><br />\r\n                      Auf [[HOST]] hat sich ein neuer Benutzer angemeldet.<br />\r\n                      <br />\r\n                      Zum Profil: <a href=\"[[NODE_ACCESS_USER]]?id=[[USER_ID]]\">[[PROFILE_NAME]]</a><br />\r\n                      &nbsp;\r\n                      <div class=\"profile\">[[PROFILE_DATA]]</div>\r\n                       </td>\r\n                   </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n       <tr>\r\n            <td style=\"border-top: 1px solid #B9B9B9\">\r\n            <div align=\"center\">\r\n          <table cellspacing=\"0\" width=\"596\">\r\n             <tbody>\r\n                 <tr>\r\n                        <td align=\"left\" style=\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\" width=\"50%\">&copy; [[YEAR]] Cloudrexx AG</td>\r\n                        <td align=\"right\" style=\"padding-top: 12px;\" width=\"33%\">&nbsp;</td>\r\n                      <td align=\"right\" style=\"padding-top: 12px;\"><a href=\"https://www.cloudrexx.com/\" style=\"font-family: arial; color: #0071bc; font-size: 11px;\">www.cloudrexx.com</a></td>\r\n                   </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n   </tbody>\r\n</table>\r\n</body>\r\n</html>\r\n')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES ('user_profile_modification',0,'".$_CONFIG['coreAdminEmail']."','Cloudrexx','Benutzerprofil wurd bearbeitet','multipart','Folgende Benutzerprofil-Mutation wurde auf [[HOST]] vorgenommen:\r\n\r\nBenutzer: [[PROFILE_NAME]]\r\n\r\nProfil-Daten:\r\n[[PROFILE_DATA]]\r\n\r\n\r\nLink zum Profil: [[NODE_ACCESS_USER]]?id=[[USER_ID]]','<html>\r\n<head>\r\n  <title></title>\r\n</head>\r\n<body style=\"cursor: auto;\" tcap-name=\"framey0\">\r\n<style type=\"text/css\">*, html, body, table {padding: 0;margin: 0;font-size: 14px;font-family: arial;line-height: 1.5;color: #000000;}\r\n  body {background-color: #fff;}\r\n  a {color: #0071bc;text-decoration: none;}\r\n  div.profile thead th {text-align:left;}\r\n  div.profile table {width:100%;}\r\n</style>\r\n<table bgcolor=\"#fff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n <tbody>\r\n     <tr>\r\n            <td bgcolor=\"#0071bc\" style=\"height:75px;\">\r\n         <div align=\"center\">\r\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"596\">\r\n              <tbody>\r\n                 <tr>\r\n                        <td align=\"left\" style=\"padding-top: 15px; padding-bottom: 12px;\"><img alt=\"logo\" src=\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\" /></td>\r\n                     <td align=\"right\" style=\"padding-top: 12px; padding-bottom: 12px;\"><span style=\"color:#fff; font-size: 18px;font-family: arial;\">Benutzerprofil-Mutation</span></td>\r\n                  </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n       <tr>\r\n            <td>&nbsp;</td>\r\n     </tr>\r\n       <tr>\r\n            <td>\r\n            <div align=\"center\">\r\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"480\">\r\n              <tbody>\r\n                 <tr>\r\n                        <td style=\"padding-bottom: 13px;  font-family: arial; font-size:14px\"><br />\r\n                      Folgende Benutzerprofil-Mutation wurde auf [[HOST]] vorgenommen.<br />\r\n                      <br />\r\n                      Angepasstes Profil: <a href=\"[[NODE_ACCESS_USER]]?id=[[USER_ID]]\">[[PROFILE_NAME]]</a><br />\r\n                      &nbsp;\r\n                      <div class=\"profile\">[[PROFILE_DATA]]</div>\r\n                       </td>\r\n                   </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n       <tr>\r\n            <td style=\"border-top: 1px solid #B9B9B9\">\r\n            <div align=\"center\">\r\n          <table cellspacing=\"0\" width=\"596\">\r\n             <tbody>\r\n                 <tr>\r\n                        <td align=\"left\" style=\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\" width=\"50%\">&copy; [[YEAR]] Cloudrexx AG</td>\r\n                        <td align=\"right\" style=\"padding-top: 12px;\" width=\"33%\">&nbsp;</td>\r\n                      <td align=\"right\" style=\"padding-top: 12px;\"><a href=\"https://www.cloudrexx.com/\" style=\"font-family: arial; color: #0071bc; font-size: 11px;\">www.cloudrexx.com</a></td>\r\n                   </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n   </tbody>\r\n</table>\r\n</body>\r\n</html>\r\n')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //Update script for moving the folder
        $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
        $sourceImagePath = $imagePath . '/access';
        $targetImagePath = $imagePath . '/Access';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceImagePath, $targetImagePath
            ));
            return false;
        }
        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        try {
            foreach ($pathsToMigrate as $oldPath => $newPath) {
                \Cx\Lib\UpdateUtil::migratePath(
                    '`' . DBPREFIX . 'access_user_profile`',
                    '`picture`',
                    $oldPath,
                    $newPath
                );
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Benutzerverwaltung (Access)'
            ));
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_attribute_value',
                array(
                    'attribute_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'user_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'attribute_id'),
                    'history_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'user_id'),
                    'value' => array('type' => 'text', 'after' => 'history_id')
                ),
                array(
                    'value' => array('fields' => array('value'), 'type' => 'FULLTEXT'),
                    'contrexx_access_user_attribute_value_user_id_ibfk' => array('fields' => array('user_id')),
                    'attribute_user_idx' => array('fields' => array('attribute_id', 'user_id')),
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_attribute',
                array(
                    'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'parent_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => false, 'default' => null, 'after' => 'id'),
                    'type' => array('type' => 'ENUM(\'text\',\'textarea\',\'mail\',\'uri\',\'date\',\'image\',\'checkbox\',\'menu\',\'menu_option\',\'group\',\'frame\',\'history\')', 'notnull' => true, 'default' => 'text', 'after' => 'parent_id'),
                    'mandatory' => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'type'),
                    'sort_type' => array('type' => 'ENUM(\'asc\',\'desc\',\'custom\')', 'notnull' => true, 'default' => 'asc', 'after' => 'mandatory'),
                    'order_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'sort_type'),
                    'access_special' => array('type' => 'ENUM(\'\',\'menu_select_higher\',\'menu_select_lower\')', 'notnull' => true, 'default' => '', 'after' => 'order_id'),
                    'access_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'access_special'),
                    'read_access_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'access_id'),
                ),
                array(
                    'tree' => array('fields' => array('id', 'order_id', 'parent_id')),
                ),
                'InnoDB'
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'access_user_attribute_name',
                array(
                    'attribute_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'lang_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true, 'after' => 'attribute_id'),
                    'name' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'lang_id')
                ), array(
                    'id_name' => array('fields' => array('attribute_id', 'name')),
                ),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}

/**
 * Update duplicate or empty email addresses for all Users
 *
 * After running this, email addresses in the access_users table are
 * guaranteed to be non-empty, and unique.
 * Previously existing addresses have their username part suffixed with
 * "+<User ID>" (plus optional dash and index, like "-1").
 * Empty emails are filled with addresses created from the username
 * (or "anonymous", if none), optional suffix as above, and the system's
 * domain name.
 * Adds messages using setUpdateMsg() and returns false if any changes
 * are made.
 * @global  ADOConnection   $objDatabase
 * @global  array           $_CONFIG
 * @global  array           $_ARRAYLANG
 * @global  array           $_CORELANG
 * @return  bool                                True iff no email was updated
 * @throws  \Cx\Lib\Update_DatabaseException    on error
 */
function _accessForceUniqueEmail()
{
    global $objDatabase, $_CONFIG, $_ARRAYLANG, $_CORELANG;
    $domain = $_CONFIG['domainUrl'];
    $emailUnique = '';
    $messages = [];
    while (true) {
        $result = _accessFindNonUniqueEmail($objDatabase);
        if ($result->EOF) {
            break;
        }
        $email = $result->fields['email'];
        $resultConflict = _accessFindByEmail($objDatabase, $email);
        if ($resultConflict->EOF) {
            throw new \Cx\Lib\Update_DatabaseException(
                sprintf(
                    $_ARRAYLANG['TXT_UPDATE_USER_ERROR_FINDING_EMAIL'],
                    $email
                )
            );
        }
        $id = $resultConflict->fields['id'];
        $username = $resultConflict->fields['username'];
        $index = 0;
        while (true) {
            $emailUnique = preg_replace(
                '/^(.+)@([^@]+)$/',
                '$1+' . $id
                . ($index++ ? '-' . $index : '')
                . '@$2',
                $email
            );
            $resultConflict = _accessFindByEmail($objDatabase, $emailUnique);
            if ($resultConflict->EOF) {
                break;
            }
        }
        try {
            _accessUpdateEmail($objDatabase, $id, $emailUnique);
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            throw new \Cx\Lib\Update_DatabaseException(
                sprintf(
                    $_ARRAYLANG['TXT_UPDATE_USER_ERROR_UPDATING_EMAIL_DUPLICATE'],
                    $id, $username, $email, $emailUnique
                )
            );
        }
        $messages[] = sprintf($_ARRAYLANG['TXT_UPDATE_USER_EMAIL_CHANGED'],
            $id, $username, $email, $emailUnique
        );
    }
    while (true) {
        $result = _accessFindEmptyEmail($objDatabase);
        if ($result->EOF) {
            break;
        }
        $id = $result->fields['id'];
        $username = $result->fields['username'] ?: 'anonymous';
        $index = 0;
        while (true) {
            $emailUnique = $username
                . '+' . $id
                . ($index++ ? '-' . $index : '')
                . '@' . $domain;
            $resultConflict = _accessFindByEmail($objDatabase, $emailUnique);
            if ($resultConflict->EOF) {
                break;
            }
        }
        try {
            _accessUpdateEmail($objDatabase, $id, $emailUnique);
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            throw new \Cx\Lib\Update_DatabaseException(
                sprintf(
                    $_ARRAYLANG['TXT_UPDATE_USER_ERROR_UPDATING_EMAIL_EMPTY'],
                    $id, $username, $emailUnique
                )
            );
        }
        $messages[] = sprintf($_ARRAYLANG['TXT_UPDATE_USER_EMAIL_CHANGED'],
            $id, $username, '-', $emailUnique
        );
    }
    if ($messages) {
        setUpdateMsg($_ARRAYLANG['TXT_UPDATE_USER_EMAIL_UNIQUE'], 'title');
        foreach ($messages as $message) {
            setUpdateMsg($message, 'msg');
        }
        setUpdateMsg(
            '<input type="submit" name="updateNext"'
            .' value="' . $_CORELANG['TXT_UPDATE_NEXT'] . '" />'
            .'<input type="hidden" name="processUpdate" id="processUpdate" />',
            'button'
        );
        return false;
    }
    return true;
}

/**
 * Return the first User RecordSet with an empty email
 *
 * The record contains the id and username column values.
 * Mind that usernames may be not unique, or even empty.
 * @global  array           $_ARRAYLANG
 * @param   ADOConnection   $objDatabase
 * @return  ADORecordSet
 * @throws  \Cx\Lib\Update_DatabaseException    on error
 */
function _accessFindEmptyEmail(ADOConnection $objDatabase)
{
    $result = $objDatabase->SelectLimit('
        SELECT `id`, `username`
        FROM `' . DBPREFIX . 'access_users`
        WHERE `email` IS NULL OR `email`=""',
        1
    );
    if (!$result) {
        global $_ARRAYLANG;
        throw new \Cx\Lib\Update_DatabaseException(
            $_ARRAYLANG['TXT_UPDATE_USER_ERROR_QUERY_EMPTY_EMAIL']
        );
    }
    return $result;
}

/**
 * Return the first User RecordSet with a duplicate email
 *
 * The Record contains the email column only.
 * @global  array           $_ARRAYLANG
 * @param   ADOConnection   $objDatabase
 * @return  ADORecordSet
 * @throws  \Cx\Lib\Update_DatabaseException    on error
 */
function _accessFindNonUniqueEmail(ADOConnection $objDatabase)
{
    $result = $objDatabase->SelectLimit('
        SELECT `email`
        FROM `' . DBPREFIX . 'access_users`
        GROUP BY `email`
        HAVING `email` IS NOT NULL AND `email`!="" AND COUNT(*)>1',
        1
    );
    if (!$result) {
        global $_ARRAYLANG;
        throw new \Cx\Lib\Update_DatabaseException(
            $_ARRAYLANG['TXT_UPDATE_USER_ERROR_QUERY_NONUNIQUE_EMAIL']
        );
    }
    return $result;
}

/**
 * Return the first User RecordSet with the given email
 *
 * The record contains the id and username column values.
 * @global  array           $_ARRAYLANG
 * @param   ADOConnection   $objDatabase
 * @param   string          $email
 * @return  ADORecordSet
 * @throws  \Cx\Lib\Update_DatabaseException    on error
 */
function _accessFindByEmail(ADOConnection $objDatabase, string $email)
{
    $result = $objDatabase->SelectLimit('
        SELECT `id`, `username`
        FROM `' . DBPREFIX . 'access_users`
        WHERE `email`=?',
        1, -1,
        [$email]
    );
    if (!$result) {
        global $_ARRAYLANG;
        throw new \Cx\Lib\Update_DatabaseException(
            sprintf(
                $_ARRAYLANG['TXT_UPDATE_USER_ERROR_QUERY_EMAIL'], $email
            )
        );
    }
    return $result;
}

/**
 * Update the email for the given User ID
 *
 * The caller is supposed to rethrow the Exception in order to cast
 * a localized message (including the original email address).
 * @param   ADOConnection   $objDatabase
 * @param   int             $id
 * @param   string          $email
 * @throws  \Cx\Lib\Update_DatabaseException    on error
 */
function _accessUpdateEmail(ADOConnection $objDatabase, int $id, string $email)
{
    $result = $objDatabase->Execute('
        UPDATE `' . DBPREFIX . 'access_users`
        SET `email`=?
        WHERE `id`=?',
        [$email, $id]
    );
    if (!$result) {
        throw new \Cx\Lib\Update_DatabaseException(
            'Failed to update User email'
        );
    }
}

function _accessInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_group_dynamic_ids` (
  `access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `group_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`access_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_group_static_ids` (
  `access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `group_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`access_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'1\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'2\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'3\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'4\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'4\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'5\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'5\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'5\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'6\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'6\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'6\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'7\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'7\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'7\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'8\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'8\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'9\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'9\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'10\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'10\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'10\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'11\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'11\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'12\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'12\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'13\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'13\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'14\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'14\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'16\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'16\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'16\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'17\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'18\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'18\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'20\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'21\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'21\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'22\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'22\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'26\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'26\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'27\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'27\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'31\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'32\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'32\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'32\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'35\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'35\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'35\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'36\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'36\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'38\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'38\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'39\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'39\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'41\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'46\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'47\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'48\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'49\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'51\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'52\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'53\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'53\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'55\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'59\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'61\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'64\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'64\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'65\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'65\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'66\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'66\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'67\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'68\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'69\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'70\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'70\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'75\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'75\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'76\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'76\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'76\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'77\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'77\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'78\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'78\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'82\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'82\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'83\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'83\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'84\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'84\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'84\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'85\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'87\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'87\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'88\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'92\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'94\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'96\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'96\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'97\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'98\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'98\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'99\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'102\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'106\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'106\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'107\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'107\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'108\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'108\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'109\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'109\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'115\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'115\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'119\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'119\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'119\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'120\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'120\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'120\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'121\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'121\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'121\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'122\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'122\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'123\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'123\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'124\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'124\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'125\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'125\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'127\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'127\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'127\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'129\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'129\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'130\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'130\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'131\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'131\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'132\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'132\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'133\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'133\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'134\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'134\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'140\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'141\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'141\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'141\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'142\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'142\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'142\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'146\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'147\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'148\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'148\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'149\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'151\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'152\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'152\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'153\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'153\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'154\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'155\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'156\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'157\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'158\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'159\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'160\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'160\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'160\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'161\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'161\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'162\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'162\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'162\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'163\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'163\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'163\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'164\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'164\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'164\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'165\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'165\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'166\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'166\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'166\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'167\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'167\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'167\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'168\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'168\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'168\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'169\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'169\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'169\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'170\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'170\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'171\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'171\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'172\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'172\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'174\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'174\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'175\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'175\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'176\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'177\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'178\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'178\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'178\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'180\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'180\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'181\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'181\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'182\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'182\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'194\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'194\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'195\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'201\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'202\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'203\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_group_static_ids` (`access_id`, `group_id`) VALUES (\'204\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_rel_user_group` (
  `user_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `group_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_settings` (
  `key` varchar(32) NOT NULL DEFAULT \'\',
  `value` varchar(255) NOT NULL DEFAULT \'\',
  `status` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'assigne_to_groups\', \'3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_birthday_users\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_birthday_users_only_with_p\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_birthday_users_pic\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_currently_online_users\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_currently_online_users_onl\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_currently_online_users_pic\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_last_active_users\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_last_active_users_only_wit\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_last_active_users_pic\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_latest_reg_users\', \'5\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_latest_reg_users_pic\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_latest_registered_users\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_latest_registered_users_on\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_next_birthday_users\', \'5\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_next_birthday_users_pic\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'block_random_access_users\', \'5\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'default_email_access\', \'members_only\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'default_profile_access\', \'members_only\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_pic_height\', \'600\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_pic_size\', \'199987.2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_pic_width\', \'600\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_profile_pic_height\', \'160\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_profile_pic_size\', \'30003.2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_profile_pic_width\', \'160\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_thumbnail_pic_height\', \'130\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'max_thumbnail_pic_width\', \'130\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'notification_address\', \'info@example.com\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'profile_thumbnail_method\', \'crop\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'profile_thumbnail_pic_height\', \'60\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'profile_thumbnail_pic_width\', \'80\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'profile_thumbnail_scale_color\', \'#FFFFFF\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'session_user_interval\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'sociallogin\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'sociallogin_activation_timeout\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'sociallogin_active_automatically\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'sociallogin_assign_to_groups\', \'3\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'sociallogin_show_signup\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'use_usernames\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_accept_tos_on_signup\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_account_verification\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_activation\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_activation_timeout\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_captcha\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'signup_notification_address\', \'info@example.com\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_change_notification_address\', \'info@example.com\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_config_email_access\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_config_profile_access\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_settings` (`key`, `value`, `status`) VALUES (\'user_delete_account\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_users` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `auth_token` varchar(32) NOT NULL,
  `auth_token_timeout` int(14) unsigned NOT NULL DEFAULT \'0\',
  `regdate` int(14) unsigned NOT NULL DEFAULT \'0\',
  `expiration` int(14) unsigned NOT NULL DEFAULT \'0\',
  `validity` int(10) unsigned NOT NULL DEFAULT \'0\',
  `last_auth` int(14) unsigned NOT NULL DEFAULT \'0\',
  `last_auth_status` int(1) NOT NULL DEFAULT \'1\',
  `last_activity` int(14) unsigned NOT NULL DEFAULT \'0\',
  `email` varchar(255) NOT NULL,
  `email_access` enum(\'everyone\',\'members_only\',\'nobody\') NOT NULL DEFAULT \'nobody\',
  `frontend_lang_id` int(2) unsigned NOT NULL DEFAULT \'0\',
  `backend_lang_id` int(2) unsigned NOT NULL DEFAULT \'0\',
  `active` tinyint(1) NOT NULL DEFAULT \'0\',
  `verified` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `primary_group` int(6) unsigned NOT NULL DEFAULT \'0\',
  `profile_access` enum(\'everyone\',\'members_only\',\'nobody\') NOT NULL DEFAULT \'members_only\',
  `restore_key` varchar(32) NOT NULL DEFAULT \'\',
  `restore_key_time` int(14) unsigned NOT NULL DEFAULT \'0\',
  `u2u_active` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_7CD32875E7927C74` (`email`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_attribute` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `type` enum(\'text\',\'textarea\',\'mail\',\'uri\',\'date\',\'image\',\'checkbox\',\'menu\',\'menu_option\',\'group\',\'frame\',\'history\') NOT NULL DEFAULT \'text\',
  `mandatory` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `sort_type` enum(\'asc\',\'desc\',\'custom\') NOT NULL DEFAULT \'asc\',
  `order_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `access_special` enum(\'\',\'menu_select_higher\',\'menu_select_lower\') NOT NULL DEFAULT \'\',
  `access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `read_access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  INDEX `tree` (`id`, `order_id`, `parent_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_attribute_name` (
  `attribute_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`attribute_id`,`lang_id`),
  INDEX `id_name` (`attribute_id`, `name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_attribute_value` (
  `attribute_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `user_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `history_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `value` text NOT NULL,
  PRIMARY KEY (`attribute_id`,`user_id`,`history_id`),
  FULLTEXT KEY `value` (`value`),
  INDEX `contrexx_access_user_attribute_value_user_id_ibfk` (`user_id`),
  INDEX `attribute_user_idx` (`attribute_id`, `user_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_core_attribute` (
  `id` varchar(25) NOT NULL,
  `mandatory` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `sort_type` enum(\'asc\',\'desc\',\'custom\') NOT NULL DEFAULT \'asc\',
  `order_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `access_special` enum(\'\',\'menu_select_higher\',\'menu_select_lower\') NOT NULL DEFAULT \'\',
  `access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `read_access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(100) NOT NULL DEFAULT \'\',
  `group_description` varchar(255) NOT NULL DEFAULT \'\',
  `is_active` tinyint(4) NOT NULL DEFAULT \'1\',
  `type` enum(\'frontend\',\'backend\') NOT NULL DEFAULT \'frontend\',
  `homepage` varchar(255) NOT NULL DEFAULT \'\',
  `toolbar` int(6) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_groups` (`group_id`, `group_name`, `group_description`, `is_active`, `type`, `homepage`, `toolbar`) VALUES (\'1\', \'Manager\', \'Administrator\', \'1\', \'backend\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_groups` (`group_id`, `group_name`, `group_description`, `is_active`, `type`, `homepage`, `toolbar`) VALUES (\'3\', \'Community\', \'Community\', \'1\', \'frontend\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_groups` (`group_id`, `group_name`, `group_description`, `is_active`, `type`, `homepage`, `toolbar`) VALUES (\'6\', \'Customer\', \'Customer im Shop\', \'1\', \'frontend\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_groups` (`group_id`, `group_name`, `group_description`, `is_active`, `type`, `homepage`, `toolbar`) VALUES (\'7\', \'Reseller\', \'Reseller im Shop\', \'1\', \'frontend\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_groups` (`group_id`, `group_name`, `group_description`, `is_active`, `type`, `homepage`, `toolbar`) VALUES (\'9\', \'Moderator\', \'Inhaltspflege und Statistiken\', \'1\', \'backend\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_mail` (
  `type` enum(\'reg_confirm\',\'reset_pw\',\'user_activated\',\'user_deactivated\',\'new_user\',\'user_account_invitation\',\'signup_notification\',\'user_profile_modification\') NOT NULL DEFAULT \'reg_confirm\',
  `lang_id` tinyint(2) unsigned NOT NULL DEFAULT \'0\',
  `sender_mail` varchar(255) NOT NULL DEFAULT \'\',
  `sender_name` varchar(255) NOT NULL DEFAULT \'\',
  `subject` varchar(255) NOT NULL DEFAULT \'\',
  `format` enum(\'text\',\'html\',\'multipart\') NOT NULL DEFAULT \'text\',
  `body_text` text NOT NULL,
  `body_html` text NOT NULL,
  UNIQUE KEY `mail` (`type`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'reg_confirm\', \'0\', \'info@cloudrexx.com\', \'Cloudrexx\', \'Benutzerregistrierung bestätigen\', \'multipart\', \'Willkommen bei Cloudrexx!\\r\\n\\r\\nHerzlichen Dank für Ihre Anmeldung bei Cloudrexx.\\r\\nBitte klicken Sie auf den folgenden Link, um Ihre E-Mail-Adresse zu bestätigen:\\r\\n[[ACTIVATION_LINK]]\\r\\n\\r\\nUm sich später einzuloggen, geben Sie bitte Ihre E-Mail Adresse \\\"[[USERNAME]]\\\" und Ihr gewähltes Kennwort ein.\\r\\n\\r\\n\\r\\nSupport\\r\\n--------------------------------------\\r\\nHaben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter support@cloudrexx.com. Wir sind gerne für Sie da.\\r\\n\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr Cloudrexx-Team\', \'<html>\\r\\n<head>\\r\\n	<title></title>\\r\\n</head>\\r\\n<body style=\\\"cursor: auto;\\\">\\r\\n<style type=\\\"text/css\\\">*, html, body, table {\\r\\n                padding: 0;\\r\\n                margin: 0;\\r\\n                font-size: 14px;\\r\\n                font-family: arial;\\r\\n                line-height: 1.5;\\r\\n                color: #000000;\\r\\n            }\\r\\n            body {\\r\\n                background-color: #fff;\\r\\n            }\\r\\n            a {\\r\\n                color: #0071bc;\\r\\n                text-decoration: none;\\r\\n            }\\r\\n</style>\\r\\n<table bgcolor=\\\"#fff\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\\r\\n	<tbody>\\r\\n		<tr>\\r\\n			<td bgcolor=\\\"#0071bc\\\" style=\\\"height:75px;\\\">\\r\\n			<div align=\\\"center\\\">\\r\\n			<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td align=\\\"left\\\" style=\\\"padding-top: 15px; padding-bottom: 12px;\\\"><img alt=\\\"logo\\\" src=\\\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\\\" /></td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px; padding-bottom: 12px;\\\"><span style=\\\"color:#fff; font-size: 18px;  font-family: arial;\\\">Benutzerregistrierung</span></td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td>&nbsp;</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td>\\r\\n			<div align=\\\"center\\\">\\r\\n			<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"480\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td style=\\\"padding-bottom: 7px;\\\">\\r\\n						<h1 style=\\\" font-size: 25px; font-family: arial;\\\">Willkommen bei Cloudrexx!</h1>\\r\\n						</td>\\r\\n					</tr>\\r\\n					<tr>\\r\\n						<td style=\\\"padding-bottom: 13px;  font-family: arial; font-size:14px\\\"><br />\\r\\n						Herzlichen Dank f&uuml;r Ihre Anmeldung bei Cloudrexx.<br />\\r\\n						Bitte klicken Sie auf den folgenden Link, um Ihre E-Mail-Adresse zu best&auml;tigen:<br />\\r\\n						[[ACTIVATION_LINK]]<br />\\r\\n						<br />\\r\\n						&nbsp;\\r\\n						<h2 style=\\\"font-size: 19px;\\\">Support</h2>\\r\\n						<span style=\\\"line-height: 21px;\\\">Haben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter </span><a href=\\\"mailto:support@cloudrexx.com\\\" style=\\\"line-height: 21px;\\\">support@cloudrexx.com</a><span style=\\\"line-height: 21px;\\\">. Wir sind gerne f&uuml;r Sie da.<br />\\r\\n						<br />\\r\\n						Freundliche Gr&uuml;sse<br />\\r\\n						Ihr Cloudrexx-Team</span><br />\\r\\n						&nbsp;</td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td style=\\\"border-top: 1px solid #B9B9B9\\\">\\r\\n			<div align=\\\"center\\\">\\r\\n			<table cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td align=\\\"left\\\" style=\\\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\\\" width=\\\"50%\\\">&copy; [[YEAR]] Cloudrexx AG</td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\" width=\\\"33%\\\">&nbsp;</td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\"><a href=\\\"https://www.cloudrexx.com/\\\" style=\\\"font-family: arial; color: #0071bc; font-size: 11px;\\\">www.cloudrexx.com</a></td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n	</tbody>\\r\\n</table>\\r\\n</body>\\r\\n</html>\\r\\n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'reset_pw\', \'0\', \'info@cloudrexx.com\', \'Cloudrexx\', \'Cloudrexx Kennwort zurücksetzen\', \'multipart\', \'Guten Tag,\\r\\n\\r\\nUm ein neues Kennwort zu setzen, müssen Sie auf die unten aufgeführte URL gehen und dort Ihr neues Kennwort definieren.\\r\\n\\r\\nWICHTIG: Die Gültigkeit der URL wird nach 60 Minuten verfallen, nachdem diese E-Mail abgeschickt wurde.\\r\\nFalls Sie mehr Zeit benötigen, geben Sie Ihre E-Mail Adresse einfach ein weiteres Mal ein.\\r\\n\\r\\nIhre URL:\\r\\n[[URL]]\\r\\n\\r\\n\\r\\nSupport\\r\\n--------------------------------------\\r\\nHaben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter support@cloudrexx.com. Wir sind gerne für Sie da.\\r\\n\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr Cloudrexx-Team\', \'<html>\\r\\n<head>\\r\\n	<title></title>\\r\\n</head>\\r\\n<body style=\\\"cursor: auto;\\\">\\r\\n<style type=\\\"text/css\\\">*, html, body, table {\\r\\n                padding: 0;\\r\\n                margin: 0;\\r\\n                font-size: 14px;\\r\\n                font-family: arial;\\r\\n                line-height: 1.5;\\r\\n                color: #000000;\\r\\n            }\\r\\n            body {\\r\\n                background-color: #fff;\\r\\n            }\\r\\n            a {\\r\\n                color: #0071bc;\\r\\n                text-decoration: none;\\r\\n            }\\r\\n</style>\\r\\n<table bgcolor=\\\"#fff\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\\r\\n	<tbody>\\r\\n		<tr>\\r\\n			<td bgcolor=\\\"#0071bc\\\" style=\\\"height:75px;\\\">\\r\\n			<div align=\\\"center\\\">\\r\\n			<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td align=\\\"left\\\" style=\\\"padding-top: 15px; padding-bottom: 12px;\\\"><img alt=\\\"logo\\\" src=\\\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\\\" /></td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px; padding-bottom: 12px;\\\"><span style=\\\"color:#fff; font-size: 18px;  font-family: arial;\\\">Kennwort R&uuml;cksetzung</span></td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td>&nbsp;</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td>\\r\\n			<div align=\\\"center\\\">\\r\\n			<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"480\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td style=\\\"padding-bottom: 7px;\\\">\\r\\n						<h1 style=\\\" font-size: 25px; font-family: arial;\\\">Kennwort zur&uuml;cksetzen!</h1>\\r\\n						</td>\\r\\n					</tr>\\r\\n					<tr>\\r\\n						<td style=\\\"padding-bottom: 13px;  font-family: arial; font-size:14px\\\"><br />\\r\\n						F&uuml;r Ihre Cloudrexx Konto wurde eine Kennwort-R&uuml;cksetzung beantragt.<br />\\r\\n						<br />\\r\\n						Falls diese Anfrage nicht von Ihnen ausgegangen ist, k&ouml;nnen Sie diese Nachricht ignorieren.<br />\\r\\n						<br />\\r\\n						Klicken Sie <a href=\\\"[[URL]]\\\">hier</a> um jetzt ein neues Kennwort zu definieren.<br />\\r\\n						<br />\\r\\n						<em>WICHTIG: Die G&uuml;ltigkeit dieser Anfrage wird nach einer Stunde verfallen, nachdem diese E-Mail abgeschickt wurde.<br />\\r\\n						Falls Sie mehr Zeit ben&ouml;tigen, beantragen Sie einfach eine erneute Kennwort-R&uuml;cksetzung.</em><br />\\r\\n						&nbsp;\\r\\n						<h2 style=\\\"font-size: 19px;\\\">Support</h2>\\r\\n						<span style=\\\"line-height: 21px;\\\">Haben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter </span><a href=\\\"mailto:support@cloudrexx.com\\\" style=\\\"line-height: 21px;\\\">support@cloudrexx.com</a><span style=\\\"line-height: 21px;\\\">. Wir sind gerne f&uuml;r Sie da.<br />\\r\\n						<br />\\r\\n						Freundliche Gr&uuml;sse<br />\\r\\n						Ihr Cloudrexx-Team</span><br />\\r\\n						&nbsp;</td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td style=\\\"border-top: 1px solid #B9B9B9\\\">\\r\\n			<div align=\\\"center\\\">\\r\\n			<table cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td align=\\\"left\\\" style=\\\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\\\" width=\\\"50%\\\">&copy; [[YEAR]] Cloudrexx AG</td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\" width=\\\"33%\\\">&nbsp;</td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\"><a href=\\\"https://www.cloudrexx.com/\\\" style=\\\"font-family: arial; color: #0071bc; font-size: 11px;\\\">www.cloudrexx.com</a></td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n	</tbody>\\r\\n</table>\\r\\n</body>\\r\\n</html>\\r\\n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'user_activated\', \'0\', \'info@example.com\', \'Cloudrexx\', \'Ihr Benutzerkonto wurde aktiviert\', \'text\', \'Hallo [[USERNAME]],\\r\\n\\r\\nIhr Benutzerkonto auf [[HOST]] wurde soeben aktiviert und kann von nun an verwendet werden.\\r\\n\\r\\n\\r\\n--\\r\\n[[SENDER]]\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'user_deactivated\', \'0\', \'info@example.com\', \'Cloudrexx\', \'Ihr Benutzerkonto wurde deaktiviert\', \'text\', \'Hallo [[USERNAME]],\\r\\n\\r\\nIhr Benutzerkonto auf [[HOST]] wurde soeben deaktiviert.\\r\\n\\r\\n\\r\\n--\\r\\n[[SENDER]]\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'new_user\', \'0\', \'info@example.com\', \'Cloudrexx\', \'Ein neuer Benutzer hat sich registriert\', \'text\', \'Der Benutzer [[USERNAME]] hat sich soeben registriert und muss nun frei geschaltet werden.\\r\\n\\r\\nÜber die folgende Adresse kann das Benutzerkonto von [[USERNAME]] verwaltet werden:\\r\\n[[LINK]]\\r\\n\\r\\n\\r\\n--\\r\\n[[SENDER]]\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'user_account_invitation\', \'0\', \'info@cloudrexx.com\', \'Cloudrexx\', \'Willkommen bei Cloudrexx\', \'multipart\', \'Hallo [[FIRSTNAME]] [[LASTNAME]]\\r\\n\\r\\nFür Sie wurde ein persönlicher Zugang bei [[WEBSITE]] eingerichtet.\\r\\nSie können sich unter dem Link [[LINK]] mit den folgenden Angaben anmelden:\\r\\nBenutzername: [[EMAIL]]\\r\\nPasswort: [[PASSWORD]]\\r\\n\\r\\n\\r\\nSupport\\r\\n--------------------------------------\\r\\nHaben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter support@cloudrexx.com. Wir sind gerne für Sie da.\\r\\n\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr Cloudrexx-Team\', \'<html>\\r\\n<head>\\r\\n	<title></title>\\r\\n</head>\\r\\n<body style=\\\"cursor: auto;\\\">\\r\\n<style type=\\\"text/css\\\">*, html, body, table {padding: 0;margin: 0;font-size: 14px;font-family: arial;line-height: 1.5;color: #000000;}\\r\\n  body {background-color: #fff;}\\r\\n  a {color: #0071bc;text-decoration: none;}\\r\\n</style>\\r\\n<table bgcolor=\\\"#fff\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\\r\\n	<tbody>\\r\\n		<tr>\\r\\n			<td bgcolor=\\\"#0071bc\\\" style=\\\"height:75px;\\\">\\r\\n			<div align=\\\"center\\\">\\r\\n			<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td align=\\\"left\\\" style=\\\"padding-top: 15px; padding-bottom: 12px;\\\"><img alt=\\\"logo\\\" src=\\\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\\\" /></td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px; padding-bottom: 12px;\\\"><span style=\\\"color:#fff; font-size: 18px;font-family: arial;\\\">Ihr pers&ouml;nlicher Zugang</span></td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td>&nbsp;</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td>\\r\\n			<div align=\\\"center\\\">\\r\\n			<table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"480\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td style=\\\"padding-bottom: 7px;\\\">\\r\\n						<h1 style=\\\"font-size: 25px;font-family: arial;\\\">Willkommen bei Cloudrexx!</h1>\\r\\n						</td>\\r\\n					</tr>\\r\\n					<tr>\\r\\n						<td style=\\\"padding-bottom: 13px;  font-family: arial; font-size:14px\\\"><br />\\r\\n						Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\\r\\n						<br />\\r\\n						F&uuml;r Sie wurde ein pers&ouml;nlicher Zugang bei [[WEBSITE]] eingerichtet.<br />\\r\\n						Sie k&ouml;nnen sich unter dem Link [[LINK]] mit den folgenden Angaben anmelden:<br />\\r\\n						<br />\\r\\n						Benutzername: [[EMAIL]]<br />\\r\\n						Passwort: [[PASSWORD]]\\r\\n						<h2 style=\\\"font-size: 19px;\\\">Support</h2>\\r\\n						<span style=\\\"line-height: 21px;\\\">Haben Sie noch Fragen? Antworten Sie einfach auf diese E-Mail oder melden Sie sich unter </span><a href=\\\"mailto:support@cloudrexx.com\\\" style=\\\"line-height: 21px;\\\">support@cloudrexx.com</a><span style=\\\"line-height: 21px;\\\">. Wir sind gerne f&uuml;r Sie da.<br />\\r\\n						<br />\\r\\n						Freundliche Gr&uuml;sse<br />\\r\\n						Ihr Cloudrexx-Team</span><br />\\r\\n						&nbsp;</td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n		<tr>\\r\\n			<td style=\\\"border-top: 1px solid #B9B9B9\\\">\\r\\n			<div align=\\\"center\\\">\\r\\n			<table cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n				<tbody>\\r\\n					<tr>\\r\\n						<td align=\\\"left\\\" style=\\\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\\\" width=\\\"50%\\\">&copy; [[YEAR]] Cloudrexx AG</td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\" width=\\\"33%\\\">&nbsp;</td>\\r\\n						<td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\"><a href=\\\"https://www.cloudrexx.com/\\\" style=\\\"font-family: arial; color: #0071bc; font-size: 11px;\\\">www.cloudrexx.com</a></td>\\r\\n					</tr>\\r\\n				</tbody>\\r\\n			</table>\\r\\n			</div>\\r\\n			</td>\\r\\n		</tr>\\r\\n	</tbody>\\r\\n</table>\\r\\n</body>\\r\\n</html>\\r\\n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'signup_notification\', \'0\', \'info@cloudrexx.com\', \'Cloudrexx\', \'Neue Benutzeranmeldung\', \'multipart\', \'Auf [[HOST]] hat sich ein neuer Benutzer angemeldet:\r\n\r\nBenutzer: [[PROFILE_NAME]]\r\n\r\nProfil-Daten:\r\n[[PROFILE_DATA]]\r\n\r\n\r\nLink zum Profil: [[NODE_ACCESS_USER]]?id=[[USER_ID]]\', \'<html>\r\n<head>\r\n  <title></title>\r\n</head>\r\n<body style=\"cursor: auto;\" tcap-name=\"framey0\">\r\n<style type=\"text/css\">*, html, body, table {padding: 0;margin: 0;font-size: 14px;font-family: arial;line-height: 1.5;color: #000000;}\r\n  body {background-color: #fff;}\r\n  a {color: #0071bc;text-decoration: none;}\r\n  div.profile thead th {text-align:left;}\r\n  div.profile table {width:100%;}\r\n</style>\r\n<table bgcolor=\"#fff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\">\r\n <tbody>\r\n     <tr>\r\n            <td bgcolor=\"#0071bc\" style=\"height:75px;\">\r\n         <div align=\"center\">\r\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"596\">\r\n              <tbody>\r\n                 <tr>\r\n                        <td align=\"left\" style=\"padding-top: 15px; padding-bottom: 12px;\"><img alt=\"logo\" src=\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\" /></td>\r\n                     <td align=\"right\" style=\"padding-top: 12px; padding-bottom: 12px;\"><span style=\"color:#fff; font-size: 18px;font-family: arial;\">Neue Benutzeranmeldung</span></td>\r\n                  </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n       <tr>\r\n            <td>&nbsp;</td>\r\n     </tr>\r\n       <tr>\r\n            <td>\r\n            <div align=\"center\">\r\n          <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"480\">\r\n              <tbody>\r\n                 <tr>\r\n                        <td style=\"padding-bottom: 13px;  font-family: arial; font-size:14px\"><br />\r\n                      Auf [[HOST]] hat sich ein neuer Benutzer angemeldet.<br />\r\n                      <br />\r\n                      Zum Profil: <a href=\"[[NODE_ACCESS_USER]]?id=[[USER_ID]]\">[[PROFILE_NAME]]</a><br />\r\n                      &nbsp;\r\n                      <div class=\"profile\">[[PROFILE_DATA]]</div>\r\n                       </td>\r\n                   </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n       <tr>\r\n            <td style=\"border-top: 1px solid #B9B9B9\">\r\n            <div align=\"center\">\r\n          <table cellspacing=\"0\" width=\"596\">\r\n             <tbody>\r\n                 <tr>\r\n                        <td align=\"left\" style=\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\" width=\"50%\">&copy; [[YEAR]] Cloudrexx AG</td>\r\n                        <td align=\"right\" style=\"padding-top: 12px;\" width=\"33%\">&nbsp;</td>\r\n                      <td align=\"right\" style=\"padding-top: 12px;\"><a href=\"https://www.cloudrexx.com/\" style=\"font-family: arial; color: #0071bc; font-size: 11px;\">www.cloudrexx.com</a></td>\r\n                   </tr>\r\n               </tbody>\r\n            </table>\r\n            </div>\r\n          </td>\r\n       </tr>\r\n   </tbody>\r\n</table>\r\n</body>\r\n</html>\r\n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_mail` (`type`, `lang_id`, `sender_mail`, `sender_name`, `subject`, `format`, `body_text`, `body_html`) VALUES (\'user_profile_modification\', \'0\', \'info@cloudrexx.com\', \'Cloudrexx\', \'Benutzerprofil wurd bearbeitet\', \'multipart\', \'Folgende Benutzerprofil-Mutation wurde auf [[HOST]] vorgenommen:\\r\\n\\r\\nBenutzer: [[PROFILE_NAME]]\\r\\n\\r\\nProfil-Daten:\\r\\n[[PROFILE_DATA]]\\r\\n\\r\\n\\r\\nLink zum Profil: [[NODE_ACCESS_USER]]?id=[[USER_ID]]\', \'<html>\\r\\n<head>\\r\\n  <title></title>\\r\\n</head>\\r\\n<body style=\\\"cursor: auto;\\\" tcap-name=\\\"framey0\\\">\\r\\n<style type=\\\"text/css\\\">*, html, body, table {padding: 0;margin: 0;font-size: 14px;font-family: arial;line-height: 1.5;color: #000000;}\\r\\n  body {background-color: #fff;}\\r\\n  a {color: #0071bc;text-decoration: none;}\\r\\n  div.profile thead th {text-align:left;}\\r\\n  div.profile table {width:100%;}\\r\\n</style>\\r\\n<table bgcolor=\\\"#fff\\\" border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\\r\\n <tbody>\\r\\n     <tr>\\r\\n            <td bgcolor=\\\"#0071bc\\\" style=\\\"height:75px;\\\">\\r\\n         <div align=\\\"center\\\">\\r\\n          <table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n              <tbody>\\r\\n                 <tr>\\r\\n                        <td align=\\\"left\\\" style=\\\"padding-top: 15px; padding-bottom: 12px;\\\"><img alt=\\\"logo\\\" src=\\\"https://media.cloudrexx.com/1.0.0/cloudrexx_logo_145x25.png\\\" /></td>\\r\\n                     <td align=\\\"right\\\" style=\\\"padding-top: 12px; padding-bottom: 12px;\\\"><span style=\\\"color:#fff; font-size: 18px;font-family: arial;\\\">Benutzerprofil-Mutation</span></td>\\r\\n                  </tr>\\r\\n               </tbody>\\r\\n            </table>\\r\\n            </div>\\r\\n          </td>\\r\\n       </tr>\\r\\n       <tr>\\r\\n            <td>&nbsp;</td>\\r\\n     </tr>\\r\\n       <tr>\\r\\n            <td>\\r\\n            <div align=\\\"center\\\">\\r\\n          <table border=\\\"0\\\" cellpadding=\\\"0\\\" cellspacing=\\\"0\\\" width=\\\"480\\\">\\r\\n              <tbody>\\r\\n                 <tr>\\r\\n                        <td style=\\\"padding-bottom: 13px;  font-family: arial; font-size:14px\\\"><br />\\r\\n                      Folgende Benutzerprofil-Mutation wurde auf [[HOST]] vorgenommen.<br />\\r\\n                      <br />\\r\\n                      Angepasstes Profil: <a href=\\\"[[NODE_ACCESS_USER]]?id=[[USER_ID]]\\\">[[PROFILE_NAME]]</a><br />\\r\\n                      &nbsp;\\r\\n                      <div class=\\\"profile\\\">[[PROFILE_DATA]]</div>\\r\\n                       </td>\\r\\n                   </tr>\\r\\n               </tbody>\\r\\n            </table>\\r\\n            </div>\\r\\n          </td>\\r\\n       </tr>\\r\\n       <tr>\\r\\n            <td style=\\\"border-top: 1px solid #B9B9B9\\\">\\r\\n            <div align=\\\"center\\\">\\r\\n          <table cellspacing=\\\"0\\\" width=\\\"596\\\">\\r\\n             <tbody>\\r\\n                 <tr>\\r\\n                        <td align=\\\"left\\\" style=\\\"font-family: arial; color: #939fa2; font-size: 11px; padding-top: 14px;\\\" width=\\\"50%\\\">&copy; [[YEAR]] Cloudrexx AG</td>\\r\\n                        <td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\" width=\\\"33%\\\">&nbsp;</td>\\r\\n                      <td align=\\\"right\\\" style=\\\"padding-top: 12px;\\\"><a href=\\\"https://www.cloudrexx.com/\\\" style=\\\"font-family: arial; color: #0071bc; font-size: 11px;\\\">www.cloudrexx.com</a></td>\\r\\n                   </tr>\\r\\n               </tbody>\\r\\n            </table>\\r\\n            </div>\\r\\n          </td>\\r\\n       </tr>\\r\\n   </tbody>\\r\\n</table>\\r\\n</body>\\r\\n</html>\\r\\n\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_network` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `oauth_provider` varchar(100) NOT NULL DEFAULT \'\',
  `oauth_id` varchar(100) NOT NULL DEFAULT \'\',
  `user_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_profile` (
  `user_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `gender` enum(\'gender_undefined\',\'gender_female\',\'gender_male\') NOT NULL DEFAULT \'gender_undefined\',
  `title` int(10) unsigned NOT NULL DEFAULT \'0\',
  `designation` varchar(255) NOT NULL DEFAULT \'\',
  `firstname` varchar(255) NOT NULL DEFAULT \'\',
  `lastname` varchar(255) NOT NULL DEFAULT \'\',
  `company` varchar(255) NOT NULL DEFAULT \'\',
  `address` varchar(255) NOT NULL DEFAULT \'\',
  `city` varchar(50) NOT NULL DEFAULT \'\',
  `zip` varchar(10) NOT NULL DEFAULT \'\',
  `country` smallint(5) unsigned NOT NULL DEFAULT \'0\',
  `phone_office` varchar(20) NOT NULL DEFAULT \'\',
  `phone_private` varchar(20) NOT NULL DEFAULT \'\',
  `phone_mobile` varchar(20) NOT NULL DEFAULT \'\',
  `phone_fax` varchar(20) NOT NULL DEFAULT \'\',
  `birthday` varchar(11) DEFAULT NULL,
  `website` varchar(255) NOT NULL DEFAULT \'\',
  `profession` varchar(150) NOT NULL DEFAULT \'\',
  `interests` text ,
  `signature` text ,
  `picture` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`user_id`),
  KEY `profile` (`firstname`(100),`lastname`(100),`company`(50))
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_title` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `order_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_title` (`id`, `title`, `order_id`) VALUES (\'1\', \'Sehr geehrte Frau\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_title` (`id`, `title`, `order_id`) VALUES (\'2\', \'Sehr geehrter Herr\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_title` (`id`, `title`, `order_id`) VALUES (\'3\', \'Dear Ms\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_title` (`id`, `title`, `order_id`) VALUES (\'4\', \'Dear Mr\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_title` (`id`, `title`, `order_id`) VALUES (\'5\', \'Madame\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_title` (`id`, `title`, `order_id`) VALUES (\'6\', \'Monsieur\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'access_user_validity` (
  `validity` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`validity`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'15\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'31\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'62\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'92\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'123\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'184\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'366\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'access_user_validity` (`validity`) VALUES (\'731\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'core_modules_access_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allowed_protocols` longtext NOT NULL COMMENT \'(DC2Type:array)\',
  `allowed_methods` longtext NOT NULL COMMENT \'(DC2Type:array)\',
  `requires_login` tinyint(1) NOT NULL,
  `valid_user_groups` longtext NOT NULL COMMENT \'(DC2Type:array)\',
  `valid_access_ids` longtext NOT NULL COMMENT \'(DC2Type:array)\',
  `callback` longtext NOT NULL COMMENT \'(DC2Type:array)\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
