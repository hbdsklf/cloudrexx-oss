<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _datasourceUpdate()
{
    global $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_data_source',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'identifier'     => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'options'        => array('type' => 'text', 'after' => 'identifier', 'comment' => '(DC2Type:array)'),
                    'type'           => array('type' => 'ENUM(\'dataSource\',\'mediaSource\',\'ymlRepository\',\'jsonDataSource\',\'doctrineRepository\',\'legacyDatabaseRepository\')', 'notnull' => true, 'after' => 'options'),
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.2')) {
        try {
            \Cx\Lib\UpdateUtil::sql('UPDATE `' . DBPREFIX . 'core_data_source` SET `options` = \'a:0:{}\'');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    return true;
}
