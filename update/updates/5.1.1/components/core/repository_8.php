<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_8()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 8,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'8', '8', '{APPLICATION_DATA}', 'News', '', '', '0', 'on', 'system', '18');
SQL
                                ),
		array(
                                    'id' => 9,
                                    'parid' => 8,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'9', '8', '{APPLICATION_DATA}', 'News hinzufügen', 'submit', '', '8', 'on', 'system', '27');
SQL
                                ),
		array(
                                    'id' => 10,
                                    'parid' => 8,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'10', '8', '{APPLICATION_DATA}', 'Direktzugriff auf eine Newskategorie', '1', '', '8', 'on', 'system', '29');
SQL
                                ),
		array(
                                    'id' => 11,
                                    'parid' => 8,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'11', '8', '{APPLICATION_DATA}', 'News Feed', 'feed', '', '8', 'on', 'system', '31');
SQL
                                ),
		array(
                                    'id' => 12,
                                    'parid' => 8,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'12', '8', '{APPLICATION_DATA}', 'Newsmeldung', 'details', '', '8', '', 'system', '33');
SQL
                                ),
		array(
                                    'id' => 13,
                                    'parid' => 8,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'13', '8', '{APPLICATION_DATA}', 'Archiv', 'archive', '', '8', 'on', 'system', '37');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=8";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
