<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_20()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 50,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'50', '20', '{APPLICATION_DATA}', 'Forum', '', '', '0', 'on', 'system', '210');
SQL
                                ),
		array(
                                    'id' => 51,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'51', '20', '{APPLICATION_DATA}', 'Abonnierte Foren/Kategorien', 'notification', '', '50', 'on', 'system', '211');
SQL
                                ),
		array(
                                    'id' => 52,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'52', '20', '{APPLICATION_DATA}', 'Tag Cloud', 'cloud', '', '50', 'on', 'system', '213');
SQL
                                ),
		array(
                                    'id' => 53,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'53', '20', '{APPLICATION_DATA}', 'Populärste Diskussionen', 'toplist', '', '50', 'on', 'system', '215');
SQL
                                ),
		array(
                                    'id' => 54,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'54', '20', '{APPLICATION_DATA}', 'Suche nach Tags', 'searchTags', '', '50', 'on', 'system', '217');
SQL
                                ),
		array(
                                    'id' => 55,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'55', '20', '{APPLICATION_DATA}', 'Thread anzeigen', 'thread', '', '50', '', 'system', '219');
SQL
                                ),
		array(
                                    'id' => 56,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'56', '20', '{APPLICATION_DATA}', 'Kategorie anzeigen', 'cat', '', '50', '', 'system', '221');
SQL
                                ),
		array(
                                    'id' => 57,
                                    'parid' => 50,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'57', '20', '{APPLICATION_DATA}', 'Forum anzeigen', 'board', '', '50', '', 'system', '223');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=20";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
