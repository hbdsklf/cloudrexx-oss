<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_16()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 42,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'42', '16', '{APPLICATION_DATA}', 'Online Shop', '', '', '0', 'on', 'system', '49');
SQL
                                ),
		array(
                                    'id' => 40,
                                    'parid' => 34,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'40', '16', '{APPLICATION_DATA}', 'Passwort Hilfe', 'sendpass', '', '34', '', 'system', '61');
SQL
                                ),
		array(
                                    'id' => 41,
                                    'parid' => 34,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'41', '16', '{APPLICATION_DATA}', 'Passwort ändern', 'changepass', '', '34', '', 'system', '63');
SQL
                                ),
		array(
                                    'id' => 29,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'29', '16', '{APPLICATION_DATA}', 'Sonderangebote', 'discounts', '', '42', 'on', 'system', '50');
SQL
                                ),
		array(
                                    'id' => 30,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'30', '16', '{APPLICATION_DATA}', 'Ihr Warenkorb', 'cart', '', '42', 'on', 'system', '52');
SQL
                                ),
		array(
                                    'id' => 31,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'31', '16', '{APPLICATION_DATA}', 'Einzugsermächtigung', 'lsv', '', '42', '', 'system', '54');
SQL
                                ),
		array(
                                    'id' => 32,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'32', '16', '{APPLICATION_DATA}', 'Kontoangaben', 'account', '', '42', '', 'system', '56');
SQL
                                ),
		array(
                                    'id' => 33,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'33', '16', '{APPLICATION_DATA}', 'Detaillierte Produktedaten', 'details', '', '42', '', 'system', '58');
SQL
                                ),
		array(
                                    'id' => 34,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'34', '16', '{APPLICATION_DATA}', 'Login', 'login', '', '42', '', 'system', '60');
SQL
                                ),
		array(
                                    'id' => 35,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'35', '16', '{APPLICATION_DATA}', 'Transaktionsstatus', 'success', '', '42', '', 'system', '66');
SQL
                                ),
		array(
                                    'id' => 36,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'36', '16', '{APPLICATION_DATA}', 'Allgemeine Geschäftsbedingungen', 'terms', '', '42', '', 'system', '68');
SQL
                                ),
		array(
                                    'id' => 37,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'37', '16', '{APPLICATION_DATA}', 'Bezahlung und Versand', 'payment', '', '42', '', 'system', '70');
SQL
                                ),
		array(
                                    'id' => 38,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'38', '16', '{APPLICATION_DATA}', 'Bestellinformationen', 'confirm', '', '42', '', 'system', '72');
SQL
                                ),
		array(
                                    'id' => 39,
                                    'parid' => 42,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'39', '16', '{APPLICATION_DATA}', 'Versandkonditionen', 'shipment', '', '42', 'on', 'system', '74');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=16";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
