<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_33()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 100,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'100', '33', '{APPLICATION_DATA}', 'Kleinanzeigemarkt', '', '', '0', 'on', 'system', '254');
SQL
                                ),
		array(
                                    'id' => 101,
                                    'parid' => 100,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'101', '33', '{APPLICATION_DATA}', 'Inserat suchen', 'search', '', '100', '', 'system', '255');
SQL
                                ),
		array(
                                    'id' => 102,
                                    'parid' => 100,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'102', '33', '{APPLICATION_DATA}', 'Inserat erstellen', 'add', '', '100', 'on', 'system', '257');
SQL
                                ),
		array(
                                    'id' => 103,
                                    'parid' => 100,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'103', '33', '{APPLICATION_DATA}', 'Inserat anzeigen', 'detail', '', '100', '', 'system', '269');
SQL
                                ),
		array(
                                    'id' => 104,
                                    'parid' => 102,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'104', '33', '{APPLICATION_DATA}', 'Insarat Aktivieren', 'confirm', '', '102', '', 'system', '258');
SQL
                                ),
		array(
                                    'id' => 105,
                                    'parid' => 102,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'105', '33', '{APPLICATION_DATA}', 'Inserat löschen', 'del', '', '102', '', 'system', '264');
SQL
                                ),
		array(
                                    'id' => 106,
                                    'parid' => 102,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'106', '33', '{APPLICATION_DATA}', 'Inserat editieren', 'edit', '', '102', '', 'system', '266');
SQL
                                ),
		array(
                                    'id' => 107,
                                    'parid' => 103,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'107', '33', '{APPLICATION_DATA}', 'Mitteilung verschickt', 'send', '', '103', '', 'system', '270');
SQL
                                ),
		array(
                                    'id' => 108,
                                    'parid' => 104,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'108', '33', '{APPLICATION_DATA}', 'Freischaltcode Eingeben', 'paypal_successfull', '', '104', '', 'system', '259');
SQL
                                ),
		array(
                                    'id' => 109,
                                    'parid' => 104,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'109', '33', '{APPLICATION_DATA}', 'PayPal fehlgeschlagen', 'paypal_error', '', '104', '', 'system', '261');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=33";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
