<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_21()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 67,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'67', '21', '{APPLICATION_DATA}', 'Veranstaltungen', '', '', '0', 'on', 'system', '89');
SQL
                                ),
		array(
                                    'id' => 68,
                                    'parid' => 63,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'68', '21', '{APPLICATION_DATA}', 'Veranstaltung bearbeiten', 'edit', '', '63', '', 'system', '101');
SQL
                                ),
		array(
                                    'id' => 58,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'58', '21', '{APPLICATION_DATA}', 'Auflistung aller Veranstaltungen', 'list', '', '67', 'on', 'system', '90');
SQL
                                ),
		array(
                                    'id' => 59,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'59', '21', '{APPLICATION_DATA}', 'Quartalsübersicht', 'boxes', '', '67', 'on', 'system', '92');
SQL
                                ),
		array(
                                    'id' => 60,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'60', '21', '{APPLICATION_DATA}', 'Kategorisierte Anzeige', 'category', '', '67', 'on', 'system', '94');
SQL
                                ),
		array(
                                    'id' => 61,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'61', '21', '{APPLICATION_DATA}', 'Archiv', 'archive', '', '67', 'on', 'system', '96');
SQL
                                ),
		array(
                                    'id' => 62,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'62', '21', '{APPLICATION_DATA}', 'Veranstaltung erfassen', 'add', '', '67', 'on', 'system', '98');
SQL
                                ),
		array(
                                    'id' => 63,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'63', '21', '{APPLICATION_DATA}', 'Meine Veranstaltungen', 'my_events', '', '67', 'on', 'system', '100');
SQL
                                ),
		array(
                                    'id' => 64,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'64', '21', '{APPLICATION_DATA}', 'Veranstaltungsinformationen', 'detail', '', '67', '', 'system', '104');
SQL
                                ),
		array(
                                    'id' => 65,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'65', '21', '{APPLICATION_DATA}', 'An- \\ Abmelden', 'register', '', '67', '', 'system', '106');
SQL
                                ),
		array(
                                    'id' => 66,
                                    'parid' => 67,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'66', '21', '{APPLICATION_DATA}', 'Status', 'Status', '', '67', '', 'system', '108');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=21";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
