<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_23()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 93,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'93', '23', '{APPLICATION_DATA}', 'Benutzer', 'members', '', '0', 'on', 'system', '127');
SQL
                                ),
		array(
                                    'id' => 75,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'75', '23', '{APPLICATION_DATA}', 'Registrieren', 'signup', '', '0', '', 'system', '325');
SQL
                                ),
		array(
                                    'id' => 82,
                                    'parid' => 78,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'82', '23', '{APPLICATION_DATA}', 'Mein Profil', 'settings_profile', '', '78', 'on', 'system', '131');
SQL
                                ),
		array(
                                    'id' => 83,
                                    'parid' => 78,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'83', '23', '{APPLICATION_DATA}', 'Kontoeinstellungen', 'settings_account', '', '78', 'on', 'system', '139');
SQL
                                ),
		array(
                                    'id' => 79,
                                    'parid' => 82,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'79', '23', '{APPLICATION_DATA}', 'Profildaten', 'settings_profiledata', '', '82', 'on', 'system', '132');
SQL
                                ),
		array(
                                    'id' => 80,
                                    'parid' => 82,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'80', '23', '{APPLICATION_DATA}', 'Profilbild', 'settings_avatar', '', '82', 'on', 'system', '134');
SQL
                                ),
		array(
                                    'id' => 81,
                                    'parid' => 82,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'81', '23', '{APPLICATION_DATA}', 'Verbundene Netzwerke', 'settings_networks', '', '82', 'on', 'system', '136');
SQL
                                ),
		array(
                                    'id' => 84,
                                    'parid' => 83,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'84', '23', '{APPLICATION_DATA}', 'Konto', 'settings_accountdata', '', '83', 'on', 'system', '140');
SQL
                                ),
		array(
                                    'id' => 85,
                                    'parid' => 83,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'85', '23', '{APPLICATION_DATA}', 'Kennwort', 'settings_password', '', '83', 'on', 'system', '142');
SQL
                                ),
		array(
                                    'id' => 86,
                                    'parid' => 83,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'86', '23', '{APPLICATION_DATA}', 'Konto löschen', 'settings_delete', '', '83', 'on', 'system', '144');
SQL
                                ),
		array(
                                    'id' => 77,
                                    'parid' => 93,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'77', '23', '{APPLICATION_DATA}', 'Benutzerinfos', 'user', '', '93', '', 'system', '128');
SQL
                                ),
		array(
                                    'id' => 78,
                                    'parid' => 93,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'78', '23', '{APPLICATION_DATA}', 'Benutzerbereich', '', '', '93', 'on', 'system', '130');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=23";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
