<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_45()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 115,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'115', '45', '<p><a href=\"http://de.wikipedia.org/wiki/Allgemeine_Gesch%C3%A4ftsbedingungen\">Allgemeine Gesch&auml;ftsbedingungen</a> (abgek&uuml;rzt &bdquo;AGB&ldquo;, nicht-standardsprachlich auch oft &bdquo;AGBs&ldquo;,&bdquo;AGB&#39;s&ldquo; oder &bdquo;AGBen&ldquo;) sind alle f&uuml;r eine Vielzahl von Vertr&auml;gen vorformulierten Vertragsbedingungen, die eine Vertragspartei (der Verwender) der anderen Vertragspartei bei Abschluss eines Vertrages stellt.</p>\r\n\r\n<p>Dabei ist es gleichg&uuml;ltig, ob die Bestimmung einen &auml;u&szlig;erlich gesonderten Bestandteil des Vertrags (umgangssprachlich &bdquo;das Kleingedruckte&ldquo; genannt) bilden oder in die Vertragsurkunde selbst aufgenommen werden.</p>\r\n', 'AGBs', '', '', '0', '', 'system', '323');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=45";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
