<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_46()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 116,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'116', '46', '<h2>\r\n	Zugriffs- und Anwendungsbedingungen f&uuml;r diese Website</h2>\r\nMit dem Zugriff auf diese Website und ihre Links erkl&auml;ren Sie, dass Sie die nachfolgenden&nbsp;Anwendungsbedingungen und rechtlichen Informationen im Zusammenhang mit dieser Website&nbsp;(und den darin enthaltenen Elementen) verstanden haben und diese anerkennen. Sollten Sie mit&nbsp;diesen Bedingungen nicht einverstanden sein, unterlassen Sie den Zugriff auf diese Website.<br />\r\n<br />\r\n<h2>\r\n	Zweck dieser Website / Keine Gew&auml;hrleistung</h2>\r\nDer Zweck dieser Website ist die Informationsvermittlung &uuml;ber unsere Gesellschaft und&nbsp;Dienstleistungen. Durch das Aufrufen dieser Website durch den Benutzer geht {CONTACT_COMPANY} mit&nbsp;dem Benutzer keinerlei vertragliche Beziehung ein. Die publizierten Tools begr&uuml;nden weder eine&nbsp;Aufforderung zur Offertenstellung noch ein Angebot. {CONTACT_COMPANY} bem&uuml;ht sich, die Inhalte auf&nbsp;dieser Website aktuell, vollst&auml;ndig und richtig zu halten. Da diese Informationen jedoch raschen&nbsp;&Auml;nderungen unterliegen k&ouml;nnen, kann die Aktualit&auml;t, Richtigkeit und Vollst&auml;ndigkeit nicht&nbsp;vollst&auml;ndig gew&auml;hrleistet werden.&nbsp;Die Angaben auf dieser Website stellen keine verbindlichen Entscheidungshilfen und Antworten auf&nbsp;Beratungsfragen dar, noch sollten aufgrund dieser Angaben arbeitsrechtliche oder andere&nbsp;Entscheide gef&auml;llt werden. Jegliches Handeln, welches aufgrund der Angaben auf dieser Website&nbsp;getroffen wird, erfolgt auf das alleinige Risiko des Benutzers.<br />\r\n<br />\r\n<h2>\r\n	Funktionen dieser Website</h2>\r\n{CONTACT_COMPANY} &uuml;bernimmt keine Verantwortung und gibt keine Garantie daf&uuml;r ab, dass die&nbsp;Funktionen auf ihrer Website dauernd und ununterbrochen zur Verf&uuml;gung stehen, fehlerfrei sind&nbsp;oder Fehler behoben werden oder dass die Website oder die jeweiligen Server frei von Viren oder&nbsp;sonstigen sch&auml;dlichen Bestandteilen sind.<br />\r\n<br />\r\n<h2>\r\n	Links</h2>\r\nDiese Website enth&auml;lt Links zu Websites von Dritten. Deren Inhalte sind dem Einfluss von {CONTACT_COMPANY} entzogen, weshalb weder f&uuml;r deren Richtigkeit, Vollst&auml;ndigkeit und Rechtm&auml;ssigkeit&nbsp;irgendeine Gew&auml;hrleistung &uuml;bernommen werden kann. {CONTACT_COMPANY} &uuml;bernimmt daher keinerlei&nbsp;Haftung f&uuml;r die Inhalte auf den Seiten dieser Dritten.<br />\r\n<br />\r\n<h2>\r\n	Keine Sicherheit der Daten&uuml;bertragung</h2>\r\nDie E-Mail-Kommunikation findet &uuml;ber ein offenes, jedermann zug&auml;ngliches und grenz&uuml;berschreitendes&nbsp;Netz statt. Sie ist unverschl&uuml;sselt. Es kann somit nicht ausgeschlossen werden,&nbsp;dass so gesendete Daten durch Dritte eingesehen und damit auch die Kontaktaufnahme zu {CONTACT_COMPANY} nachvollzogen werden kann.<br />\r\n<br />\r\n<h2>\r\n	Datenschutz</h2>\r\n{CONTACT_COMPANY} beh&auml;lt sich vor, personenbezogene Daten, welche ihr elektronisch &uuml;bermittelt&nbsp;werden, f&uuml;r eigene Marketingzwecke zu erfassen und auszuwerten. Sie beachtet dabei die&nbsp;Bestimmungen des Eidgen&ouml;ssischen Datenschutzgesetzes und gibt die Daten nicht an Dritte weiter.<br />\r\n<br />\r\n<h2>\r\n	Haftungsausschluss</h2>\r\nDie Inhalte dieser Website beinhalten keinerlei Zusicherungen oder Garantien. {CONTACT_COMPANY}&nbsp;haftet in keinem Fall, inklusive Fahrl&auml;ssigkeit und Haftung gegen&uuml;ber Drittpersonen, f&uuml;r allf&auml;llige&nbsp;direkte, indirekte oder Folgesch&auml;den, welche als Folge des Gebrauchs von Informationen und&nbsp;Material aus dieser Website oder durch den Zugriff &uuml;ber Links auf andere Websites entstehen.<br />\r\n<br />\r\n<h2>\r\n	Copyright und andere Schutzrechte</h2>\r\nDer gesamte Inhalt der Website von {CONTACT_COMPANY} ist urheberrechtlich gesch&uuml;tzt. Alle Rechte&nbsp;sind vorbehalten. (Das Logo von {CONTACT_COMPANY} ist ein eingetragenes Markenzeichen). Durch den&nbsp;Zugriff auf die Website von {CONTACT_COMPANY} das Herunterladen oder Kopieren von Daten dieser&nbsp;Website oder Teilen davon, werden dem Benutzer keinerlei Rechte am Inhalt, an der Software,&nbsp;einer eingetragenen Marke oder an sonst einem Element der Website einger&auml;umt. Jegliche&nbsp;Modifikation, Reproduktion oder Benutzung der Website, des Logos von {CONTACT_COMPANY} oder Teile&nbsp;davon f&uuml;r einen &ouml;ffentlichen oder kommerziellen Zweck ist ohne vorherige schriftliche Zustimmung&nbsp;von {CONTACT_COMPANY} strikt untersagt.', 'Rechtliche Hinweise', '', '', '0', '', 'system', '319');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=46";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
