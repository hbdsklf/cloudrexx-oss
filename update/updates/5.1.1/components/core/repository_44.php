<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_44()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 114,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'114', '44', 'Verantwortlich f&uuml;r Inhalt, Konzeption &amp; Realisierung des Onlineportals:<br />\r\n<br />\r\n<strong>{CONTACT_COMPANY}</strong><br />\r\n{CONTACT_ADDRESS}<br />\r\n{CONTACT_ZIP} {CONTACT_PLACE}<br />\r\n{CONTACT_COUNTRY}<br />\r\n&nbsp;\r\n<h2>Gew&auml;hrleistung</h2>\r\nWir bem&uuml;hen uns um m&ouml;glichst korrekte und aktuelle Information. Die Inhalte wurden sorgf&auml;ltig erarbeitet.&nbsp;Trotzdem k&ouml;nnen Fehler auftreten. Autoren und Herausgeber &uuml;bernehmen&nbsp;keine Gew&auml;hr und Haftung f&uuml;r die Richtigkeit, Zuverl&auml;ssigkeit, Vollst&auml;ndigkeit und Aktualit&auml;t der Information. Die Verwendung der Website und der darin enthaltenen Informationen erfolgt auf eigene Gefahr. &Auml;nderungen k&ouml;nnen jederzeit vorgenommen werden.<br />\r\n&nbsp;\r\n<h2>Web Content Management System (CMS)</h2>\r\nDieser Internetauftritt basiert auf <a href=\"https://www.cloudrexx.com/\" target=\"_blank\">Cloudrexx</a>.', 'Impressum', '', '', '0', '', 'system', '321');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=44";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
