<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _updateModuleRepository_54()
{
    global $objDatabase;

    $arrModuleRepositoryPages = array(
		array(
                                    'id' => 127,
                                    'parid' => 0,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'127', '54', '{APPLICATION_DATA}', 'Nachrichten', 'inbox', '', '0', 'on', 'system', '147');
SQL
                                ),
		array(
                                    'id' => 128,
                                    'parid' => 127,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'128', '54', '{APPLICATION_DATA}', 'Neue Nachricht', '', '', '127', 'on', 'system', '148');
SQL
                                ),
		array(
                                    'id' => 129,
                                    'parid' => 127,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'129', '54', '{APPLICATION_DATA}', 'Benachrichtigung', 'notification', '', '127', '', 'system', '150');
SQL
                                ),
		array(
                                    'id' => 130,
                                    'parid' => 127,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'130', '54', '{APPLICATION_DATA}', 'Private Nachricht', 'show', '', '127', '', 'system', '152');
SQL
                                ),
		array(
                                    'id' => 131,
                                    'parid' => 127,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'131', '54', '{APPLICATION_DATA}', 'Postausgang', 'outbox', '', '127', 'on', 'system', '154');
SQL
                                ),
		array(
                                    'id' => 132,
                                    'parid' => 127,
                                    'query' => 'INSERT INTO `'.DBPREFIX.'module_repository` (`id`, `moduleid`, `content`, `title`, `cmd`, `expertmode`, `parid`, `displaystatus`, `username`, `displayorder`) VALUES ('.<<<SQL
'132', '54', '{APPLICATION_DATA}', 'Adressbuch', 'buddiesList', '', '127', 'on', 'system', '156');
SQL
                                )
	);

    $query = "DELETE FROM ".DBPREFIX."module_repository WHERE `moduleid`=54";
    if ($objDatabase->Execute($query) === false) {
        return _databaseError($query, $objDatabase->ErrorMsg());
    }

    $arrPageId = array();
    foreach ($arrModuleRepositoryPages as $arrPage) {
        $arrPage['query'] = str_replace(
            '[[PKG_MODULE_REPOSITORY_PAGE_PARID]]',
            (empty($arrPageId[$arrPage['parid']])
              ? 0 : $arrPageId[$arrPage['parid']]
            ),
            $arrPage['query']
        );

        if ($objDatabase->Execute($arrPage['query']) === false) {
            return _databaseError($arrPage['query'], $objDatabase->ErrorMsg());
        }
        $arrPageId[$arrPage['id']] = $objDatabase->Insert_ID();
    }
    return true;
}

?>
