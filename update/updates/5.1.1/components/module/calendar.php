<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _calendarUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_calendar', array(
                    'id' => array('type' => 'INT(11)', 'notnull' => true, 'primary' => true, 'auto_increment' => true),
                    'active' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'id'),
                    'catid' => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'active'),
                    'startdate' => array('type' => 'INT(14)', 'notnull' => false, 'after' => 'catid'),
                    'enddate' => array('type' => 'INT(14)', 'notnull' => false, 'after' => 'startdate'),
                    'priority' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '3', 'after' => 'enddate'),
                    'access' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'priority'),
                    'name' => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => '', 'after' => 'access'),
                    'comment' => array('type' => 'text', 'after' => 'name'),
                    'placeName' => array('type' => 'VARCHAR(255)', 'after' => 'comment'),
                    'link' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => 'http://', 'after' => 'placeName'),
                    'pic' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'link'),
                    'attachment' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'pic'),
                    'placeStreet' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'attachment'),
                    'placeZip' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'placeStreet'),
                    'placeCity' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'placeZip'),
                    'placeLink' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'placeCity'),
                    'placeMap' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'placeLink'),
                    'organizerName' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'placeMap'),
                    'organizerStreet' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'organizerName'),
                    'organizerZip' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'organizerStreet'),
                    'organizerPlace' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'organizerZip'),
                    'organizerMail' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'organizerPlace'),
                    'organizerLink' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'organizerMail'),
                    'key' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'organizerLink'),
                    'num' => array('type' => 'INT(5)', 'notnull' => true, 'default' => '0', 'after' => 'key'),
                    'mailTitle' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'num'),
                    'mailContent' => array('type' => 'text', 'after' => 'mailTitle'),
                    'registration' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'mailContent'),
                    'groups' => array('type' => 'text', 'after' => 'registration'),
                    'all_groups' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'groups'),
                    'public' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'all_groups'),
                    'notification' => array('type' => 'INT(1)', 'after' => 'public'),
                    'notification_address' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'notification'),
                    'series_status' => array('type' => 'TINYINT(4)', 'after' => 'notification_address'),
                    'series_type' => array('type' => 'INT(11)', 'after' => 'series_status'),
                    'series_pattern_count' => array('type' => 'INT(11)', 'after' => 'series_type'),
                    'series_pattern_weekday' => array('type' => 'VARCHAR(7)', 'after' => 'series_pattern_count'),
                    'series_pattern_day' => array('type' => 'INT(11)', 'after' => 'series_pattern_weekday'),
                    'series_pattern_week' => array('type' => 'INT(11)', 'after' => 'series_pattern_day'),
                    'series_pattern_month' => array('type' => 'INT(11)', 'after' => 'series_pattern_week'),
                    'series_pattern_type' => array('type' => 'INT(11)', 'after' => 'series_pattern_month'),
                    'series_pattern_dourance_type' => array('type' => 'INT(11)', 'after' => 'series_pattern_type'),
                    'series_pattern_end' => array('type' => 'INT(11)', 'after' => 'series_pattern_dourance_type'),
                    'series_pattern_begin' => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_end'),
                    'series_pattern_exceptions' => array('type' => 'longtext', 'after' => 'series_pattern_begin')
                ),
                array(
                    'name' => array('fields' => array('name', 'comment', 'placeName'), 'type' => 'FULLTEXT')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        //2.1.1


        $query = "SELECT status FROM " . DBPREFIX . "modules WHERE id='21'";
        $objResultCheck = $objDatabase->SelectLimit($query, 1);

        if ($objResultCheck !== false) {
            if ($objResultCheck->fields['status'] == 'y') {
                $calendarStatus = true;
            } else {
                $calendarStatus = false;
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        if ($calendarStatus) {
            $arrContentSites = array();

            $arrContentSites[0]['module'] = 'calendar';
            $arrContentSites[0]['cmd'] = '';
            $arrContentSites[1]['module'] = 'calendar';
            $arrContentSites[1]['cmd'] = 'eventlist';
            $arrContentSites[2]['module'] = 'calendar';
            $arrContentSites[2]['cmd'] = 'boxes';


            //insert new link placeholder in content, if module is active
            foreach ($arrContentSites as $key => $siteArray) {

                $module = $siteArray['module'];
                $cmd = $siteArray['cmd'];

                try {
                    \Cx\Lib\UpdateUtil::migrateContentPage(
                        $module,
                        $cmd,
                        '<a href="index.php?section=calendar&amp;cmd=event&amp;id={CALENDAR_ID}">{CALENDAR_TITLE}</a>',
                        '{CALENDAR_DETAIL_LINK}',
                        '3.0.0'
                    );
                } catch (\Cx\Lib\UpdateException $e) {
                    return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                }
            }


            try {
                \Cx\Lib\UpdateUtil::migrateContentPage(
                    'calendar',
                    'sign',
                    '<input type="hidden" name="id" value="{CALENDAR_NOTE_ID}" />',
                    '<input type="hidden" name="id" value="{CALENDAR_NOTE_ID}" /><input type="hidden" name="date" value="{CALENDAR_NOTE_DATE}" />',
                    '3.0.0'
                );
                \Cx\Lib\UpdateUtil::migrateContentPage(
                    'calendar',
                    'sign',
                    '<a href="index.php?section=calendar&amp;id={CALENDAR_NOTE_ID}">{TXT_CALENDAR_BACK}</a>',
                    '{CALENDAR_LINK_BACK}',
                    '3.0.0'
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        try {
            // delete obsolete table  contrexx_module_calendar_access
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX . 'module_calendar_access');

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_calendar_form_data', array(
                    'reg_id' => array('type' => 'INT(10)', 'notnull' => true, 'default' => '0'),
                    'field_id' => array('type' => 'INT(10)', 'notnull' => true, 'default' => '0'),
                    'data' => array('type' => 'TEXT', 'notnull' => true)
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_calendar_form_fields', array(
                    'id' => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'note_id' => array('type' => 'INT(10)', 'notnull' => true, 'default' => '0'),
                    'name' => array('type' => 'TEXT'),
                    'type' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0'),
                    'required' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0'),
                    'order' => array('type' => 'INT(3)', 'notnull' => true, 'default' => '0'),
                    'key' => array('type' => 'INT(7)', 'notnull' => true, 'default' => '0')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_calendar_registrations', array(
                    'id' => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'note_id' => array('type' => 'INT(7)', 'notnull' => true, 'default' => '0'),
                    'note_date' => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'note_id'),
                    'time' => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0'),
                    'host' => array('type' => 'VARCHAR(255)'),
                    'ip_address' => array('type' => 'VARCHAR(15)'),
                    'type' => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }



    // initialize calendar update system
    $CalendarUpdate = new CalendarUpdate();

    // migrate calendar to version 3.1
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
        $calendarUpdateFeedback = $CalendarUpdate->runUpdate31();
        if ($calendarUpdateFeedback !== true) {
            return $calendarUpdateFeedback;
        }
    }

    // migrate old content pages to new ones
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.1')) {
        $calendarUpdateFeedback = $CalendarUpdate->migrateContentPages();
        if ($calendarUpdateFeedback !== true) {
            return $calendarUpdateFeedback;
        }
    }

    // migrate settings tables
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.2.0')) {
        $calendarUpdateFeedback = $CalendarUpdate->migrateSettings();
        if ($calendarUpdateFeedback !== true) {
            return $calendarUpdateFeedback;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.2.0')) {
        try {
            // in version 3.2.0 the type of startdate and enddate have changed to timestamp
            // to prevent any data loss, we have to ensure that this conversion will be done only once
            // therefore, we will run the following table command only in case the datatypes of those fields have not net been migrated
            if (!\Cx\Lib\UpdateUtil::check_column_type(DBPREFIX.'module_calendar_event', 'startdate', 'timestamp')) {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_calendar_event',
                    array(
                        'id'                                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'type'                               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                        'startdate'                          => array('type' => 'INT(14)', 'notnull' => false, 'after' => 'type'),
                        'enddate'                            => array('type' => 'INT(14)', 'notnull' => false, 'after' => 'startdate'),
                        // add startdate_timestamp and enddate_timestamp to migrate the datatypes of startdate and enddate
                        'startdate_timestamp'                => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'enddate'),
                        'enddate_timestamp'                  => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'startdate_timestamp'),
                        'use_custom_date_display'            => array('type' => 'TINYINT(1)', 'after' => 'enddate_timestamp'),
                        'showStartDateList'                  => array('type' => 'INT(1)', 'after' => 'use_custom_date_display'),
                        'showEndDateList'                    => array('type' => 'INT(1)', 'after' => 'showStartDateList'),
                        'showStartTimeList'                  => array('type' => 'INT(1)', 'after' => 'showEndDateList'),
                        'showEndTimeList'                    => array('type' => 'INT(1)', 'after' => 'showStartTimeList'),
                        'showTimeTypeList'                   => array('type' => 'INT(1)', 'after' => 'showEndTimeList'),
                        'showStartDateDetail'                => array('type' => 'INT(1)', 'after' => 'showTimeTypeList'),
                        'showEndDateDetail'                  => array('type' => 'INT(1)', 'after' => 'showStartDateDetail'),
                        'showStartTimeDetail'                => array('type' => 'INT(1)', 'after' => 'showEndDateDetail'),
                        'showEndTimeDetail'                  => array('type' => 'INT(1)', 'after' => 'showStartTimeDetail'),
                        'showTimeTypeDetail'                 => array('type' => 'INT(1)', 'after' => 'showEndTimeDetail'),
                        'google'                             => array('type' => 'INT(11)', 'after' => 'showTimeTypeDetail'),
                        'access'                             => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'google'),
                        'priority'                           => array('type' => 'INT(1)', 'notnull' => true, 'default' => '3', 'after' => 'access'),
                        'price'                              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'priority'),
                        'link'                               => array('type' => 'VARCHAR(255)', 'after' => 'price'),
                        'pic'                                => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'link'),
                        'attach'                             => array('type' => 'VARCHAR(255)', 'after' => 'pic'),
                        'place_mediadir_id'                  => array('type' => 'INT(11)', 'after' => 'attach'),
                        'catid'                              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'place_mediadir_id'),
                        'show_in'                            => array('type' => 'VARCHAR(255)', 'after' => 'catid'),
                        'invited_groups'                     => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'show_in'),
                        'invited_crm_groups'                 => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'invited_groups'),
                        'excluded_crm_groups'                => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'invited_crm_groups'),
                        'invited_mails'                      => array('type' => 'mediumtext', 'notnull' => false, 'after' => 'excluded_crm_groups'),
                        'invitation_sent'                    => array('type' => 'INT(1)', 'after' => 'invited_mails'),
                        'invitation_email_template'          => array('type' => 'VARCHAR(255)', 'after' => 'invitation_sent'),
                        'registration'                       => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'invitation_email_template'),
                        'registration_form'                  => array('type' => 'INT(11)', 'after' => 'registration'),
                        'registration_num'                   => array('type' => 'VARCHAR(45)', 'notnull' => false, 'after' => 'registration_form'),
                        'registration_notification'          => array('type' => 'VARCHAR(1024)', 'notnull' => false, 'after' => 'registration_num'),
                        'email_template'                     => array('type' => 'VARCHAR(255)', 'after' => 'registration_notification'),
                        'registration_external_link'         => array('type' => 'text', 'notnull' => true, 'after' => 'email_template'),
                        'registration_external_fully_booked' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'registration_external_link'),
                        'ticket_sales'                       => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'registration_external_fully_booked'),
                        'num_seating'                        => array('type' => 'text', 'after' => 'ticket_sales'),
                        'series_status'                      => array('type' => 'TINYINT(4)', 'notnull' => true, 'default' => '0', 'after' => 'num_seating'),
                        'independent_series'                 => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'series_status'),
                        'series_type'                        => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'independent_series'),
                        'series_pattern_count'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_type'),
                        'series_pattern_weekday'             => array('type' => 'VARCHAR(7)', 'after' => 'series_pattern_count'),
                        'series_pattern_day'                 => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_weekday'),
                        'series_pattern_week'                => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_day'),
                        'series_pattern_month'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_week'),
                        'series_pattern_type'                => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_month'),
                        'series_pattern_dourance_type'       => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_type'),
                        'series_pattern_end'                 => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_dourance_type'),
                        'series_pattern_end_date'            => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'series_pattern_end'),
                        'series_pattern_begin'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_end_date'),
                        'series_pattern_exceptions'          => array('type' => 'longtext', 'after' => 'series_pattern_begin'),
                        'series_additional_recurrences'      => array('type' => 'longtext', 'after' => 'series_pattern_exceptions'),
                        'status'                             => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'series_additional_recurrences'),
                        'confirmed'                          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'status'),
                        'show_detail_view'                   => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'confirmed'),
                        'author'                             => array('type' => 'VARCHAR(255)', 'after' => 'show_detail_view'),
                        'all_day'                            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'author'),
                        'location_type'                      => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'all_day'),
                        'place'                              => array('type' => 'VARCHAR(255)', 'after' => 'location_type'),
                        'place_id'                           => array('type' => 'INT(11)', 'after' => 'place'),
                        'place_street'                       => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_id'),
                        'place_zip'                          => array('type' => 'VARCHAR(10)', 'notnull' => false, 'after' => 'place_street'),
                        'place_city'                         => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_zip'),
                        'place_country'                      => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_city'),
                        'place_website'                      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'place_country'),
                        'place_link'                         => array('type' => 'VARCHAR(255)', 'after' => 'place_website'),
                        'place_phone'                        => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'place_link'),
                        'place_map'                          => array('type' => 'VARCHAR(255)', 'after' => 'place_phone'),
                        'host_type'                          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'place_map'),
                        'org_name'                           => array('type' => 'VARCHAR(255)', 'after' => 'host_type'),
                        'org_street'                         => array('type' => 'VARCHAR(255)', 'after' => 'org_name'),
                        'org_zip'                            => array('type' => 'VARCHAR(10)', 'after' => 'org_street'),
                        'org_city'                           => array('type' => 'VARCHAR(255)', 'after' => 'org_zip'),
                        'org_country'                        => array('type' => 'VARCHAR(255)', 'after' => 'org_city'),
                        'org_website'                        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'org_country'),
                        'org_link'                           => array('type' => 'VARCHAR(255)', 'after' => 'org_website'),
                        'org_phone'                          => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'org_link'),
                        'org_email'                          => array('type' => 'VARCHAR(255)', 'after' => 'org_phone'),
                        'host_mediadir_id'                   => array('type' => 'INT(11)', 'after' => 'org_email')
                    ),
                    array(
                        'fk_contrexx_module_calendar_notes_contrexx_module_calendar_ca1' => array('fields' => array('catid'))
                    )
                );
            }

            // migrate dates to timestamp
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_calendar_event', 'startdate_timestamp')) {
                \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_event` SET `startdate_timestamp` = FROM_UNIXTIME(`startdate`)');
                \Cx\Lib\UpdateUtil::sql("ALTER TABLE `".DBPREFIX."module_calendar_event` CHANGE `startdate` `startdate` timestamp NULL DEFAULT '0000-00-00 00:00:00'");
                \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_event` SET `startdate` = `startdate_timestamp`');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_calendar_event` DROP COLUMN `startdate_timestamp`');
            }
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_calendar_event', 'enddate_timestamp')) {
                \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_event` SET `enddate_timestamp` = FROM_UNIXTIME(`enddate`)');
                \Cx\Lib\UpdateUtil::sql("ALTER TABLE `".DBPREFIX."module_calendar_event` CHANGE `enddate` `enddate` timestamp NULL DEFAULT '0000-00-00 00:00:00'");
                \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_event` SET `enddate` = `enddate_timestamp`');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_calendar_event` DROP COLUMN `enddate_timestamp`');
            }

            \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_event` SET `series_pattern_end_date` = FROM_UNIXTIME(`series_pattern_end`)');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // update calendar data to version 3.2.0
        $languages = FWLanguage::getLanguageArray();

        try {
            $result = \Cx\Lib\UpdateUtil::sql('SELECT `id`, `invitation_email_template`, `email_template` FROM `'.DBPREFIX.'module_calendar_event`');
            if ($result && $result->RecordCount() > 0) {
                while (!$result->EOF) {
                    // if the event has been already migrated, continue
                    if (intval($result->fields['invitation_email_template']) != $result->fields['invitation_email_template']) {
                        $result->MoveNext();
                        continue;
                    }
                    $invitationEmailTemplate = array();
                    $emailTemplate = array();
                    foreach ($languages as $langId => $langData) {
                        $invitationEmailTemplate[$langId] = $result->fields['invitation_email_template'];
                        $emailTemplate[$langId] = $result->fields['email_template'];
                    }
                    $invitationEmailTemplate = json_encode($invitationEmailTemplate);
                    $emailTemplate = json_encode($emailTemplate);
                    \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_event` SET
                                            `invitation_email_template`=\''.contrexx_raw2db($invitationEmailTemplate).'\',
                                            `email_template`=\''.contrexx_raw2db($emailTemplate).'\' WHERE `id`='.intval($result->fields['id']));
                    $result->MoveNext();
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    // finally, define the definite structure for version 5.0
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {

        if (!isset($_SESSION['contrexx_update']['calendar'])) {
            $_SESSION['contrexx_update']['calendar'] = array();
        }

        try {
            if (empty($_SESSION['contrexx_update']['calendar']['places_migrated'])) {

                // try to drop index (if it exists at all)
                try {
                    \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_calendar_event_field` DROP INDEX `eventIndex`');
                } catch (\Exception $e) {}

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_calendar_event_field',
                    array(
                        'event_id' => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                        'lang_id' => array('type' => 'VARCHAR(225)', 'notnull' => false, 'after' => 'event_id'),
                        'title' => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'lang_id'),
                        'teaser' => array('type' => 'text', 'notnull' => false, 'after' => 'title'),
                        'description' => array('type' => 'mediumtext', 'notnull' => false, 'after' => 'teaser'),
                        'redirect' => array('type' => 'VARCHAR(255)', 'after' => 'description'),
                        'place' => array('type' => 'VARCHAR(255)', 'after' => 'redirect'),
                        'place_city' => array('type' => 'VARCHAR(255)', 'after' => 'place'),
                        'place_country' => array('type' => 'VARCHAR(255)', 'after' => 'place_city'),
                        'org_name' => array('type' => 'VARCHAR(255)', 'after' => 'place_country'),
                        'org_city' => array('type' => 'VARCHAR(255)', 'after' => 'org_name'),
                        'org_country' => array('type' => 'VARCHAR(255)', 'after' => 'org_city')
                    ),
                    array(
                        'lang_field' => array('fields' => array('title')),
                        'event_id'   => array('fields' => array('event_id')),
                        'eventIndex' => array('fields' => array('title', 'teaser', 'description'), 'type' => 'FULLTEXT')
                    ),
                    'InnoDB'
                );

            \Cx\Lib\UpdateUtil::sql(
                'UPDATE `'.DBPREFIX.'module_calendar_event_field` AS field 
                    INNER JOIN `'.DBPREFIX.'module_calendar_event` AS event
                    ON event.id = field.event_id
                    SET field.`place`           = event.`place`,
                        field.`place_city`      = event.`place_city`,
                        field.`place_country`   = event.`place_country`,
                        field.`org_name`        = event.`org_name`,
                        field.`org_city`        = event.`org_city`,
                        field.`org_country`     = event.`org_country`'
            );

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_calendar_event',
                    array(
                        'id'                                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'type'                               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                        'startdate'                          => array('type' => 'timestamp', 'notnull' => false, 'default' => '0000-00-00 00:00:00', 'after' => 'type'),
                        'enddate'                            => array('type' => 'timestamp', 'notnull' => false, 'default' => '0000-00-00 00:00:00', 'after' => 'startdate'),
                        'use_custom_date_display'            => array('type' => 'TINYINT(1)', 'after' => 'enddate'),
                        'showStartDateList'                  => array('type' => 'INT(1)', 'after' => 'use_custom_date_display'),
                        'showEndDateList'                    => array('type' => 'INT(1)', 'after' => 'showStartDateList'),
                        'showStartTimeList'                  => array('type' => 'INT(1)', 'after' => 'showEndDateList'),
                        'showEndTimeList'                    => array('type' => 'INT(1)', 'after' => 'showStartTimeList'),
                        'showTimeTypeList'                   => array('type' => 'INT(1)', 'after' => 'showEndTimeList'),
                        'showStartDateDetail'                => array('type' => 'INT(1)', 'after' => 'showTimeTypeList'),
                        'showEndDateDetail'                  => array('type' => 'INT(1)', 'after' => 'showStartDateDetail'),
                        'showStartTimeDetail'                => array('type' => 'INT(1)', 'after' => 'showEndDateDetail'),
                        'showEndTimeDetail'                  => array('type' => 'INT(1)', 'after' => 'showStartTimeDetail'),
                        'showTimeTypeDetail'                 => array('type' => 'INT(1)', 'after' => 'showEndTimeDetail'),
                        'google'                             => array('type' => 'INT(11)', 'after' => 'showTimeTypeDetail'),
                        'access'                             => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'google'),
                        'priority'                           => array('type' => 'INT(1)', 'notnull' => true, 'default' => '3', 'after' => 'access'),
                        'price'                              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'priority'),
                        'link'                               => array('type' => 'VARCHAR(255)', 'after' => 'price'),
                        'pic'                                => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'link'),
                        'attach'                             => array('type' => 'VARCHAR(255)', 'after' => 'pic'),
                        'place_mediadir_id'                  => array('type' => 'INT(11)', 'after' => 'attach'),
                        'catid'                              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'place_mediadir_id'),
                        'show_in'                            => array('type' => 'VARCHAR(255)', 'after' => 'catid'),
                        'invited_groups'                     => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'show_in'),
                        'invited_crm_groups'                 => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'invited_groups'),
                        'excluded_crm_groups'                => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'invited_crm_groups'),
                        'invited_mails'                      => array('type' => 'mediumtext', 'notnull' => false, 'after' => 'excluded_crm_groups'),
                        'invitation_sent'                    => array('type' => 'INT(1)', 'after' => 'invited_mails'),
                        'invitation_email_template'          => array('type' => 'VARCHAR(255)', 'after' => 'invitation_sent'),
                        'registration'                       => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'invitation_email_template'),
                        'registration_form'                  => array('type' => 'INT(11)', 'after' => 'registration'),
                        'registration_num'                   => array('type' => 'VARCHAR(45)', 'notnull' => false, 'after' => 'registration_form'),
                        'registration_notification'          => array('type' => 'VARCHAR(1024)', 'notnull' => false, 'after' => 'registration_num'),
                        'email_template'                     => array('type' => 'VARCHAR(255)', 'after' => 'registration_notification'),
                        'registration_external_link'         => array('type' => 'text', 'notnull' => true, 'after' => 'email_template'),
                        'registration_external_fully_booked' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'registration_external_link'),
                        'ticket_sales'                       => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'registration_external_fully_booked'),
                        'num_seating'                        => array('type' => 'text', 'after' => 'ticket_sales'),
                        'series_status'                      => array('type' => 'TINYINT(4)', 'notnull' => true, 'default' => '0', 'after' => 'num_seating'),
                        'independent_series'                 => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'series_status'),
                        'series_type'                        => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'independent_series'),
                        'series_pattern_count'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_type'),
                        'series_pattern_weekday'             => array('type' => 'VARCHAR(7)', 'after' => 'series_pattern_count'),
                        'series_pattern_day'                 => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_weekday'),
                        'series_pattern_week'                => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_day'),
                        'series_pattern_month'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_week'),
                        'series_pattern_type'                => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_month'),
                        'series_pattern_dourance_type'       => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_type'),
                        'series_pattern_end'                 => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_dourance_type'),
                        'series_pattern_end_date'            => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'series_pattern_end'),
                        'series_pattern_begin'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_end_date'),
                        'series_pattern_exceptions'          => array('type' => 'longtext', 'after' => 'series_pattern_begin'),
                        'series_additional_recurrences'      => array('type' => 'longtext', 'after' => 'series_pattern_exceptions'),
                        'status'                             => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'series_additional_recurrences'),
                        'confirmed'                          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'status'),
                        'show_detail_view'                   => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'confirmed'),
                        'author'                             => array('type' => 'VARCHAR(255)', 'after' => 'show_detail_view'),
                        'all_day'                            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'author'),
                        'location_type'                      => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'all_day'),
                        //'place'                              => array('type' => 'VARCHAR(255)', 'after' => 'location_type'),
                        'place_id'                           => array('type' => 'INT(11)', 'after' => 'location_type'),
                        'place_street'                       => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_id'),
                        'place_zip'                          => array('type' => 'VARCHAR(10)', 'notnull' => false, 'after' => 'place_street'),
                        //'place_city'                         => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_zip'),
                        //'place_country'                      => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_city'),
                        'place_website'                      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'place_zip'),
                        'place_link'                         => array('type' => 'VARCHAR(255)', 'after' => 'place_website'),
                        'place_phone'                        => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'place_link'),
                        'place_map'                          => array('type' => 'VARCHAR(255)', 'after' => 'place_phone'),
                        'host_type'                          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'place_map'),
                        //'org_name'                           => array('type' => 'VARCHAR(255)', 'after' => 'host_type'),
                        'org_street'                         => array('type' => 'VARCHAR(255)', 'after' => 'host_type'),
                        'org_zip'                            => array('type' => 'VARCHAR(10)', 'after' => 'org_street'),
                        //'org_city'                           => array('type' => 'VARCHAR(255)', 'after' => 'org_zip'),
                        //'org_country'                        => array('type' => 'VARCHAR(255)', 'after' => 'org_city'),
                        'org_website'                        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'org_zip'),
                        'org_link'                           => array('type' => 'VARCHAR(255)', 'after' => 'org_website'),
                        'org_phone'                          => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'org_link'),
                        'org_email'                          => array('type' => 'VARCHAR(255)', 'after' => 'org_phone'),
                        'host_mediadir_id'                   => array('type' => 'INT(11)', 'after' => 'org_email')
                    ),
                    array(
                        'fk_contrexx_module_calendar_notes_contrexx_module_calendar_ca1' => array('fields' => array('catid'))
                    )
                );
                $_SESSION['contrexx_update']['calendar']['places_migrated'] = true;
            }

            \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_calendar_event` SET `independent_series` = 0");

        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    // fix settings
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            // add missing section
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (19,1,4,'location_host','TXT_CALENDAR_EVENT_LOCATION')");

            // add missing settings
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES ('65','5','frontendPastEvents','TXT_CALENDAR_FRONTEND_PAST_EVENTS','0','','5','TXT_CALENDAR_FRONTEND_PAST_EVENTS_DAY,TXT_CALENDAR_FRONTEND_PAST_EVENTS_END','',15)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (62,19,'placeDataForm','',0,'',5,'','getPlaceDataDorpdown',8)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (20,19,'placeData','TXT_CALENDAR_PLACE_DATA',1,'TXT_CALENDAR_PLACE_DATA_STATUS_INFO',3,'TXT_CALENDAR_PLACE_DATA_DEFAULT,TXT_CALENDAR_PLACE_DATA_FROM_MEDIADIR,TXT_CALENDAR_PLACE_DATA_FROM_BOTH','',7)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (63,19,'placeDataHost','TXT_CALENDAR_PLACE_DATA_HOST',1,'TXT_CALENDAR_PLACE_DATA_STATUS_INFO',3,'TXT_CALENDAR_PLACE_DATA_DEFAULT,TXT_CALENDAR_PLACE_DATA_FROM_MEDIADIR,TXT_CALENDAR_PLACE_DATA_FROM_BOTH','',9)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (64,19,'placeDataHostForm','',0,'',5,'','getPlaceDataDorpdown',10)");

            // fix setting order
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_calendar_settings` SET `order`= 8 WHERE `name` = 'showStartTimeDetail' AND `section_id` = 17");
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_calendar_settings` SET `order`= 9 WHERE `name` = 'showEndTimeDetail' AND `section_id` = 17");
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_calendar_settings` SET `order`= 8 WHERE `name` = 'showStartTimeList' AND `section_id` = 16");
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_calendar_settings` SET `order`= 9 WHERE `name` = 'showEndTimeList' AND `section_id` = 16");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_calendar_invite',
                array(
                    'id'                 => array('type' => 'int', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'event_id'           => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'id'),
                    'date'               => array('type' => 'bigint', 'unsigned' => true, 'notnull' => true, 'after' => 'event_id'),
                    'invitee_type'       => array('type' => 'ENUM(\'-\', \'AccessUser\',\'CrmContact\')', 'notnull' => true, 'after' => 'date'),
                    'invitee_id'         => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'invitee_type'),
                    'email'              => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'invitee_id'),
                    'token'              => array('type' => 'VARCHAR(32)', 'notnull' => true, 'after' => 'email')
                ),
                array(),
                'InnoDB'
            );
            \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_calendar_host` CHANGE `key` `key` varchar(32) NOT NULL');
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_calendar_registration',
                array(
                    'id'                 => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'event_id'           => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'id'),
                    'date'               => array('type' => 'INT(15)', 'notnull' => true, 'after' => 'event_id'),
                    'submission_date'    => array('type' => 'timestamp', 'notnull' => false, 'default' => '0000-00-00 00:00:00', 'after' => 'date'),
                    'type'               => array('type' => 'INT(1)', 'notnull' => true, 'after' => 'submission_date'),
                    'invite_id'          => array('type' => 'int', 'notnull' => false, 'after' => 'type'),
                    'key'                => array('type' => 'VARCHAR(45)', 'notnull' => true, 'after' => 'invite_id'),
                    'user_id'            => array('type' => 'INT(7)', 'notnull' => true, 'after' => 'key'),
                    'lang_id'            => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'user_id'),
                    'export'             => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'lang_id'),
                    'payment_method'     => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'export'),
                    'paid'               => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'payment_method')
                ),
                array(),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            $result = \Cx\Lib\UpdateUtil::sql("SELECT 1 FROM `".DBPREFIX."module_calendar_mail_action` WHERE `name` NOT IN ('invitationTemplate', 'confirmationRegistration', 'notificationRegistration', 'notificationNewEntryFE')");
            if ($result->RecordCount() > 0) {
                // TODO: implement proper user interaction
                return false;
            }
                
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX.'module_calendar_mail_action');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_calendar_events_categories',
                array(
                    'event_id'       => array('type' => 'INT(11)', 'notnull' => true, 'primary' => true),
                    'category_id'    => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'event_id', 'primary' => true)
                ),
                array(
                    'category_id'    => array('fields' => array('category_id'))
                ),
                'InnoDB'
            );
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_calendar_event', 'catid')) {
                \Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_events_categories` (event_id, category_id) SELECT id, catid FROM `'.DBPREFIX.'module_calendar_event` WHERE catid > 0');
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_calendar_event` DROP `catid`');
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.2')) {
        try {
            \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_calendar_settings` SET `info` = \'TXT_CALENDAR_NUM_EVENTS_ENTRANCE_INFO\' WHERE `name` = \'numEntrance\'');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    // migrate image path
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            //following queries for changing the path from images/calendar into images/Calendar
            \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_calendar_event` SET `pic` = REPLACE(`pic`, 'images/calendar', 'images/Calendar'),
                                     `attach` = REPLACE(`attach`, 'images/calendar', 'images/Calendar'),
                                     `place_map` = REPLACE(`place_map`, 'images/calendar', 'images/Calendar')
                                     WHERE `pic` LIKE ('" . ASCMS_PATH_OFFSET . "/images/calendar%') ");

            //Update script for moving the folder
            $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
            $sourceImagePath = $imagePath . '/calendar';
            $targetImagePath = $imagePath . '/Calendar';
            try {
                \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
            } catch (\Exception $e) {
                \DBG::log($e->getMessage());
                setUpdateMsg(sprintf(
                    $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                    $sourceImagePath, $targetImagePath
                ));
                return false;
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'attach'        => 'module_calendar_event',
            'pic'           => 'module_calendar_event',
            'place_map'     => 'module_calendar_event',
            'description'   => 'module_calendar_event_field',
            'content_html'  => 'module_calendar_mail',
            'content_text'  => 'module_calendar_mail',
        );
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Veranstaltungskalender (Calendar)'
            ));
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (66, '5', 'constrainAdditionalRecurrences', 'TXT_CALENDAR_CONSTRAIN_ADDITIONAL_RECURRENCES', '1', 'TXT_CALENDAR_CONSTRAIN_ADDITIONAL_RECURRENCES_INFO', '3', 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', '8')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_calendar_registration_form_field_name',
                array(
                    'field_id' => array('type' => 'INT(7)', 'primary' => true),
                    'form_id' => array('type' => 'INT(11)', 'after' => 'field_id', 'primary' => true),
                    'lang_id' => array('type' => 'INT(1)', 'after' => 'form_id', 'primary' => true),
                    'name' => array('type' => 'VARCHAR(255)', 'after' => 'lang_id'),
                    'default' => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'name')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_calendar_registration_form_field_value',
                array(
                    'reg_id' => array('type' => 'INT(7)', 'primary' => true),
                    'field_id' => array('type' => 'INT(7)', 'after' => 'reg_id', 'primary' => true),
                    'value' => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'field_id')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.2')) {
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (67, '5', 'seriesDouranceType', 'TXT_CALENDAR_DEFAULT_DOURANCE_TYPE', '0', '', '5', 'TXT_CALENDAR_SERIES_PATTERN_NO_ENDDATE,TXT_CALENDAR_SERIES_PATTERN_END_AFTER_X_OCCURRENCES,TXT_CALENDAR_SERIES_PATTERN_END_BY_DATE', '', '11')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (68, '5', 'seriesDouranceEvents', 'TXT_CALENDAR_DEFAULT_OCCURRENCES_COUNT', '5', 'TXT_CALENDAR_DEFAULT_OCCURRENCES_COUNT_TOOLTIP', '1', '', '', '12')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (69, '5', 'backendSortOrder', 'TXT_CALENDAR_BACKEND_SORT_ORDER', '0', '', '5', 'TXT_CALENDAR_ASC,TXT_CALENDAR_DESC', '', '13')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (70, '5', 'useWaitlist', 'TXT_CALENDAR_USE_WAITLIST', '2', 'TXT_CALENDAR_USE_WAITLIST_TOOLTIP', '3', 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', '16')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}

class CalendarUpdate
{
    protected $db;
    protected $categoryLanguages;

    public function __construct() {
        global $objDatabase;
        $this->db = $objDatabase;

        if (!isset($_SESSION['contrexx_update']['calendar'])) {
            $_SESSION['contrexx_update']['calendar'] = array();
        }

        // set constant for all table names

        // old tables (contrexx version < 3.1)
        define('CALENDAR_OLD_CATEGORY_TABLE', DBPREFIX . 'module_calendar_categories');
        define('CALENDAR_OLD_SETTINGS_TABLE', DBPREFIX . 'module_calendar_settings');
        define('CALENDAR_OLD_EVENT_TABLE', DBPREFIX . 'module_calendar');
        define('CALENDAR_OLD_REGISTRATIONS_TABLE', DBPREFIX . 'module_calendar_registrations');
        define('CALENDAR_OLD_FORM_FIELD_TABLE', DBPREFIX . 'module_calendar_form_fields');
        define('CALENDAR_OLD_FORM_DATA_TABLE', DBPREFIX . 'module_calendar_form_data');


        // new tables (contrexx version >= 3.1)
        define('CALENDAR_NEW_CATEGORY_TABLE', DBPREFIX . 'module_calendar_category');
        define('CALENDAR_NEW_CATEGORY_NAME_TABLE', DBPREFIX . 'module_calendar_category_name');

        define('CALENDAR_NEW_HOST_TABLE', DBPREFIX . 'module_calendar_host');
        define('CALENDAR_NEW_REL_HOST_EVENT_TABLE', DBPREFIX . 'module_calendar_rel_event_host');

        define('CALENDAR_NEW_SETTINGS_TABLE', DBPREFIX . 'module_calendar_settings');
        define('CALENDAR_NEW_SETTINGS_SECTION_TABLE', DBPREFIX . 'module_calendar_settings_section');

        define('CALENDAR_NEW_MAIL_TABLE', DBPREFIX . 'module_calendar_mail');
        define('CALENDAR_NEW_MAIL_ACTION_TABLE', DBPREFIX . 'module_calendar_mail_action');

        define('CALENDAR_NEW_EVENT_TABLE', DBPREFIX . 'module_calendar_event');
        define('CALENDAR_NEW_EVENT_FIELD_TABLE', DBPREFIX . 'module_calendar_event_field');

        define('CALENDAR_NEW_REGISTRATION_TABLE', DBPREFIX . 'module_calendar_registration');
        define('CALENDAR_NEW_REGISTRATION_FORM_TABLE', DBPREFIX . 'module_calendar_registration_form');
        define('CALENDAR_NEW_REGISTRATION_FORM_FIELD_TABLE', DBPREFIX . 'module_calendar_registration_form_field');
        define('CALENDAR_NEW_REGISTRATION_FORM_FIELD_NAME_TABLE', DBPREFIX . 'module_calendar_registration_form_field_name');
        define('CALENDAR_NEW_REGISTRATION_FORM_FIELD_VALUE_TABLE', DBPREFIX . 'module_calendar_registration_form_field_value');
    }

    public function runUpdate31() {
        // create new tables
        if (empty($_SESSION['contrexx_update']['calendar']['tables_created'])) {
            $createTablesState = $this->createNewTables();
            if ($createTablesState !== true || !checkMemoryLimit() || !checkTimeoutLimit()) {
                return ($createTablesState === true ? 'timeout' : $createTablesState);
            }
            $_SESSION['contrexx_update']['calendar']['tables_created'] = true;
        }

        // insert demo data for mail tables
        if (empty($_SESSION['contrexx_update']['calendar']['mail_demo_data'])) {
            $mailDemoData = $this->insertMailDemoData();
            if ($mailDemoData !== true || !checkMemoryLimit() || !checkTimeoutLimit()) {
                return ($mailDemoData === true ? 'timeout' : $mailDemoData);
            }
            $_SESSION['contrexx_update']['calendar']['mail_demo_data'] = true;
        }

        // migrate categories
        if (empty($_SESSION['contrexx_update']['calendar']['categories']) || $_SESSION['contrexx_update']['calendar']['categories'] !== true) {
            $migrateCategories = $this->migrateCategories();
            if ($migrateCategories !== true || !checkMemoryLimit() || !checkTimeoutLimit()) {
                return ($migrateCategories === true ? 'timeout' : $migrateCategories);
            }
            $_SESSION['contrexx_update']['calendar']['categories'] = true;
        }

        // write language ids in relation to category id in an array
        // this array is used for the events, so we know in which language events of these categories
        // are available
        $this->categoryLanguages = $this->getCategoryLanguages();

        // migrate events
        if (empty($_SESSION['contrexx_update']['calendar']['events']) || $_SESSION['contrexx_update']['calendar']['events'] !== true) {
            $migrateEvents = $this->migrateEvents();
            if ($migrateEvents !== true || !checkMemoryLimit() || !checkTimeoutLimit()) {
                return ($migrateEvents === true ? 'timeout' : $migrateEvents);
            }
            $_SESSION['contrexx_update']['calendar']['events'] = true;
        }

        // drop old tables
        if (empty($_SESSION['contrexx_update']['calendar']['migration_completed']) == true) {
            $dropTables = $this->dropOldTables();
            if ($dropTables !== true || !checkMemoryLimit() || !checkTimeoutLimit()) {
                return ($dropTables === true ? 'timeout' : $dropTables);
            }
            $_SESSION['contrexx_update']['calendar']['migration_completed'] = true;
        }

        // add access to access ids 164/165/166/167 for user groups which had access to access id 16
        try {
            $result = \Cx\Lib\UpdateUtil::sql("SELECT `group_id` FROM `" . DBPREFIX . "access_group_static_ids` WHERE access_id = 16 GROUP BY group_id");
            if ($result !== false) {
                while (!$result->EOF) {
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `" . DBPREFIX . "access_group_static_ids` (`access_id`, `group_id`)
                                                VALUES
                                                (165, " . intval($result->fields['group_id']) . "),
                                                (180, " . intval($result->fields['group_id']) . "),
                                                (181, " . intval($result->fields['group_id']) . "),
                                                (182, " . intval($result->fields['group_id']) . ")
                                            ");
                    $result->MoveNext();
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        return true;
    }


    /**
     * create new tables
     * @return string|boolean
     */
    protected function createNewTables()
    {
        try {
            // host table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_HOST_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_HOST_TABLE,
                    array(
                        'id' => array('type' => 'INT(1)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'title' => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                        'uri' => array('type' => 'mediumtext', 'after' => 'title'),
                        'cat_id' => array('type' => 'INT(11)', 'after' => 'uri'),
                        'key' => array('type' => 'VARCHAR(40)', 'after' => 'cat_id'),
                        'confirmed' => array('type' => 'INT(11)', 'after' => 'key'),
                        'status' => array('type' => 'INT(1)', 'after' => 'confirmed')
                    ),
                    array(
                        'fk_contrexx_module_calendar_shared_hosts_contrexx_module_cale1' => array('fields' => array('cat_id'))
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // relation table for host - event
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_REL_HOST_EVENT_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_REL_HOST_EVENT_TABLE,
                    array(
                        'host_id' => array('type' => 'INT(11)'),
                        'event_id' => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'host_id')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // registration table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_REGISTRATION_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_REGISTRATION_TABLE,
                    array(
                        'id' => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'event_id' => array('type' => 'INT(7)', 'after' => 'id'),
                        'date' => array('type' => 'INT(15)', 'after' => 'event_id'),
                        'submission_date' => array('type' => 'TIMESTAMP', 'notnull' => false, 'default' => '0000-00-00 00:00:00', 'after' => 'date'),
                        'type' => array('type' => 'INT(1)', 'after' => 'submission_date'),
                        'key' => array('type' => 'VARCHAR(45)', 'after' => 'type'),
                        'user_id' => array('type' => 'INT(7)', 'after' => 'key'),
                        'lang_id' => array('type' => 'INT(11)', 'after' => 'user_id'),
                        'export' => array('type' => 'INT(11)', 'after' => 'lang_id'),
                        'payment_method' => array('type' => 'INT(11)', 'after' => 'export'),
                        'paid' => array('type' => 'INT(11)', 'after' => 'payment_method')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // registration form table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_REGISTRATION_FORM_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_REGISTRATION_FORM_TABLE,
                    array(
                        'id' => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'status' => array('type' => 'INT(11)', 'after' => 'id'),
                        'order' => array('type' => 'INT(11)', 'after' => 'status'),
                        'title' => array('type' => 'VARCHAR(255)', 'after' => 'order')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // registration form field table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_REGISTRATION_FORM_FIELD_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_calendar_registration_form_field',
                    array(
                        'id' => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'form' => array('type' => 'INT(11)', 'after' => 'id'),
                        'type' => array('type' => 'ENUM(\'inputtext\',\'textarea\',\'select\',\'radio\',\'checkbox\',\'mail\',\'seating\',\'agb\',\'salutation\',\'firstname\',\'lastname\',\'selectBillingAddress\',\'fieldset\')', 'after' => 'form'),
                        'required' => array('type' => 'INT(1)', 'after' => 'type'),
                        'order' => array('type' => 'INT(3)', 'after' => 'required'),
                        'affiliation' => array('type' => 'VARCHAR(45)', 'after' => 'order')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // registration form field name table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_REGISTRATION_FORM_FIELD_NAME_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_REGISTRATION_FORM_FIELD_NAME_TABLE,
                    array(
                        'field_id' => array('type' => 'INT(7)', 'primary' => true),
                        'form_id' => array('type' => 'INT(11)', 'after' => 'field_id', 'primary' => true),
                        'lang_id' => array('type' => 'INT(1)', 'after' => 'form_id', 'primary' => true),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'lang_id'),
                        'default' => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'name')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // registration form field value table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_REGISTRATION_FORM_FIELD_VALUE_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_REGISTRATION_FORM_FIELD_VALUE_TABLE,
                    array(
                        'reg_id' => array('type' => 'INT(7)', 'primary' => true),
                        'field_id' => array('type' => 'INT(7)', 'after' => 'reg_id', 'primary' => true),
                        'value' => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'field_id')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // category table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_CATEGORY_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_CATEGORY_TABLE,
                    array(
                        'id' => array('type' => 'INT(5)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'pos' => array('type' => 'INT(5)', 'notnull' => false, 'after' => 'id'),
                        'status' => array('type' => 'INT(1)', 'notnull' => false, 'after' => 'pos')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // category name table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_CATEGORY_NAME_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_CATEGORY_NAME_TABLE,
                    array(
                        'cat_id' => array('type' => 'INT(11)'),
                        'lang_id' => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'cat_id'),
                        'name' => array('type' => 'VARCHAR(225)', 'notnull' => false, 'after' => 'lang_id')
                    ),
                    array(
                        'fk_contrexx_module_calendar_category_names_contrexx_module_ca1' => array('fields' => array('cat_id'))
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // mail table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_MAIL_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_MAIL_TABLE,
                    array(
                        'id' => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'title' => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                        'content_text' => array('type' => 'longtext', 'after' => 'title'),
                        'content_html' => array('type' => 'longtext', 'after' => 'content_text'),
                        'recipients' => array('type' => 'mediumtext', 'after' => 'content_html'),
                        'lang_id' => array('type' => 'INT(1)', 'after' => 'recipients'),
                        'action_id' => array('type' => 'INT(1)', 'after' => 'lang_id'),
                        'is_default' => array('type' => 'INT(1)', 'after' => 'action_id'),
                        'status' => array('type' => 'INT(1)', 'after' => 'is_default')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // mail action table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_MAIL_ACTION_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_MAIL_ACTION_TABLE,
                    array(
                        'id' => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                        'default_recipient' => array('type' => 'ENUM(\'empty\',\'admin\',\'author\')', 'after' => 'name'),
                        'need_auth' => array('type' => 'INT(11)', 'after' => 'default_recipient')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // event table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_EVENT_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_EVENT_TABLE,
                    array(
                        'id'                                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'type'                               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                        'startdate'                          => array('type' => 'INT(14)', 'notnull' => false, 'after' => 'type'),
                        'enddate'                            => array('type' => 'INT(14)', 'notnull' => false, 'after' => 'startdate'),
                        'use_custom_date_display'            => array('type' => 'TINYINT(1)', 'after' => 'enddate'),
                        'showStartDateList'                  => array('type' => 'INT(1)', 'after' => 'use_custom_date_display'),
                        'showEndDateList'                    => array('type' => 'INT(1)', 'after' => 'showStartDateList'),
                        'showStartTimeList'                  => array('type' => 'INT(1)', 'after' => 'showEndDateList'),
                        'showEndTimeList'                    => array('type' => 'INT(1)', 'after' => 'showStartTimeList'),
                        'showTimeTypeList'                   => array('type' => 'INT(1)', 'after' => 'showEndTimeList'),
                        'showStartDateDetail'                => array('type' => 'INT(1)', 'after' => 'showTimeTypeList'),
                        'showEndDateDetail'                  => array('type' => 'INT(1)', 'after' => 'showStartDateDetail'),
                        'showStartTimeDetail'                => array('type' => 'INT(1)', 'after' => 'showEndDateDetail'),
                        'showEndTimeDetail'                  => array('type' => 'INT(1)', 'after' => 'showStartTimeDetail'),
                        'showTimeTypeDetail'                 => array('type' => 'INT(1)', 'after' => 'showEndTimeDetail'),
                        'google'                             => array('type' => 'INT(11)', 'after' => 'showTimeTypeDetail'),
                        'access'                             => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'google'),
                        'priority'                           => array('type' => 'INT(1)', 'notnull' => true, 'default' => '3', 'after' => 'access'),
                        'price'                              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'priority'),
                        'link'                               => array('type' => 'VARCHAR(255)', 'after' => 'price'),
                        'pic'                                => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'link'),
                        'attach'                             => array('type' => 'VARCHAR(255)', 'after' => 'pic'),
                        'place_mediadir_id'                  => array('type' => 'INT(11)', 'after' => 'attach'),
                        'catid'                              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'place_mediadir_id'),
                        'show_in'                            => array('type' => 'VARCHAR(255)', 'after' => 'catid'),
                        'invited_groups'                     => array('type' => 'VARCHAR(45)', 'notnull' => false, 'after' => 'show_in'),
                        'invited_mails'                      => array('type' => 'mediumtext', 'notnull' => false, 'after' => 'invited_groups'),
                        'invitation_sent'                    => array('type' => 'INT(1)', 'after' => 'invited_mails'),
                        'invitation_email_template'          => array('type' => 'VARCHAR(255)', 'after' => 'invitation_sent'),
                        'registration'                       => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'invitation_email_template'),
                        'registration_form'                  => array('type' => 'INT(11)', 'after' => 'registration'),
                        'registration_num'                   => array('type' => 'VARCHAR(45)', 'notnull' => false, 'after' => 'registration_form'),
                        'registration_notification'          => array('type' => 'VARCHAR(1024)', 'notnull' => false, 'after' => 'registration_num'),
                        'email_template'                     => array('type' => 'VARCHAR(255)', 'after' => 'registration_notification'),
                        'registration_external_link'         => array('type' => 'text', 'notnull' => true, 'after' => 'email_template'),
                        'registration_external_fully_booked' => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'registration_external_link'),
                        'ticket_sales'                       => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'registration_external_fully_booked'),
                        'num_seating'                        => array('type' => 'text', 'after' => 'ticket_sales'),
                        'series_status'                      => array('type' => 'TINYINT(4)', 'notnull' => true, 'default' => '0', 'after' => 'num_seating'),
                        'independent_series'                 => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'series_status'),
                        'series_type'                        => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'independent_series'),
                        'series_pattern_count'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_type'),
                        'series_pattern_weekday'             => array('type' => 'VARCHAR(7)', 'after' => 'series_pattern_count'),
                        'series_pattern_day'                 => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_weekday'),
                        'series_pattern_week'                => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_day'),
                        'series_pattern_month'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_week'),
                        'series_pattern_type'                => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_month'),
                        'series_pattern_dourance_type'       => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_type'),
                        'series_pattern_end'                 => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_dourance_type'),
                        'series_pattern_end_date'            => array('type' => 'timestamp', 'notnull' => true, 'default' => '0000-00-00 00:00:00', 'after' => 'series_pattern_end'),
                        'series_pattern_begin'               => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'series_pattern_end_date'),
                        'series_pattern_exceptions'          => array('type' => 'longtext', 'after' => 'series_pattern_begin'),
                        'series_additional_recurrences'      => array('type' => 'longtext', 'after' => 'series_pattern_exceptions'),
                        'status'                             => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'series_additional_recurrences'),
                        'confirmed'                          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'status'),
                        'show_detail_view'                   => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'confirmed'),
                        'author'                             => array('type' => 'VARCHAR(255)', 'after' => 'show_detail_view'),
                        'all_day'                            => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'author'),
                        'location_type'                      => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'all_day'),
                        'place'                              => array('type' => 'VARCHAR(255)', 'after' => 'location_type'),
                        'place_id'                           => array('type' => 'INT(11)', 'after' => 'place'),
                        'place_street'                       => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_id'),
                        'place_zip'                          => array('type' => 'VARCHAR(10)', 'notnull' => false, 'after' => 'place_street'),
                        'place_city'                         => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_zip'),
                        'place_country'                      => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'place_city'),
                        'place_website'                      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'place_country'),
                        'place_link'                         => array('type' => 'VARCHAR(255)', 'after' => 'place_website'),
                        'place_phone'                        => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'place_link'),
                        'place_map'                          => array('type' => 'VARCHAR(255)', 'after' => 'place_phone'),
                        'host_type'                          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'place_map'),
                        'org_name'                           => array('type' => 'VARCHAR(255)', 'after' => 'host_type'),
                        'org_street'                         => array('type' => 'VARCHAR(255)', 'after' => 'org_name'),
                        'org_zip'                            => array('type' => 'VARCHAR(10)', 'after' => 'org_street'),
                        'org_city'                           => array('type' => 'VARCHAR(255)', 'after' => 'org_zip'),
                        'org_country'                        => array('type' => 'VARCHAR(255)', 'after' => 'org_city'),
                        'org_website'                        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'org_country'),
                        'org_link'                           => array('type' => 'VARCHAR(255)', 'after' => 'org_website'),
                        'org_phone'                          => array('type' => 'VARCHAR(20)', 'notnull' => true, 'default' => '', 'after' => 'org_link'),
                        'org_email'                          => array('type' => 'VARCHAR(255)', 'after' => 'org_phone'),
                        'host_mediadir_id'                   => array('type' => 'INT(11)', 'after' => 'org_email')
                    ),
                    array(
                        'fk_contrexx_module_calendar_notes_contrexx_module_calendar_ca1' => array('fields' => array('catid'))
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // event field table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_EVENT_FIELD_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_EVENT_FIELD_TABLE,
                    array(
                        'event_id' => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                        'lang_id' => array('type' => 'VARCHAR(225)', 'notnull' => false, 'after' => 'event_id'),
                        'title' => array('type' => 'VARCHAR(255)', 'notnull' => false, 'after' => 'lang_id'),
                        'teaser' => array('type' => 'text', 'notnull' => false, 'after' => 'title'),
                        'description' => array('type' => 'mediumtext', 'notnull' => false, 'after' => 'teaser'),
                        'redirect' => array('type' => 'VARCHAR(255)', 'after' => 'description')
                    ),
                    array(
                        'lang_field' => array('fields' => array('title')),
                        'fk_contrexx_module_calendar_note_field_contrexx_module_calend1' => array('fields' => array('event_id')),
                        'eventIndex' => array('fields' => array('title', 'teaser', 'description'), 'type' => 'FULLTEXT')
                    )
                );
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        return true;

        // the style table hasn't been modified
    }

    /**
     * insert demo data for settings tables
     * @return bool|string
     */
    public function migrateSettings()
    {
        global $_CONFIG;

        if (!empty($_SESSION['contrexx_update']['calendar']['settings_migration'])) {
            return true;
        }

        // migrate settings structure
        try {
            if (\Cx\Lib\UpdateUtil::column_exist(CALENDAR_OLD_SETTINGS_TABLE, 'setname')) {
                // remove old settings data
                \Cx\Lib\UpdateUtil::drop_table(CALENDAR_OLD_SETTINGS_TABLE);
            }

            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_SETTINGS_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_SETTINGS_TABLE,
                    array(
                        'id' => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'section_id' => array('type' => 'INT(11)', 'after' => 'id'),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'section_id'),
                        'title' => array('type' => 'VARCHAR(255)', 'after' => 'name'),
                        'value' => array('type' => 'mediumtext', 'after' => 'title'),
                        'info' => array('type' => 'mediumtext', 'after' => 'value'),
                        'type' => array('type' => 'INT(11)', 'after' => 'info'),
                        'options' => array('type' => 'mediumtext', 'after' => 'type'),
                        'special' => array('type' => 'VARCHAR(255)', 'after' => 'options'),
                        'order' => array('type' => 'INT(11)', 'after' => 'special')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            // settings section table
            if (!\Cx\Lib\UpdateUtil::table_exist(CALENDAR_NEW_SETTINGS_SECTION_TABLE)) {
                \Cx\Lib\UpdateUtil::table(
                    CALENDAR_NEW_SETTINGS_SECTION_TABLE,
                    array(
                        'id' => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'parent' => array('type' => 'INT(11)', 'after' => 'id'),
                        'order' => array('type' => 'INT(11)', 'after' => 'parent'),
                        'name' => array('type' => 'VARCHAR(255)', 'after' => 'order'),
                        'title' => array('type' => 'VARCHAR(255)', 'after' => 'name')
                    )
                );
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }



            // migrate settings data
            if (\Cx\Lib\UpdateUtil::table_empty(CALENDAR_NEW_SETTINGS_TABLE)) {
                $headlinesActivated = $_CONFIG['calendarheadlines'];
                $headlinesCategory = $_CONFIG['calendarheadlinescat'];
                $headlinesCount = $_CONFIG['calendarheadlinescount'];
                $defaultCount = $_CONFIG['calendardefaultcount'];
                \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_SETTINGS_TABLE . "` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`)
                    VALUES
                        (8, 5, 'numPaging', 'TXT_CALENDAR_NUM_PAGING', '" . $defaultCount . "', '', 1, '', '', 1),
                        (9, 5, 'numEntrance', 'TXT_CALENDAR_NUM_EVENTS_ENTRANCE', '" . $defaultCount . "', 'TXT_CALENDAR_NUM_EVENTS_ENTRANCE_INFO', 1, '', '', 2),
                        (10, 6, 'headlinesStatus', 'TXT_CALENDAR_HEADLINES_STATUS', '" . $headlinesActivated . "', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 1),
                        (11, 6, 'headlinesCategory', 'TXT_CALENDAR_HEADLINES_CATEGORY', '" . $headlinesCategory . "', '', 5, '', 'getCategoryDorpdown', 3),
                        (12, 6, 'headlinesNum', 'TXT_CALENDAR_HEADLINES_NUM', '" . $headlinesCount . "', '', 1, '', '', 2),
                        (14, 7, 'publicationStatus', 'TXT_CALENDAR_PUBLICATION_STATUS', '2', 'TXT_CALENDAR_PUBLICATION_STATUS_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 0),
                        (15, 15, 'dateFormat', 'TXT_CALENDAR_DATE_FORMAT', '0', 'TXT_CALENDAR_DATE_FORMAT_INFO', 5, 'TXT_CALENDAR_DATE_FORMAT_DD.MM.YYYY,TXT_CALENDAR_DATE_FORMAT_DD/MM/YYYY,TXT_CALENDAR_DATE_FORMAT_YYYY.MM.DD,TXT_CALENDAR_DATE_FORMAT_MM/DD/YYYY,TXT_CALENDAR_DATE_FORMAT_YYYY-MM-DD', '', 3),
                        (16, 8, 'countCategoryEntries', 'TXT_CALENDAR_CATEGORY_COUNT_ENTRIES', '2', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 0),
                        (18, 18, 'addEventsFrontend', 'TXT_CALENDAR_ADD_EVENTS_FRONTEND', '0', '', 5, 'TXT_CALENDAR_DEACTIVATE,TXT_CALENDAR_ACTIVATE_ALL,TXT_CALENDAR_ACTIVATE_ONLY_COMMUNITY', '', 5),
                        (19, 18, 'confirmFrontendEvents', 'TXT_CALENDAR_CONFIRM_FRONTEND_EVENTS', '2', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 6),
                        (22, 10, 'paymentStatus', 'TXT_CALENDAR_PAYMENT_STATUS', '2', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 1),
                        (23, 10, 'paymentCurrency', 'TXT_CALENDAR_PAYMENT_CURRENCY', 'CHF', '', 1, '', '', 2),
                        (24, 10, 'paymentVatRate', 'TXT_CALENDAR_PAYMENT_VAT_RATE', '8', '', 1, '', '', 3),
                        (25, 11, 'paymentBillStatus', 'TXT_CALENDAR_PAYMENT_BILL_STATUS', '2', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 1),
                        (26, 11, 'paymentlBillGrace', 'TXT_CALENDAR_PAYMENT_BILL_GRACE', '30', '', 1, '', '', 2),
                        (27, 12, 'paymentYellowpayStatus', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_STATUS', '2', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 1),
                        (28, 12, 'paymentYellowpayPspid', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_PSPID', '', '', 1, '', '', 2),
                        (29, 12, 'paymentYellowpayShaIn', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_SHA_IN', '', '', 1, '', '', 3),
                        (30, 12, 'paymentYellowpayShaOut', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_SHA_OUT', '', '', 1, '', '', 4),
                        (31, 12, 'paymentYellowpayAuthorization', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_AUTHORIZATION', '0', '', 5, 'TXT_CALENDAR_PAYMENT_YELLOWPAY_AUTHORIZATION_SALE,TXT_CALENDAR_PAYMENT_YELLOWPAY_AUTHORIZATION', '', 5),
                        (32, 12, 'paymentTestserver', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_TESTSERVER', '2', '', 3, 'TXT_CALENDAR_YES,TXT_CALENDAR_NO', '', 7),
                        (33, 12, 'paymentYellowpayMethods', 'TXT_CALENDAR_PAYMENT_YELLOWPAY_METHODS', '0', '', 4, 'TXT_CALENDAR_PAYMENT_YELLOWPAY_POSTFINANCE,TXT_CALENDAR_PAYMENT_YELLOWPAY_POSTFINANCE_EFINANCE,TXT_CALENDAR_PAYMENT_YELLOWPAY_MASTERCARD,TXT_CALENDAR_PAYMENT_YELLOWPAY_VISA,TXT_CALENDAR_PAYMENT_YELLOWPAY_AMEX,TXT_CALENDAR_PAYMENT_YELLOWPAY_DINERS', '', 6),
                        (34, 10, 'paymentVatNumber', 'TXT_CALENDAR_PAYMENT_VAT_NUMBER', '', '', 1, '', '', 4),
                        (35, 13, 'paymentBank', 'TXT_CALENDAR_PAYMENT_BANK', '', '', 1, '', '', 1),
                        (36, 13, 'paymentBankAccount', 'TXT_CALENDAR_PAYMENT_BANK_ACCOUNT', '', '', 1, '', '', 2),
                        (37, 13, 'paymentBankIBAN', 'TXT_CALENDAR_PAYMENT_BANK_IBAN', '', '', 1, '', '', 3),
                        (38, 13, 'paymentBankCN', 'TXT_CALENDAR_PAYMENT_BANK_CN', '', '', 1, '', '', 4),
                        (39, 13, 'paymentBankSC', 'TXT_CALENDAR_PAYMENT_BANK_SC', '', '', 1, '', '', 5),
                        (40, 17, 'separatorDateDetail', 'TXT_CALENDAR_SEPARATOR_DATE', '1', 'TXT_CALENDAR_SEPARATOR_DATE_INFO', 5, 'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO', '', 1),
                        (41, 17, 'separatorTimeDetail', 'TXT_CALENDAR_SEPARATOR_TIME', '3', 'TXT_CALENDAR_SEPARATOR_TIME_INFO', 5, 'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO', '', 2),
                        (42, 17, 'separatorDateTimeDetail', 'TXT_CALENDAR_SEPARATOR_DATE_TIME', '3', 'TXT_CALENDAR_SEPARATOR_DATE_TIME_INFO', 5, 'TXT_CALENDAR_SEPARATOR_NOTHING,TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_BREAK,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON', '', 3),
                        (43, 17, 'separatorSeveralDaysDetail', 'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS', '2', 'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS_INFO', 5, 'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_TO,TXT_CALENDAR_SEPARATOR_BREAK', '', 4),
                        (44, 17, 'showClockDetail', 'TXT_CALENDAR_SHOW_CLOCK', '1', 'TXT_CALENDAR_SEPARATOR_SHOW_CLOCK_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 5),
                        (45, 17, 'showStartDateDetail', 'TXT_CALENDAR_SHOW_START_DATE', '1', 'TXT_CALENDAR_SHOW_START_DATE_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 6),
                        (46, 17, 'showEndDateDetail', 'TXT_CALENDAR_SHOW_END_DATE', '1', 'TXT_CALENDAR_SHOW_END_DATE_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 7),
                        (47, 17, 'showStartTimeDetail', 'TXT_CALENDAR_SHOW_START_TIME', '1', 'TXT_CALENDAR_SHOW_START_TIME_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 9),
                        (48, 17, 'showEndTimeDetail', 'TXT_CALENDAR_SHOW_END_TIME', '1', 'TXT_CALENDAR_SHOW_END_TIME_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 10),
                        (49, 16, 'separatorDateList', 'TXT_CALENDAR_SEPARATOR_DATE', '1', 'TXT_CALENDAR_SEPARATOR_DATE_INFO', 5, 'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO', '', 1),
                        (50, 16, 'separatorTimeList', 'TXT_CALENDAR_SEPARATOR_TIME', '1', 'TXT_CALENDAR_SEPARATOR_TIME_INFO', 5, 'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO', '', 2),
                        (51, 16, 'separatorDateTimeList', 'TXT_CALENDAR_SEPARATOR_DATE_TIME', '1', 'TXT_CALENDAR_SEPARATOR_DATE_TIME_INFO', 5, 'TXT_CALENDAR_SEPARATOR_NOTHING,TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_BREAK,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON', '', 3),
                        (52, 16, 'separatorSeveralDaysList', 'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS', '2', 'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS_INFO', 5, 'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_TO,TXT_CALENDAR_SEPARATOR_BREAK', '', 4),
                        (53, 16, 'showClockList', 'TXT_CALENDAR_SHOW_CLOCK', '1', 'TXT_CALENDAR_SEPARATOR_SHOW_CLOCK_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 5),
                        (54, 16, 'showStartDateList', 'TXT_CALENDAR_SHOW_START_DATE', '1', 'TXT_CALENDAR_SHOW_START_DATE_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 6),
                        (55, 16, 'showEndDateList', 'TXT_CALENDAR_SHOW_END_DATE', '2', 'TXT_CALENDAR_SHOW_END_DATE_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 7),
                        (56, 16, 'showStartTimeList', 'TXT_CALENDAR_SHOW_START_TIME', '2', 'TXT_CALENDAR_SHOW_START_TIME_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 9),
                        (57, 16, 'showEndTimeList', 'TXT_CALENDAR_SHOW_END_TIME', '2', 'TXT_CALENDAR_SHOW_END_TIME_INFO', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 10),
                        (58, 5, 'maxSeriesEndsYear', 'TXT_CALENDAR_MAX_SERIES_ENDS_YEAR', '0', 'TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_INFO', 5, 'TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_1_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_2_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_3_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_4_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_5_YEARS', '', 9),
                        (59, 5, 'showEventsOnlyInActiveLanguage', 'TXT_CALENDAR_SHOW_EVENTS_ONLY_IN_ACTIVE_LANGUAGE', '1', '', 3, 'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE', '', 10),
                        (60, 16, 'listViewPreview', 'TXT_CALENDAR_SHOW_PREVIEW', '0', '', 7, '', 'listPreview', 10),
                        (61, 17, 'detailViewPreview', 'TXT_CALENDAR_SHOW_PREVIEW', '0', '', 7, '', 'detailPreview', 10),
                        (20,19,'placeData','TXT_CALENDAR_PLACE_DATA','1','TXT_CALENDAR_PLACE_DATA_STATUS_INFO',3,'TXT_CALENDAR_PLACE_DATA_DEFAULT,TXT_CALENDAR_PLACE_DATA_FROM_MEDIADIR,TXT_CALENDAR_PLACE_DATA_FROM_BOTH','',7),
                        (62,19,'placeDataForm','','0','',5,'','getPlaceDataDorpdown',8),
                        (63,19,'placeDataHost','TXT_CALENDAR_PLACE_DATA_HOST','1','TXT_CALENDAR_PLACE_DATA_STATUS_INFO',3,'TXT_CALENDAR_PLACE_DATA_DEFAULT,TXT_CALENDAR_PLACE_DATA_FROM_MEDIADIR,TXT_CALENDAR_PLACE_DATA_FROM_BOTH','',9),
                        (64,19,'placeDataHostForm','','0','',5,'','getPlaceDataDorpdown',10)
                ");
            }

            // fix ordering
            \Cx\Lib\UpdateUtil::sql("UPDATE ".CALENDAR_NEW_SETTINGS_TABLE." SET `order` = 9 WHERE `name` = 'showStartTimeDetail'");
            \Cx\Lib\UpdateUtil::sql("UPDATE ".CALENDAR_NEW_SETTINGS_TABLE." SET `order` = 10 WHERE `name` = 'showEndTimeDetail'");
            \Cx\Lib\UpdateUtil::sql("UPDATE ".CALENDAR_NEW_SETTINGS_TABLE." SET `order` = 9 WHERE `name` = 'showStartTimeList'");
            \Cx\Lib\UpdateUtil::sql("UPDATE ".CALENDAR_NEW_SETTINGS_TABLE." SET `order` = 10 WHERE `name` = 'showEndTimeList'");
            \Cx\Lib\UpdateUtil::sql("UPDATE ".CALENDAR_NEW_SETTINGS_TABLE." SET `order` = 9 WHERE `name` = 'maxSeriesEndsYear'");
            \Cx\Lib\UpdateUtil::sql("UPDATE ".CALENDAR_NEW_SETTINGS_TABLE." SET `order` = 10 WHERE `name` = 'showEventsOnlyInActiveLanguage'");

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            if (\Cx\Lib\UpdateUtil::table_empty(CALENDAR_NEW_SETTINGS_SECTION_TABLE)) {
                \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_SETTINGS_SECTION_TABLE . "` (`id`, `parent`, `order`, `name`, `title`)
                        VALUES
                            (5, 1, 0, 'global', 'TXT_CALENDAR_GLOBAL'),
                            (6, 1, 1, 'headlines', 'TXT_CALENDAR_HEADLINES'),
                            (1, 0, 0, 'global', ''),
                            (2, 0, 1, 'form', ''),
                            (3, 0, 2, 'mails', ''),
                            (4, 0, 3, 'hosts', ''),
                            (7, 4, 0, 'publication', 'TXT_CALENDAR_PUBLICATION'),
                            (8, 1, 2, 'categories', 'TXT_CALENDAR_CATEGORIES'),
                            (9, 0, 4, 'payment', ''),
                            (10, 9, 0, 'payment', 'TXT_CALENDAR_PAYMENT'),
                            (11, 9, 1, 'paymentBill', 'TXT_CALENDAR_PAYMENT_BILL'),
                            (12, 9, 2, 'paymentYellowpay', 'TXT_CALENDAR_PAYMENT_YELLOWPAY'),
                            (13, 9, 1, 'paymentBank', 'TXT_CALENDAR_PAYMENT_BANK'),
                            (14, 0, 5, 'dateDisplay', 'TXT_CALENDAR_DATE_DISPLAY'),
                            (15, 14, 0, 'dateGlobal', 'TXT_CALENDAR_GLOBAL'),
                            (16, 14, 1, 'dateDisplayList', 'TXT_CALENDAR_DATE_DISPLAY_LIST'),
                            (17, 14, 2, 'dateDisplayDetail', 'TXT_CALENDAR_DATE_DISPLAY_DETAIL'),
                            (18, 1, 3, 'frontend_submission', 'TXT_CALENDAR_FRONTEND_SUBMISSION'),
                            (19, 1, 4, 'location_host', 'TXT_CALENDAR_EVENT_LOCATION')
                ");
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            $_SESSION['contrexx_update']['calendar']['settings_migration'] = true;
            return true;
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    /**
     * insert demo data to mail tables
     * @return bool|string
     */
    protected function insertMailDemoData()
    {
        try {
            if (\Cx\Lib\UpdateUtil::table_empty(CALENDAR_NEW_MAIL_TABLE)) {
                // migrate old mail data
                \Cx\Lib\UpdateUtil::sql("
                    INSERT INTO  `" . CALENDAR_NEW_MAIL_TABLE . "` (`title`, `content_text`, `content_html`, `lang_id`, `action_id`,  `status`)
                    SELECT title.setvalue, content.setvalue, REPLACE(content.setvalue, '\r\n', '<br />\n') , 1 , 4 , 1
                    FROM `" . CALENDAR_OLD_SETTINGS_TABLE . "` as content
                    JOIN `" . CALENDAR_OLD_SETTINGS_TABLE . "` as title ON title.setid = 1
                    WHERE content.setid = 2
                ");
                \Cx\Lib\UpdateUtil::sql("
                    INSERT INTO  `" . CALENDAR_NEW_MAIL_TABLE . "` (`title`, `content_text`, `content_html`, `lang_id`, `action_id`,  `status`)
                    SELECT title.setvalue, content.setvalue, REPLACE(content.setvalue, '\r\n', '<br />\n') , 1 , 2 , 1
                    FROM `" . CALENDAR_OLD_SETTINGS_TABLE . "` as content
                    JOIN `" . CALENDAR_OLD_SETTINGS_TABLE . "` as title ON title.setid = 3
                    WHERE content.setid = 4
                ");
                \Cx\Lib\UpdateUtil::sql("
                    INSERT INTO  `" . CALENDAR_NEW_MAIL_TABLE . "` (`title`, `content_text`, `content_html`, `lang_id`, `action_id`,  `status`)
                    SELECT title.setvalue, content.setvalue, REPLACE(content.setvalue, '\r\n', '<br />\n') , 1 , 3 , 1
                    FROM `" . CALENDAR_OLD_SETTINGS_TABLE . "` as content
                    JOIN `" . CALENDAR_OLD_SETTINGS_TABLE . "` as title ON title.setid = 5
                    WHERE content.setid = 6
                ");

                // load demo mail data
                \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_MAIL_TABLE . "` (`id`, `title`, `content_text`, `content_html`, `recipients`, `lang_id`, `action_id`, `is_default`, `status`)
                        VALUES
                            (1, '[[URL]] - Einladung zu [[TITLE]]', 'Hallo [[FIRSTNAME]] [[LASTNAME]] \r\n\r\nSie wurden auf [[URL]] zum Event \"[[TITLE]]\" eingeladen.\r\nDetails: [[LINK_EVENT]]\r\n\r\nFolgen Sie dem unten stehenden Link um sich f&uuml;r diesen Event an- oder abzumelden.\r\nHinweis: Sollte der Link nicht funktionieren, kopieren Sie die komplette Adresse ohne Zeilenumbr&uuml;che in die Adresszeile Ihres Browsers und dr&uuml;cken Sie anschliessend \"Enter\".\r\n\r\n[[LINK_REGISTRATION]]\r\n\r\n\r\n--\r\nDiese Nachricht wurde automatisch generiert\r\n[[DATE]]', 'Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\r\n<br />\r\nSie wurden auf <a href=\"http://[[URL]]\" title=\"[[URL]]\">[[URL]]</a> zum Event <a href=\"[[LINK_EVENT]]\" title=\"Event Details\">&quot;[[TITLE]]&quot;</a> eingeladen. <br />\r\nKlicken Sie <a href=\"[[LINK_REGISTRATION]]\" title=\"Anmeldung\">hier</a>, um sich an diesem Event an- oder abzumelden.<br />\r\n<br />\r\n<br />\r\n--<br />\r\n<em>Diese Nachricht wurde automatisch generiert</em><br />\r\n<em>[[DATE]]</em>', '', 1, 1, 1, 1),
                            (15, '[[URL]] - Neue [[REGISTRATION_TYPE]] f&uuml;r [[TITLE]]', 'Hallo\r\n\r\nAuf [[URL]] wurde eine neue [[REGISTRATION_TYPE]] f&uuml;r den Termin \"[[TITLE]]\" eingetragen.\r\n\r\nInformationen zur [[REGISTRATION_TYPE]]\r\n[[REGISTRATION_DATA]]\r\n\r\n-- \r\nDiese Nachricht wurde automatisch generiert [[DATE]]', 'Hallo<br />\r\n<br />\r\nAuf [[URL]] wurde eine neue [[REGISTRATION_TYPE]] f&uuml;r den Termin &quot;[[TITLE]]&quot; eingetragen.<br />\r\n<br />\r\n<h2>Informationen zur [[REGISTRATION_TYPE]]</h2>\r\n[[REGISTRATION_DATA]] <br />\r\n<br />\r\n-- <br />\r\nDiese Nachricht wurde automatisch generiert [[DATE]]', '', 1, 3, 1, 1),
                            (14, '[[URL]] - Erfolgreiche [[REGISTRATION_TYPE]]', 'Hallo [[FIRSTNAME]] [[LASTNAME]]\r\n\r\nIhre [[REGISTRATION_TYPE]] zum Event \"[[TITLE]]\" vom [[START_DATE]] wurde erfolgreich in unserem System eingetragen.\r\n\r\n\r\n--\r\nDiese Nachricht wurde automatisch generiert\r\n[[DATE]]', 'Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\r\n<br />\r\nIhre [[REGISTRATION_TYPE]] zum Event <a title=\"[[TITLE]]\" href=\"[[LINK_EVENT]]\">[[TITLE]]</a> vom [[START_DATE]] wurde erfolgreich in unserem System eingetragen.<br />\r\n<br />\r\n--<br />\r\n<em>Diese Nachricht wurde automatisch generiert<br />\r\n[[DATE]]</em>', '', 1, 2, 1, 1),
                            (16, '[[URL]] - Neuer Termin: [[TITLE]]', 'Hallo [[FIRSTNAME]] [[LASTNAME]] \r\n\r\nUnter [[URL]] finden Sie den neuen Event \"[[TITLE]]\".\r\nDetails: [[LINK_EVENT]]\r\n\r\n\r\n--\r\nDiese Nachricht wurde automatisch generiert\r\n[[DATE]]', 'Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\r\n<br />\r\nUnter <a title=\"[[URL]]\" href=\"http://[[URL]]\">[[URL]]</a> finden Sie den neuen Event <a title=\"Event Details\" href=\"[[LINK_EVENT]]\">&quot;[[TITLE]]&quot;</a>. <br />\r\n<br />\r\n<br />\r\n--<br />\r\n<em>Diese Nachricht wurde automatisch generiert</em><br />\r\n<em>[[DATE]]</em>', '', 1, 4, 1, 1)
                ");
            }

            if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                return 'timeout';
            }

            if (\Cx\Lib\UpdateUtil::table_empty(CALENDAR_NEW_MAIL_ACTION_TABLE)) {
                \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_MAIL_ACTION_TABLE . "`
                        VALUES
                            (1, 'invitationTemplate', 'empty', 0),
                            (2, 'confirmationRegistration', 'author', 0),
                            (3, 'notificationRegistration', 'empty', 0),
                            (4, 'notificationNewEntryFE', 'admin', 0)
                ");
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        return true;
    }

    /**
     * migrate old categories to new category table
     */
    protected function migrateCategories()
    {
        try {
            $where = '';
            if (!empty($_SESSION['contrexx_update']['calendar']['categories'])) {
                $where = ' WHERE `id` > ' . $_SESSION['contrexx_update']['calendar']['categories'];
            }
            $result = \Cx\Lib\UpdateUtil::sql("SELECT `id`, `name`, `status`, `pos`, `lang` FROM `" . CALENDAR_OLD_CATEGORY_TABLE . "`" . $where . " ORDER BY `id`");
            $languages = \FWLanguage::getLanguageArray();
            while (!$result->EOF) {
                \Cx\Lib\UpdateUtil::sql(
                    "INSERT IGNORE INTO `" . CALENDAR_NEW_CATEGORY_TABLE . "` (`id`, `pos`,`status`)
                        VALUES (
                            " . intval($result->fields['id']) . ",
                            " . intval($result->fields['pos']) . ",
                            " . intval($result->fields['status']) . "
                        )"
                );
                foreach ($languages as $id => $languageData) {
                    \Cx\Lib\UpdateUtil::sql(
                        "INSERT IGNORE INTO `" . CALENDAR_NEW_CATEGORY_NAME_TABLE . "` (`cat_id`,`lang_id`,`name`)
                            VALUES (
                                " . intval($result->fields['id']) . ",
                                " . intval($id) . ",
                                '" . contrexx_raw2db($result->fields['name']) . "'
                            )"
                    );
                }

                $_SESSION['contrexx_update']['calendar']['categories'] = intval($result->fields['id']);

                if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                    return 'timeout';
                }

                $result->MoveNext();
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        return true;
    }

    /**
     * get the language ids of each category
     */
    protected function getCategoryLanguages()
    {
        $languageIds = array();
        try {
            $result = \Cx\Lib\UpdateUtil::sql("SELECT `id`, `lang` FROM `" . CALENDAR_OLD_CATEGORY_TABLE . "`");
            while (!$result->EOF) {
                $languageIds[$result->fields['id']] = $result->fields['lang'];
                $result->MoveNext();
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        return $languageIds;
    }

    /**
     * migrate old events to new events table
     */
    protected function migrateEvents()
    {
        $eventId = null;
        $mailTemplateId = null;
        try {
            // migration old events to new event table
            // migrate entries
            $where = '';
            if (!empty($_SESSION['contrexx_update']['calendar']['events'])) {
                $where = ' AND `id` > ' . $_SESSION['contrexx_update']['calendar']['events'];
            }
            $result = \Cx\Lib\UpdateUtil::sql("SELECT * FROM `" . CALENDAR_OLD_EVENT_TABLE . "` WHERE catid > 0 " . $where . " ORDER BY `id`");
            while (!$result->EOF) {
                $langId = null;
                $registrationFormId = null;
                $mailTemplateId = null;
                $eventId = null;

                $langId = $this->categoryLanguages[$result->fields['catid']];
                $name = $result->fields['name'];

                // added event name to mail title
                $mailTemplateId = $this->addMailTemplate($result->fields['mailTitle'] . ' (' . $name . ')', $result->fields['mailContent'], $langId);

                // insert event
                \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_EVENT_TABLE . "` (
                        `status`,
                        `catid`,
                        `startdate`,
                        `enddate`,
                        `priority`,
                        `access`,
                        `place`,
                        `link`,
                        `pic`,
                        `attach`,
                        `place_street`,
                        `place_zip`,
                        `place_city`,
                        `place_link`,
                        `place_map`,
                        `org_name`,
                        `org_street`,
                        `org_zip`,
                        `org_city`,
                        `org_email`,
                        `org_link`,
                        `registration_num`,
                        `registration`,
                        `invited_groups`,
                        `registration_notification`,
                        `invited_mails`,
                        `invitation_email_template`,
                        `series_status`,
                        `series_type`,
                        `series_pattern_count`,
                        `series_pattern_weekday`,
                        `series_pattern_day`,
                        `series_pattern_week`,
                        `series_pattern_month`,
                        `series_pattern_type`,
                        `series_pattern_dourance_type`,
                        `series_pattern_end`,
                        `series_pattern_begin`,
                        `series_pattern_exceptions`,
                        `series_additional_recurrences`,
                        `use_custom_date_display`,
                        `showStartDateList`,
                        `showEndDateList`,
                        `showStartTimeList`,
                        `showEndTimeList`,
                        `showTimeTypeList`,
                        `showStartDateDetail`,
                        `showEndDateDetail`,
                        `showStartTimeDetail`,
                        `showEndTimeDetail`,
                        `showTimeTypeDetail`,
                        `google`,
                        `price`,
                        `place_mediadir_id`,
                        `show_in`,
                        `invitation_sent`,
                        `registration_external_link`,
                        `registration_external_fully_booked`,
                        `ticket_sales`,
                        `num_seating`,
                        `confirmed`,
                        `author`,
                        `all_day`,
                        `place_id`,
                        `place_country`,
                        `registration_form`,
                        `email_template`
                    ) VALUES (
                        '" . contrexx_raw2db($result->fields['active']) . "',
                        '" . contrexx_raw2db($result->fields['catid']) . "',
                        '" . contrexx_raw2db($result->fields['startdate']) . "',
                        '" . contrexx_raw2db($result->fields['enddate']) . "',
                        '" . contrexx_raw2db($result->fields['priority']) . "',
                        '" . contrexx_raw2db($result->fields['access']) . "',
                        '" . contrexx_raw2db($result->fields['placeName']) . "',
                        '" . contrexx_raw2db($result->fields['link']) . "',
                        '" . contrexx_raw2db($result->fields['pic']) . "',
                        '" . contrexx_raw2db($result->fields['attachment']) . "',
                        '" . contrexx_raw2db($result->fields['placeStreet']) . "',
                        '" . contrexx_raw2db($result->fields['placeZip']) . "',
                        '" . contrexx_raw2db($result->fields['placeCity']) . "',
                        '" . contrexx_raw2db($result->fields['placeLink']) . "',
                        '" . contrexx_raw2db($result->fields['placeMap']) . "',
                        '" . contrexx_raw2db($result->fields['organizerName']) . "',
                        '" . contrexx_raw2db($result->fields['organizerStreet']) . "',
                        '" . contrexx_raw2db($result->fields['organizerZip']) . "',
                        '" . contrexx_raw2db($result->fields['organizerPlace']) . "',
                        '" . contrexx_raw2db($result->fields['organizerMail']) . "',
                        '" . contrexx_raw2db($result->fields['organizerLink']) . "',
                        '" . contrexx_raw2db($result->fields['num']) . "',
                        '" . contrexx_raw2db($result->fields['registration']) . "',
                        '" . contrexx_raw2db($result->fields['groups']) . "',
                        " . ($result->fields['notification'] ? "'" . contrexx_raw2db($result->fields['notification_address']) . "'" : "''") . ",
                        '',
                        " . $mailTemplateId . ",
                        '" . contrexx_raw2db($result->fields['series_status']) . "',
                        '" . contrexx_raw2db($result->fields['series_type']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_count']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_weekday']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_day']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_week']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_month']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_type']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_dourance_type']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_end']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_begin']) . "',
                        '" . contrexx_raw2db($result->fields['series_pattern_exceptions']) . "',
                        '',
                        0,
                        1,
                        0,
                        0,
                        0,
                        0,
                        1,
                        1,
                        1,
                        1,
                        1,
                        0,
                        0,
                        0,
                        " . $langId . ",
                        1,
                        '',
                        0,
                        0,
                        '',
                        1,
                        " . $_SESSION['contrexx_update']['user_id'] . ",
                        0,
                        0,
                        '',
                        0,
                        0
                    )
                ");

                $eventId = $this->db->Insert_ID();

                // add language fields for event
                \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_EVENT_FIELD_TABLE . "` (`event_id`, `lang_id`, `title`, `description`, `redirect`)
                    VALUES (
                        " . $eventId . ",
                        " . $langId . ",
                        '" . contrexx_raw2db($name) . "',
                        '" . contrexx_raw2db($result->fields['comment']) . "',
                        ''
                    )
                ");

                // add registration form fields
                $resultFormFields = \Cx\Lib\UpdateUtil::sql("
                    SELECT `id` FROM `" . CALENDAR_OLD_FORM_FIELD_TABLE . "`
                        WHERE `note_id` = " . $result->fields['id'] . "
                ");
                if ($resultFormFields->RecordCount() > 0) {
                    // add registration form
                    $registrationFormId = $this->addRegistrationFormForEvent($name);
                    $formFieldsMap = $this->addRegistrationFormFields($registrationFormId, $result->fields['id'], $langId);
                    \Cx\Lib\UpdateUtil::sql("UPDATE `" . CALENDAR_NEW_EVENT_TABLE . "` SET `registration_form` = " . $registrationFormId . " WHERE `id` = " . $eventId);
                    // add registration data
                    $this->addRegistrationData($result->fields['id'], $eventId, $langId, $formFieldsMap);
                }

                $_SESSION['contrexx_update']['calendar']['events'] = intval($result->fields['id']);

                if (!checkMemoryLimit() || !checkTimeoutLimit()) {
                    return 'timeout';
                }

                // take next event
                $result->MoveNext();
            }
        } catch (\Cx\Lib\UpdateException $e) {
            // remove already inserted data from failed event
            if ($eventId) {
                \Cx\Lib\UpdateUtil::sql("DELETE FROM `" . CALENDAR_NEW_EVENT_TABLE . "` WHERE `id` = " . $eventId);
                \Cx\Lib\UpdateUtil::sql("DELETE FROM `" . CALENDAR_NEW_EVENT_FIELD_TABLE . "` WHERE `event_id` = " . $eventId);
            }
            if ($mailTemplateId) {
                \Cx\Lib\UpdateUtil::sql("DELETE FROM `" . CALENDAR_NEW_MAIL_TABLE . "` WHERE `id` = " . $mailTemplateId);
            }
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        return true;
    }

    /**
     * insert registration form
     * @param string $name the name of the event
     * @return bool|void
     */
    protected function addRegistrationFormForEvent($name)
    {
        \Cx\Lib\UpdateUtil::sql("
                INSERT IGNORE INTO `" . CALENDAR_NEW_REGISTRATION_FORM_TABLE . "` (
                    `status`,
                    `order`,
                    `title`
                ) VALUES (
                    1,
                    99,
                    '" . contrexx_raw2db($name) . "'
                )
            ");
        return $this->db->Insert_ID();
    }

    /**
     * @param string $title the subject for the mail template
     * @param string $content the content for the mail template
     * @param int $langId the language id
     * @return bool|void
     */
    protected function addMailTemplate($title, $content, $langId)
    {
        // replace old placeholders with new placeholders
        $title = str_replace('[[REG_LINK]]', '[[LINK_REGISTRATION]]', $title);
        $content = str_replace('[[REG_LINK]]', '[[LINK_REGISTRATION]]', $content);
        \Cx\Lib\UpdateUtil::sql("
                INSERT IGNORE INTO `" . CALENDAR_NEW_MAIL_TABLE . "` (
                    `title`,
                    `content_text`,
                    `content_html`,
                    `recipients`,
                    `lang_id`,
                    `action_id`,
                    `is_default`,
                    `status`
                ) VALUES (
                    '" . contrexx_raw2db($title) . "',
                    '" . contrexx_raw2db($content) . "',
                    '" . contrexx_raw2db($content) . "',
                    '', " . $langId . ", 1, 0, 1)
            ");
        return $this->db->Insert_ID();
    }

    /**
     * @param int $registrationFormId the registration form id
     * @param int $eventId the event id
     * @param int $langId the language id
     * @return array|bool|void
     */
    protected function addRegistrationFormFields($registrationFormId, $eventId, $langId)
    {
        $formFieldIdMap = array();
        $resultFormFields = \Cx\Lib\UpdateUtil::sql("
                SELECT `id`, `name`, `type`, `required`, `order`, `key` FROM `" . CALENDAR_OLD_FORM_FIELD_TABLE . "`
                    WHERE `note_id` = " . $eventId . "
            ");
        while (!$resultFormFields->EOF) {
            $default = '';

            if ($resultFormFields->fields['key'] == 13) {
                // person number field
                // this value should be able to set in frontend
                $resultMaxSeatingNumber = \Cx\Lib\UpdateUtil::sql("
                    SELECT MAX(CONVERT(`data`, UNSIGNED)) AS `maxSeatingNumber`
                        FROM `" . CALENDAR_OLD_FORM_DATA_TABLE . "`
                        WHERE `field_id` = " . $resultFormFields->fields['id']
                );
                $resultFormFields->fields['type'] = 'seating';
                $default = implode(',',
                    range(1, $resultMaxSeatingNumber->fields['maxSeatingNumber'] + 1)
                );
            }

            if ($resultFormFields->fields['type'] == 3) { // checkbox
                $resultFormFields->fields['type'] = 'checkbox';
            }
            \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_REGISTRATION_FORM_FIELD_TABLE . "` (`form`, `type`, `required`, `order`, `affiliation`)
                    VALUES (
                        " . $registrationFormId . ",
                        '" . contrexx_raw2db($resultFormFields->fields['type']) . "',
                        '" . contrexx_raw2db($resultFormFields->fields['required']) . "',
                        '" . contrexx_raw2db($resultFormFields->fields['order']) . "',
                        '" . contrexx_raw2db($resultFormFields->fields['key']) . "'
                    )
                ");

            $formFieldId = $this->db->Insert_ID();
            $formFieldIdMap[$resultFormFields->fields['id']] = $formFieldId;

            \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_REGISTRATION_FORM_FIELD_NAME_TABLE . "` (`field_id`, `form_id`, `lang_id`, `name`, `default`)
                    VALUES (
                        " . $formFieldId . ",
                        " . $registrationFormId . ",
                        " . $langId . ",
                        '" . contrexx_raw2db($resultFormFields->fields['name']) . "',
                        '" . contrexx_raw2db($default) . "'
                    )
                ");

            $resultFormFields->MoveNext();
        }
        return $formFieldIdMap;
    }

    /**
     * @param int $oldEventId
     * @param int $newEventId
     * @param int $langId
     * @param array $formFieldMap
     */
    protected function addRegistrationData($oldEventId, $newEventId, $langId, $formFieldMap)
    {
        $resultRegistrations = \Cx\Lib\UpdateUtil::sql("
                SELECT `id`, `note_date`, `time`, `type` FROM `" . CALENDAR_OLD_REGISTRATIONS_TABLE . "`
                    WHERE `note_id` = " . $oldEventId . "
            ");
        while (!$resultRegistrations->EOF) {
            $key = $this->generateRandomKey();
            \Cx\Lib\UpdateUtil::sql("
                    INSERT IGNORE INTO `" . CALENDAR_NEW_REGISTRATION_TABLE . "`
                        (`event_id`, `date`, `type`, `key`, `user_id`, `lang_id`, `export`, `payment_method`, `paid`)
                    VALUES (
                        " . $newEventId . ",
                        '" . contrexx_raw2db($resultRegistrations->fields['time']) . "',
                        '" . contrexx_raw2db($resultRegistrations->fields['type']) . "',
                        '" . contrexx_raw2db($key) . "',
                        0,
                        " . $langId . ",
                        0,
                        0,
                        0
                    )
                ");
            $registrationId = $this->db->Insert_ID();

            $resultRegistrationData = \Cx\Lib\UpdateUtil::sql("
                    SELECT `field_id`, `data` FROM `" . CALENDAR_OLD_FORM_DATA_TABLE . "`
                    WHERE `reg_id` = " . $resultRegistrations->fields['id'] . "
                ");
            while (!$resultRegistrationData->EOF) {

                // if a seating number field exists, add this number to data
                $resultNewFieldType = \Cx\Lib\UpdateUtil::sql("
                    SELECT `type` FROM `" . CALENDAR_NEW_REGISTRATION_FORM_FIELD_TABLE . "`
                        WHERE `id` = " . $formFieldMap[$resultRegistrationData->fields['field_id']]
                );
                if ($resultNewFieldType->fields['type'] == 'seating') {
                    $resultRegistrationData->fields['data'] = intval($resultRegistrationData->fields['data']) + 1;
                }

                \Cx\Lib\UpdateUtil::sql("
                        INSERT IGNORE INTO `" . CALENDAR_NEW_REGISTRATION_FORM_FIELD_VALUE_TABLE . "` (
                            `reg_id`,
                            `field_id`,
                            `value`
                        ) VALUES (
                            '" . $registrationId . "',
                            " . $formFieldMap[$resultRegistrationData->fields['field_id']] . ",
                            '" . contrexx_raw2db($resultRegistrationData->fields['data']) . "'
                        )
                    ");
                $resultRegistrationData->MoveNext();
            }

            $resultRegistrations->MoveNext();
        }
    }

    /**
     * Generate a random key
     * @see \CalendarLibrary
     * @return string
     */
    protected function generateRandomKey()
    {
        $arrRandom = array();
        $arrChars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
        $arrNumerics = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

        for ($i = 0; $i <= rand(15, 40); $i++) {
            $charOrNum = rand(0, 1);
            if ($charOrNum == 1) {
                $posChar = rand(0, 25);
                $upOrLow = rand(0, 1);

                if ($upOrLow == 0) {
                    $arrRandom[$i] = strtoupper($arrChars[$posChar]);
                } else {
                    $arrRandom[$i] = strtolower($arrChars[$posChar]);
                }
            } else {
                $posNum = rand(0, 9);
                $arrRandom[$i] = $arrNumerics[$posNum];
            }
        }

        $key = join('',$arrRandom);

        return $key;
    }

    /**
     * drop old tables
     * @return bool|void
     */
    protected function dropOldTables()
    {
        try {
            \Cx\Lib\UpdateUtil::drop_table(CALENDAR_OLD_EVENT_TABLE);
            \Cx\Lib\UpdateUtil::drop_table(CALENDAR_OLD_CATEGORY_TABLE);
            \Cx\Lib\UpdateUtil::drop_table(CALENDAR_OLD_FORM_DATA_TABLE);
            \Cx\Lib\UpdateUtil::drop_table(CALENDAR_OLD_FORM_FIELD_TABLE);
            \Cx\Lib\UpdateUtil::drop_table(CALENDAR_OLD_REGISTRATIONS_TABLE);
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        return true;
    }

    /**
     * Updates the content pages of the calendar module
     * @return bool
     */
    public function migrateContentPages()
    {
        $em = \Env::get('em');
        $pageRepo = $em->getRepository('Cx\Core\ContentManager\Model\Entity\Page');

        // all cmd changes of all calendar pages
        // array key = old cmd, value = new cmd
        $cmdMigrations = array(
            'eventlist' => 'list',
            'event' => 'detail',
            'sign' => 'register',
        );

        foreach($cmdMigrations as $oldCmd => $newCmd) {
            $calendarPages = $pageRepo->findBy(
                array(
                    'module' => 'calendar',
                    'cmd' => $oldCmd,
                )
            );
            foreach ($calendarPages as $page) {
                $page->setCmd($newCmd);
                $em->persist($page);
                $em->flush($page);
            }
        }
        return true;
    }
}
function _calendarInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_category` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `pos` int(5) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_category` (`id`, `pos`, `status`) VALUES (\'46\', NULL, \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_category` (`id`, `pos`, `status`) VALUES (\'49\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_category_name` (
  `cat_id` int(11) NOT NULL,
  `lang_id` int(11) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  KEY `fk_'.DBPREFIX.'module_calendar_category_names_'.DBPREFIX.'module_ca1` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_category_name` (`cat_id`, `lang_id`, `name`) VALUES (\'46\', \'1\', \'Veranstaltungen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_category_name` (`cat_id`, `lang_id`, `name`) VALUES (\'46\', \'2\', \'Events\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_category_name` (`cat_id`, `lang_id`, `name`) VALUES (\'49\', \'1\', \'Weitere\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_category_name` (`cat_id`, `lang_id`, `name`) VALUES (\'49\', \'2\', \'Others\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL DEFAULT \'0\',
  `startdate` timestamp NULL DEFAULT \'0000-00-00 00:00:00\',
  `enddate` timestamp NULL DEFAULT \'0000-00-00 00:00:00\',
  `use_custom_date_display` tinyint(1) NOT NULL,
  `showStartDateList` int(1) NOT NULL,
  `showEndDateList` int(1) NOT NULL,
  `showStartTimeList` int(1) NOT NULL,
  `showEndTimeList` int(1) NOT NULL,
  `showTimeTypeList` int(1) NOT NULL,
  `showStartDateDetail` int(1) NOT NULL,
  `showEndDateDetail` int(1) NOT NULL,
  `showStartTimeDetail` int(1) NOT NULL,
  `showEndTimeDetail` int(1) NOT NULL,
  `showTimeTypeDetail` int(1) NOT NULL,
  `google` int(11) NOT NULL,
  `access` int(1) NOT NULL DEFAULT \'0\',
  `priority` int(1) NOT NULL DEFAULT \'3\',
  `price` int(11) NOT NULL DEFAULT \'0\',
  `link` varchar(255) NOT NULL,
  `pic` varchar(255) NOT NULL DEFAULT \'\',
  `attach` varchar(255) NOT NULL,
  `place_mediadir_id` int(11) NOT NULL,
  `show_in` varchar(255) NOT NULL,
  `invited_groups` varchar(255) DEFAULT NULL,
  `invited_crm_groups` varchar(255) DEFAULT NULL,
  `excluded_crm_groups` varchar(255) DEFAULT NULL,
  `invited_mails` mediumtext ,
  `invitation_sent` int(1) NOT NULL,
  `invitation_email_template` varchar(255) NOT NULL,
  `registration` int(1) NOT NULL DEFAULT \'0\',
  `registration_form` int(11) NOT NULL,
  `registration_num` varchar(45) DEFAULT NULL,
  `registration_notification` varchar(1024) DEFAULT NULL,
  `email_template` varchar(255) NOT NULL,
  `registration_external_link` text NOT NULL,
  `registration_external_fully_booked` tinyint(1) NOT NULL DEFAULT \'0\',
  `ticket_sales` tinyint(1) NOT NULL DEFAULT \'0\',
  `num_seating` text NOT NULL,
  `series_status` tinyint(4) NOT NULL DEFAULT \'0\',
  `independent_series` tinyint(2) NOT NULL DEFAULT \'1\',
  `series_type` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_count` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_weekday` varchar(7) NOT NULL,
  `series_pattern_day` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_week` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_month` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_type` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_dourance_type` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_end` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_end_date` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  `series_pattern_begin` int(11) NOT NULL DEFAULT \'0\',
  `series_pattern_exceptions` longtext NOT NULL,
  `series_additional_recurrences` longtext NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT \'1\',
  `confirmed` tinyint(1) NOT NULL DEFAULT \'1\',
  `show_detail_view` tinyint(1) NOT NULL DEFAULT \'1\',
  `author` varchar(255) NOT NULL,
  `all_day` tinyint(1) NOT NULL DEFAULT \'0\',
  `location_type` tinyint(1) NOT NULL DEFAULT \'1\',
  `place_id` int(11) NOT NULL,
  `place_street` varchar(255) DEFAULT NULL,
  `place_zip` varchar(10) DEFAULT NULL,
  `place_website` varchar(255) NOT NULL DEFAULT \'\',
  `place_link` varchar(255) NOT NULL,
  `place_phone` varchar(20) NOT NULL DEFAULT \'\',
  `place_map` varchar(255) NOT NULL,
  `host_type` tinyint(1) NOT NULL DEFAULT \'1\',
  `org_street` varchar(255) NOT NULL,
  `org_zip` varchar(10) NOT NULL,
  `org_website` varchar(255) NOT NULL DEFAULT \'\',
  `org_link` varchar(255) NOT NULL,
  `org_phone` varchar(20) NOT NULL DEFAULT \'\',
  `org_email` varchar(255) NOT NULL,
  `host_mediadir_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_event` (`id`, `type`, `startdate`, `enddate`, `use_custom_date_display`, `showStartDateList`, `showEndDateList`, `showStartTimeList`, `showEndTimeList`, `showTimeTypeList`, `showStartDateDetail`, `showEndDateDetail`, `showStartTimeDetail`, `showEndTimeDetail`, `showTimeTypeDetail`, `google`, `access`, `priority`, `price`, `link`, `pic`, `attach`, `place_mediadir_id`, `show_in`, `invited_groups`, `invited_crm_groups`, `excluded_crm_groups`, `invited_mails`, `invitation_sent`, `invitation_email_template`, `registration`, `registration_form`, `registration_num`, `registration_notification`, `email_template`, `registration_external_link`, `registration_external_fully_booked`, `ticket_sales`, `num_seating`, `series_status`, `independent_series`, `series_type`, `series_pattern_count`, `series_pattern_weekday`, `series_pattern_day`, `series_pattern_week`, `series_pattern_month`, `series_pattern_type`, `series_pattern_dourance_type`, `series_pattern_end`, `series_pattern_end_date`, `series_pattern_begin`, `series_pattern_exceptions`, `series_additional_recurrences`, `status`, `confirmed`, `show_detail_view`, `author`, `all_day`, `location_type`, `place_id`, `place_street`, `place_zip`, `place_website`, `place_link`, `place_phone`, `place_map`, `host_type`, `org_street`, `org_zip`, `org_website`, `org_link`, `org_phone`, `org_email`, `host_mediadir_id`) VALUES (\'88\', \'0\', \'2018-04-04 00:00:00\', \'2018-04-04 23:59:00\', \'0\', \'1\', \'1\', \'0\', \'0\', \'2\', \'1\', \'1\', \'1\', \'1\', \'2\', \'0\', \'0\', \'3\', \'0\', \'\', \'images/content/cloudrexx.png\', \'\', \'0\', \'1, 2\', NULL, NULL, NULL, NULL, \'0\', \'{\\\"1\\\":\\\"1\\\",\\\"2\\\":\\\"1\\\"}\', \'1\', \'1\', \'250\', \'info@example.com\', \'{\\\"1\\\":\\\"14\\\",\\\"2\\\":\\\"14\\\"}\', \'\', \'0\', \'0\', \'\', \'1\', \'1\', \'3\', \'0\', \'0\', \'4\', \'0\', \'12\', \'1\', \'1\', \'0\', \'0000-00-00 00:00:00\', \'0\', \'\', \'\', \'1\', \'1\', \'1\', \'1\', \'1\', \'1\', \'0\', NULL, NULL, \'\', \'\', \'\', \'\', \'1\', \'\', \'\', \'\', \'\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_event` (`id`, `type`, `startdate`, `enddate`, `use_custom_date_display`, `showStartDateList`, `showEndDateList`, `showStartTimeList`, `showEndTimeList`, `showTimeTypeList`, `showStartDateDetail`, `showEndDateDetail`, `showStartTimeDetail`, `showEndTimeDetail`, `showTimeTypeDetail`, `google`, `access`, `priority`, `price`, `link`, `pic`, `attach`, `place_mediadir_id`, `show_in`, `invited_groups`, `invited_crm_groups`, `excluded_crm_groups`, `invited_mails`, `invitation_sent`, `invitation_email_template`, `registration`, `registration_form`, `registration_num`, `registration_notification`, `email_template`, `registration_external_link`, `registration_external_fully_booked`, `ticket_sales`, `num_seating`, `series_status`, `independent_series`, `series_type`, `series_pattern_count`, `series_pattern_weekday`, `series_pattern_day`, `series_pattern_week`, `series_pattern_month`, `series_pattern_type`, `series_pattern_dourance_type`, `series_pattern_end`, `series_pattern_end_date`, `series_pattern_begin`, `series_pattern_exceptions`, `series_additional_recurrences`, `status`, `confirmed`, `show_detail_view`, `author`, `all_day`, `location_type`, `place_id`, `place_street`, `place_zip`, `place_website`, `place_link`, `place_phone`, `place_map`, `host_type`, `org_street`, `org_zip`, `org_website`, `org_link`, `org_phone`, `org_email`, `host_mediadir_id`) VALUES (\'89\', \'0\', \'2020-01-01 13:00:00\', \'2020-01-01 13:30:00\', \'0\', \'1\', \'0\', \'0\', \'0\', \'0\', \'1\', \'1\', \'1\', \'1\', \'1\', \'0\', \'0\', \'3\', \'0\', \'\', \'\', \'\', \'0\', \'1\', NULL, NULL, NULL, NULL, \'0\', \'0\', \'0\', \'1\', NULL, NULL, \'14\', \'\', \'0\', \'0\', \'\', \'0\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0000-00-00 00:00:00\', \'0\', \'\', \'\', \'1\', \'1\', \'1\', \'1\', \'0\', \'1\', \'0\', NULL, NULL, \'\', \'\', \'\', \'\', \'1\', \'\', \'\', \'\', \'\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_events_categories` (
  `event_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`category_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_events_categories` (`event_id`, `category_id`) VALUES (\'88\', \'46\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_events_categories` (`event_id`, `category_id`) VALUES (\'89\', \'49\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_event_field` (
  `event_id` int(11) NOT NULL DEFAULT \'0\',
  `lang_id` varchar(225) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `teaser` text ,
  `description` mediumtext ,
  `redirect` varchar(255) NOT NULL,
  `place` varchar(255) NOT NULL,
  `place_city` varchar(255) NOT NULL,
  `place_country` varchar(255) NOT NULL,
  `org_name` varchar(255) NOT NULL,
  `org_city` varchar(255) NOT NULL,
  `org_country` varchar(255) NOT NULL,
  KEY `lang_field` (`title`),
  KEY `event_id` (`event_id`),
  FULLTEXT KEY `eventIndex` (`title`,`teaser`,`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_event_field` (`event_id`, `lang_id`, `title`, `teaser`, `description`, `redirect`, `place`, `place_city`, `place_country`, `org_name`, `org_city`, `org_country`) VALUES (\'88\', \'2\', \'Cloudrexx Birthday\', NULL, \'Cloudrexx celebrates its birthday. The first release was published to the public on the 4th of April 2005.\', \'\', \'\', \'\', \'\', \'\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_event_field` (`event_id`, `lang_id`, `title`, `teaser`, `description`, `redirect`, `place`, `place_city`, `place_country`, `org_name`, `org_city`, `org_country`) VALUES (\'88\', \'1\', \'Cloudrexx Geburtstag\', NULL, \'<h2>Geschichte</h2>\\r\\n<strong>Cloudrexx</strong> wurde von der Unternehmung <a class=\\\"out\\\" href=\\\"https://www.cloudrexx.com/\\\" target=\\\"_blank\\\">Cloudrexx AG</a> in der serverseitigen Skriptsprache PHP geschrieben, ist plattformunabh&auml;ngig und ben&ouml;tigt eine MySQL-Datenbank. Das erste Release wurde am 4. April 2005 ver&ouml;ffentlicht.\\r\\n\\r\\n<h2>&Uuml;ber Cloudrexx</h2>\\r\\n\\r\\n<div>\\r\\n<p>Mit <a class=\\\"out\\\" href=\\\"https://www.cloudrexx.com/\\\" target=\\\"_blank\\\">Cloudrexx</a> werden Webseiten f&uuml;r Intranet, Extranet und Internet schnell, benutzerfreundlich und sicher erstellt. Die integrierten Anwendungen von Cloudrexx erlauben ein rasches Umsetzen von individuellen Bed&uuml;rfnissen, wenn n&ouml;tig mit professioneller Unterst&uuml;tzung.<br />\\r\\nDas webbasierte System ist f&uuml;r die Verwaltung von flexiblen Internet- und Intranet L&ouml;sungen optimiert. Der modulare Aufbau und das Workflow System erm&ouml;glicht eine 100% Individualisierung der Logik und des Designs und f&ouml;rdert ein konsistentes und stetig wachsendes Wissensnetzwerk.<br />\\r\\n<br />\\r\\nCloudrexx Download beinhaltet einen Webinstaller, sowie verschiedene Anwendungen wie ein Linkverzeichnis, Podcast, Onlineschalter, Adressverzeichnis, Sitemap, Bildergalerie, Newsletter, G&auml;stebuch, RSS Feed Verzeichnis, News System, Abstimmung, Online Shop, Blockmodul und die M&ouml;glichkeiten f&uuml;r passwortgesch&uuml;tzte Bereiche.</p>\\r\\n\\r\\n<h2>Professioneller und innovativer Vertrieb</h2>\\r\\nCloudrexx wird weltweit &uuml;ber ein Partnernetzwerk vertrieben. Die <a class=\\\"out\\\" href=\\\"https://partner.cloudrexx.com/\\\" target=\\\"_blank\\\">Cloudrexx Solutions Partner</a> sind autorisierte Partner welche ausgewiesene Kompetenzen im den Bereichen WebDesign, Grafik oder Internetmarketing besitzen. Damit erhalten Sie als Kunde professionelle Unterst&uuml;tzung mit hohen Kompetenzen aus Ihrer N&auml;he.</div>\\r\\n\', \'\', \'\', \'\', \'\', \'\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_event_field` (`event_id`, `lang_id`, `title`, `teaser`, `description`, `redirect`, `place`, `place_city`, `place_country`, `org_name`, `org_city`, `org_country`) VALUES (\'89\', \'1\', \'Veranstaltung erstellen\', NULL, \'Beschreiben Sie zuerst, worum es bei Ihrer Veranstaltung geht. Erg&auml;nzen Sie danach in den Tabs die weiteren Informationen.\', \'\', \'\', \'\', \'\', \'\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_host` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `uri` mediumtext NOT NULL,
  `cat_id` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `confirmed` int(11) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_'.DBPREFIX.'module_calendar_shared_hosts_'.DBPREFIX.'module_cale1` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_invite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `date` bigint(20) unsigned NOT NULL,
  `invitee_type` enum(\'-\',\'AccessUser\',\'CrmContact\') NOT NULL,
  `invitee_id` int(11) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `token` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_mail` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content_text` longtext NOT NULL,
  `content_html` longtext NOT NULL,
  `recipients` mediumtext NOT NULL,
  `lang_id` int(1) NOT NULL,
  `action_id` int(1) NOT NULL,
  `is_default` int(1) NOT NULL,
  `status` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_mail` (`id`, `title`, `content_text`, `content_html`, `recipients`, `lang_id`, `action_id`, `is_default`, `status`) VALUES (\'1\', \'[[URL]] - Einladung zu [[TITLE]]\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] \\r\\n\\r\\nSie wurden auf [[URL]] zum Event \\\\\\\"[[TITLE]]\\\\\\\" eingeladen.\\r\\nDetails: [[LINK_EVENT]]\\r\\n\\r\\nFolgen Sie dem unten stehenden Link um sich f&uuml;r diesen Event an- oder abzumelden.\\r\\nHinweis: Sollte der Link nicht funktionieren, kopieren Sie die komplette Adresse ohne Zeilenumbr&uuml;che in die Adresszeile Ihres Browsers und dr&uuml;cken Sie anschliessend \\\\\\\"Enter\\\\\\\".\\r\\n\\r\\n[[LINK_REGISTRATION]]\\r\\n\\r\\n\\r\\n--\\r\\nDiese Nachricht wurde automatisch generiert\\r\\n[[DATE]]\', \'Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\\r\\n<br />\\r\\nSie wurden auf <a href=\\\"http://[[URL]]\\\" title=\\\"[[URL]]\\\">[[URL]]</a> zum Event <a href=\\\"[[LINK_EVENT]]\\\" title=\\\"Event Details\\\">&quot;[[TITLE]]&quot;</a> eingeladen. <br />\\r\\nKlicken Sie <a href=\\\"[[LINK_REGISTRATION]]\\\" title=\\\"Anmeldung\\\">hier</a>, um sich an diesem Event an- oder abzumelden.<br />\\r\\n<br />\\r\\n<br />\\r\\n--<br />\\r\\n<em>Diese Nachricht wurde automatisch generiert</em><br />\\r\\n<em>[[DATE]]</em>\', \'\', \'1\', \'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_mail` (`id`, `title`, `content_text`, `content_html`, `recipients`, `lang_id`, `action_id`, `is_default`, `status`) VALUES (\'14\', \'[[URL]] - Erfolgreiche [[REGISTRATION_TYPE]]\', \'Hallo [[FIRSTNAME]] [[LASTNAME]]\\r\\n\\r\\nIhre [[REGISTRATION_TYPE]] zum Event \\\\\\\"[[TITLE]]\\\\\\\" vom [[START_DATE]] wurde erfolgreich in unserem System eingetragen.\\r\\n\\r\\n\\r\\n--\\r\\nDiese Nachricht wurde automatisch generiert\\r\\n[[DATE]]\', \'Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\\r\\n<br />\\r\\nIhre [[REGISTRATION_TYPE]] zum Event <a title=\\\"[[TITLE]]\\\" href=\\\"[[LINK_EVENT]]\\\">[[TITLE]]</a> vom [[START_DATE]] wurde erfolgreich in unserem System eingetragen.<br />\\r\\n<br />\\r\\n--<br />\\r\\n<em>Diese Nachricht wurde automatisch generiert<br />\\r\\n[[DATE]]</em>\', \'\', \'1\', \'2\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_mail` (`id`, `title`, `content_text`, `content_html`, `recipients`, `lang_id`, `action_id`, `is_default`, `status`) VALUES (\'15\', \'[[URL]] - Neue [[REGISTRATION_TYPE]] f&uuml;r [[TITLE]]\', \'Hallo\\r\\n\\r\\nAuf [[URL]] wurde eine neue [[REGISTRATION_TYPE]] f&uuml;r den Termin \\\\\\\"[[TITLE]]\\\\\\\" eingetragen.\\r\\n\\r\\nInformationen zur [[REGISTRATION_TYPE]]\\r\\n[[REGISTRATION_DATA]]\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde automatisch generiert [[DATE]]\', \'Hallo<br />\\r\\n<br />\\r\\nAuf [[URL]] wurde eine neue [[REGISTRATION_TYPE]] f&uuml;r den Termin &quot;[[TITLE]]&quot; eingetragen.<br />\\r\\n<br />\\r\\n<h2>Informationen zur [[REGISTRATION_TYPE]]</h2>\\r\\n[[REGISTRATION_DATA]] <br />\\r\\n<br />\\r\\n-- <br />\\r\\nDiese Nachricht wurde automatisch generiert [[DATE]]\', \'\', \'1\', \'3\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_mail` (`id`, `title`, `content_text`, `content_html`, `recipients`, `lang_id`, `action_id`, `is_default`, `status`) VALUES (\'16\', \'[[URL]] - Neuer Termin: [[TITLE]]\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] \\r\\n\\r\\nUnter [[URL]] finden Sie den neuen Event \\\\\\\"[[TITLE]]\\\\\\\".\\r\\nDetails: [[LINK_EVENT]]\\r\\n\\r\\n\\r\\n--\\r\\nDiese Nachricht wurde automatisch generiert\\r\\n[[DATE]]\', \'Hallo [[FIRSTNAME]] [[LASTNAME]]<br />\\r\\n<br />\\r\\nUnter <a title=\\\"[[URL]]\\\" href=\\\"http://[[URL]]\\\">[[URL]]</a> finden Sie den neuen Event <a title=\\\"Event Details\\\" href=\\\"[[LINK_EVENT]]\\\">&quot;[[TITLE]]&quot;</a>. <br />\\r\\n<br />\\r\\n<br />\\r\\n--<br />\\r\\n<em>Diese Nachricht wurde automatisch generiert</em><br />\\r\\n<em>[[DATE]]</em>\', \'\', \'1\', \'4\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_registration` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `date` int(15) NOT NULL,
  `submission_date` timestamp NULL DEFAULT \'0000-00-00 00:00:00\',
  `type` int(1) NOT NULL,
  `invite_id` int(11) DEFAULT NULL,
  `key` varchar(45) NOT NULL,
  `user_id` int(7) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `export` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `paid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration` (`id`, `event_id`, `date`, `submission_date`, `type`, `invite_id`, `key`, `user_id`, `lang_id`, `export`, `payment_method`, `paid`) VALUES (\'1\', \'55\', \'1333548000\', \'0000-00-00 00:00:00\', \'1\', NULL, \'478roF24hkH248854429F1\', \'1\', \'1\', \'1311860736\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration` (`id`, `event_id`, `date`, `submission_date`, `type`, `invite_id`, `key`, `user_id`, `lang_id`, `export`, `payment_method`, `paid`) VALUES (\'2\', \'55\', \'1333548000\', \'0000-00-00 00:00:00\', \'1\', NULL, \'3JJ5xq4f32A387K908XC0\', \'1\', \'1\', \'1311860736\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_registration_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form` (`id`, `status`, `order`, `title`) VALUES (\'1\', \'1\', \'99\', \'Standardformular\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_registration_form_field` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `form` int(11) NOT NULL,
  `type` enum(\'inputtext\',\'textarea\',\'select\',\'radio\',\'checkbox\',\'mail\',\'seating\',\'agb\',\'salutation\',\'firstname\',\'lastname\',\'selectBillingAddress\',\'fieldset\') NOT NULL,
  `required` int(1) NOT NULL,
  `order` int(3) NOT NULL,
  `affiliation` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field` (`id`, `form`, `type`, `required`, `order`, `affiliation`) VALUES (\'1\', \'1\', \'salutation\', \'1\', \'0\', \'form\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field` (`id`, `form`, `type`, `required`, `order`, `affiliation`) VALUES (\'2\', \'1\', \'inputtext\', \'0\', \'1\', \'form\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field` (`id`, `form`, `type`, `required`, `order`, `affiliation`) VALUES (\'3\', \'1\', \'firstname\', \'1\', \'2\', \'form\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field` (`id`, `form`, `type`, `required`, `order`, `affiliation`) VALUES (\'4\', \'1\', \'lastname\', \'1\', \'3\', \'form\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field` (`id`, `form`, `type`, `required`, `order`, `affiliation`) VALUES (\'5\', \'1\', \'mail\', \'1\', \'4\', \'form\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field` (`id`, `form`, `type`, `required`, `order`, `affiliation`) VALUES (\'6\', \'1\', \'textarea\', \'0\', \'5\', \'form\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_registration_form_field_name` (
  `field_id` int(7) NOT NULL,
  `form_id` int(11) NOT NULL,
  `lang_id` int(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `default` mediumtext NOT NULL,
  PRIMARY KEY (`field_id`,`form_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'6\', \'1\', \'2\', \'Message\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'6\', \'1\', \'1\', \'Bemerkung\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'5\', \'1\', \'2\', \'E-Mail\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'5\', \'1\', \'1\', \'E-Mail\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'4\', \'1\', \'2\', \'Lastname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'4\', \'1\', \'1\', \'Nachname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'3\', \'1\', \'2\', \'Firstname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'3\', \'1\', \'1\', \'Vorname\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'2\', \'1\', \'2\', \'Company\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'2\', \'1\', \'1\', \'Firma\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'1\', \'1\', \'2\', \'Salutation\', \'Dear Ms.,Dear Mr.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_registration_form_field_name` (`field_id`, `form_id`, `lang_id`, `name`, `default`) VALUES (\'1\', \'1\', \'1\', \'Anrede\', \'Sehr geehrte Frau,Sehr geehrter Herr\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_registration_form_field_value` (
  `reg_id` int(7) NOT NULL,
  `field_id` int(7) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`reg_id`,`field_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_rel_event_host` (
  `host_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_settings` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `value` mediumtext NOT NULL,
  `info` mediumtext NOT NULL,
  `type` int(11) NOT NULL,
  `options` mediumtext NOT NULL,
  `special` varchar(255) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'8\', \'5\', \'numPaging\', \'TXT_CALENDAR_NUM_PAGING\', \'15\', \'\', \'1\', \'\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'9\', \'5\', \'numEntrance\', \'TXT_CALENDAR_NUM_EVENTS_ENTRANCE\', \'5\', \'TXT_CALENDAR_NUM_EVENTS_ENTRANCE_INFO\', \'1\', \'\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'10\', \'6\', \'headlinesStatus\', \'TXT_CALENDAR_HEADLINES_STATUS\', \'2\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'11\', \'6\', \'headlinesCategory\', \'TXT_CALENDAR_HEADLINES_CATEGORY\', \'\', \'\', \'5\', \'\', \'getCategoryDorpdown\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'12\', \'6\', \'headlinesNum\', \'TXT_CALENDAR_HEADLINES_NUM\', \'3\', \'\', \'1\', \'\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'14\', \'7\', \'publicationStatus\', \'TXT_CALENDAR_PUBLICATION_STATUS\', \'2\', \'TXT_CALENDAR_PUBLICATION_STATUS_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'15\', \'15\', \'dateFormat\', \'TXT_CALENDAR_DATE_FORMAT\', \'0\', \'TXT_CALENDAR_DATE_FORMAT_INFO\', \'5\', \'TXT_CALENDAR_DATE_FORMAT_DD.MM.YYYY,TXT_CALENDAR_DATE_FORMAT_DD/MM/YYYY,TXT_CALENDAR_DATE_FORMAT_YYYY.MM.DD,TXT_CALENDAR_DATE_FORMAT_MM/DD/YYYY,TXT_CALENDAR_DATE_FORMAT_YYYY-MM-DD\', \'\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'16\', \'8\', \'countCategoryEntries\', \'TXT_CALENDAR_CATEGORY_COUNT_ENTRIES\', \'2\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'18\', \'18\', \'addEventsFrontend\', \'TXT_CALENDAR_ADD_EVENTS_FRONTEND\', \'0\', \'\', \'5\', \'TXT_CALENDAR_DEACTIVATE,TXT_CALENDAR_ACTIVATE_ALL,TXT_CALENDAR_ACTIVATE_ONLY_COMMUNITY\', \'\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'19\', \'18\', \'confirmFrontendEvents\', \'TXT_CALENDAR_CONFIRM_FRONTEND_EVENTS\', \'2\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'20\', \'19\', \'placeData\', \'TXT_CALENDAR_PLACE_DATA\', \'1\', \'TXT_CALENDAR_PLACE_DATA_STATUS_INFO\', \'3\', \'TXT_CALENDAR_PLACE_DATA_DEFAULT,TXT_CALENDAR_PLACE_DATA_FROM_MEDIADIR,TXT_CALENDAR_PLACE_DATA_FROM_BOTH\', \'\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'22\', \'10\', \'paymentStatus\', \'TXT_CALENDAR_PAYMENT_STATUS\', \'2\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'23\', \'10\', \'paymentCurrency\', \'TXT_CALENDAR_PAYMENT_CURRENCY\', \'CHF\', \'\', \'1\', \'\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'24\', \'10\', \'paymentVatRate\', \'TXT_CALENDAR_PAYMENT_VAT_RATE\', \'8\', \'\', \'1\', \'\', \'\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'25\', \'11\', \'paymentBillStatus\', \'TXT_CALENDAR_PAYMENT_BILL_STATUS\', \'2\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'26\', \'11\', \'paymentlBillGrace\', \'TXT_CALENDAR_PAYMENT_BILL_GRACE\', \'30\', \'\', \'1\', \'\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'27\', \'12\', \'paymentYellowpayStatus\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_STATUS\', \'2\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'28\', \'12\', \'paymentYellowpayPspid\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_PSPID\', \'\', \'\', \'1\', \'\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'29\', \'12\', \'paymentYellowpayShaIn\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_SHA_IN\', \'\', \'\', \'1\', \'\', \'\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'30\', \'12\', \'paymentYellowpayShaOut\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_SHA_OUT\', \'\', \'\', \'1\', \'\', \'\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'31\', \'12\', \'paymentYellowpayAuthorization\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_AUTHORIZATION\', \'0\', \'\', \'5\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_AUTHORIZATION_SALE,TXT_CALENDAR_PAYMENT_YELLOWPAY_AUTHORIZATION\', \'\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'32\', \'12\', \'paymentTestserver\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_TESTSERVER\', \'2\', \'\', \'3\', \'TXT_CALENDAR_YES,TXT_CALENDAR_NO\', \'\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'33\', \'12\', \'paymentYellowpayMethods\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_METHODS\', \'0\', \'\', \'4\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY_POSTFINANCE,TXT_CALENDAR_PAYMENT_YELLOWPAY_POSTFINANCE_EFINANCE,TXT_CALENDAR_PAYMENT_YELLOWPAY_MASTERCARD,TXT_CALENDAR_PAYMENT_YELLOWPAY_VISA,TXT_CALENDAR_PAYMENT_YELLOWPAY_AMEX,TXT_CALENDAR_PAYMENT_YELLOWPAY_DINERS\', \'\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'34\', \'10\', \'paymentVatNumber\', \'TXT_CALENDAR_PAYMENT_VAT_NUMBER\', \'\', \'\', \'1\', \'\', \'\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'35\', \'13\', \'paymentBank\', \'TXT_CALENDAR_PAYMENT_BANK\', \'\', \'\', \'1\', \'\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'36\', \'13\', \'paymentBankAccount\', \'TXT_CALENDAR_PAYMENT_BANK_ACCOUNT\', \'\', \'\', \'1\', \'\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'37\', \'13\', \'paymentBankIBAN\', \'TXT_CALENDAR_PAYMENT_BANK_IBAN\', \'\', \'\', \'1\', \'\', \'\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'38\', \'13\', \'paymentBankCN\', \'TXT_CALENDAR_PAYMENT_BANK_CN\', \'\', \'\', \'1\', \'\', \'\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'39\', \'13\', \'paymentBankSC\', \'TXT_CALENDAR_PAYMENT_BANK_SC\', \'\', \'\', \'1\', \'\', \'\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'40\', \'17\', \'separatorDateDetail\', \'TXT_CALENDAR_SEPARATOR_DATE\', \'1\', \'TXT_CALENDAR_SEPARATOR_DATE_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'41\', \'17\', \'separatorTimeDetail\', \'TXT_CALENDAR_SEPARATOR_TIME\', \'3\', \'TXT_CALENDAR_SEPARATOR_TIME_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'42\', \'17\', \'separatorDateTimeDetail\', \'TXT_CALENDAR_SEPARATOR_DATE_TIME\', \'3\', \'TXT_CALENDAR_SEPARATOR_DATE_TIME_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_NOTHING,TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_BREAK,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON\', \'\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'43\', \'17\', \'separatorSeveralDaysDetail\', \'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS\', \'2\', \'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_TO,TXT_CALENDAR_SEPARATOR_BREAK\', \'\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'44\', \'17\', \'showClockDetail\', \'TXT_CALENDAR_SHOW_CLOCK\', \'1\', \'TXT_CALENDAR_SEPARATOR_SHOW_CLOCK_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'45\', \'17\', \'showStartDateDetail\', \'TXT_CALENDAR_SHOW_START_DATE\', \'1\', \'TXT_CALENDAR_SHOW_START_DATE_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'46\', \'17\', \'showEndDateDetail\', \'TXT_CALENDAR_SHOW_END_DATE\', \'1\', \'TXT_CALENDAR_SHOW_END_DATE_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'47\', \'17\', \'showStartTimeDetail\', \'TXT_CALENDAR_SHOW_START_TIME\', \'1\', \'TXT_CALENDAR_SHOW_START_TIME_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'48\', \'17\', \'showEndTimeDetail\', \'TXT_CALENDAR_SHOW_END_TIME\', \'1\', \'TXT_CALENDAR_SHOW_END_TIME_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'49\', \'16\', \'separatorDateList\', \'TXT_CALENDAR_SEPARATOR_DATE\', \'1\', \'TXT_CALENDAR_SEPARATOR_DATE_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'50\', \'16\', \'separatorTimeList\', \'TXT_CALENDAR_SEPARATOR_TIME\', \'1\', \'TXT_CALENDAR_SEPARATOR_TIME_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON,TXT_CALENDAR_SEPARATOR_TO\', \'\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'51\', \'16\', \'separatorDateTimeList\', \'TXT_CALENDAR_SEPARATOR_DATE_TIME\', \'1\', \'TXT_CALENDAR_SEPARATOR_DATE_TIME_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_NOTHING,TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_BREAK,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_COLON\', \'\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'52\', \'16\', \'separatorSeveralDaysList\', \'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS\', \'2\', \'TXT_CALENDAR_SEPARATOR_SEVERAL_DAYS_INFO\', \'5\', \'TXT_CALENDAR_SEPARATOR_SPACE,TXT_CALENDAR_SEPARATOR_HYPHEN,TXT_CALENDAR_SEPARATOR_TO,TXT_CALENDAR_SEPARATOR_BREAK\', \'\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'53\', \'16\', \'showClockList\', \'TXT_CALENDAR_SHOW_CLOCK\', \'1\', \'TXT_CALENDAR_SEPARATOR_SHOW_CLOCK_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'54\', \'16\', \'showStartDateList\', \'TXT_CALENDAR_SHOW_START_DATE\', \'1\', \'TXT_CALENDAR_SHOW_START_DATE_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'55\', \'16\', \'showEndDateList\', \'TXT_CALENDAR_SHOW_END_DATE\', \'2\', \'TXT_CALENDAR_SHOW_END_DATE_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'56\', \'16\', \'showStartTimeList\', \'TXT_CALENDAR_SHOW_START_TIME\', \'2\', \'TXT_CALENDAR_SHOW_START_TIME_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'57\', \'16\', \'showEndTimeList\', \'TXT_CALENDAR_SHOW_END_TIME\', \'2\', \'TXT_CALENDAR_SHOW_END_TIME_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'58\', \'5\', \'maxSeriesEndsYear\', \'TXT_CALENDAR_MAX_SERIES_ENDS_YEAR\', \'0\', \'TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_INFO\', \'5\', \'TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_1_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_2_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_3_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_4_YEARS,TXT_CALENDAR_MAX_SERIES_ENDS_YEAR_5_YEARS\', \'\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'59\', \'5\', \'showEventsOnlyInActiveLanguage\', \'TXT_CALENDAR_SHOW_EVENTS_ONLY_IN_ACTIVE_LANGUAGE\', \'1\', \'\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'60\', \'16\', \'listViewPreview\', \'TXT_CALENDAR_SHOW_PREVIEW\', \'0\', \'\', \'7\', \'\', \'listPreview\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'61\', \'17\', \'detailViewPreview\', \'TXT_CALENDAR_SHOW_PREVIEW\', \'0\', \'\', \'7\', \'\', \'detailPreview\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'62\', \'19\', \'placeDataForm\', \'\', \'0\', \'\', \'5\', \'\', \'getPlaceDataDorpdown\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'63\', \'19\', \'placeDataHost\', \'TXT_CALENDAR_PLACE_DATA_HOST\', \'1\', \'TXT_CALENDAR_PLACE_DATA_STATUS_INFO\', \'3\', \'TXT_CALENDAR_PLACE_DATA_DEFAULT,TXT_CALENDAR_PLACE_DATA_FROM_MEDIADIR,TXT_CALENDAR_PLACE_DATA_FROM_BOTH\', \'\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'64\', \'19\', \'placeDataHostForm\', \'\', \'0\', \'\', \'5\', \'\', \'getPlaceDataDorpdown\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'65\', \'5\', \'frontendPastEvents\', \'TXT_CALENDAR_FRONTEND_PAST_EVENTS\', \'0\', \'\', \'5\', \'TXT_CALENDAR_FRONTEND_PAST_EVENTS_DAY,TXT_CALENDAR_FRONTEND_PAST_EVENTS_END\', \'\', \'15\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'66\', \'5\', \'constrainAdditionalRecurrences\', \'TXT_CALENDAR_CONSTRAIN_ADDITIONAL_RECURRENCES\', \'2\', \'TXT_CALENDAR_CONSTRAIN_ADDITIONAL_RECURRENCES_INFO\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'67\', \'5\', \'seriesDouranceType\', \'TXT_CALENDAR_DEFAULT_DOURANCE_TYPE\', \'0\', \'\', \'5\', \'TXT_CALENDAR_SERIES_PATTERN_NO_ENDDATE,TXT_CALENDAR_SERIES_PATTERN_END_AFTER_X_OCCURRENCES,TXT_CALENDAR_SERIES_PATTERN_END_BY_DATE\', \'\', \'11\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'68\', \'5\', \'seriesDouranceEvents\', \'TXT_CALENDAR_DEFAULT_OCCURRENCES_COUNT\', \'5\', \'TXT_CALENDAR_DEFAULT_OCCURRENCES_COUNT_TOOLTIP\', \'1\', \'\', \'\', \'12\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'69\', \'5\', \'backendSortOrder\', \'TXT_CALENDAR_BACKEND_SORT_ORDER\', \'0\', \'\', \'5\', \'TXT_CALENDAR_ASC,TXT_CALENDAR_DESC\', \'\', \'13\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings` (`id`, `section_id`, `name`, `title`, `value`, `info`, `type`, `options`, `special`, `order`) VALUES (\'70\', \'5\', \'useWaitlist\', \'TXT_CALENDAR_USE_WAITLIST\', \'2\', \'TXT_CALENDAR_USE_WAITLIST_TOOLTIP\', \'3\', \'TXT_CALENDAR_ACTIVATE,TXT_CALENDAR_DEACTIVATE\', \'\', \'16\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_settings_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'1\', \'0\', \'0\', \'global\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'2\', \'0\', \'1\', \'form\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'3\', \'0\', \'2\', \'mails\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'4\', \'0\', \'3\', \'hosts\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'5\', \'1\', \'0\', \'global\', \'TXT_CALENDAR_GLOBAL\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'6\', \'1\', \'1\', \'headlines\', \'TXT_CALENDAR_HEADLINES\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'7\', \'4\', \'0\', \'publication\', \'TXT_CALENDAR_PUBLICATION\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'8\', \'1\', \'2\', \'categories\', \'TXT_CALENDAR_CATEGORIES\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'9\', \'0\', \'4\', \'payment\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'10\', \'9\', \'0\', \'payment\', \'TXT_CALENDAR_PAYMENT\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'11\', \'9\', \'1\', \'paymentBill\', \'TXT_CALENDAR_PAYMENT_BILL\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'12\', \'9\', \'2\', \'paymentYellowpay\', \'TXT_CALENDAR_PAYMENT_YELLOWPAY\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'13\', \'9\', \'1\', \'paymentBank\', \'TXT_CALENDAR_PAYMENT_BANK\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'14\', \'0\', \'5\', \'dateDisplay\', \'TXT_CALENDAR_DATE_DISPLAY\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'15\', \'14\', \'0\', \'dateGlobal\', \'TXT_CALENDAR_GLOBAL\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'16\', \'14\', \'1\', \'dateDisplayList\', \'TXT_CALENDAR_DATE_DISPLAY_LIST\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'17\', \'14\', \'2\', \'dateDisplayDetail\', \'TXT_CALENDAR_DATE_DISPLAY_DETAIL\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'18\', \'1\', \'3\', \'frontend_submission\', \'TXT_CALENDAR_FRONTEND_SUBMISSION\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_settings_section` (`id`, `parent`, `order`, `name`, `title`) VALUES (\'19\', \'1\', \'4\', \'location_host\', \'TXT_CALENDAR_EVENT_LOCATION\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_calendar_style` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tableWidth` varchar(4) NOT NULL DEFAULT \'141\',
  `tableHeight` varchar(4) NOT NULL DEFAULT \'92\',
  `tableColor` varchar(7) NOT NULL DEFAULT \'\',
  `tableBorder` int(11) NOT NULL DEFAULT \'0\',
  `tableBorderColor` varchar(7) NOT NULL DEFAULT \'\',
  `tableSpacing` int(11) NOT NULL DEFAULT \'0\',
  `fontSize` int(11) NOT NULL DEFAULT \'10\',
  `fontColor` varchar(7) NOT NULL DEFAULT \'\',
  `numColor` varchar(7) NOT NULL DEFAULT \'\',
  `normalDayColor` varchar(7) NOT NULL DEFAULT \'\',
  `normalDayRollOverColor` varchar(7) NOT NULL DEFAULT \'\',
  `curDayColor` varchar(7) NOT NULL DEFAULT \'\',
  `curDayRollOverColor` varchar(7) NOT NULL DEFAULT \'\',
  `eventDayColor` varchar(7) NOT NULL DEFAULT \'\',
  `eventDayRollOverColor` varchar(7) NOT NULL DEFAULT \'\',
  `shownEvents` int(4) NOT NULL DEFAULT \'10\',
  `periodTime` varchar(5) NOT NULL DEFAULT \'00 23\',
  `stdCat` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_style` (`id`, `tableWidth`, `tableHeight`, `tableColor`, `tableBorder`, `tableBorderColor`, `tableSpacing`, `fontSize`, `fontColor`, `numColor`, `normalDayColor`, `normalDayRollOverColor`, `curDayColor`, `curDayRollOverColor`, `eventDayColor`, `eventDayRollOverColor`, `shownEvents`, `periodTime`, `stdCat`) VALUES (\'1\', \'141\', \'92\', \'#ffffff\', \'1\', \'#cccccc\', \'0\', \'10\', \'#000000\', \'#0000ff\', \'#ffffff\', \'#eeeeee\', \'#00ccff\', \'#0066ff\', \'#00cc00\', \'#009900\', \'10\', \'00 23\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_calendar_style` (`id`, `tableWidth`, `tableHeight`, `tableColor`, `tableBorder`, `tableBorderColor`, `tableSpacing`, `fontSize`, `fontColor`, `numColor`, `normalDayColor`, `normalDayRollOverColor`, `curDayColor`, `curDayRollOverColor`, `eventDayColor`, `eventDayRollOverColor`, `shownEvents`, `periodTime`, `stdCat`) VALUES (\'2\', \'141\', \'92\', \'#ffffff\', \'1\', \'#cccccc\', \'0\', \'10\', \'#000000\', \'#0000ff\', \'#ffffff\', \'#eeeeee\', \'#00ccff\', \'#0066ff\', \'#00cc00\', \'#009900\', \'10\', \'05 19\', \'1>1 2>0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
