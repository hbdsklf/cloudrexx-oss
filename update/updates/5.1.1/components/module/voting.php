<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _votingUpdate()
{
    try{
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX . 'voting_system',
            array(
                'id'               => array('type' =>    'INT',                 'notnull' => true, 'primary'     => true,   'auto_increment' => true),
                'date'             => array('type' =>    'TIMESTAMP',           'notnull' => true, 'default_expr'=> 'CURRENT_TIMESTAMP'),
                'title'            => array('type' =>    'VARCHAR(60)',         'notnull' => true, 'default'     => '',     'renamefrom' => 'name'),
                'question'         => array('type' =>    'TEXT',                'notnull' => false),
                'status'           => array('type' =>    'TINYINT(1)',          'notnull' => false,'default'     => 1),
                'votes'            => array('type' =>    'INT(11)',             'notnull' => false,'default'     => 0),
                'submit_check'     => array('type' => "ENUM('cookie','email')", 'notnull' => true, 'default'    => 'cookie'),
                'additional_nickname' => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_forename' => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_surname'  => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_phone'    => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_street'   => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_zip'      => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_email'    => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_city'     => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
                'additional_comment'  => array('type' => 'TINYINT(1)',          'notnull' => true, 'default'     => 0),
            )
        );
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'voting_additionaldata',
            array(
                'id'                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                'nickname'           => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => '', 'renamefrom' => 'name'),
                'surname'            => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => ''),
                'phone'              => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => ''),
                'street'             => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => ''),
                'zip'                => array('type' => 'VARCHAR(30)', 'notnull' => true, 'default' => ''),
                'city'               => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => ''),
                'email'              => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => ''),
                'comment'            => array('type' => 'TEXT', 'after' => 'email'),
                'voting_system_id'   => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'renamefrom' => 'voting_sytem_id'),
                'date_entered'       => array('type' => 'TIMESTAMP', 'notnull' => true, 'default_expr'=> 'CURRENT_TIMESTAMP', 'on_update' => 'CURRENT_TIMESTAMP'),
                'forename'           => array('type' => 'VARCHAR(80)', 'notnull' => true, 'default' => '')
            ),
            array(
                'voting_system_id'   => array('fields' => array('voting_system_id'))
            )
        );
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'voting_email',
            array(
                'id'     => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                'email'  => array('type' => 'VARCHAR(255)'),
                'valid'  => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0')
            ),
            array(
                'email'  => array('fields' => array('email'), 'type' => 'UNIQUE')
            )
        );
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'voting_rel_email_system',
            array(
                'email_id'   => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                'system_id'  => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                'voting_id'  => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                'valid'      => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0')
            ),
            array(
                'email_id'   => array('fields' => array('email_id','system_id'), 'type' => 'UNIQUE')
            )
        );
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    return true;
}
function _votingInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'voting_additionaldata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(80) NOT NULL DEFAULT \'\',
  `surname` varchar(80) NOT NULL DEFAULT \'\',
  `phone` varchar(80) NOT NULL DEFAULT \'\',
  `street` varchar(80) NOT NULL DEFAULT \'\',
  `zip` varchar(30) NOT NULL DEFAULT \'\',
  `city` varchar(80) NOT NULL DEFAULT \'\',
  `email` varchar(80) NOT NULL DEFAULT \'\',
  `comment` text NOT NULL,
  `voting_system_id` int(11) NOT NULL DEFAULT \'0\',
  `date_entered` timestamp NOT NULL ,
  `forename` varchar(80) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  KEY `voting_system_id` (`voting_system_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'voting_email` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `valid` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'voting_rel_email_system` (
  `email_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `system_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `voting_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `valid` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  UNIQUE KEY `email_id` (`email_id`,`system_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'voting_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voting_system_id` int(11) DEFAULT NULL,
  `question` char(200) DEFAULT NULL,
  `votes` int(11) DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'34\', \'8\', \'Einfache und intuitive Bedienung\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'35\', \'8\', \'Grösst mögliche Sicherheit\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'43\', \'8\', \'Vielseitige Auswahl an Modulen\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'44\', \'8\', \'Möglichst kurze Umsetzungszeit des Projektes\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'45\', \'8\', \'Kompetente und schnelle Unterstützung\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'46\', \'8\', \'Preis\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'52\', \'11\', \'Werbeagentur\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'53\', \'11\', \'IT-Firma\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'54\', \'11\', \'Consultant\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'55\', \'12\', \'Hammermässig!\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'56\', \'12\', \'Sehr schön\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'57\', \'12\', \'Ist OK\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'58\', \'12\', \'Naja...\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_results` (`id`, `voting_system_id`, `question`, `votes`) VALUES (\'59\', \'12\', \'Gar nicht\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'voting_system` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL ,
  `title` varchar(60) NOT NULL DEFAULT \'\',
  `question` text ,
  `status` tinyint(1) DEFAULT \'1\',
  `submit_check` enum(\'cookie\',\'email\') NOT NULL DEFAULT \'cookie\',
  `votes` int(11) DEFAULT \'0\',
  `additional_nickname` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_forename` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_surname` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_phone` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_street` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_zip` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_email` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_city` tinyint(1) NOT NULL DEFAULT \'0\',
  `additional_comment` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_system` (`id`, `date`, `title`, `question`, `status`, `submit_check`, `votes`, `additional_nickname`, `additional_forename`, `additional_surname`, `additional_phone`, `additional_street`, `additional_zip`, `additional_email`, `additional_city`, `additional_comment`) VALUES (\'8\', \'2010-12-13 06:44:32\', \'Wichtige Eingenschaften\', \'Welche wichtige Eigenschaften zeichnen ein anwenderfreundliches Content Management System aus?\', \'0\', \'email\', \'1\', \'1\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_system` (`id`, `date`, `title`, `question`, `status`, `submit_check`, `votes`, `additional_nickname`, `additional_forename`, `additional_surname`, `additional_phone`, `additional_street`, `additional_zip`, `additional_email`, `additional_city`, `additional_comment`) VALUES (\'11\', \'2010-12-13 06:44:39\', \'Webprojekte\', \'Mit wem würden Sie Ihr Webprojekt besprechen?\', \'0\', \'cookie\', \'4\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'voting_system` (`id`, `date`, `title`, `question`, `status`, `submit_check`, `votes`, `additional_nickname`, `additional_forename`, `additional_surname`, `additional_phone`, `additional_street`, `additional_zip`, `additional_email`, `additional_city`, `additional_comment`) VALUES (\'12\', \'2010-12-13 06:44:45\', \'Wie gefällt Ihnen das neue Layout?\', \'Wie gefällt Ihnen das neue Layout?\', \'1\', \'cookie\', \'2\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}