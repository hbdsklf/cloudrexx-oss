<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _feedUpdate()
{
    try {
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'module_feed_newsml_documents',
            array(
                'id'                     => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                'publicIdentifier'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                'providerId'             => array('type' => 'text', 'after' => 'publicIdentifier'),
                'dateId'                 => array('type' => 'INT(8)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'providerId'),
                'newsItemId'             => array('type' => 'text', 'after' => 'dateId'),
                'revisionId'             => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'newsItemId'),
                'thisRevisionDate'       => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0', 'after' => 'revisionId'),
                'urgency'                => array('type' => 'SMALLINT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'thisRevisionDate'),
                'subjectCode'            => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'urgency'),
                'headLine'               => array('type' => 'VARCHAR(67)', 'notnull' => true, 'default' => '', 'after' => 'subjectCode'),
                'dataContent'            => array('type' => 'text', 'after' => 'headLine'),
                'is_associated'          => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'dataContent'),
                'media_type'             => array('type' => 'ENUM(\'Text\',\'Graphic\',\'Photo\',\'Audio\',\'Video\',\'ComplexData\')', 'notnull' => true, 'default' => 'Text', 'after' => 'is_associated'),
                'source'                 => array('type' => 'text', 'after' => 'media_type'),
                'properties'             => array('type' => 'text', 'after' => 'source')
            ),
            array(
                'unique'                 => array('fields' => array('publicIdentifier'), 'type' => 'UNIQUE')
            )
        );
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    return true;
}
function _feedInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_feed_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL DEFAULT \'\',
  `status` int(1) NOT NULL DEFAULT \'1\',
  `time` int(100) NOT NULL DEFAULT \'0\',
  `lang` int(1) NOT NULL DEFAULT \'0\',
  `pos` int(3) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_feed_category` (`id`, `name`, `status`, `time`, `lang`, `pos`) VALUES (\'1\', \'Internet News\', \'1\', \'1134028532\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_feed_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subid` int(11) NOT NULL DEFAULT \'0\',
  `name` varchar(150) NOT NULL DEFAULT \'\',
  `link` varchar(150) NOT NULL DEFAULT \'\',
  `filename` varchar(150) NOT NULL DEFAULT \'\',
  `articles` int(2) NOT NULL DEFAULT \'0\',
  `cache` int(4) NOT NULL DEFAULT \'3600\',
  `time` int(100) NOT NULL DEFAULT \'0\',
  `image` int(1) NOT NULL DEFAULT \'1\',
  `status` int(1) NOT NULL DEFAULT \'1\',
  `pos` int(3) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_feed_news` (`id`, `subid`, `name`, `link`, `filename`, `articles`, `cache`, `time`, `image`, `status`, `pos`) VALUES (\'3\', \'1\', \'pressetext.schweiz News\', \'http://www.pressetext.ch/produkte/rss/schlagzeilen.mc?land=ch&channel=ht&show=rss_2\', \'\', \'10\', \'3600\', \'1292836792\', \'1\', \'1\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_feed_news` (`id`, `subid`, `name`, `link`, `filename`, `articles`, `cache`, `time`, `image`, `status`, `pos`) VALUES (\'4\', \'1\', \'pressetext.deutschland News\', \'http://pressetext.com/produkte/rss/schlagzeilen.mc?land=de&channel=ht&show=rss_2\', \'\', \'10\', \'3600\', \'1236268968\', \'1\', \'1\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_feed_news` (`id`, `subid`, `name`, `link`, `filename`, `articles`, `cache`, `time`, `image`, `status`, `pos`) VALUES (\'5\', \'1\', \'pressetext.österreich News\', \'http://pressetext.com/produkte/rss/schlagzeilen.mc?land=at&channel=ht&show=rss_2\', \'\', \'50\', \'3600\', \'1235730600\', \'1\', \'1\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_feed_newsml_association` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pId_master` text NOT NULL,
  `pId_slave` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_feed_newsml_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `providerId` text NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT \'\',
  `subjectCodes` text NOT NULL,
  `showSubjectCodes` enum(\'all\',\'only\',\'exclude\') NOT NULL DEFAULT \'all\',
  `template` text NOT NULL,
  `limit` smallint(6) NOT NULL DEFAULT \'0\',
  `showPics` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `auto_update` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_feed_newsml_documents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `publicIdentifier` varchar(255) NOT NULL DEFAULT \'\',
  `providerId` text NOT NULL,
  `dateId` int(8) unsigned NOT NULL DEFAULT \'0\',
  `newsItemId` text NOT NULL,
  `revisionId` int(5) unsigned NOT NULL DEFAULT \'0\',
  `thisRevisionDate` int(14) NOT NULL DEFAULT \'0\',
  `urgency` smallint(5) unsigned NOT NULL DEFAULT \'0\',
  `subjectCode` int(10) unsigned NOT NULL DEFAULT \'0\',
  `headLine` varchar(67) NOT NULL DEFAULT \'\',
  `dataContent` text NOT NULL,
  `is_associated` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `media_type` enum(\'Text\',\'Graphic\',\'Photo\',\'Audio\',\'Video\',\'ComplexData\') NOT NULL DEFAULT \'Text\',
  `source` text NOT NULL,
  `properties` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique` (`publicIdentifier`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_feed_newsml_providers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `providerId` text NOT NULL,
  `name` varchar(40) NOT NULL DEFAULT \'\',
  `path` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}