<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _marketUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        $query = "SELECT id FROM ".DBPREFIX."module_market_settings WHERE name='codeMode'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =   "INSERT INTO `".DBPREFIX."module_market_settings` ( `id` , `name` , `value` , `description` , `type` )
                            VALUES ( NULL , 'codeMode', '1', 'TXT_MARKET_SET_CODE_MODE', '2')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_market_mail');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_market_mail'));
            return false;
        }

        if (!isset($arrColumns['MAILTO'])) {
            $query = "ALTER TABLE `".DBPREFIX."module_market_mail` ADD `mailto` VARCHAR( 10 ) NOT NULL AFTER `content`" ;
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        /*****************************************************************
        * EXTENSION:    New attributes 'color' and 'sort_id' for entries *
        * ADDED:        Contrexx v2.1.0                                  *
        *****************************************************************/
        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_market');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_market'));
            return false;
        }

        if (!isset($arrColumns['SORT_ID'])) {
            $query = "ALTER TABLE `".DBPREFIX."module_market` ADD `sort_id` INT( 4 ) NOT NULL DEFAULT '0' AFTER `paypal`" ;
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!isset($arrColumns['COLOR'])) {
            $query = "ALTER TABLE `".DBPREFIX."module_market` ADD `color` VARCHAR(50) NOT NULL DEFAULT '' AFTER `description`" ;
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        try {
            // delete obsolete table  contrexx_module_market_access
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX.'module_market_access');

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_market_spez_fields',
                array(
                    'id'         => array('type' => 'INT(5)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(100)'),
                    'value'      => array('type' => 'VARCHAR(100)'),
                    'type'       => array('type' => 'INT(1)', 'notnull' => true, 'default' => '1'),
                    'lang_id'    => array('type' => 'INT(2)', 'notnull' => true, 'default' => '0'),
                    'active'     => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES ('12', 'confirmFrontend', '1', 'TXT_MARKET_SET_CONFIRM_IN_FRONTEND', '2')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES ('13', 'useTerms', '0', 'TXT_MARKET_SET_USE_TERMS', '2')");

            \Cx\Lib\UpdateUtil::sql(
                'INSERT IGNORE INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`)
                 VALUES
                     (6, \'spez_field_6\', \'\', 1, 1, 0),
                     (7, \'spez_field_7\', \'\', 1, 1, 0),
                     (8, \'spez_field_8\', \'\', 1, 1, 0),
                     (9, \'spez_field_9\', \'\', 1, 1, 0),
                     (10, \'spez_field_10\', \'\', 1, 1, 0);
            ');
        } catch (\Cx\Lib\UpdateException $e) {
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_market',
                array(
                    'id'        => array('type' => 'INT(9)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'      => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => ''),
                    'email'     => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => ''),
                    'type'      => array('type' => 'set(\'search\',\'offer\')', 'notnull' => true, 'default' => ''),
                    'title'     => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'description'   => array('type' =>  'mediumtext', 'notnull' => true),
                    'color'     => array('type' => 'varchar(50)', 'notnull' => true, 'default' => ''),
                    'premium'   => array('type' => 'int(1)', 'notnull' => true, 'default' => '0'),
                    'picture'   => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'catid'     => array('type' => 'int(4)', 'notnull' => true, 'default' => '0'),
                    'price'     => array('type' => 'varchar(10)', 'notnull' => true, 'default' => ''),
                    'regdate'   => array('type' => 'varchar(20)', 'notnull' => true, 'default' => ''),
                    'enddate'   => array('type' => 'varchar(20)', 'notnull' => true, 'default' => ''),
                    'userid'    => array('type' => 'int(4)', 'notnull' => true, 'default' => '0'),
                    'userdetails'   => array('type' => 'int(1)', 'notnull' => true, 'default' => '0'),
                    'status'    => array('type' => 'int(1)', 'notnull' => true, 'default' => '0'),
                    'regkey'    => array('type' => 'varchar(50)', 'notnull' => true, 'default' => ''),
                    'paypal'    => array('type' => 'int(1)', 'notnull' => true, 'default' => '0'),
                    'sort_id'   => array('type' => 'int(4)', 'notnull' => true, 'default' => '0'),
                    'spez_field_1'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_2'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_3'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_4'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_5'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_6'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_7'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_8'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_9'  => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                    'spez_field_10' => array('type' => 'varchar(255)', 'notnull' => true, 'default' => ''),
                ),
                array(
                    'description' => array('fields' => array('description'), 'type' => 'FULLTEXT'),
                    'title'       => array('fields' => array('description', 'title'), 'type' => 'FULLTEXT'),
                ),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //Update script for moving the folder
        $mediaPath       = ASCMS_DOCUMENT_ROOT . '/media';
        $sourceMediaPath = $mediaPath . '/market';
        $targetMediaPath = $mediaPath . '/Market';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceMediaPath, $targetMediaPath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceMediaPath, $targetMediaPath
            ));
            return false;
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'content'          => 'module_market_mail',
        );
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    return true;
}
function _marketInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_market` (
  `id` int(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `email` varchar(100) NOT NULL DEFAULT \'\',
  `type` set(\'search\',\'offer\') NOT NULL DEFAULT \'\',
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `description` mediumtext NOT NULL,
  `color` varchar(50) NOT NULL DEFAULT \'\',
  `premium` int(1) NOT NULL DEFAULT \'0\',
  `picture` varchar(255) NOT NULL DEFAULT \'\',
  `catid` int(4) NOT NULL DEFAULT \'0\',
  `price` varchar(10) NOT NULL DEFAULT \'\',
  `regdate` varchar(20) NOT NULL DEFAULT \'\',
  `enddate` varchar(20) NOT NULL DEFAULT \'\',
  `userid` int(4) NOT NULL DEFAULT \'0\',
  `userdetails` int(1) NOT NULL DEFAULT \'0\',
  `status` int(1) NOT NULL DEFAULT \'0\',
  `regkey` varchar(50) NOT NULL DEFAULT \'\',
  `paypal` int(1) NOT NULL DEFAULT \'0\',
  `sort_id` int(4) NOT NULL DEFAULT \'0\',
  `spez_field_1` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_2` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_3` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_4` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_5` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_6` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_7` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_8` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_9` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_10` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `description` (`description`),
  FULLTEXT KEY `title` (`description`,`title`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market` (`id`, `name`, `email`, `type`, `title`, `description`, `color`, `premium`, `picture`, `catid`, `price`, `regdate`, `enddate`, `userid`, `userdetails`, `status`, `regkey`, `paypal`, `sort_id`, `spez_field_1`, `spez_field_2`, `spez_field_3`, `spez_field_4`, `spez_field_5`, `spez_field_6`, `spez_field_7`, `spez_field_8`, `spez_field_9`, `spez_field_10`) VALUES (\'8\', \'CMS System Benutzer\', \'support@cloudrexx.com\', \'offer\', \'Cloudrexx\', \'Cloudrexx ist ein modernes, einzigartiges und modulares Web Content Management System (WCMS) für die komplette Verwaltung einer Webseite. Zudem kann Cloudrexx auch für andere Informationsangebote wie Intranet, Extranet, eShop, Portal und weiteres eingesetzt werden. Das Cloudrexx basiert auf neuster PHP und MySQL Technologie und besticht in der einfachen Bedienung\', \'\', \'0\', \'9d4cb8ed9e74fe0b84c32980142a4640.jpg\', \'11\', \'990\', \'1292236792\', \'1378850400\', \'1\', \'0\', \'1\', \'\', \'0\', \'0\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_market_categories` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `description` varchar(255) NOT NULL DEFAULT \'\',
  `displayorder` int(4) NOT NULL DEFAULT \'0\',
  `status` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'4\', \'Antiquitäten & Kunst\', \'Bilder, Antikes, Porzellan, Keramik, Skulpturen, Stiche, Kunstdrucke, Malerei, Grafiken, antike Einrichtung. Kaufe antike und moderne Waren und Kunst.\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'6\', \'Audio, TV & Video\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'7\', \'Auto & Motorrad\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'8\', \'Briefmarken\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'9\', \'Bücher & Comics\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'10\', \'Büro & Gewerbe\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'11\', \'Computer & Netzwerk\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'12\', \'Filme & DVD\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'13\', \'Foto & Optik\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'14\', \'Games & Spielkonsolen\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'15\', \'Handwerk & Garten\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'16\', \'Handy, Festnetz, Funk\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'17\', \'Haushalt & Wohnen\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'18\', \'Kind & Baby\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'19\', \'Kleidung & Accessoires\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'20\', \'Kosmetik & Pflege\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'21\', \'Modellbau & Hobby\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'22\', \'Münzen\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'23\', \'Musik & Musikinstrumente\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'24\', \'Sammeln & Seltenes\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'25\', \'Spielzeug & Basteln\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'26\', \'Sport\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'27\', \'Tickets & Gutscheine\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'28\', \'Tierzubehör\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'29\', \'Uhren & Schmuck\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_categories` (`id`, `name`, `description`, `displayorder`, `status`) VALUES (\'30\', \'Wein & Genuss\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_market_mail` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `content` longtext NOT NULL,
  `mailto` varchar(10) NOT NULL,
  `mailcc` mediumtext NOT NULL,
  `active` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_mail` (`id`, `title`, `content`, `mailto`, `mailcc`, `active`) VALUES (\'1\', \'Ihr Inserat mit dem Titel [[TITLE]] wurde freigeschaltet\', \'Lieber Inserent\\r\\n\\r\\nBesten Dank für Ihre Geduld. Um eine hohe Qualität garantieren zu können, haben wir Ihr Inserat geprüft. \\r\\n\\r\\nIhr Inserat mit dem Titel «[[TITLE]]» und der ID \\\"[[ID]]\\\" wurde von unseren Mitarbeiterinnen und Mitarbeitern geprüft und freigeschaltet.\\r\\n\\r\\nIhr Inserat ist ab sofort unter [[URL]] einsehbar.\\r\\n\\r\\nSie können Ihr Inserat jederzeit unter [[LINK]] gratis ändern oder löschen.\\r\\n\\r\\nHoffentlich bis bald wieder und mit freundlichen Grüssen\\r\\n\\r\\nIhr Marktplatz Team\\r\\n\\r\\nAutomatisch generierte Nachricht\\r\\n[[DATE]]\\r\\n\\r\\n\\r\\nDer online Marktplatz\', \'\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_mail` (`id`, `title`, `content`, `mailto`, `mailcc`, `active`) VALUES (\'2\', \'Neues Inserat auf [[URL]] - ID: [[ID]]\', \'Hallo \\r\\n\\r\\nAuf [[URL]] wurde ein neues Inserat eingetragen.\\r\\n\\r\\nID:          [[ID]]\\r\\nTitel:       [[TITLE]]\\r\\nCode:        [[CODE]]\\r\\nUsername:    [[USERNAME]]\\r\\nName:        [[NAME]]\\r\\nE-Mail:      [[EMAIL]]\\r\\n\\r\\nAutomatisch generierte Nachricht\\r\\n[[DATE]]\\r\\n\', \'\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_market_paypal` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `active` int(1) NOT NULL DEFAULT \'0\',
  `profile` varchar(255) NOT NULL DEFAULT \'\',
  `price` varchar(10) NOT NULL DEFAULT \'\',
  `price_premium` varchar(10) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_paypal` (`id`, `active`, `profile`, `price`, `price_premium`) VALUES (\'1\', \'0\', \'noreply@example.com\', \'5.00\', \'2.00\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_market_settings` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `value` varchar(255) NOT NULL DEFAULT \'\',
  `description` varchar(255) NOT NULL DEFAULT \'\',
  `type` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'1\', \'maxday\', \'14\', \'TXT_MARKET_SET_MAXDAYS\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'2\', \'description\', \'0\', \'TXT_MARKET_SET_DESCRIPTION\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'3\', \'paging\', \'10\', \'TXT_MARKET_SET_PAGING\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'4\', \'currency\', \'CHF\', \'TXT_MARKET_SET_CURRENCY\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'5\', \'addEntry_only_community\', \'1\', \'TXT_MARKET_SET_ADD_ENTRY_ONLY_COMMUNITY\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'6\', \'addEntry\', \'1\', \'TXT_MARKET_SET_ADD_ENTRY\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'7\', \'editEntry\', \'1\', \'TXT_MARKET_SET_EDIT_ENTRY\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'8\', \'indexview\', \'0\', \'TXT_MARKET_SET_INDEXVIEW\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'9\', \'maxdayStatus\', \'0\', \'TXT_MARKET_SET_MAXDAYS_ON\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'10\', \'searchPrice\', \'100,200,500,1000,2000,5000\', \'TXT_MARKET_SET_EXP_SEARCH_PRICE\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'11\', \'codeMode\', \'1\', \'TXT_MARKET_SET_CODE_MODE\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'12\', \'confirmFrontend\', \'0\', \'TXT_MARKET_SET_CONFIRM_IN_FRONTEND\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_settings` (`id`, `name`, `value`, `description`, `type`) VALUES (\'13\', \'useTerms\', \'0\', \'TXT_MARKET_SET_USE_TERMS\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_market_spez_fields` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `type` int(1) NOT NULL DEFAULT \'1\',
  `lang_id` int(2) NOT NULL DEFAULT \'0\',
  `active` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'1\', \'spez_field_1\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'2\', \'spez_field_2\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'3\', \'spez_field_3\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'4\', \'spez_field_4\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'5\', \'spez_field_5\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'6\', \'spez_field_6\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'7\', \'spez_field_7\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'8\', \'spez_field_8\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'9\', \'spez_field_9\', \'\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_market_spez_fields` (`id`, `name`, `value`, `type`, `lang_id`, `active`) VALUES (\'10\', \'spez_field_10\', \'\', \'1\', \'1\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
