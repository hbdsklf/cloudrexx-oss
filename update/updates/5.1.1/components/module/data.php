<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _dataUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        $arrTables  = $objDatabase->MetaTables('TABLES');
        if (!sizeof($arrTables)) {
            return _databaseError("MetaTables('TABLES')", 'Could not read Table metadata');
        }
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        // Create neccessary tables if not present
        $tables = array(
            'module_data_categories' => "CREATE TABLE `".DBPREFIX."module_data_categories` (
                  `category_id` int(4) unsigned NOT NULL default '0',
                  `lang_id` int(2) unsigned NOT NULL default '0',
                  `is_active` enum('0','1') NOT NULL default '1',
                  `parent_id` int(10) unsigned NOT NULL default '0',
                  `name` varchar(100) NOT NULL default '',
                  `active` enum('0','1') NOT NULL default '1',
                  `cmd` int(10) unsigned NOT NULL default '1',
                  `action` enum('content','overlaybox','subcategories') NOT NULL default 'content',
                  `sort` int(10) unsigned NOT NULL default '1',
                  `box_height` int(10) unsigned NOT NULL default '500',
                  `box_width` int(11) NOT NULL default '350',
                  `template` text NOT NULL,
                  PRIMARY KEY (`category_id`,`lang_id`)
                ) ENGINE=InnoDB",
            #################################################################################
            'module_data_message_to_category' => "CREATE TABLE `".DBPREFIX."module_data_message_to_category` (
                  `message_id` int(6) unsigned NOT NULL default '0',
                  `category_id` int(4) unsigned NOT NULL default '0',
                  `lang_id` int(2) unsigned NOT NULL default '0',
                  PRIMARY KEY (`message_id`,`category_id`,`lang_id`),
                  KEY `category_id` (`category_id`)
                ) ENGINE=InnoDB",
            #################################################################################
            'module_data_messages' => "CREATE TABLE `".DBPREFIX."module_data_messages` (
                  `message_id` int(6) unsigned NOT NULL auto_increment,
                  `user_id` int(5) unsigned NOT NULL default '0',
                  `time_created` int(14) unsigned NOT NULL default '0',
                  `time_edited` int(14) unsigned NOT NULL default '0',
                  `hits` int(7) unsigned NOT NULL default '0',
                  `active` enum('0','1') NOT NULL default '1',
                  `sort` int(10) unsigned NOT NULL default '1',
                  `mode` set('normal','forward') NOT NULL default 'normal',
                  `release_time` int(15) NOT NULL default '0',
                  `release_time_end` int(15) NOT NULL default '0',
                  PRIMARY KEY (`message_id`)
                ) ENGINE=InnoDB",
            #################################################################################
            'module_data_settings' => "CREATE TABLE `".DBPREFIX."module_data_settings` (
                  `name` varchar(50) NOT NULL default '',
                  `value` text NOT NULL,
                  PRIMARY KEY (`name`)
                ) ENGINE=InnoDB"
        );

          ///////////////////////////////////////////////////////////////////
         // Create tables                                                 //
        ///////////////////////////////////////////////////////////////////
        foreach ($tables as $name => $query) {
            if (in_array(DBPREFIX.$name, $arrTables)) continue;
            if (!$objDatabase->Execute($query)) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
    // TODO: Unused
    //        $installed[] = $name;
        }



        try{
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_data_placeholders',
                array(
                    'id'             => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'type'           => array('type' => 'SET(\'cat\',\'entry\')', 'notnull' => true, 'default' => ''),
                    'ref_id'         => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'placeholder'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '')
                ),
                array(
                    'placeholder'    => array('fields' => array('placeholder'), 'type' => 'UNIQUE'),
                    'type'           => array('fields' => array('type','ref_id'), 'type' => 'UNIQUE')
                )
            );
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }




        $settings_query = "
    INSERT INTO `".DBPREFIX."module_data_settings` (`name`, `value`) VALUES
    ('data_block_activated', '0'),
    ('data_block_messages', '3'),
    ('data_comments_activated', '1'),
    ('data_comments_anonymous', '1'),
    ('data_comments_autoactivate', '1'),
    ('data_comments_editor', 'wysiwyg'),
    ('data_comments_notification', '1'),
    ('data_comments_timeout', '30'),
    ('data_entry_action', 'overlaybox'),
    ('data_general_introduction', '150'),
    ('data_rss_activated', '0'),
    ('data_rss_comments', '10'),
    ('data_rss_messages', '5'),
    ('data_tags_hitlist', '5'),
    ('data_target_cmd', '1'),
    ('data_template_category',
    '<!-- BEGIN datalist_category -->
    <!-- this displays the category and the subcategories -->
    <div class=\\\"datalist_block\\\">
    <dl>
      <!-- BEGIN category -->
      <dt class=\\\"cattitle\\\"><div class=\\\"bg\\\"><h4>[[CATTITLE]]</h4></div></dt>
      <dd class=\\\"catcontent\\\">
        <dl>
          <!-- BEGIN entry -->
          <dt>[[TITLE]]</dt>
          <dd>
            [[IMAGE]] [[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]] [[TARGET]]>[[TXT_MORE]]</a>
            <br style=\\\"clear: both;\\\" />
          </dd>
          <!-- END entry -->
        </dl>
      </dd>
      <!-- END category -->
    </dl>
    </div>
    <!-- END datalist_category -->
    <!-- BEGIN datalist_single_category-->
    <!-- this displays just the entries of the category -->
    <div class=\\\"datalist_block\\\">
    <dl>
      <!-- BEGIN single_entry -->
      <dt class=\\\"cattitle\\\"><div class=\\\"bg\\\"><h4>[[TITLE]]</h4></div></dt>
      <dd class=\\\"catcontent2\\\">
        [[IMAGE]] <p>[[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]] [[TARGET]]>[[TXT_MORE]]</a></p>
        <div style=\\\"clear: both;\\\" />
      </dd>
      <!-- END single_entry -->
    </dl>
    </div>
    <!-- END datalist_single_category -->
    '),
    ('data_template_entry',
    '<!-- BEGIN datalist_entry-->
    <div class=\\\"datalist_block\\\">
    <dl>
      <dt>[[TITLE]]</dt>
      <dd>
        [[IMAGE]] [[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]]>[[TXT_MORE]]</a>
        <br style=\\\"clear: both;\\\" />
      </dd>
    </dl>
    </div>
    <!-- END datalist_entry -->
        '),
    ('data_template_thickbox',
    '<!-- BEGIN thickbox -->
    <dl class=\\\"data_module\\\">
      <dt><h6 style=\\\"margin-bottom:10px;\\\">[[TITLE]]</h6></dt>
      <dd style=\\\"clear:left;\\\">
        <!-- BEGIN image -->
        <img src=\\\"[[PICTURE]]\\\" style=\\\"float: left; margin-right: 5px;\\\" />
        <!-- END image -->
        [[CONTENT]]
        <!-- BEGIN attachment -->
        <img src=\\\"/themes/default/images/arrow.gif\\\" width=\\\"16\\\" height=\\\"8\\\" />
        <a href=\\\"javascript:void(0);\\\" onclick=\\\"window.open(\\'[[HREF]]\\', \\'attachment\\');\\\">[[TXT_DOWNLOAD]]</a>
        <!-- END attachment -->
      </dd>
    </dl>
    <!--<br /><img src=\\\"/themes/default/images/arrow.gif\\\" width=\\\"16\\\" height=\\\"8\\\" /><a onclick=\\\"Javascript:window.print();\\\" style=\\\"cursor:pointer;\\\">Drucken</a>-->
    <!-- END thickbox -->
    '),
    ('data_thickbox_height', '450'),
    ('data_thickbox_width', '400'),
    ('data_voting_activated', '0');
    ";

          ///////////////////////////////////////////////////////////////////
         // data module settings                                          //
        ///////////////////////////////////////////////////////////////////
        $query = "SELECT COUNT(*) AS recordcount FROM `".DBPREFIX."module_data_settings`";
        $objResult = $objDatabase->Execute($query);
        if ($objResult === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }
        if ($objResult->fields['recordcount'] == 0) {
            // module_data_settings table is empty. Fill it with default data.
            if (!$objDatabase->Execute($settings_query)) {
                return _databaseError($settings_query, $objDatabase->ErrorMsg());
            }
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        /*********************************************************
        * EXTENSION:	Thunbmail Image & Attachment description *
        * ADDED:		Contrexx v2.1.0					         *
        *********************************************************/
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_data_messages_lang',
                array(
                    'message_id'               => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'lang_id'                  => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'is_active'                => array('type' => 'ENUM(\'0\',\'1\')', 'default' => '1'),
                    'subject'                  => array('type' => 'VARCHAR(250)', 'default' => ''),
                    'content'                  => array('type' => 'text'),
                    'tags'                     => array('type' => 'VARCHAR(250)', 'default' => ''),
                    'image'                    => array('type' => 'VARCHAR(250)', 'default' => ''),
                    'thumbnail'                => array('type' => 'VARCHAR(250)'),
                    'thumbnail_type'           => array('type' => 'ENUM(\'original\',\'thumbnail\')', 'default' => 'original', 'after' => 'thumbnail'),
                    'thumbnail_width'          => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'thumbnail_height'         => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'attachment'               => array('type' => 'VARCHAR(255)', 'default' => ''),
                    'attachment_description'   => array('type' => 'VARCHAR(255)', 'default' => ''),
                    'mode'                     => array('type' => 'SET(\'normal\',\'forward\')', 'default' => 'normal'),
                    'forward_url'              => array('type' => 'VARCHAR(255)', 'default' => ''),
                    'forward_target'           => array('type' => 'VARCHAR(40)', 'notnull' => false)
                ),
                array(),
                'InnoDB'
            );
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'content' => 'module_data_messages_lang',
            'thumbnail' => 'module_data_messages_lang',
            'template' => 'module_data_categories',
            'value' => 'module_data_settings',
        );
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    return true;
}
function _dataInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_data_categories` (
  `category_id` int(4) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'0\',
  `is_active` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `parent_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `active` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `cmd` int(10) unsigned NOT NULL DEFAULT \'1\',
  `action` enum(\'content\',\'overlaybox\',\'subcategories\') NOT NULL DEFAULT \'content\',
  `sort` int(10) unsigned NOT NULL DEFAULT \'1\',
  `box_height` int(10) unsigned NOT NULL DEFAULT \'500\',
  `box_width` int(11) NOT NULL DEFAULT \'350\',
  `template` text NOT NULL,
  PRIMARY KEY (`category_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_categories` (`category_id`, `lang_id`, `is_active`, `parent_id`, `name`, `active`, `cmd`, `action`, `sort`, `box_height`, `box_width`, `template`) VALUES (\'1\', \'1\', \'1\', \'0\', \'Links\', \'1\', \'0\', \'overlaybox\', \'1\', \'300\', \'500\', \'<!-- BEGIN datalist_category -->\\r\\n<!-- this displays the category and the subcategories -->\\r\\n<div class=\\\"datalist_block\\\">\\r\\n<dl>\\r\\n	<!-- BEGIN category -->\\r\\n	<dt class=\\\"cattitle\\\">[[CATTITLE]]</div></dt>\\r\\n	<dd class=\\\"catcontent\\\">\\r\\n		<dl>\\r\\n		<!-- BEGIN entry -->\\r\\n		<dt>[[TITLE]]</dt>\\r\\n		<dd>\\r\\n			[[IMAGE]] [[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]] [[TARGET]]>[[TXT_MORE]]</a>\\r\\n			<div style=\\\"clear: both;\\\"></div>\\r\\n		</dd>\\r\\n		<!-- END entry -->\\r\\n		</dl>\\r\\n	</dd>\\r\\n	<!-- END category -->\\r\\n</dl>\\r\\n</div>\\r\\n<!-- END datalist_category -->\\r\\n\\r\\n<!-- BEGIN datalist_single_category -->\\r\\n<!-- this displays just the entries of the category -->\\r\\n<div class=\\\"datalist_block\\\">\\r\\n<dl>\\r\\n    <!-- BEGIN single_entry -->\\r\\n    <dt class=\\\"cattitle\\\">[[TITLE]]</dt>\\r\\n    <dd class=\\\"catcontent2\\\">\\r\\n        [[IMAGE]] <p>[[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]] [[TARGET]]>[[TXT_MORE]]</a></p>\\r\\n        <div style=\\\"clear: both;\\\" ></div>\\r\\n    </dd>\\r\\n    <!-- END single_entry -->\\r\\n</dl>\\r\\n</div>\\r\\n<!-- END datalist_single_category -->\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_data_messages` (
  `message_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `time_created` int(14) unsigned NOT NULL DEFAULT \'0\',
  `time_edited` int(14) unsigned NOT NULL DEFAULT \'0\',
  `hits` int(7) unsigned NOT NULL DEFAULT \'0\',
  `active` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `sort` int(10) unsigned NOT NULL DEFAULT \'1\',
  `mode` set(\'normal\',\'forward\') NOT NULL DEFAULT \'normal\',
  `release_time` int(15) NOT NULL DEFAULT \'0\',
  `release_time_end` int(15) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_messages` (`message_id`, `user_id`, `time_created`, `time_edited`, `hits`, `active`, `sort`, `mode`, `release_time`, `release_time_end`) VALUES (\'1\', \'0\', \'1213712506\', \'1236273980\', \'0\', \'1\', \'3\', \'normal\', \'1236207600\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_data_messages_lang` (
  `message_id` int(6) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'0\',
  `is_active` enum(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `subject` varchar(250) NOT NULL DEFAULT \'\',
  `content` text NOT NULL,
  `tags` varchar(250) NOT NULL DEFAULT \'\',
  `image` varchar(250) NOT NULL DEFAULT \'\',
  `thumbnail` varchar(250) NOT NULL,
  `thumbnail_type` enum(\'original\',\'thumbnail\') NOT NULL DEFAULT \'original\',
  `thumbnail_width` int(11) unsigned NOT NULL DEFAULT \'0\',
  `thumbnail_height` int(11) unsigned NOT NULL DEFAULT \'0\',
  `attachment` varchar(255) NOT NULL DEFAULT \'\',
  `attachment_description` varchar(255) NOT NULL DEFAULT \'\',
  `mode` set(\'normal\',\'forward\') NOT NULL DEFAULT \'normal\',
  `forward_url` varchar(255) NOT NULL DEFAULT \'\',
  `forward_target` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`message_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_messages_lang` (`message_id`, `lang_id`, `is_active`, `subject`, `content`, `tags`, `image`, `thumbnail`, `thumbnail_type`, `thumbnail_width`, `thumbnail_height`, `attachment`, `attachment_description`, `mode`, `forward_url`, `forward_target`) VALUES (\'1\', \'1\', \'1\', \'Cloudrexx Support\', \'Wiki: <a href=\\\"http://wiki.cloudrexx.com/\\\">http://wiki.cloudrexx.com/</a><br />\\r\\nSupport-System: <a href=\\\"https://support.cloudrexx.com\\\">https://support.cloudrexx.com</a>\', \'\', \'\', \'\', \'original\', \'80\', \'80\', \'\', \'\', \'normal\', \'\', NULL)');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_data_message_to_category` (
  `message_id` int(6) unsigned NOT NULL DEFAULT \'0\',
  `category_id` int(4) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`message_id`,`category_id`,`lang_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_message_to_category` (`message_id`, `category_id`, `lang_id`) VALUES (\'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_data_placeholders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` set(\'cat\',\'entry\') NOT NULL DEFAULT \'\',
  `ref_id` int(11) NOT NULL DEFAULT \'0\',
  `placeholder` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `placeholder` (`placeholder`),
  UNIQUE KEY `type` (`type`,`ref_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_placeholders` (`id`, `type`, `ref_id`, `placeholder`) VALUES (\'1\', \'cat\', \'1\', \'LINKS\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_placeholders` (`id`, `type`, `ref_id`, `placeholder`) VALUES (\'2\', \'entry\', \'1\', \'DETAIL_2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_data_settings` (
  `name` varchar(50) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_block_activated\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_block_messages\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_comments_activated\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_comments_anonymous\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_comments_autoactivate\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_comments_editor\', \'wysiwyg\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_comments_notification\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_comments_timeout\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_entry_action\', \'overlaybox\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_general_introduction\', \'150\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_rss_activated\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_rss_comments\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_rss_messages\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_shadowbox_height\', \'300\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_shadowbox_width\', \'500\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_tags_hitlist\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_target_cmd\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_template_category\', \'<!-- BEGIN datalist_category -->\\r\\n<!-- this displays the category and the subcategories -->\\r\\n<div class=\\\"datalist_block\\\">\\r\\n<dl>\\r\\n	<!-- BEGIN category -->\\r\\n	<dt class=\\\"cattitle\\\">[[CATTITLE]]</dt>\\r\\n	<dd class=\\\"catcontent\\\">\\r\\n		<dl>\\r\\n		<!-- BEGIN entry -->\\r\\n		<dt>[[TITLE]]</dt>\\r\\n		<dd>\\r\\n			[[IMAGE]] [[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]] [[TARGET]]>[[TXT_MORE]]</a>\\r\\n			<br style=\\\"clear: both;\\\" />\\r\\n		</dd>\\r\\n		<!-- END entry -->\\r\\n		</dl>\\r\\n	</dd>\\r\\n	<!-- END category -->\\r\\n</dl>\\r\\n</div>\\r\\n<!-- END datalist_category -->\\r\\n\\r\\n<!-- BEGIN datalist_single_category -->\\r\\n<!-- this displays just the entries of the category -->\\r\\n<div class=\\\"datalist_block\\\">\\r\\n<dl>\\r\\n    <!-- BEGIN single_entry -->\\r\\n    <dt class=\\\"cattitle\\\">[[TITLE]]</dt>\\r\\n    <dd class=\\\"catcontent2\\\">\\r\\n        [[IMAGE]] <p>[[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]] [[TARGET]]>[[TXT_MORE]]</a></p>\\r\\n        <div style=\\\"clear: both;\\\"></div>\\r\\n    </dd>\\r\\n    <!-- END single_entry -->\\r\\n</dl>\\r\\n</div>\\r\\n<!-- END datalist_single_category -->\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_template_entry\', \'<!-- BEGIN datalist_entry -->\\r\\n<div class=\\\"datalist_block\\\">\\r\\n<dl>\\r\\n    <dt>[[TITLE]]</dt>\\r\\n    <dd>\\r\\n        [[IMAGE]] [[CONTENT]] <a href=\\\"[[HREF]]\\\" [[CLASS]]>[[TXT_MORE]]</a>\\r\\n        <div style=\\\"clear: both;\\\"></div>\\r\\n    </dd>\\r\\n</dl>\\r\\n</div>\\r\\n<!-- END datalist_entry -->\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_template_shadowbox\', \'<!-- BEGIN shadowbox -->\\r\\n<html>\\r\\n<head>\\r\\n<link rel=\\\"stylesheet\\\" type=\\\"text/css\\\" href=\\\"themes/[[THEMES_PATH]]/modules.css\\\" />\\r\\n</head>\\r\\n<body style=\\\"background-color:#060606;\\\">\\r\\n<dl class=\\\"data_module\\\">\\r\\n    <dt>[[TITLE]]</dt>\\r\\n    <dd style=\\\"clear:left;\\\">\\r\\n	<!-- BEGIN image -->\\r\\n	<img src=\\\"[[PICTURE]]\\\" style=\\\"float: left; margin-right: 5px;\\\" />\\r\\n	<!-- END image -->\\r\\n        [[CONTENT]]\\r\\n        \\r\\n        <!-- BEGIN attachment -->\\r\\n    </dd>\\r\\n    <br />\\r\\n    <dt><img src=\\\"themes/default/images/arrow.gif\\\" width=\\\"16\\\" height=\\\"8\\\" /><a href=\\\"javascript:void(0);\\\" onclick=\\\"window.open(\\\'[[HREF]]\\\', \\\'attachment\\\');\\\">[[TXT_DOWNLOAD]]</a>\\r\\n        <!-- END attachment -->\\r\\n    </dt>\\r\\n</dl>\\r\\n<!--<br />\\r\\n<img src=\\\"themes/default/images/arrow.gif\\\" width=\\\"16\\\" height=\\\"8\\\" /><a onclick=\\\"Javascript:window.print();\\\" style=\\\"cursor:pointer;\\\">Drucken</a>-->\\r\\n</body>\\r\\n</html>\\r\\n<!-- END shadowbox -->\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_data_settings` (`name`, `value`) VALUES (\'data_voting_activated\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
