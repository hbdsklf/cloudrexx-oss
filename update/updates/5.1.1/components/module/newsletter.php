<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _newsletterUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG;

    if (!in_array('newsletter_phase_1', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try{
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter',
                    array(
                        'id'                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'subject'            => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                        'template'           => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'subject'),
                        'content'            => array('type' => 'mediumtext', 'after' => 'template'),
                        'attachment'         => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'content'),
                        'priority'           => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'attachment'),
                        'sender_email'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'priority'),
                        'sender_name'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'sender_email'),
                        'return_path'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'sender_name'),
                        'smtp_server'        => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'return_path'),
                        'status'             => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'smtp_server'),
                        'count'              => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'status'),
                        'recipient_count'    => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'count'),
                        'date_create'        => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'recipient_count'),
                        'date_sent'          => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'date_create'),
                        'tmp_copy'           => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'date_sent')
                    )
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_1';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_2', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            try{
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_category',
                    array(
                        'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'status'                 => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                        'name'                   => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'status'),
                        'notification_email'     => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'name')
                    ),
                    array(
                        'name'                   => array('fields' => array('name'))
                    )
                );

                if (\Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'module_newsletter_confirm_mail')) {
                    \Cx\Lib\UpdateUtil::table(
                        DBPREFIX.'module_newsletter_confirm_mail',
                        array(
                            'id'             => array('type' => 'INT(1)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                            'title'          => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                            'content'        => array('type' => 'longtext', 'after' => 'title'),
                            'recipients'     => array('type' => 'text', 'after' => 'content')
                        )
                    );
                }

                DBG::msg("Done checking tables.. going to check settings");
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }


            //the two values notifyOnUnsubscribe and notificationUnsubscribe have been merged into the latter.
            $unsubscribeVal=1;
            try {
                DBG::msg("Retrieving old unsubscribe value if set.");
                $res = \Cx\Lib\UpdateUtil::sql("SELECT setvalue FROM ".DBPREFIX."module_newsletter_settings WHERE setname='notifyOnUnsubscribe'");
                
                if(!$res->EOF){
                    $unsubscribeVal = $res->fields['setvalue'];
                }
                else //maybe update ran already => preserve new value
                {
                    DBG::msg("Not found. Retrieving new unsubscribe value if set.");
                    $res = \Cx\Lib\UpdateUtil::sql("SELECT setvalue FROM ".DBPREFIX."module_newsletter_settings WHERE setname='notificatonUnsubscribe'");
                    if(!$res->EOF)
                    $unsubscribeVal = $res->fields['setvalue'];
                }

            }
            catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
                
            $settings = array(
                'sender_mail'             => array('setid' =>  1, 'setname' => 'sender_mail',             'setvalue' => 'info@example.com', 'status' => 1),
                'sender_name'             => array('setid' =>  2, 'setname' => 'sender_name',             'setvalue' => 'admin',            'status' => 1),
                'reply_mail'              => array('setid' =>  3, 'setname' => 'reply_mail',              'setvalue' => 'info@example.com', 'status' => 1),
                'mails_per_run'           => array('setid' =>  4, 'setname' => 'mails_per_run',           'setvalue' => '30',               'status' => 1),
                'text_break_after'        => array('setid' =>  5, 'setname' => 'text_break_after',        'setvalue' => '100',              'status' => 1),
                'test_mail'               => array('setid' =>  6, 'setname' => 'test_mail',               'setvalue' => 'info@example.com', 'status' => 1),
                'overview_entries_limit'  => array('setid' =>  7, 'setname' => 'overview_entries_limit',  'setvalue' => '10',               'status' => 1),
                'rejected_mail_operation' => array('setid' =>  8, 'setname' => 'rejected_mail_operation', 'setvalue' => 'delete',           'status' => 1),
                'defUnsubscribe'          => array('setid' =>  9, 'setname' => 'defUnsubscribe',          'setvalue' => '0',                'status' => 1),
                'notificationSubscribe'   => array('setid' => 11, 'setname' => 'notificationSubscribe',   'setvalue' => '1',                'status' => 1),
                'notificationUnsubscribe' => array('setid' => 10, 'setname' => 'notificationUnsubscribe', 'setvalue' => $unsubscribeVal,    'status' => 1),
                'recipient_attribute_status' => array('setid' => 12, 'setname' => 'recipient_attribute_status', 'setvalue' => '{"recipient_sex":{"active":true,"required":false},"recipient_salutation":{"active":true,"required":false},"recipient_title":{"active":false,"required":false},"recipient_firstname":{"active":true,"required":false},"recipient_lastname":{"active":true,"required":false},"recipient_position":{"active":false,"required":false},"recipient_company":{"active":true,"required":false},"recipient_industry":{"active":false,"required":false},"recipient_address":{"active":true,"required":false},"recipient_city":{"active":true,"required":false},"recipient_zip":{"active":true,"required":false},"recipient_country":{"active":true,"required":false},"recipient_phone":{"active":true,"required":false},"recipient_private":{"active":false,"required":false},"recipient_mobile":{"active":false,"required":false},"recipient_fax":{"active":false,"required":false},"recipient_birthday":{"active":true,"required":false},"recipient_website":{"active":false,"required":false}}',    'status' => 1),
                'reject_info_mail_text'   => array('setid' => 13, 'setname' => 'reject_info_mail_text', 'setvalue' => 'Der Newsletter konnte an folgende E-Mail-Adresse nicht versendet werden:\r\n[[EMAIL]]\r\n\r\nUm die E-Mail Adresse zu bearbeiten, klicken Sie bitte auf den folgenden Link:\r\n[[LINK]]',    'status' => 1),
            );

            try {
                DBG::msg("Reading current settings");
                $res = \Cx\Lib\UpdateUtil::sql("SELECT * FROM ".DBPREFIX."module_newsletter_settings");
                while (!$res->EOF) {
                    $field = $res->fields['setname'];
                    DBG::msg("...merging $field with default settings");
                    if(isset($settings[$field])) //do we have another value for this?
                        $settings[$field]['setvalue'] = $res->fields['setvalue'];
                    $res->MoveNext();
                }
                DBG::msg("Updating settings");
                foreach ($settings as $entry) {
                    $setid = intval    ($entry['setid']);
                    $field = addslashes($entry['setname']);
                    $value = addslashes($entry['setvalue']);
                    $status= intval    ($entry['status']);
                    DBG::msg("...deleting field $field");
                    \Cx\Lib\UpdateUtil::sql("DELETE FROM ".DBPREFIX."module_newsletter_settings WHERE setid = '$setid' OR setname = '$field'");
                    DBG::msg("...rewriting field $field");
                    \Cx\Lib\UpdateUtil::sql("
                        INSERT INTO ".DBPREFIX."module_newsletter_settings
                            (setid, setname, setvalue, status)
                        VALUES (
                            '$setid', '$field', '$value', '$status'
                        );
                    ");
                }
                DBG::msg("Deleting old unsubscribe key if set");
                \Cx\Lib\UpdateUtil::sql("DELETE FROM ".DBPREFIX."module_newsletter_settings WHERE setname='notifyOnUnsubscribe'");
                DBG::msg("Done with newsletter update");
            }
            catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }

            try {
                DBG::msg("Setting recipient count");
                $objResult = \Cx\Lib\UpdateUtil::sql("SELECT `newsletter`, COUNT(1) AS recipient_count FROM `".DBPREFIX."module_newsletter_tmp_sending` GROUP BY `newsletter`");
                if ($objResult->RecordCount()) {
                    while(!$objResult->EOF) {
                        \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_newsletter` SET `recipient_count` = ".$objResult->fields['recipient_count']." WHERE `id`=".$objResult->fields['newsletter']);
                        $objResult->MoveNext();
                    }
                }
            }
            catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }




            // Add notification recipients to confirm_mail table
            try {
                if (\Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'module_newsletter_confirm_mail')) {
                    $objResult = \Cx\Lib\UpdateUtil::sql("SELECT id FROM `".DBPREFIX."module_newsletter_confirm_mail` WHERE id='3'");
                    if ($objResult->RecordCount() == 0) {
                        DBG::msg("inserting standard confirm mails");
                        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_newsletter_confirm_mail` (`id` ,`title` ,`content` ,`recipients`) VALUES ('3', '[[url]] - Neue Newsletter Empfänger [[action]]', 'Hallo Admin Eine neue Empfänger [[action]] in ihrem Newsletter System. Automatisch generierte Nachricht [[date]]', '');");
                    }
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_2';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_3', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_access_user',
                    array(
                        'accessUserID'               => array('type' => 'INT(5)', 'unsigned' => true),
                        'newsletterCategoryID'       => array('type' => 'INT(11)', 'after' => 'accessUserID'),
                        'code'                       => array('type' => 'VARCHAR(255)', 'after' => 'newsletterCategoryID', 'notnull' => true, 'default' => ''),
                        'source'                     => array('type' => 'ENUM(\'backend\',\'opt-in\',\'api\')', 'after' => 'code', 'notnull' => true, 'default' => 'backend'),
                        'consent'                    => array('type' => 'TIMESTAMP', 'notnull' => false, 'default' => null, 'after' => 'source'),
                    ),
                    array(
                        'rel'                        => array('fields' => array('accessUserID','newsletterCategoryID'), 'type' => 'UNIQUE'),
                        'accessUserID'               => array('fields' => array('accessUserID'))
                    )
                );

                // set random newsletter code for access recipients
                \Cx\Lib\UpdateUtil::sql('UPDATE '.DBPREFIX.'module_newsletter_access_user SET `code` = SUBSTR(MD5(RAND()),1,12) WHERE `code` = \'\'');

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_rel_user_cat',
                    array(
                        'user'           => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'primary' => true),
                        'category'       => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'user', 'primary' => true),
                        'source'         => array('type' => 'ENUM(\'backend\',\'opt-in\',\'api\')', 'notnull' => true, 'default' => 'backend', 'after' => 'category'),
                        'consent'        => array('type' => 'timestamp', 'notnull' => false, 'default' => null, 'after' => 'source')
                    )
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_3';
        return 'timeout';
    }


    if (!in_array('newsletter_phase_4', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            try {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_rel_usergroup_newsletter',
                    array(
                        'userGroup'      => array('type' => 'INT(10)', 'unsigned' => true),
                        'newsletter'     => array('type' => 'INT(10)', 'unsigned' => true, 'after' => 'userGroup')
                    ),
                    array(
                        'uniq'           => array('fields' => array('userGroup','newsletter'), 'type' => 'UNIQUE')
                    )
                );

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_settings',
                    array(
                        'setid'          => array('type' => 'INT(6)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'setname'        => array('type' => 'VARCHAR(250)', 'after' => 'setid', 'notnull' => true, 'default' => ''),
                        'setvalue'       => array('type' => 'text', 'after' => 'setname'),
                        'status'         => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'setvalue')
                    ),
                    array(
                        'setname'        => array('fields' => array('setname'), 'type' => 'UNIQUE')
                    )
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }


        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_tmp_sending',
                    array(
                        'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'newsletter'     => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                        'email'          => array('type' => 'VARCHAR(255)', 'after' => 'newsletter', 'notnull' => true, 'default' => ''),
                        'sendt'          => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '0', 'after' => 'email'),
                        'type'           => array('type' => 'ENUM(\'access\',\'newsletter\',\'core\',\'crm\')', 'notnull' => true, 'default' => 'newsletter', 'after' => 'sendt'),
                        'code'           => array('type' => 'VARCHAR(10)', 'after' => 'type')
                    ),
                    array(
                        'unique_email'   => array('fields' => array('newsletter','email'), 'type' => 'UNIQUE'),
                        'email'          => array('fields' => array('email'))
                    )
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_4';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_5', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            try {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_email_link',
                    array(
                        'id'             => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'email_id'       => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'id'),
                        'title'          => array('type' => 'VARCHAR(255)', 'after' => 'email_id'),
                        'url'            => array('type' => 'VARCHAR(255)', 'after' => 'title')
                    ),
                    array(
                        'email_id'       => array('fields' => array('email_id'))
                    )
                );

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_email_link_feedback',
                    array(
                        'id'                 => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'link_id'            => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'id'),
                        'email_id'           => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'link_id'),
                        'recipient_id'       => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'email_id'),
                        'recipient_type'     => array('type' => 'ENUM(\'access\',\'newsletter\',\'crm\')', 'after' => 'recipient_id')
                    ),
                    array(
                        'link_id'            => array('fields' => array('link_id','email_id','recipient_id','recipient_type'), 'type' => 'UNIQUE'),
                        'email_id'           => array('fields' => array('email_id'))
                    )
                );

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_template',
                    array(
                        'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'name'           => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                        'description'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'name'),
                        'html'           => array('type' => 'text', 'after' => 'description'),
                        'required'       => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'html'),
                        'type'           => array('type' => 'ENUM(\'e-mail\',\'news\')', 'notnull' => true, 'default' => 'e-mail', 'after' => 'required')
                    )
                );

                // migrate country field
                if (newsletter_migrate_country_field() == 'timeout') {
                    return 'timeout';
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_5';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_6', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try { 
                // IMPORTANT: the table definition statement of module_newsletter_user must be AFTER newsletter_migrate_country_field() has been called!
                // fix missing columns & rename old columns if required
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_user',
                    array(
                        'id'                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'code'               => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                        'email'              => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'code'),
                        'uri'                => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'email'),
                        'sex'                => array('type' => 'ENUM(\'m\',\'f\')', 'notnull' => false, 'after' => 'uri'),
                        'salutation'         => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'sex', 'renamefrom' => 'title'),
                        'title'              => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'salutation'),
                        'lastname'           => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'title'),
                        'firstname'          => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'lastname'),
                        'position'           => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'firstname'),
                        'company'            => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'position'),
                        'industry_sector'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'company'),
                        'address'            => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'industry_sector', 'renamefrom' => 'street'),
                        'zip'                => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'address'),
                        'city'               => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'zip'),
                        'country_id'         => array('type' => 'SMALLINT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'city'),
                        'phone_office'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'country_id', 'renamefrom' => 'phone'),
                        'phone_private'      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'phone_office'),
                        'phone_mobile'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'phone_private'),
                        'fax'                => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'phone_mobile'),
                        'notes'              => array('type' => 'text', 'after' => 'fax'),
                        'birthday'           => array('type' => 'VARCHAR(10)', 'notnull' => true, 'default' => '00-00-0000', 'after' => 'notes'),
                        'status'             => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'birthday'),
                        'emaildate'          => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'status'),
                        'language'           => array('type' => 'INT(3)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'emaildate'),
                        'source'             => array('type' => 'ENUM(\'backend\',\'opt-in\',\'api\')', 'after' => 'language', 'notnull' => true, 'default' => 'backend'),
                        'consent'            => array('type' => 'TIMESTAMP', 'notnull' => false, 'default' => null, 'after' => 'source'),
                    ),
                    array(
                        'email'              => array('fields' => array('email'), 'type' => 'UNIQUE'),
                        'status'             => array('fields' => array('status'))
                    )
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_6';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_7', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        try {
            // fix user's SALUTATION of previews updates
            if (   !$objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')
                && $objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.3'
            )) {
                // set user's SALUTATION based of previews updates
                \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_newsletter_user` SET `salutation` = `title`, `title` = '' WHERE `salutation` = '0' AND `title` REGEXP '^[0-9]+$'");
                
                // clear all user's TITLE attribute that consist only of a number (it is most likely not the case that a user's TITLE is a number,
                // so we assume that it is a left over of the preview update bug, which did not migrate the user's TITLE attribute to the user's SALUTATION attribute
                \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_newsletter_user` SET `title` = '' WHERE `title` REGEXP '^[0-9]+$'");
            }


            // switch to source mode for all newsletter content pages
            \Cx\Lib\UpdateUtil::setSourceModeOnContentPage(array('module' => 'newsletter'), '3.0.1');

            // replace several placeholders that have changed
            $search = array(
                '/TXT_NEWSLETTER_URI/',
                '/NEWSLETTER_URI/',
                '/TXT_NEWSLETTER_STREET/',
                '/NEWSLETTER_STREET/',
            );
            $replace = array(
                'TXT_NEWSLETTER_WEBSITE',
                'NEWSLETTER_WEBSITE',
                'TXT_NEWSLETTER_ADDRESS',
                'NEWSLETTER_ADDRESS',
            );
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array('module' => 'newsletter'), $search, $replace, array('content'), '3.0.1');


            // sorry, brainfuck coming up...
            // this adds the missing template block newsletter_list as well as the placeholder [[NEWSLETTER_LIST_SELECTED]]
            $search = array(
                '/(<!--\s+BEGIN\s+newsletter_lists\s+-->)(.*)(<!--\s+END\s+newsletter_lists\s+-->)/ms',
            );
            $callback = function($matches) {
                if (preg_match('/^(.*)(<[^>]+[\'"]list\[\{NEWSLETTER_LIST_ID\}\][\'"])([^>]*>)(.*)$/ms', $matches[2], $listMatches)) {
                    if (strpos($listMatches[2].$listMatches[3], '{NEWSLETTER_LIST_SELECTED}') === false) {
                        $matches[2] = $listMatches[1].$listMatches[2].' {NEWSLETTER_LIST_SELECTED} '.$listMatches[3].$listMatches[4];
                    } else {
                        $matches[2] = $listMatches[1].$listMatches[2].$listMatches[3].$listMatches[4];
                    }
                }

                if (!preg_match('/<!--\s+BEGIN\s+newsletter_list\s+-->.*<!--\s+END\s+newsletter_list\s+-->/ms', $matches[2])) {
                    return $matches[1].'<!-- BEGIN newsletter_list -->'.$matches[2].'<!-- END newsletter_list -->'.$matches[3];
                } else {
                    return $matches[1].$matches[2].$matches[3];
                };
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'newsletter'), $search, $callback, array('content'), '3.0.1');


            // this adds the missing placeholders [[SELECTED_DAY]], [[SELECTED_MONTH]], [[SELECTED_YEAR]]
            $search = array(
                '/(<option[^>]+\{USERS_BIRTHDAY_(DAY|MONTH|YEAR)\}[\'"])([^>]*>)/ms',
            );
            $callback = function($matches) {
                if (strpos($matches[1].$matches[3], '{SELECTED_'.$matches[2].'}') === false) {
                    return $matches[1].' {SELECTED_'.$matches[2].'} '.$matches[3];
                } else {
                    return $matches[1].$matches[3];
                }
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'newsletter'), $search, $callback, array('content'), '3.0.1');


            // replace [[TXT_NEWSLETTER_TITLE]] to [[TXT_NEWSLETTER_SALUTATION]]
            // replace [[NEWSLETTER_TITLE]] to [[NEWSLETTER_SALUTATION]]
            $search = array(
                '/.*\{NEWSLETTER_TITLE\}.*/ms',
            );
            $callback = function($matches) {
                if (   !preg_match('/<!--\s+BEGIN\s+recipient_title\s+-->.*\{NEWSLETTER_TITLE\}.*<!--\s+END\s+recipient_title\s+-->/ms', $matches[0])
                    && !preg_match('/<!--\s+BEGIN\s+recipient_salutation\s+-->/ms', $matches[0])
                    && !preg_match('/\{NEWSLETTER_SALUTATION\}/ms', $matches[0])
                ) {
                    return str_replace(array('TXT_NEWSLETTER_TITLE', '{NEWSLETTER_TITLE}'), array('TXT_NEWSLETTER_SALUTATION', '{NEWSLETTER_SALUTATION}'), $matches[0]);
                } else {
                    return $matches[0];
                }
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'newsletter'), $search, $callback, array('content'), '3.0.1');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_7';
        return 'timeout';
    }


    if (!in_array('newsletter_phase_8', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.3') && empty($_SESSION['contrexx_update']['newsletter_links_decoded'])) {
            try {
                $objResult = \Cx\Lib\UpdateUtil::sql('SELECT `id`, `url` FROM `'.DBPREFIX.'module_newsletter_email_link`');
                if ($objResult !== false && $objResult->RecordCount() > 0) {
                    while (!$objResult->EOF) {
                        \Cx\Lib\UpdateUtil::sql(
                            'UPDATE `'.DBPREFIX.'module_newsletter_email_link` SET `url` = ? WHERE `id` = ?',
                            array(html_entity_decode($objResult->fields['url'], ENT_QUOTES, CONTREXX_CHARSET), $objResult->fields['id'])
                        );
                        $objResult->MoveNext();
                    }
                }
                $_SESSION['contrexx_update']['newsletter_links_decoded'] = true;
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_8';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_9', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            // add access to access ids 152/171/172/174/175/176 for user groups which had access to access id 25
            try {
                $result = \Cx\Lib\UpdateUtil::sql("SELECT `group_id` FROM `" . DBPREFIX . "access_group_static_ids` WHERE access_id = 25 GROUP BY group_id");
                if ($result !== false) {
                    while (!$result->EOF) {
                        \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `" . DBPREFIX . "access_group_static_ids` (`access_id`, `group_id`)
                                                    VALUES
                                                    (152, " . intval($result->fields['group_id']) . "),
                                                    (171, " . intval($result->fields['group_id']) . "),
                                                    (172, " . intval($result->fields['group_id']) . "),
                                                    (174, " . intval($result->fields['group_id']) . "),
                                                    (175, " . intval($result->fields['group_id']) . "),
                                                    (176, " . intval($result->fields['group_id']) . ")
                                                    ");
                        $result->MoveNext();
                    }
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        // add access id 176 for user groups which had access to 172 if version is older than 3.1.0
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {
            try {
                $result = \Cx\Lib\UpdateUtil::sql("SELECT `group_id` FROM `" . DBPREFIX . "access_group_static_ids` WHERE access_id = 172 GROUP BY `group_id`");
                if ($result !== false) {
                    while (!$result->EOF) {
                        \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `" . DBPREFIX . "access_group_static_ids` (`access_id`, `group_id`)
                                                    VALUES (176, " . intval($result->fields['group_id']) . ")");
                        $result->MoveNext();
                    }
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_rel_crm_membership_newsletter',
                    array(
                        'membership_id'      => array('type' => 'INT(10)', 'unsigned' => true),
                        'newsletter_id'      => array('type' => 'INT(10)', 'unsigned' => true, 'after' => 'membership_id'),
                        'type'               => array('type' => 'ENUM(\'associate\',\'include\',\'exclude\')', 'after' => 'newsletter_id')
                    ),
                    array(
                        'uniq'               => array('fields' => array('membership_id','newsletter_id','type'), 'type' => 'UNIQUE')
                    )
                );

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_newsletter_email_link_feedback',
                    array(
                        'id'                 => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'link_id'            => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'id'),
                        'email_id'           => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'link_id'),
                        'recipient_id'       => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'email_id'),
                        'recipient_type'     => array('type' => 'ENUM(\'access\',\'newsletter\',\'crm\')', 'after' => 'recipient_id')
                    ),
                    array(
                        'link_id'            => array('fields' => array('link_id','email_id','recipient_id','recipient_type'), 'type' => 'UNIQUE'),
                        'email_id'           => array('fields' => array('email_id'))
                    )
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
            try {
                \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_newsletter_settings` SET `setvalue` = '24' WHERE `setname` = 'confirmLinkHour' AND `setvalue` = '0'");
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_9';
        return 'timeout';
    }


    if (!in_array('newsletter_phase_10', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            // migrate path to images and media
            $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
            $attributes = array(
                'module_newsletter'                 => 'content',
                'module_newsletter_attachment'     => 'file_name',
                'module_newsletter_settings'        => 'setvalue',
                'module_newsletter_template'        => 'html',
                'module_newsletter_confirm_mail'    => 'content',
            );
            try {
                foreach ($attributes as $table => $attribute) {
                    foreach ($pathsToMigrate as $oldPath => $newPath) {
                        \Cx\Lib\UpdateUtil::migratePath(
                            '`' . DBPREFIX . $table . '`',
                            '`' . $attribute . '`',
                            $oldPath,
                            $newPath
                        );
                    }
                }
            } catch (\Cx\Lib\Update_DatabaseException $e) {
                \DBG::log($e->getMessage());
                return false;
            }

        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_10';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_11', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try {
                // 5.0.0
                if (!\Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'core_mail_template')) {
                    \Cx\Lib\UpdateUtil::table(
                        DBPREFIX.'core_mail_template',
                        array(
                            'key' => array('type' => 'tinytext', 'notnull' => true),
                            'section' => array('type' => 'tinytext', 'notnull' => true, 'renamefrom' => 'module_id', 'after' => 'key'),
                            'text_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'renamefrom' => 'text_name_id', 'after' => 'section'),
                            'html' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'text_id'),
                            'protected' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'html'),
                        ),
                        null,
                        'InnoDB',
                        'cx3upgrade'
                    );
                    \Cx\Lib\UpdateUtil::sql("
                        ALTER TABLE `".DBPREFIX."core_mail_template`
                        ADD PRIMARY KEY (`key` (32), `section` (32))
                    ");
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_11';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_12', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try {
                if (\Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'module_newsletter_confirm_mail')) {
                    migrateNewsletterEmailTemplates();
                } 
            } catch (\Cx\Lib\Update_DatabaseException $e) {
                \DBG::log($e->getMessage());
                return false;
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_12';
        return 'timeout';
    }


    if (!in_array('newsletter_phase_13', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            try {
                // 5.0.0
                $result = \Cx\Lib\UpdateUtil::sql("SELECT `text_id` FROM `" . DBPREFIX . "core_mail_template` WHERE `key` = 'notify_subscription_list_additional'");
                if ($result->RecordCount()) {
                    // Get old notification mail
                    $oldMailTextId = $result->fields['text_id'];

                    // "auto" increment
                    $result = \Cx\Lib\UpdateUtil::sql("SELECT MAX(`text_id`) + 1 AS 'newTextId' FROM " . DBPREFIX ."core_mail_template");
                    $newMailTextId = $result->fields['newTextId'];

                    //  add new mailtemplate
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `" . DBPREFIX . "core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('consent_confirmation_email','Newsletter',$newMailTextId,1,1)");


                    // copy from, sender, reply, to, cc and bcc from old mail
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO
                        `" . DBPREFIX ."core_text` (
                            `id`,
                            `lang_id`,
                            `section`,
                            `key`,
                            `text`
                        )
                    SELECT
                        $newMailTextId,
                        `l`.`id`,
                        `t`.`section`,
                        `t`.`key`,
                        `t`.`text`
                    FROM
                        `" . DBPREFIX ."core_text` AS `t`
                    JOIN
                        `" . DBPREFIX ."core_locale_locale` AS `l`
                    ON
                        `l`.`id` = `t`.`lang_id`
                    WHERE
                        `t`.`id` = $oldMailTextId AND
                        `t`.`section`='Newsletter' AND
                        `t`.`key` IN('core_mail_template_from','core_mail_template_sender','core_mail_template_reply','core_mail_template_to','core_mail_template_cc','core_mail_template_bcc')");

                    // insert non-copied texts in "DE"
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_name','Zustimmungsbestätigung' FROM `".DBPREFIX."core_locale_locale` WHERE `source_language` = 'de'");
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter Zustimmungsbestätigung' FROM `".DBPREFIX."core_locale_locale` WHERE `source_language` = 'de'");
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]\\r\\n\\r\\nBitte bestätigen Sie, dass Sie Mails der folgenden Mailing-Listen erhalten möchten:\\r\\n[[NEWSLETTER_LISTS]\\r\\n   [NEWSLETTER_LIST]\\r\\n[NEWSLETTER_LISTS]]\\r\\n\\r\\nUm dies zu bestätigen, klicken Sie bitte auf den folgenden Link oder kopieren Sie ihn in die Adressleiste Ihres Webbrowsers:\\r\\n\\r\\n[NEWSLETTER_CONSENT_CONFIRM_CODE]\\r\\n\\r\\n--\\r\\n\\r\\nDies ist eine automatisch generierte Nachricht.' FROM `".DBPREFIX."core_locale_locale` WHERE `source_language` = 'de'");
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_message_html','<html>\\r\\n<head>\\r\\n  <title></title>\\r\\n</head>\\r\\n<body>[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]<br />\\r\\n<br />\\r\\nBitte best&auml;tigen Sie, dass Sie Mails der folgenden Mailing-Listen erhalten m&ouml;chten:<br />\\r\\n[[NEWSLETTER_LISTS]<br />\\r\\n&nbsp; &nbsp;[NEWSLETTER_LIST]<br />\\r\\n[NEWSLETTER_LISTS]]<br />\\r\\n<br />\\r\\nUm dies zu best&auml;tigen, klicken Sie bitte auf den folgenden Link oder kopieren Sie ihn in die Adressleiste Ihres Webbrowsers:<br />\\r\\n<br />\\r\\n<a href=\"[NEWSLETTER_CONSENT_CONFIRM_CODE]\">[NEWSLETTER_CONSENT_CONFIRM_CODE]</a><br />\\r\\n<br />\\r\\n--<br />\\r\\n<br />\\r\\nDies ist eine automatisch generierte Nachricht.</body>\\r\\n</html>\\r\\n' FROM `".DBPREFIX."core_locale_locale` WHERE `source_language` = 'de'");

                    // insert non-copied texts in "EN"
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_name','Consent confirmation' FROM `".DBPREFIX."core_locale_locale`");
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter consent confirmation' FROM `".DBPREFIX."core_locale_locale`");
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]\\r\\n\\r\\nPlease confirm that you would like to receive mails from the following mailing lists:\\r\\n[[NEWSLETTER_LISTS]\\r\\n   [NEWSLETTER_LIST]\\r\\n[NEWSLETTER_LISTS]]\\r\\n\\r\\nIn order to confirm this please click on the following link or copy it to the address bar of your webbrowser:\\r\\n\\r\\n[NEWSLETTER_CONSENT_CONFIRM_CODE]\\r\\n\\r\\n--\\r\\n\\r\\nThis is an automatically generated message.' FROM `".DBPREFIX."core_locale_locale`");
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) SELECT $newMailTextId,`id`,'Newsletter','core_mail_template_message_html','<html>\\r\\n<head>\\r\\n  <title></title>\\r\\n</head>\\r\\n<body>[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]<br />\\r\\n<br />\\r\\nPlease confirm that you would like to receive mails from the following mailing lists:<br />\\r\\n[[NEWSLETTER_LISTS]<br />\\r\\n&nbsp; &nbsp;[NEWSLETTER_LIST]<br />\\r\\n[NEWSLETTER_LISTS]]<br />\\r\\n<br />\\r\\nIn order to confirm this please click on the following link or copy it to the address bar of your webbrowser:<br />\\r\\n<br />\\r\\n<a href=\"[NEWSLETTER_CONSENT_CONFIRM_CODE]\">[NEWSLETTER_CONSENT_CONFIRM_CODE]</a><br />\\r\\n<br />\\r\\n--<br />\\r\\n<br />\\r\\nThis is an automatically generated message.</body>\\r\\n</html>\\r\\n' FROM `".DBPREFIX."core_locale_locale`");

                    // clean up
                    \Cx\Lib\UpdateUtil::sql("DELETE FROM `".DBPREFIX."core_text` WHERE `id` = $oldMailTextId and `section` = 'Newsletter'");
                    \Cx\Lib\UpdateUtil::sql("DELETE FROM `".DBPREFIX."core_mail_template` WHERE `key` = 'notify_subscription_list_additional' AND `section` = 'Newsletter'");
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_13';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_14', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.2')) {
            try {
                $result = \Cx\Lib\UpdateUtil::sql("SELECT 1 FROM `" . DBPREFIX . "core_mail_template` WHERE `protected` = 1 AND `key` = 'consent_confirmation_email' AND `section` = 'Newsletter'");
                if ($result && !$result->RecordCount()) {
                    \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "core_mail_template` SET `protected` = 1 WHERE `key` = 'consent_confirmation_email' AND `section` = 'Newsletter' LIMIT 1");
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_14';
        return 'timeout';
    }

    if (!in_array('newsletter_phase_15', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.0')) {
            try {
                \Cx\Lib\UpdateUtil::sql("DELETE FROM `" . DBPREFIX . "module_newsletter_settings` WHERE `setname` = 'text_break_after'");

                $arrOptions = array(
                    array(
                        'id'        => 13,
                        'name'      => 'statistics',
                        'value'     => '1',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                    array(
                        'id'        => 14,
                        'name'      => 'agbTermsConditions',
                        'value'     => '0',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                    array(
                        'id'        => 15,
                        'name'      => 'confirmLinkHour',
                        'value'     => '24',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                    array(
                        'id'        => 16,
                        'name'      => 'deliver_user_recipients',
                        'value'     => '1',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                    array(
                        'id'        => 17,
                        'name'      => 'deliver_crm_recipients',
                        'value'     => '1',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                    array(
                        'id'        => 18,
                        'name'      => 'use_crm_filter',
                        'value'     => '1',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                    array(
                        'id'        => 19,
                        'name'      => 'unsubscribe_behavior',
                        'value'     => 'automatic',
                        'status'    => 1,
                        'stack_cnt' => 0,
                    ),
                );
                while ($option = array_shift($arrOptions)) {
                    // check if the id of the option is already used by a wrong option
                    $result = \Cx\Lib\UpdateUtil::sql(
                        "SELECT 1 FROM `" . DBPREFIX . "module_newsletter_settings` WHERE `setid` = " . $option['id'] . " AND `setname` != '" . $option['name'] . "'"
                    );
                    // if wrong option by same id is already present
                    // do append option to the end of the stack and skip to the next option
                    if ($result->RecordCount() > 0) {
                        // id is used by a wrong option
                        \DBG::dump($result);
                        // get number of options left to process
                        $stackCnt = count($arrOptions);
                        // if previous remembered stack count is still the same,
                        // then this means that nothing has changed in meanwhile,
                        // this inicates that we are in an infinite loop,
                        // so we have to abort now
                        if ($option['stack_cnt'] == $stackCnt) {
                            \DBG::dump($result);
                            \DBG::dump($option);
                            \DBG::dump($arrOptions);
                            setUpdateMsg('Die Konfiguration konnte nicht migriert werden.');
                            return false;
                        }
                        // remember stack count.
                        // will be used above to identify an infinite loop
                        $option['stack_cnt'] = count($arrOptions);
                        // readd option to stack to let is process later
                        $arrOptions[] = $option;
                        continue;
                    }
                    // id is free. so lets check if the option itself is already present
                    // but uses a wrong id
                    $result = \Cx\Lib\UpdateUtil::sql(
                        "SELECT `setid` FROM `" . DBPREFIX . "module_newsletter_settings` WHERE `setname` = '" . $option['name'] . "'"
                    );
                    // add option in case it's not yet present
                    if (!$result->RecordCount()) {
                        \Cx\Lib\UpdateUtil::sql(
                            "INSERT INTO `" . DBPREFIX . "module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (" . $option['id'] . ", '" . $option['name'] . "','" . $option['value'] . "', " . $option['status'] . ")"
                        );
                        continue;
                    }
                    // fix id in case the existing option uses a wrong one
                    if (
                        $result->RecordCount() &&
                        $result->fields['setid'] != $option['id']
                    ) {
                        \Cx\Lib\UpdateUtil::sql(
                            "UPDATE `" . DBPREFIX . "module_newsletter_settings` SET `setid` = " . $option['id'] . " WHERE `setname` = '" . $option['name'] . "'"
                        );
                    }
                }
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        $_SESSION['contrexx_update']['update']['done'][] = 'newsletter_phase_15';
        return 'timeout';
    }

    return true;
}

/**
 * Migrate the existing newsletter mail templates to Emailtemplate component (\MailTemplate)
 */
function migrateNewsletterEmailTemplates()
{
    if (!\Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'module_newsletter_confirm_mail')) {
        return;
    }
    $mailTemplateArray = array(
        1 => array(
            'name' => array(
                1 => 'Anmeldung zum Newsletter',
                2 => 'Newsletter Activation',
            ),
            'subject' => array(
                1 => '[NEWSLETTER_DOMAIN_URL] - Anmeldung zum Newsletter',
                2 => '[NEWSLETTER_DOMAIN_URL] - Newsletter subscription',
            ),
            'content' => array(
                1 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

Wir freuen uns,Sie bei unserem Newsletter begrüssen zu dürfen und wünschen Ihnen viel Freude damit.
Sie erhalten ab sofort wöchentlich die neuesten Informationen in elektronischer Form zu gestellt.

Um die Bestellung des Newsletters zu bestätigen,bitten wir Sie,auf den folgenden Link zu klicken bzw. ihn in die Adresszeile Ihres Browsers zu kopieren:

[NEWSLETTER_CONFIRM_CODE]

Um zu verhindern,dass unser Newsletter in Ihrem Spam-Ordner landet,fügen Sie bitte die Adresse dieser E-Mail Ihrem Adressbuch hinzu.

Sofern Sie diese E-Mail ungewünscht erhalten haben,bitten wir um Entschuldigung. Sie werden keine weitere E-Mail mehr von uns erhalten.

--
Dies ist eine automatisch generierte Nachricht.
[NEWSLETTER_CURRENT_DATE]',
                2 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

We are glad to welcome you to our newsletter and wish you much enjoyment with it.
From now on you will receive the most actual news weekly.

In order to confirm the newsletter subscription please click on the following link or copy it to the address bar of your webbrowser:

[NEWSLETTER_CONFIRM_CODE]

To prevent our newsletter from ending up in your spam folder, please add this email\'s sender address to your contact list.

In case you did not request to sign up to this newsletter by yourself, please ignore this email. You may not receive any further emails from us.

--
This is an automatically generated message.
[NEWSLETTER_CURRENT_DATE]',
            ),
            'key' => 'activation_email',
            'text_id' => 24,
        ),
        2 => array(
            'name' => array(
                1 => 'Bestätigung der Anmeldung zum Newsletter',
                2 => 'Newsletter Activation',
            ),
            'subject' => array(
                1 => '[NEWSLETTER_DOMAIN_URL] - Bestätigung zur Newsletteranmeldung',
                2 => '[NEWSLETTER_DOMAIN_URL] - Newsletter subscription confirmation',
            ),
            'content' => array(
                1 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

Ihr Newsletter Abonnement wurde erfolgreich registriert.
Sie werden nun in Zukunft unsere Newsletter erhalten.

--
Dies ist eine automatisch generierte Nachricht.
[NEWSLETTER_CURRENT_DATE]',
                2 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

Your subscription to this newsletter was successfully registered.
You will receive our newsletter messages.

--
This is an automatically generated message.
[NEWSLETTER_CURRENT_DATE]',
            ),
            'key' => 'confirm_email',
            'text_id' => 25,
        ),
        3 => array(
            'name' => array(
                1 => 'Benachrichtigung über eine Änderung in Ihrer Newsletter-Anmeldung',
                2 => 'Newsletter Notification Email',
            ),
            'subject' => array(
                1 => '[NEWSLETTER_DOMAIN_URL] - Newsletter Empfänger [NEWSLETTER_NOTIFICATION_ACTION]',
                2 => '[NEWSLETTER_DOMAIN_URL] - Newsletter receiver [NEWSLETTER_NOTIFICATION_ACTION]',
            ),
            'content' => array(
                1 => 'Folgende Mutation wurde im Newsletter System getätigt:

Getätigte Aktion: [NEWSLETTER_NOTIFICATION_ACTION]
Geschlecht:       [NEWSLETTER_USER_SEX]
Anrede:           [NEWSLETTER_USER_TITLE]
Vorname:          [NEWSLETTER_USER_FIRSTNAME]
Nachname:         [NEWSLETTER_USER_LASTNAME]
E-Mail:           [NEWSLETTER_USER_EMAIL]

--
Dies ist eine automatisch generierte Nachricht.
[NEWSLETTER_CURRENT_DATE]',
                2 => 'The following changes were made to the newsletter system:

Action:     [NEWSLETTER_NOTIFICATION_ACTION]
Gender:     [NEWSLETTER_USER_SEX]
Salutation: [NEWSLETTER_USER_TITLE]
Firstname:  [NEWSLETTER_USER_FIRSTNAME]
Lastname:   [NEWSLETTER_USER_LASTNAME]
Email:      [NEWSLETTER_USER_EMAIL]

--
This is an automatically generated message.
[NEWSLETTER_CURRENT_DATE]',
            ),
            'key' => 'notification_email',
            'text_id' => 26,
        ),
        4 => array(
            'name' => array(
                1 => 'Benachrichtung über nicht zustellbaren Newsletter',
                2 => 'Undeliverable mail notification',
            ),
            'subject' => array(
                1 => '[NEWSLETTER_SUBJECT] - Benachrichtigung über unzustellbare E-Mail',
                2 => '[NEWSLETTER_SUBJECT] - Undeliverable email notification',
            ),
            'content' => array(
                1 => 'Der Newsletter konnte an folgende E-Mail-Adresse nicht versendet werden:
[NEWSLETTER_USER_EMAIL]

Um die E-Mail Adresse zu bearbeiten,klicken Sie bitte auf den folgenden Link:
[NEWSLETTER_USER_EDIT_LINK]

--
Dies ist eine automatisch generierte Nachricht.
[NEWSLETTER_CURRENT_DATE]',
                2 => 'Sending the newsletter to the following email address has failed:
[NEWSLETTER_USER_EMAIL]

In order to edit the email address please click the following link:
[NEWSLETTER_USER_EDIT_LINK]

--
This is an automatically generated message.
[NEWSLETTER_CURRENT_DATE]',
            ),
            'key' => 'notify_undelivered_email',
            'text_id' => 27,
        ),
    );

    $languages     = \FWLanguage::getLanguageArray();
    $defaultLangId = \FWLanguage::getDefaultLangId();

    $query = '
        SELECT
            `id`,
            `title`,
            CONVERT(`content` USING "utf8") as "setvalue",
            `recipients`
        FROM
            `' . DBPREFIX . 'module_newsletter_confirm_mail`
        WHERE
            id IN (1,2,3)
        UNION
        SELECT
            4,
            "",
            CONVERT(`setvalue` USING "utf8") as "setvalue",
            ""
        FROM
            `' . DBPREFIX . 'module_newsletter_settings` WHERE `setname` = "reject_info_mail_text"';
    $mailTemplates = \Cx\Lib\UpdateUtil::sql($query);
    if (!$mailTemplates) {
        return;
    }
    while (!$mailTemplates->EOF) {
        $mailTemplateReplacement =   isset($mailTemplateArray[$mailTemplates->fields['id']])
                                   ? $mailTemplateArray[$mailTemplates->fields['id']]
                                   : array();
        if (empty($mailTemplateReplacement)) {
            $mailTemplates->MoveNext();
            continue;
        }

        $key    = $mailTemplateReplacement['key'];
        $textId = $mailTemplateReplacement['text_id'];

        foreach ($languages as $lang) {
            $langId = $lang['id'];
            $name   =  isset($mailTemplateReplacement['name'][$langId])
                     ? $mailTemplateReplacement['name'][$langId]
                     : $mailTemplateReplacement['name'][1];
            $title = $content = '';
            if ($langId == $defaultLangId) {
                $title   = $mailTemplates->fields['title'];
                $content = $mailTemplates->fields['setvalue'];
            }
            if (empty($title)) {
                $title  =  isset($mailTemplateReplacement['subject'][$langId])
                         ? $mailTemplateReplacement['subject'][$langId]
                         : $mailTemplateReplacement['subject'][1];
            }
            if (empty($content)) {
                $content =  isset($mailTemplateReplacement['content'][$langId])
                          ? $mailTemplateReplacement['content'][$langId]
                          : $mailTemplateReplacement['content'][1];
            }
            insertMailTemplate($key, $textId, $langId, $name, $title, $content);
        }

        $mailTemplates->MoveNext();
    }

    // insert new template keys
    $newMailTemplates = array(
        array(
            'name' => array(
                1 => 'Informiert den Abonnenten über die erneute Anmeldung an den Newsletter',
                2 => 'Notify user about subscribing to the same newsletter list again',
            ),
            'subject' => array(
                1 => '[NEWSLETTER_DOMAIN_URL] - Anmeldung zum Newsletter',
                2 => '[NEWSLETTER_DOMAIN_URL] - Newsletter subscription',
            ),
            'content' => array(
                1 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

Sie sind bereits Abonnent des gewählten Newsletters.

--
Dies ist eine automatisch generierte Nachricht.
[NEWSLETTER_CURRENT_DATE]',
                2 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

You have already subscribed to this newsletter list.

--
This is an automatically generated message.
[NEWSLETTER_CURRENT_DATE]',
            ),
            'key' => 'notify_subscription_list_same',
            'text_id' => 28,
        ),
        array(
            'name' => array(
                1 => 'Informiert den Abonnenten über die Anmeldung an einen weiteren Newsletter',
                2 => 'Notify user about the additional newsletter list subscription',
            ),
            'subject' => array(
                1 => '[NEWSLETTER_DOMAIN_URL] - Anmeldung zum Newsletter',
                2 => '[NEWSLETTER_DOMAIN_URL] - Newsletter subscription',
            ),
            'content' => array(
                1 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

Sie haben sich erfolgreich in den/die folgende(n) Newsletter eingetragen:
[[NEWSLETTER_LISTS]
   [NEWSLETTER_LIST]
[NEWSLETTER_LISTS]]

--
Dies ist eine automatisch generierte Nachricht.
[NEWSLETTER_CURRENT_DATE]',
                2 => '[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]

You have successfully registered to the following newsletter list(s):
[[NEWSLETTER_LISTS]
    [NEWSLETTER_LIST]
[NEWSLETTER_LISTS]]

--
This is an automatically generated message.
[NEWSLETTER_CURRENT_DATE]',
            ),
            'key' => 'notify_subscription_list_additional',
            'text_id' => 29,
        ),
    );

    foreach ($newMailTemplates as $mailTemplateReplacement) {

        $key    = $mailTemplateReplacement['key'];
        $textId = $mailTemplateReplacement['text_id'];
        $mailTo = '[NEWSLETTER_USER_EMAIL]';

        foreach ($languages as $lang) {
            $langId = $lang['id'];
            $name   = $title = $content = '';
            if (isset($mailTemplateReplacement['name'][$langId])) {
                $name = $mailTemplateReplacement['name'][$langId];
            } else {
                $name = $mailTemplateReplacement['name'][1];
            }
            if (isset($mailTemplateReplacement['subject'][$langId])) {
                $title = $mailTemplateReplacement['subject'][$langId];
            } else {
                $title = $mailTemplateReplacement['subject'][1];
            }
            if (isset($mailTemplateReplacement['content'][$langId])) {
                $content = $mailTemplateReplacement['content'][$langId];
            } else {
                $content = $mailTemplateReplacement['content'][1];
            }
            insertMailTemplate($key, $textId, $langId, $name, $title, $content, $mailTo);
        }
    }

    try {
        \Cx\Lib\UpdateUtil::sql('DELETE FROM `'. DBPREFIX .'module_newsletter_settings` WHERE `setname` = "reject_info_mail_text"');
        \Cx\Lib\UpdateUtil::drop_table(DBPREFIX . 'module_newsletter_confirm_mail');
    } catch (\Cx\Lib\UpdateException $e) {
        return "Error: $e->sql";
    }

}

/**
 * Insert MailTemplate into database
 *
 * @param string  $key      Mail template key
 * @param string  $textId   Core text id
 * @param integer $langId   Language id
 * @param string  $name     Mail template name
 * @param string  $title    Mail template title
 * @param string  $content  Mail template content
 */
function insertMailTemplate($key, $textId, $langId, $name, $title, $content)
{

    //replase placeholder
    $array_1 = array(
        '[[title]]',
        '[[sex]]',
        '[[firstname]]',
        '[[lastname]]',
        '[[e-mail]]',
        '[[url]]',
        '[[date]]',
        '[[code]]',
        '[[action]]',
        '[[EMAIL]]',
        '[[LINK]]',
    );
    $array_2 = array(
        '[NEWSLETTER_USER_TITLE]',
        '[NEWSLETTER_USER_SEX]',
        '[NEWSLETTER_USER_FIRSTNAME]',
        '[NEWSLETTER_USER_LASTNAME]',
        '[NEWSLETTER_USER_EMAIL]',
        '[NEWSLETTER_DOMAIN_URL]',
        '[NEWSLETTER_CURRENT_DATE]',
        '[NEWSLETTER_CONFIRM_CODE]',
        '[NEWSLETTER_NOTIFICATION_ACTION]',
        '[NEWSLETTER_USER_EMAIL]',
        '[NEWSLETTER_USER_EDIT_LINK]',
    );

    $mailTitle   = str_replace($array_1, $array_2, $title);
    $mailContent = str_replace($array_1, $array_2, $content);

    \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `' . DBPREFIX . 'core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES("'. $key .'", "Newsletter", '. $textId .', 0, 1)');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_bcc\', \'\')');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_cc\', \'\')');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_from\', \'\')');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_message\', "'. addslashes($mailContent) .'")');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_name\', "'. $name . '")');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_reply\', \'\')');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_sender\', \'\')');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_subject\', "'. $mailTitle .'")');
    \Cx\Lib\UpdateUtil::sql('INSERT INTO `' . DBPREFIX . 'core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES('. $textId .', '. $langId .', \'Newsletter\', \'core_mail_template_to\', \'\')');

}

/**
 * Note: the body of this function is by intention not enclosed in a try/catch block. We wan't the calling sections to catch and handle exceptions themself.
 */
function newsletter_migrate_country_field()
{
/*
TEST
                $countryId = 0;
$text = 'Switzerland';
                $objText= \Cx\Lib\UpdateUtil::sql("SELECT `id` FROM `".DBPREFIX."core_text` WHERE `section` = 'core' AND `key` = 'core_country_name' AND `text` = '".contrexx_raw2db($text)."'");
                if (!$objResult->EOF) {
                    $countryId = $objText->fields['id'];
                }
\DBG::dump($countryId);
return;
*/
    ///////////////////////////
    // MIGRATE COUNTRY FIELD //
    ///////////////////////////
    // 1. backup country column to country_old
    if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_newsletter_user', 'country')) {
        \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_newsletter_user` CHANGE `country` `country_old` VARCHAR(255) NOT NULL DEFAULT \'\'');
    }
    // 2. add new column country_id (format int)
    if (!\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_newsletter_user', 'country_id')) {
        \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_newsletter_user` ADD `country_id` SMALLINT( 5 ) UNSIGNED NOT NULL DEFAULT \'0\' AFTER `country_old`');
    }

    // 3. migrate to new country format (using IDs)
    if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_newsletter_user', 'country_old')) {
        $objResult = \Cx\Lib\UpdateUtil::sql('SELECT `id`, `country_old` FROM `'.DBPREFIX.'module_newsletter_user` WHERE `country_id` = 0 AND `country_old` <> \'\'');
        if ($objResult->RecordCount()) {
            while (!$objResult->EOF) {
                // try setting country_id based on a guess from country_old
                $countryId = 0;
                $objText= \Cx\Lib\UpdateUtil::sql("SELECT `id` FROM `".DBPREFIX."core_text` WHERE `section` = 'core' AND `key` = 'core_country_name' AND `text` = '".contrexx_raw2db($objResult->fields['country_old'])."'");
                if (!$objResult->EOF) {
                    $countryId = $objText->fields['id'];
                }
                \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_newsletter_user` SET `country_id` = \''.contrexx_raw2db($countryId).'\', `country_old` = \'\' WHERE `id` = '.$objResult->fields['id']);
                if (!checkTimeoutLimit()) {
                    return 'timeout';
                }
                $objResult->MoveNext();
            }
        }

        // backup literal country name in field notes
        if (!\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_newsletter_user', 'notes')) {
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_newsletter_user', 'fax')) {
                $column = 'fax';
            } else {
                // versions pre 3.0.0 didn't have the column 'fax' yet
                $column = 'phone';
            }
            \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_newsletter_user` ADD `notes` text NOT NULL AFTER `'.$column.'`');
        }
        \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_newsletter_user` SET `notes` = `country_old`');
        // drop obsolete column country_old'
        \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_newsletter_user` DROP `country_old`');
    }
    ////////////////////////////////
    // END: MIGRATE COUNTRY FIELD //
    ////////////////////////////////
}
function _newsletterInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) NOT NULL DEFAULT \'\',
  `template` int(11) NOT NULL DEFAULT \'0\',
  `content` mediumtext NOT NULL,
  `attachment` enum(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `priority` tinyint(1) NOT NULL DEFAULT \'0\',
  `sender_email` varchar(255) NOT NULL DEFAULT \'\',
  `sender_name` varchar(255) NOT NULL DEFAULT \'\',
  `return_path` varchar(255) NOT NULL DEFAULT \'\',
  `smtp_server` int(10) unsigned NOT NULL DEFAULT \'0\',
  `status` int(1) NOT NULL DEFAULT \'0\',
  `count` int(11) NOT NULL DEFAULT \'0\',
  `recipient_count` int(11) unsigned NOT NULL DEFAULT \'0\',
  `date_create` int(14) unsigned NOT NULL DEFAULT \'0\',
  `date_sent` int(14) unsigned NOT NULL DEFAULT \'0\',
  `tmp_copy` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter` (`id`, `subject`, `template`, `content`, `attachment`, `priority`, `sender_email`, `sender_name`, `return_path`, `smtp_server`, `status`, `count`, `recipient_count`, `date_create`, `date_sent`, `tmp_copy`) VALUES (\'2\', \'Neue Homepage\', \'1\', \'[[salutation]] [[lastname]]<br />\\r\\n<br />\\r\\nGerne machen wir Sie auf unsere neue Website aufmerksam. Der Internetauftritt wurde komplett erneuert und aufgefrischt. Sie finden nun alle Informationen &uuml;bersichtlich dargestellt und ein modernisiertes Design.<br />\\r\\n<br />\\r\\nF&uuml;r unser Webprojekt haben wir <a  href=\\\"https://www.cloudrexx.com\\\" rel=\\\"newsletter_link_1\\\" target=\\\"_blank\\\">Cloudrexx</a> als Web Content Management System ausgew&auml;hlt. Cloudrexx beinhaltet alle wichtigen Anwendungen, die eine Website heute braucht. Die Inhalte und Funktionen k&ouml;nnen jederzeit mit kleinem Aufwand bearbeitet und aufgefrischt werden, um stets eine aktuelle und ansprechende Webseite zu pr&auml;sentieren.<br />\\r\\n<br />\\r\\n<br />\\r\\nIhr MaxMuster-Team\', \'0\', \'3\', \'info@example.com\', \'admin\', \'info@maxmusterag.com\', \'0\', \'0\', \'0\', \'0\', \'1348220511\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_access_user` (
  `accessUserID` int(5) unsigned NOT NULL,
  `newsletterCategoryID` int(11) NOT NULL,
  `code` varchar(255) NOT NULL DEFAULT \'\',
  `source` enum(\'backend\',\'opt-in\',\'api\') NOT NULL DEFAULT \'backend\',
  `consent` timestamp NULL DEFAULT NULL,
  UNIQUE KEY `rel` (`accessUserID`,`newsletterCategoryID`),
  KEY `accessUserID` (`accessUserID`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_attachment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter` int(11) NOT NULL DEFAULT \'0\',
  `file_name` varchar(255) NOT NULL DEFAULT \'\',
  `file_nr` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `newsletter` (`newsletter`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `notification_email` varchar(250) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_category` (`id`, `status`, `name`, `notification_email`) VALUES (\'1\', \'1\', \'Demo Liste\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_email_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `email_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email_id` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_email_link_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `link_id` int(11) unsigned NOT NULL,
  `email_id` int(11) unsigned NOT NULL,
  `recipient_id` int(11) unsigned NOT NULL,
  `recipient_type` enum(\'access\',\'newsletter\',\'crm\') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `link_id` (`link_id`,`email_id`,`recipient_id`,`recipient_type`),
  KEY `email_id` (`email_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_rel_cat_news` (
  `newsletter` int(11) NOT NULL DEFAULT \'0\',
  `category` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`newsletter`,`category`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_rel_cat_news` (`newsletter`, `category`) VALUES (\'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_rel_crm_membership_newsletter` (
  `membership_id` int(10) unsigned NOT NULL,
  `newsletter_id` int(10) unsigned NOT NULL,
  `type` enum(\'associate\',\'include\',\'exclude\') NOT NULL,
  UNIQUE KEY `uniq` (`membership_id`,`newsletter_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_rel_usergroup_newsletter` (
  `userGroup` int(10) unsigned NOT NULL,
  `newsletter` int(10) unsigned NOT NULL,
  UNIQUE KEY `uniq` (`userGroup`,`newsletter`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_rel_user_cat` (
  `user` int(11) NOT NULL DEFAULT \'0\',
  `category` int(11) NOT NULL DEFAULT \'0\',
  `source` enum(\'backend\',\'opt-in\',\'api\') NOT NULL DEFAULT \'backend\',
  `consent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user`,`category`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_rel_user_cat` (`user`, `category`, `source`, `consent`) VALUES (\'1\', \'1\', \'backend\', NULL)');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_settings` (
  `setid` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `setname` varchar(250) NOT NULL DEFAULT \'\',
  `setvalue` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`setid`),
  UNIQUE KEY `setname` (`setname`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'1\', \'sender_mail\', \'info@example.com\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'2\', \'sender_name\', \'admin\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'3\', \'reply_mail\', \'info@example.com\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'4\', \'mails_per_run\', \'30\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'6\', \'test_mail\', \'info@example.com\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'7\', \'overview_entries_limit\', \'10\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'8\', \'rejected_mail_operation\', \'delete\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'9\', \'defUnsubscribe\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'10\', \'notificationUnsubscribe\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'11\', \'notificationSubscribe\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'12\', \'recipient_attribute_status\', \'{\\\"recipient_sex\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_salutation\\\":{\\\"active\\\":true,\\\"required\\\":false},\\\"recipient_title\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_firstname\\\":{\\\"active\\\":true,\\\"required\\\":false},\\\"recipient_lastname\\\":{\\\"active\\\":true,\\\"required\\\":false},\\\"recipient_position\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_company\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_industry\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_address\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_city\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_zip\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_country\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_phone\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_private\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_mobile\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_fax\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_birthday\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"recipient_website\\\":{\\\"active\\\":false,\\\"required\\\":false},\\\"captcha\\\":{\\\"active\\\":true,\\\"required\\\":false}}\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'13\', \'statistics\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'14\', \'agbTermsConditions\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'15\', \'confirmLinkHour\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'16\', \'deliver_user_recipients\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'17\', \'deliver_crm_recipients\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'18\', \'use_crm_filter\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'19\', \'unsubscribe_behavior\', \'automatic\', \'1 \')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `description` varchar(255) NOT NULL DEFAULT \'\',
  `html` text NOT NULL,
  `required` int(1) NOT NULL DEFAULT \'0\',
  `type` enum(\'e-mail\',\'news\') NOT NULL DEFAULT \'e-mail\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_template` (`id`, `name`, `description`, `html`, `required`, `type`) VALUES (\'1\', \'Standard\', \'Standard Template\', \'<html>\\r\\n    <head>\\r\\n        <style type=\\\"text/css\\\">\\r\\n        *, html, body, table {\\r\\n			padding: 0;\\r\\n			margin: 0;\\r\\n			font-size: 12px;\\r\\n			font-family: arial;\\r\\n			line-height: 1.5;\\r\\n			color: #000000;\\r\\n        }\\r\\n\\r\\n        h1 {\\r\\n			padding: 20px 0 5px 0;\\r\\n            font-size: 20px;\\r\\n			color: #487EAD;\\r\\n        }\\r\\n\\r\\n        h2 {\\r\\n			padding: 18px 0 4px 0;\\r\\n            font-size: 16px;\\r\\n			color: #487EAD;\\r\\n        }\\r\\n\\r\\n        h3, h4, h5, h6 {\\r\\n			padding: 16px 0 3px 0;\\r\\n            font-size: 13px;\\r\\n			font-weight: bold;\\r\\n			color: #487EAD;\\r\\n        }\\r\\n\\r\\n		a,\\r\\n        a:link,\\r\\n		a:hover,\\r\\n		a:focus,\\r\\n		a:active,\\r\\n		a:visited {\\r\\n            color: #487EAD !imprtant;\\r\\n        }\\r\\n        </style>\\r\\n    </head>\\r\\n    <body>\\r\\n        <table height=\\\"100%\\\" width=\\\"100%\\\" cellspacing=\\\"60\\\" style=\\\"background-color: rgb(204, 204, 204);\\\">\\r\\n            <tbody>\\r\\n                <tr>\\r\\n                    <td align=\\\"center\\\">\\r\\n                    <table width=\\\"660\\\" cellspacing=\\\"30\\\" bgcolor=\\\"#ffffff\\\" style=\\\"border: 7px solid rgb(72, 126, 173);\\\">\\r\\n                        <tbody>\\r\\n                            <tr>\\r\\n                                <td style=\\\"font-family: arial; font-size: 12px;\\\"><a target=\\\"_blank\\\" style=\\\"font-family: arial; font-size: 20px; text-decoration: none; color: rgb(72, 126, 173);\\\" href=\\\"http://www.example.com\\\">example.com</a><br />\\r\\n                                <span style=\\\"font-size: 20px; font-family: arial; text-decoration: none; color: rgb(72, 126, 173);\\\">Newsletter</span></td>\\r\\n                            </tr>\\r\\n                            <tr>\\r\\n                                <td style=\\\"font-family: arial; font-size: 12px; color: rgb(0, 0, 0);\\\">[[content]]</td>\\r\\n                            </tr>\\r\\n                            <tr>\\r\\n                                <td>\\r\\n                                <table width=\\\"600\\\" cellspacing=\\\"0\\\">\\r\\n                                    <tbody>\\r\\n                                        <tr>\\r\\n                                            <td height=\\\"30\\\" colspan=\\\"3\\\" style=\\\"border-top: 3px solid rgb(72, 126, 173);\\\">&nbsp;</td>\\r\\n                                        </tr>\\r\\n                                        <tr>\\r\\n                                            <td width=\\\"235\\\" valign=\\\"top\\\" style=\\\"font-family: arial; font-size: 11px; color: rgb(0, 0, 0);\\\">\\r\\n                                            <h3 style=\\\"padding: 0pt; margin: 0pt 0pt 5px;\\\">Impressum</h3>\\r\\n                                            Beispiel AG<br />\\r\\n                                            Firmenstrasse 1<br />\\r\\n                                            CH-1234 Irgendwo<br />\\r\\n                                            <br />\\r\\n                                            Telefon: + 41 (0)12 345 67 89<br />\\r\\n                                            Fax: + 41 (0)12 345 67 90<br />\\r\\n                                            <br />\\r\\n                                            E-Mail: <a href=\\\"mailto:info@example.com\\\">info@example.com</a><br />\\r\\n                                            Web: <a href=\\\"http://www.example.com\\\">www.example.com</a></td>\\r\\n                                            <td width=\\\"30\\\" valign=\\\"top\\\">&nbsp;</td>\\r\\n                                            <td width=\\\"335\\\" valign=\\\"top\\\" style=\\\"font-family: arial; font-size: 11px; color: rgb(135, 135, 135);\\\">Diese Art der Korrespondenz ist absichtlich von uns gew&auml;hlt worden, um wertvolle Naturressourcen zu schonen. Dieses E-Mail ist ausdr&uuml;cklich nicht verschickt worden, um Ihre betrieblichen Vorg&auml;nge zu st&ouml;ren und dient ausschliesslich dazu, Sie auf einfachste Weise unverbindlich zu informieren. Falls Sie sich dadurch trotzdem bel&auml;stigt f&uuml;hlen, bitten wir Sie, umgehend mit einem Klick auf &quot;Newsletter abmelden&quot; sich abzumelden, so dass wir Sie aus unserem Verteiler l&ouml;schen k&ouml;nnen.<br />\\r\\n                                            <br />\\r\\n                                            <span>[[unsubscribe]]&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[[profile_setup]]</span></td>\\r\\n                                        </tr>\\r\\n                                    </tbody>\\r\\n                                </table>\\r\\n                                </td>\\r\\n                            </tr>\\r\\n                        </tbody>\\r\\n                    </table>\\r\\n                    </td>\\r\\n                </tr>\\r\\n            </tbody>\\r\\n        </table>\\r\\n    </body>\\r\\n</html>\', \'1\', \'e-mail\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_template` (`id`, `name`, `description`, `html`, `required`, `type`) VALUES (\'2\', \'Standard\', \'Standard News-Import Template\', \'<table border=\\\"0\\\" cellpadding=\\\"3\\\" cellspacing=\\\"0\\\" width=\\\"100%\\\">\\r\\n    <!-- BEGIN news_list --><!-- BEGIN news_category -->\\r\\n    <tbody>\\r\\n        <tr>\\r\\n            <td colspan=\\\"2\\\" style=\\\"text-align:left;\\\">\\r\\n                <h2>\\r\\n                    [[NEWS_CATEGORY_NAME]]</h2>\\r\\n            </td>\\r\\n        </tr>\\r\\n        <!-- END news_category --><!-- BEGIN news_message -->\\r\\n        <tr>\\r\\n            <td style=\\\"text-align:left;\\\" width=\\\"25%\\\">\\r\\n                <!-- BEGIN news_image --><img alt=\\\"\\\" height=\\\"100\\\" src=\\\"[[NEWS_IMAGE_SRC]]\\\" width=\\\"150\\\" /><!-- END news_image --></td>\\r\\n            <td style=\\\"text-align:left;\\\" width=\\\"75%\\\">\\r\\n                <h3>\\r\\n                    [[NEWS_TITLE]]</h3>\\r\\n                <p>\\r\\n                    [[NEWS_TEASER_TEXT]]</p>\\r\\n                <p>\\r\\n                    <a href=\\\"[[NEWS_URL]]\\\">Meldung lesen...</a></p>\\r\\n            </td>\\r\\n        </tr>\\r\\n        <!-- END news_message --><!-- END news_list -->\\r\\n    </tbody>\\r\\n</table>\\r\\n\', \'1\', \'news\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_tmp_sending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `newsletter` int(11) NOT NULL DEFAULT \'0\',
  `email` varchar(255) NOT NULL DEFAULT \'\',
  `sendt` tinyint(1) NOT NULL DEFAULT \'0\',
  `type` enum(\'access\',\'newsletter\',\'core\',\'crm\') NOT NULL DEFAULT \'newsletter\',
  `code` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_email` (`newsletter`,`email`),
  KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL DEFAULT \'\',
  `email` varchar(255) NOT NULL DEFAULT \'\',
  `uri` varchar(255) NOT NULL DEFAULT \'\',
  `sex` enum(\'m\',\'f\') DEFAULT NULL,
  `salutation` int(10) unsigned NOT NULL DEFAULT \'0\',
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `lastname` varchar(255) NOT NULL DEFAULT \'\',
  `firstname` varchar(255) NOT NULL DEFAULT \'\',
  `position` varchar(255) NOT NULL DEFAULT \'\',
  `company` varchar(255) NOT NULL DEFAULT \'\',
  `industry_sector` varchar(255) NOT NULL DEFAULT \'\',
  `address` varchar(255) NOT NULL DEFAULT \'\',
  `zip` varchar(255) NOT NULL DEFAULT \'\',
  `city` varchar(255) NOT NULL DEFAULT \'\',
  `country_id` smallint(5) unsigned NOT NULL DEFAULT \'0\',
  `phone_office` varchar(255) NOT NULL DEFAULT \'\',
  `phone_private` varchar(255) NOT NULL DEFAULT \'\',
  `phone_mobile` varchar(255) NOT NULL DEFAULT \'\',
  `fax` varchar(255) NOT NULL DEFAULT \'\',
  `notes` text NOT NULL,
  `birthday` varchar(10) NOT NULL DEFAULT \'00-00-0000\',
  `status` int(1) NOT NULL DEFAULT \'0\',
  `emaildate` int(14) unsigned NOT NULL DEFAULT \'0\',
  `language` int(3) unsigned NOT NULL DEFAULT \'0\',
  `source` enum(\'backend\',\'opt-in\',\'api\') NOT NULL DEFAULT \'backend\',
  `consent` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user` (`id`, `code`, `email`, `uri`, `sex`, `salutation`, `title`, `lastname`, `firstname`, `position`, `company`, `industry_sector`, `address`, `zip`, `city`, `country_id`, `phone_office`, `phone_private`, `phone_mobile`, `fax`, `notes`, `birthday`, `status`, `emaildate`, `language`, `source`, `consent`) VALUES (\'1\', \'btKCKTku5u\', \'noreply@example.com\', \'\', \'m\', \'2\', \'\', \'Mustermann\', \'Hans\', \'\', \'\', \'\', \'\', \'\', \'\', \'204\', \'\', \'\', \'\', \'\', \'\', \'01-01-2011\', \'1\', \'1153137001\', \'1\', \'backend\', NULL)');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_newsletter_user_title` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user_title` (`id`, `title`) VALUES (\'6\', \'Cher Monsieur\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user_title` (`id`, `title`) VALUES (\'5\', \'Chère Madame\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user_title` (`id`, `title`) VALUES (\'4\', \'Dear Mr\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user_title` (`id`, `title`) VALUES (\'3\', \'Dear Ms\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user_title` (`id`, `title`) VALUES (\'1\', \'Sehr geehrte Frau\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_newsletter_user_title` (`id`, `title`) VALUES (\'2\', \'Sehr geehrter Herr\')');
        if (!\Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'core_mail_template')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_mail_template',
                array(
                    'key' => array('type' => 'tinytext', 'notnull' => true),
                    'section' => array('type' => 'tinytext', 'notnull' => true, 'renamefrom' => 'module_id', 'after' => 'key'),
                    'text_id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'renamefrom' => 'text_name_id', 'after' => 'section'),
                    'html' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'text_id'),
                    'protected' => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'html'),
                ),
                null,
                'InnoDB',
                'cx3upgrade'
            );
            \Cx\Lib\UpdateUtil::sql("
                ALTER TABLE `".DBPREFIX."core_mail_template`
                ADD PRIMARY KEY (`key` (32), `section` (32))
            ");
        }
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('activation_email','Newsletter',24,0,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('confirm_email','Newsletter',25,0,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('notification_email','Newsletter',26,0,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('notify_undelivered_email','Newsletter',27,0,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('notify_subscription_list_same','Newsletter',28,0,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('notify_subscription_list_additional','Newsletter',29,0,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_mail_template` (`key`, `section`, `text_id`, `html`, `protected`) VALUES ('consent_confirmation_email','Newsletter',31,1,1)");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nWir freuen uns,Sie bei unserem Newsletter begrüssen zu dürfen und wünschen Ihnen viel Freude damit.\r\nSie erhalten ab sofort wöchentlich die neuesten Informationen in elektronischer Form zu gestellt.\r\n\r\nUm die Bestellung des Newsletters zu bestätigen,bitten wir Sie,auf den folgenden Link zu klicken bzw. ihn in die Adresszeile Ihres Browsers zu kopieren:\r\n\r\n[NEWSLETTER_CONFIRM_CODE]\r\n\r\nUm zu verhindern,dass unser Newsletter in Ihrem Spam-Ordner landet,fügen Sie bitte die Adresse dieser E-Mail Ihrem Adressbuch hinzu.\r\n\r\nSofern Sie diese E-Mail ungewünscht erhalten haben,bitten wir um Entschuldigung. Sie werden keine weitere E-Mail mehr von uns erhalten.\r\n\r\n--\r\nDies ist eine automatisch generierte Nachricht.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_name','Anmeldung zum Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Anmeldung zum Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,1,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_name','Newsletter Activation')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter subscription')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (24,2,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME] \r\n\r\nWe are glad to welcome you to our newsletter and wish you much enjoyment with it.\r\nFrom now on you will receive the most actual news weekly. \r\n\r\nIn order to confirm the newsletter subscription please click on the following link or copy it to the address bar of your webbrowser: \r\n\r\n[NEWSLETTER_CONFIRM_CODE] \r\n\r\nTo prevent our newsletter from ending up in your spam folder, please add this email''s sender address to your contact list. \r\n\r\nIn case you did not request to sign up to this newsletter by yourself, please ignore this email. You may not receive any further emails from us.\r\n\r\n--\r\nThis is an automatically generated message.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nIhr Newsletter Abonnement wurde erfolgreich registriert.\r\nSie werden nun in Zukunft unsere Newsletter erhalten. \r\n\r\n--\r\nDies ist eine automatisch generierte Nachricht.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_name','Bestätigung der Anmeldung zum Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Bestätigung zur Newsletteranmeldung')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,1,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_name','Newsletter Confirm Mail')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter subscription confirmation')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (25,2,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nYour subscription to this newsletter was successfully registered.\r\nYou will receive our newsletter messages.\r\n\r\n--\r\nThis is an automatically generated message.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_message','Folgende Mutation wurde im Newsletter System getätigt:\r\n\r\nGetätigte Aktion: [NEWSLETTER_NOTIFICATION_ACTION]\r\nGeschlecht:       [NEWSLETTER_USER_SEX]\r\nAnrede:           [NEWSLETTER_USER_TITLE]\r\nVorname:          [NEWSLETTER_USER_FIRSTNAME]\r\nNachname:         [NEWSLETTER_USER_LASTNAME]\r\nE-Mail:           [NEWSLETTER_USER_EMAIL]\r\n\r\n--\r\nDies ist eine automatisch generierte Nachricht.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_name','Benachrichtigung über eine Änderung in Ihrer Newsletter-Anmeldung')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter Empfänger [NEWSLETTER_NOTIFICATION_ACTION]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,1,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_name','Newsletter Notification Email')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter receiver [NEWSLETTER_NOTIFICATION_ACTION]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (26,2,'Newsletter','core_mail_template_message','The following changes were made to the newsletter system: \r\n\r\nAction:     [NEWSLETTER_NOTIFICATION_ACTION]\r\nGender:     [NEWSLETTER_USER_SEX]\r\nSalutation: [NEWSLETTER_USER_TITLE]\r\nFirstname:  [NEWSLETTER_USER_FIRSTNAME]\r\nLastname:   [NEWSLETTER_USER_LASTNAME]\r\nEmail:      [NEWSLETTER_USER_EMAIL] \r\n\r\n--\r\nThis is an automatically generated message.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_message','Der Newsletter konnte an folgende E-Mail-Adresse nicht versendet werden:\r\n[NEWSLETTER_USER_EMAIL]\r\n\r\nUm die E-Mail Adresse zu bearbeiten,klicken Sie bitte auf den folgenden Link:\r\n[NEWSLETTER_USER_EDIT_LINK]\r\n\r\n--\r\nDies ist eine automatisch generierte Nachricht.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_name','Benachrichtung über nicht zustellbaren Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_SUBJECT] - Benachrichtigung über unzustellbare E-Mail')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,1,'Newsletter','core_mail_template_to','[NEWSLETTER_LOGGED_USER_EMAIL]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_name','Undeliverable mail notification')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_to','[NEWSLETTER_LOGGED_USER_EMAIL]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_SUBJECT] - Undeliverable email notification')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (27,2,'Newsletter','core_mail_template_message','Sending the newsletter to the following email address has failed:\r\n[NEWSLETTER_USER_EMAIL] \r\n\r\nIn order to edit the email address please click the following link:\r\n[NEWSLETTER_USER_EDIT_LINK]\r\n\r\n--\r\nThis is an automatically generated message.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nSie sind bereits Abonnent des gewählten Newsletters.\r\n\r\n--\r\nDies ist eine automatisch generierte Nachricht.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_name','Informiert den Abonnenten über die erneute Anmeldung an den Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Anmeldung zum Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,1,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nYou have already subscribed to this newsletter list.\r\n\r\n--\r\nThis is an automatically generated message.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_name','Notify user about subscribing to the same newsletter list again')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter subscription')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (28,2,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nSie haben sich erfolgreich in den/die folgende(n) Newsletter eingetragen:\r\n[[NEWSLETTER_LISTS]\r\n   [NEWSLETTER_LIST]\r\n[NEWSLETTER_LISTS]]\r\n\r\n--\r\nDies ist eine automatisch generierte Nachricht.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_name','Informiert den Abonnenten über die Anmeldung an einen weiteren Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Anmeldung zum Newsletter')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,1,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_LASTNAME]\r\n\r\nYou have successfully registered to the following newsletter list(s):\r\n[[NEWSLETTER_LISTS]\r\n    [NEWSLETTER_LIST]\r\n[NEWSLETTER_LISTS]]\r\n\r\n--\r\nThis is an automatically generated message.\r\n[NEWSLETTER_CURRENT_DATE]')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_name','Notify user about the additional newsletter list subscription')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter subscription')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (29,2,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_name','Zustimmungsbestätigung')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter Zustimmungsbestätigung')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]\r\n\r\nBitte bestätigen Sie, dass Sie Mails der folgenden Mailing-Listen erhalten möchten:\r\n[[NEWSLETTER_LISTS]\r\n   [NEWSLETTER_LIST]\r\n[NEWSLETTER_LISTS]]\r\n\r\nUm dies zu bestätigen, klicken Sie bitte auf den folgenden Link oder kopieren Sie ihn in die Adressleiste Ihres Webbrowsers:\r\n\r\n[NEWSLETTER_CONSENT_CONFIRM_CODE]\r\n\r\n--\r\n\r\nDies ist eine automatisch generierte Nachricht.')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,1,'Newsletter','core_mail_template_message_html','<html>\r\n<head>\r\n  <title></title>\r\n</head>\r\n<body>[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]<br />\r\n<br />\r\nBitte best&auml;tigen Sie, dass Sie Mails der folgenden Mailing-Listen erhalten m&ouml;chten:<br />\r\n[[NEWSLETTER_LISTS]<br />\r\n&nbsp; &nbsp;[NEWSLETTER_LIST]<br />\r\n[NEWSLETTER_LISTS]]<br />\r\n<br />\r\nUm dies zu best&auml;tigen, klicken Sie bitte auf den folgenden Link oder kopieren Sie ihn in die Adressleiste Ihres Webbrowsers:<br />\r\n<br />\r\n<a href=\"[NEWSLETTER_CONSENT_CONFIRM_CODE]\">[NEWSLETTER_CONSENT_CONFIRM_CODE]</a><br />\r\n<br />\r\n--<br />\r\n<br />\r\nDies ist eine automatisch generierte Nachricht.</body>\r\n</html>\r\n')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_name','Consent confirmation')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_from','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_sender','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_reply','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_to','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_cc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_bcc','')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_subject','[NEWSLETTER_DOMAIN_URL] - Newsletter consent confirmation')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_message','[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]\r\n\r\nPlease confirm that you would like to receive mails from the following mailing lists:\r\n[[NEWSLETTER_LISTS]\r\n   [NEWSLETTER_LIST]\r\n[NEWSLETTER_LISTS]]\r\n\r\nIn order to confirm this please click on the following link or copy it to the address bar of your webbrowser:\r\n\r\n[NEWSLETTER_CONSENT_CONFIRM_CODE]\r\n\r\n--\r\n\r\nThis is an automatically generated message.')");
        \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."core_text` (`id`, `lang_id`, `section`, `key`, `text`) VALUES (31,2,'Newsletter','core_mail_template_message_html','<html>\r\n<head>\r\n  <title></title>\r\n</head>\r\n<body>[NEWSLETTER_USER_TITLE] [NEWSLETTER_USER_FIRSTNAME]<br />\r\n<br />\r\nPlease confirm that you would like to receive mails from the following mailing lists:<br />\r\n[[NEWSLETTER_LISTS]<br />\r\n&nbsp; &nbsp;[NEWSLETTER_LIST]<br />\r\n[NEWSLETTER_LISTS]]<br />\r\n<br />\r\nIn order to confirm this please click on the following link or copy it to the address bar of your webbrowser:<br />\r\n<br />\r\n<a href=\"[NEWSLETTER_CONSENT_CONFIRM_CODE]\">[NEWSLETTER_CONSENT_CONFIRM_CODE]</a><br />\r\n<br />\r\n--<br />\r\n<br />\r\nThis is an automatically generated message.</body>\r\n</html>\r\n')");
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
