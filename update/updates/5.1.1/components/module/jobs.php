<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _jobsUpdate() {
    global $objDatabase, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_jobs',
                array(
                    'id'         => array('type' => 'INT(6)',       'notnull' => true,  'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                    'date'       => array('type' => 'INT(14)',      'notnull' => false),
                    'title'      => array('type' => 'VARCHAR(250)', 'notnull' => true,  'default' => ''),
                    'author'     => array('type' => 'VARCHAR(150)', 'notnull' => true,  'default' => ''),
                    'text'       => array('type' => 'MEDIUMTEXT'),
                    'workloc'    => array('type' => 'VARCHAR(250)', 'notnull' => true,  'default' => ''),
                    'workload'   => array('type' => 'VARCHAR(250)', 'notnull' => true,  'default' => ''),
                    'work_start' => array('type' => 'INT(14)',      'notnull' => true,  'default' => 0),
                    'catid'      => array('type' => 'INT(2)',       'notnull' => true,  'default' => 0, 'unsigned' => true),
                    'lang'       => array('type' => 'INT(2)',       'notnull' => true,  'default' => 0, 'unsigned' => true),
                    'userid'     => array('type' => 'INT(6)',       'notnull' => true,  'default' => 0, 'unsigned' => true),
                    'startdate'  => array('type' => 'TIMESTAMP',    'notnull' => true,  'default' => '0000-00-00 00:00:00'),
                    'enddate'    => array('type' => 'TIMESTAMP',    'notnull' => true,  'default' => '0000-00-00 00:00:00'),
                    'status'     => array('type' => 'TINYINT(4)',       'notnull' => true,  'default' => 1),
                    'changelog'  => array('type' => 'INT(14)',      'notnull' => true,  'default' => 0),
                    'hot'        => array('type' => 'TINYINT(4)',   'notnull' => true,  'default' => 0),
                    'paid'       => array('type' => 'TINYINT(1)',   'notnull' => true,  'default' => 0),
                ),
                array(
                    'newsindex'  => array('fields' => array('title', 'text'), 'type' => 'fulltext')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_jobs_categories',
                array(
                    'catid'      => array('type' => 'INT(2)',           'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                    'name'       => array('type' => 'VARCHAR(100)',                        'default'        => ''),
                    'lang'       => array('type' => 'INT(2)',                              'default'        => 1, 'unsigned' => true),
                    'sort_style' => array('type' => "ENUM('alpha', 'date', 'date_alpha')", 'default'        => 'alpha')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_jobs_location',
                array(
                    'id'   => array('type' => 'INT(10)',      'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                    'name' => array('type' => 'VARCHAR(100)', 'default' => '')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_jobs_rel_loc_jobs',
                array(
                    'job'        => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'location'   => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true)
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_jobs_settings',
                array(
                    'id'    => array('type' => 'INT(10)',      'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                    'name'  => array('type' => 'VARCHAR(250)', 'default' => ''),
                    'value' => array('type' => 'TEXT',         'default' => '')
                )
            );

        }
        catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        $arrSettings = array(
            array(
                'name'  => 'footnote',
                'value' => 'Hat Ihnen diese Bewerbung zugesagt? \r\nDann können Sie sich sogleich telefonisch, per E-mail oder Web Formular bewerben.'
            ),
            array(
                'name'  => 'link',
                'value' => 'Online für diese Stelle bewerben.'
            ),
            array(
                'name'  => 'url',
                'value' => 'index.php?section=contact&cmd=5&44=%URL%&43=%TITLE%'
            ),
            array(
                'name'  => 'show_location_fe',
                'value' => '1'
            ),
            array(
                'name'  => 'templateIntegration',
                'value' => '0'
            ),
            array(
                'name'  => 'sourceOfJobs',
                'value' => 'latest'
            ),
            array(
                'name'  => 'listingLimit',
                'value' => '0'
            ),
        );
        foreach ($arrSettings as $arrSetting) {
            $query = "SELECT 1 FROM `".DBPREFIX."module_jobs_settings` WHERE `name` = '".$arrSetting['name']."'";
            $objResult = $objDatabase->SelectLimit($query, 1);
            if ($objResult !== false) {
                if ($objResult->RecordCount() == 0) {
                    $query = "INSERT INTO `".DBPREFIX."module_jobs_settings` (`name`, `value`) VALUES ('".$arrSetting['name']."', '".$arrSetting['value']."')";
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'text'  =>  'module_jobs',
            'value' =>  'module_jobs_settings',
        );
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_jobs_flag',
                array(
                    'id'         => array('type' => 'int', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'icon'       => array('type' => 'text', 'after' => 'name'),
                    'value'      => array('type' => 'text', 'after' => 'icon')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_jobs_rel_flag_job',
                array(
                    'job'        => array('type' => 'INT(6)', 'unsigned' => true, 'primary' => true),
                    'flag'       => array('type' => 'int', 'after' => 'job', 'primary' => true)
                ),
                array(),
                'InnoDb',
                '',
                array(
                    'job' => array(
                        'table' => DBPREFIX.'module_jobs',
                        'column'    => 'id',
                        'onDelete'  => 'CASCADE',
                        'onUpdate'  => 'NO ACTION',
                    ),
                    'flag' => array(
                        'table' => DBPREFIX.'module_jobs_flag',
                        'column'    => 'id',
                        'onDelete'  => 'CASCADE',
                        'onUpdate'  => 'NO ACTION',
                    ),
                )
            );
    		\Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'8\', \'use_flags\', \'0\')');
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _jobsInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(14) DEFAULT NULL,
  `title` varchar(250) NOT NULL DEFAULT \'\',
  `author` varchar(150) NOT NULL DEFAULT \'\',
  `text` mediumtext NOT NULL,
  `workloc` varchar(250) NOT NULL DEFAULT \'\',
  `workload` varchar(250) NOT NULL DEFAULT \'\',
  `work_start` int(14) NOT NULL DEFAULT \'0\',
  `catid` int(2) unsigned NOT NULL DEFAULT \'0\',
  `lang` int(2) unsigned NOT NULL DEFAULT \'0\',
  `userid` int(6) unsigned NOT NULL DEFAULT \'0\',
  `startdate` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  `enddate` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  `status` tinyint(4) NOT NULL DEFAULT \'1\',
  `changelog` int(14) NOT NULL DEFAULT \'0\',
  `hot` tinyint(4) NOT NULL DEFAULT \'0\',
  `paid` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `newsindex` (`title`,`text`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs_categories` (
  `catid` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `lang` int(2) unsigned NOT NULL DEFAULT \'1\',
  `sort_style` enum(\'alpha\',\'date\',\'date_alpha\') NOT NULL DEFAULT \'alpha\',
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'1\', \'Informatik\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'2\', \'Gastronomie\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'3\', \'Baugewerbe\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'4\', \'Grafik / Zeichner\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'5\', \'Gastronomie\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'6\', \'Landwirtschaft\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'7\', \'Kaufmännische Berufe\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'8\', \'Mechanik\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'9\', \'Medizin\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'10\', \'Bankgewerbe\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'11\', \'Verkauf\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'jobs_flag` (
  `id` int NOT NULL auto_increment ,
  `name` varchar(255) NOT NULL default \'\',
  `icon` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs_location` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'1\', \'New York\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'2\', \'Tokyo\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'3\', \'London\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'4\', \'Zürich\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'5\', \'Thun\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'6\', \'Los Angeles\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'7\', \'Shanghai\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'8\', \'Moskau\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_location` (`id`, `name`) VALUES (\'9\', \'Dublin\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs_flag` (
  `id` int NOT NULL AUTO_INCREMENT ,
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `icon` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY(`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs_rel_flag_job` (
  `job` int(6) unsigned NOT NULL,
  `flag` int NOT NULL,
  PRIMARY KEY (`job`,`flag`),
  CONSTRAINT `'.DBPREFIX.'module_jobs_rel_flag_job_ibfk_1` FOREIGN KEY (`job`) REFERENCES `'.DBPREFIX.'module_jobs` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `'.DBPREFIX.'module_jobs_rel_flag_job_ibfk_2` FOREIGN KEY (`flag`) REFERENCES `'.DBPREFIX.'module_jobs_flag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs_rel_loc_jobs` (
  `job` int(10) unsigned NOT NULL DEFAULT \'0\',
  `location` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`job`,`location`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_jobs_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'1\', \'footnote\', \'Hat Ihnen diese Bewerbung zugesagt? \\r\\nDann können Sie sich sogleich telefonisch, per E-mail oder Web Formular bewerben.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'2\', \'link\', \'Online für diese Stelle bewerben.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'3\', \'url\', \'index.php?section=contact&cmd=5&44=%URL%&43=%TITLE%\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'4\', \'show_location_fe\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'5\', \'templateIntegration\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'6\', \'sourceOfJobs\', \'latest\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'7\', \'listingLimit\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_jobs_settings` (`id`, `name`, `value`) VALUES (\'8\', \'use_flags\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
