<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _mediadirUpdate()
{
    global $_ARRAYLANG, $_CORELANG, $objUpdate, $_CONFIG;

    //create / update tables
    try {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_categories',
                array(
                      'id'                     => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'parent_id'              => array('type' => 'INT(7)', 'after' => 'id'),
                        'order'                  => array('type' => 'INT(7)', 'after' => 'parent_id'),
                        'show_subcategories'     => array('type' => 'INT(11)', 'after' => 'order'),
                        'show_entries'           => array('type' => 'INT(1)', 'after' => 'show_subcategories'),
                        'picture'                => array('type' => 'mediumtext', 'after' => 'show_entries'),
                        'active'                 => array('type' => 'INT(1)', 'after' => 'picture')
                )
           );

           \Cx\Lib\UpdateUtil::table(
              DBPREFIX.'module_mediadir_comments',
              array(
                  'id'                 => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                  'entry_id'           => array('type' => 'INT(7)', 'after' => 'id'),
                  'added_by'           => array('type' => 'VARCHAR(255)', 'after' => 'entry_id'),
                  'date'               => array('type' => 'VARCHAR(100)', 'after' => 'added_by'),
                  'name'               => array('type' => 'VARCHAR(255)', 'after' => 'date'),
                  'mail'               => array('type' => 'VARCHAR(255)', 'after' => 'name'),
                  'url'                => array('type' => 'VARCHAR(255)', 'after' => 'mail'),
                  'notification'       => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'url'),
                  'comment'            => array('type' => 'mediumtext', 'after' => 'notification')
              )
          );

          \Cx\Lib\UpdateUtil::table(
              DBPREFIX.'module_mediadir_entries',
              array(
                  'id'                         => array('type' => 'INT(10)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                  'order'                      => array('type' => 'INT(7)', 'notnull' => true, 'default' => '0', 'after' => 'id'),
                  'form_id'                    => array('type' => 'INT(7)', 'after' => 'order'),
                  'create_date'                => array('type' => 'INT(50)', 'after' => 'form_id'),
                  'update_date'                => array('type' => 'INT(50)', 'after' => 'create_date'),
                  'validate_date'              => array('type' => 'INT(50)', 'after' => 'update_date'),
                  'added_by'                   => array('type' => 'INT(10)', 'after' => 'validate_date'),
                  'updated_by'                 => array('type' => 'INT(10)', 'after' => 'added_by'),
                  'lang_id'                    => array('type' => 'INT(1)', 'after' => 'updated_by'),
                  'hits'                       => array('type' => 'INT(10)', 'after' => 'lang_id'),
                  'popular_hits'               => array('type' => 'INT(10)', 'after' => 'hits'),
                  'popular_date'               => array('type' => 'VARCHAR(20)', 'after' => 'popular_hits'),
                  'last_ip'                    => array('type' => 'VARCHAR(50)', 'after' => 'popular_date'),
                  'ready_to_confirm'           => array('type' => 'INT(1)', 'notnull' => true, 'default' => '0', 'after' => 'last_ip'),
                  'confirmed'                  => array('type' => 'INT(1)', 'after' => 'ready_to_confirm'),
                  'active'                     => array('type' => 'INT(1)', 'after' => 'confirmed'),
                  'duration_type'              => array('type' => 'INT(1)', 'after' => 'active'),
                  'duration_start'             => array('type' => 'INT(50)', 'after' => 'duration_type'),
                  'duration_end'               => array('type' => 'INT(50)', 'after' => 'duration_start'),
                  'duration_notification'      => array('type' => 'INT(1)', 'after' => 'duration_end'),
                  'translation_status'         => array('type' => 'VARCHAR(255)', 'after' => 'duration_notification')
              ),
              array(
                    'lang_id'                    => array('fields' => array('lang_id')),
                  'active'                     => array('fields' => array('active'))
              )
          );

        }
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_form_names',
                array(
                    'lang_id'                => array('type' => 'INT(1)'),
                    'form_id'                => array('type' => 'INT(7)', 'after' => 'lang_id'),
                    'form_name'              => array('type' => 'VARCHAR(255)', 'after' => 'form_id'),
                    'form_description'       => array('type' => 'mediumtext', 'notnull' => true, 'after' => 'form_name')
                ),
                array(
                    'form'                   => array('fields' => array('lang_id','form_id'), 'type' => 'UNIQUE')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_categories_names',
                array(
                    'lang_id'                    => array('type' => 'INT(1)'),
                    'category_id'                => array('type' => 'INT(7)', 'after' => 'lang_id'),
                    'category_name'              => array('type' => 'VARCHAR(255)', 'after' => 'category_id'),
                    'category_description'       => array('type' => 'mediumtext', 'after' => 'category_name'),
                    'category_metadesc'          => array('type' => 'VARCHAR(160)', 'notnull' => true, 'default' => '', 'after' => 'category_description'),
                ),
                array(
                    'category'                   => array('fields' => array('lang_id','category_id'), 'type' => 'UNIQUE'),
                    'lang_id'                    => array('fields' => array('lang_id')),
                    'category_id'                => array('fields' => array('category_id'))
                )
           );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_level_names',
                array(
                    'lang_id'                => array('type' => 'INT(1)'),
                    'level_id'               => array('type' => 'INT(7)', 'after' => 'lang_id'),
                    'level_name'             => array('type' => 'VARCHAR(255)', 'after' => 'level_id'),
                    'level_description'      => array('type' => 'mediumtext', 'after' => 'level_name'),
                    'level_metadesc'        => array('type' => 'VARCHAR(160)', 'notnull' => true, 'default' => '', 'after' => 'level_description'),
                ),
                array(
                    'level'                  => array('fields' => array('lang_id','level_id'), 'type' => 'UNIQUE'),
                    'lang_id'                => array('fields' => array('lang_id')),
                    'category_id'            => array('fields' => array('level_id'))
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_forms',
                array(
                    'id'                         => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'order'                      => array('type' => 'INT(7)', 'after' => 'id'),
                    'picture'                    => array('type' => 'mediumtext', 'after' => 'order'),
                    'active'                     => array('type' => 'INT(1)', 'after' => 'picture'),
                    'use_level'                  => array('type' => 'INT(1)', 'after' => 'active'),
                    'use_category'               => array('type' => 'INT(1)', 'after' => 'use_level'),
                    'use_ready_to_confirm'       => array('type' => 'INT(1)', 'after' => 'use_category'),
                    'use_associated_entries'     => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'use_ready_to_confirm'),
                    'entries_per_page'           => array('type' => 'INT(7)', 'notnull' => true, 'default' => '0', 'after' => 'use_associated_entries'),
                    'cmd'                        => array('type' => 'VARCHAR(50)', 'after' => 'entries_per_page')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_entry_associated_entry',
                array(
                    'source_entry_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'primary' => true),
                    'target_entry_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'source_entry_id', 'primary' => true),
                    'ord'                => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'target_entry_id')
                ),
                array(
                    'ord'                => array('fields' => array('ord'))
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_form_associated_form',
                array(
                    'source_form_id'     => array('type' => 'INT(11)', 'unsigned' => true, 'primary' => true),
                    'target_form_id'     => array('type' => 'INT(11)', 'unsigned' => true, 'after' => 'source_form_id', 'primary' => true),
                    'ord'                => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'target_form_id')
                ),
                array(
                    'ord'                => array('fields' => array('ord'))
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_inputfield_names',
                array(
                    'lang_id'                => array('type' => 'INT(10)'),
                    'form_id'                => array('type' => 'INT(7)', 'after' => 'lang_id'),
                    'field_id'               => array('type' => 'INT(10)', 'after' => 'form_id'),
                    'field_name'             => array('type' => 'VARCHAR(255)', 'after' => 'field_id'),
                    'field_default_value'    => array('type' => 'mediumtext', 'after' => 'field_name'),
                    'field_info'             => array('type' => 'mediumtext', 'after' => 'field_default_value')
                    ),
                array(
                    'field'                  => array('fields' => array('lang_id','form_id','field_id'), 'type' => 'UNIQUE'),
                    'field_id'               => array('fields' => array('field_id')),
                    'lang_id'                => array('fields' => array('lang_id'))
                )
            );
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_inputfield_types',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'           => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'active'         => array('type' => 'INT(1)', 'after' => 'name'),
                    'multi_lang'     => array('type' => 'INT(1)', 'after' => 'active'),
                    'exp_search'     => array('type' => 'INT(7)', 'after' => 'multi_lang'),
                    'dynamic'        => array('type' => 'INT(1)', 'after' => 'exp_search'),
                    'comment'        => array('type' => 'VARCHAR(255)', 'after' => 'dynamic')
                    ),
                array(
                    'name'           => array('fields' => array('name'), 'type' => 'UNIQUE'),
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_inputfield_verifications',
                array(
                    'id'         => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'regex'      => array('type' => 'VARCHAR(255)', 'after' => 'name')
                    ),
                array(
                    'name'       => array('fields' => array('name'), 'type' => 'UNIQUE')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_levels',
                array(
                    'id'                 => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'parent_id'          => array('type' => 'INT(7)', 'after' => 'id'),
                    'order'              => array('type' => 'INT(7)', 'after' => 'parent_id'),
                    'show_sublevels'     => array('type' => 'INT(11)', 'after' => 'order'),
                    'show_categories'    => array('type' => 'INT(1)', 'after' => 'show_sublevels'),
                    'show_entries'       => array('type' => 'INT(1)', 'after' => 'show_categories'),
                    'picture'            => array('type' => 'mediumtext', 'after' => 'show_entries'),
                    'active'             => array('type' => 'INT(1)', 'after' => 'picture')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_mail_actions',
                array(
                    'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'                   => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'default_recipient'      => array('type' => 'ENUM(\'admin\',\'author\')', 'after' => 'name'),
                    'need_auth'              => array('type' => 'INT(11)', 'after' => 'default_recipient')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_mails',
                array(
                    'id'             => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'title'          => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'content'        => array('type' => 'longtext', 'after' => 'title'),
                    'recipients'     => array('type' => 'mediumtext', 'after' => 'content'),
                    'lang_id'        => array('type' => 'INT(1)', 'after' => 'recipients'),
                    'action_id'      => array('type' => 'INT(1)', 'after' => 'lang_id'),
                    'is_default'     => array('type' => 'INT(1)', 'after' => 'action_id'),
                    'active'         => array('type' => 'INT(1)', 'after' => 'is_default')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_masks',
                array(
                    'id'         => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'title'      => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'fields'     => array('type' => 'mediumtext', 'after' => 'title'),
                    'active'     => array('type' => 'INT(11)', 'after' => 'fields'),
                    'form_id'    => array('type' => 'INT(11)', 'after' => 'active')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_order_rel_forms_selectors',
                array(
                    'selector_id'        => array('type' => 'INT(7)'),
                    'form_id'            => array('type' => 'INT(7)', 'after' => 'selector_id'),
                    'selector_order'     => array('type' => 'INT(7)', 'after' => 'form_id'),
                    'exp_search'         => array('type' => 'INT(1)', 'notnull' => true, 'after' => 'selector_order')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_rel_entry_categories',
                array(
                    'entry_id'       => array('type' => 'INT(10)'),
                    'category_id'    => array('type' => 'INT(10)', 'after' => 'entry_id')
                ),
                array(
                    'entry_id'       => array('fields' => array('entry_id')),
                    'category_id'    => array('fields' => array('category_id'))
                )
            );
        }


        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'module_mediadir_inputfields',
                array(
                    'id'                 => array('type' => 'INT(10)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'form'               => array('type' => 'INT(7)', 'after' => 'id'),
                    'type'               => array('type' => 'INT(10)', 'after' => 'form'),
                    'verification'       => array('type' => 'INT(10)', 'after' => 'type'),
                    'search'             => array('type' => 'INT(10)', 'after' => 'verification'),
                    'required'           => array('type' => 'INT(10)', 'after' => 'search'),
                    'order'              => array('type' => 'INT(10)', 'after' => 'required'),
                    'show_in'            => array('type' => 'INT(10)', 'after' => 'order'),
                    'context_type'       => array('type' => 'ENUM(\'none\',\'title\',\'content\',\'address\',\'zip\',\'city\',\'country\',\'image\',\'slug\',\'keywords\')', 'after' => 'show_in')
                )
            );
        }


        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            if (   $objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')
                || detectCx3Version() == 'rc1'
            ) {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_mediadir_rel_entry_inputfields_clean',
                    array(
                        'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                        'entry_id'       => array('type' => 'INT(7)', 'after' => 'id'),
                        'lang_id'        => array('type' => 'INT(7)', 'after' => 'entry_id'),
                        'form_id'        => array('type' => 'INT(7)', 'after' => 'lang_id'),
                        'field_id'       => array('type' => 'INT(7)', 'after' => 'form_id'),
                        'value'          => array('type' => 'longtext', 'after' => 'field_id')
                    ),
                    array(
                        'value'          => array('fields' => array('value'), 'type' => 'FULLTEXT')
                    )
                );

                \Cx\Lib\UpdateUtil::sql('
                    INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields_clean`
                    SELECT NULL, `entry_id`, `lang_id`, `form_id`, `field_id`, `value`
                    FROM `'.DBPREFIX.'module_mediadir_rel_entry_inputfields`
                    GROUP BY `entry_id`, `form_id`, `field_id`, `lang_id`, `value`
                ');

                \Cx\Lib\UpdateUtil::sql('
                    TRUNCATE `'.DBPREFIX.'module_mediadir_rel_entry_inputfields`
                ');

                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_mediadir_rel_entry_inputfields',
                    array(
                        'entry_id'       => array('type' => 'INT(7)'),
                        'lang_id'        => array('type' => 'INT(7)', 'after' => 'entry_id'),
                        'form_id'        => array('type' => 'INT(7)', 'after' => 'lang_id'),
                        'field_id'       => array('type' => 'INT(7)', 'after' => 'form_id'),
                        'value'          => array('type' => 'longtext', 'after' => 'field_id')
                    ),
                    array(
                        'entry_id'       => array('fields' => array('entry_id','lang_id','form_id','field_id'), 'type' => 'UNIQUE'),
                        'value'          => array('fields' => array('value'), 'type' => 'FULLTEXT')
                    )
                );

                \Cx\Lib\UpdateUtil::sql('
                    INSERT IGNORE INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields`
                    SELECT `entry_id`, `lang_id`, `form_id`, `field_id`, `value`
                    FROM `'.DBPREFIX.'module_mediadir_rel_entry_inputfields_clean`
                    ORDER BY `id` DESC
                ');

                \Cx\Lib\UpdateUtil::sql('
                    DROP TABLE `'.DBPREFIX.'module_mediadir_rel_entry_inputfields_clean`
                ');
            }
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_rel_entry_levels',
                array(
                    'entry_id'       => array('type' => 'INT(10)'),
                    'level_id'       => array('type' => 'INT(10)', 'after' => 'entry_id')
                ),
                array(
                    'entry_id'       => array('fields' => array('entry_id')),
                    'category_id'    => array('fields' => array('level_id'))
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_settings',
                array(
                    'id'         => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(100)', 'after' => 'id'),
                    'value'      => array('type' => 'VARCHAR(255)', 'after' => 'name')
                ),
                array(
                    'name'       => array('fields' => array('name'), 'type' => 'UNIQUE')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_settings_num_categories',
                array(
                    'group_id'           => array('type' => 'INT(1)'),
                    'num_categories'     => array('type' => 'VARCHAR(10)', 'notnull' => true, 'after' => 'group_id')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_settings_num_entries',
                array(
                    'group_id'       => array('type' => 'INT(1)'),
                    'num_entries'    => array('type' => 'VARCHAR(10)', 'notnull' => true, 'default' => 'n', 'after' => 'group_id')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_settings_num_levels',
                array(
                    'group_id'       => array('type' => 'INT(1)'),
                    'num_levels'     => array('type' => 'VARCHAR(10)', 'notnull' => true, 'after' => 'group_id')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_settings_perm_group_forms',
                array(
                    'group_id'           => array('type' => 'INT(7)'),
                    'form_id'            => array('type' => 'INT(1)', 'after' => 'group_id'),
                    'status_group'       => array('type' => 'INT(1)', 'notnull' => true, 'after' => 'form_id')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_mediadir_votes',
                array(
                    'id'             => array('type' => 'INT(7)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'entry_id'       => array('type' => 'INT(7)', 'after' => 'id'),
                    'added_by'       => array('type' => 'VARCHAR(255)', 'after' => 'entry_id'),
                    'date'           => array('type' => 'VARCHAR(100)', 'after' => 'added_by'),
                    'ip'             => array('type' => 'VARCHAR(100)', 'after' => 'date'),
                    'vote'           => array('type' => 'INT(11)', 'after' => 'ip')
                )
            );
      
            // remove the script tag at the beginning of the mediadir pages
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array('module' => 'mediadir'), '/^\s*(<script[^>]+>.+?Shadowbox.+?<\/script>)+/sm', '', array('content'), '3.0.4');
        
        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        //insert default settings
        try {
            //mediadir_settings
            $arrValues = array(array(1,'settingsShowCategoryDescription','1'),array(2,'settingsShowCategoryImage','1'),array(3,'settingsCategoryOrder','1'),array(4,'settingsShowLevels','1'),array(5,'settingsShowLevelDescription','0'),array(6,'settingsShowLevelImage','0'),array(7,'settingsLevelOrder','1'),array(8,'settingsConfirmNewEntries','1'),array(9,'categorySelectorOrder','9'),array(10,'levelSelectorOrder','10'),array(11,'settingsConfirmUpdatedEntries','0'),array(12,'settingsCountEntries','0'),array(13,'settingsThumbSize','120'),array(15,'settingsEncryptFilenames','1'),array(16,'settingsAllowAddEntries','1'),array(17,'settingsAllowDelEntries','1'),array(18,'settingsAllowEditEntries','1'),array(19,'settingsAddEntriesOnlyCommunity','1'),array(20,'settingsLatestNumXML','10'),array(21,'settingsLatestNumOverview','5'),array(22,'settingsLatestNumBackend','5'),array(23,'settingsLatestNumFrontend','10'),array(24,'settingsPopularNumFrontend','10'),array(25,'settingsPopularNumRestore','30'),array(26,'settingsLatestNumHeadlines','6'),array(27,'settingsGoogleMapStartposition','46.749647513758326,7.6300048828125,8'),array(28,'settingsAllowVotes','1'),array(29,'settingsVoteOnlyCommunity','0'),array(30,'settingsAllowComments','1'),array(31,'settingsCommentOnlyCommunity','0'),array(32,'settingsGoogleMapAllowKml','0'),array(33,'settingsShowEntriesInAllLang','1'),array(34,'settingsPagingNumEntries','10'),array(35,'settingsGoogleMapType','0'),array(36,'settingsClassificationPoints','5'),array(37,'settingsClassificationSearch','1'),array(38,'settingsEntryDisplaydurationType','1'),array(39,'settingsEntryDisplaydurationValue','0'),array(40,'settingsEntryDisplaydurationValueType','1'),array(41,'settingsEntryDisplaydurationNotification','0'),array(42,'categorySelectorExpSearch','9'),array(43,'levelSelectorExpSearch','10'),array(44,'settingsTranslationStatus','0'),array(45,'settingsReadyToConfirm','0'),array(46,'settingsImageFilesize','300'),array(47,'settingsActiveLanguages','1,2,3'),array(48,'settingsFrontendUseMultilang','0'),array(49,'settingsIndividualEntryOrder','0'));
            foreach($arrValues as $arrValue) {
                if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_settings WHERE name="'.$arrValue[1].'"')->EOF) {
                    \Cx\Lib\UpdateUtil::sql('INSERT INTO '.DBPREFIX.'module_mediadir_settings VALUES('.$arrValue[0].',"'.$arrValue[1].'","'.$arrValue[2].'")');
                }
            }

            //mediadir_settings_num_categories
            $arrValues = array(array(3,'n'),array(4,'n'),array(5,'n'));
            foreach($arrValues as $arrValue) {
                if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_settings_num_categories WHERE group_id='.$arrValue[0])->EOF) {
                    \Cx\Lib\UpdateUtil::sql('INSERT INTO '.DBPREFIX.'module_mediadir_settings_num_categories VALUES('.$arrValue[0].',"'.$arrValue[1].'")');
                }
            }

            //mediadir_settings_num_entries
            $arrValues = array(array(3,'n'),array(4,'n'),array(5,'n'));
            foreach($arrValues as $arrValue) {
                if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_settings_num_entries WHERE group_id='.$arrValue[0])->EOF) {
                    \Cx\Lib\UpdateUtil::sql('INSERT INTO '.DBPREFIX.'module_mediadir_settings_num_entries VALUES('.$arrValue[0].',"'.$arrValue[1].'")');
                }
            }

            //mediadir_settings_num_levels
            $arrValues = array(array(3,'n'),array(4,'n'),array(5,'n'));
            foreach($arrValues as $arrValue) {
                if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_settings_num_levels WHERE group_id='.$arrValue[0])->EOF) {
                    \Cx\Lib\UpdateUtil::sql('INSERT INTO '.DBPREFIX.'module_mediadir_settings_num_levels VALUES('.$arrValue[0].',"'.$arrValue[1].'")');
                }
            }

            //mediadir_inputfield_verifications
            $arrValues = array(array(1,'normal','.*'),array(2,'e-mail',"^[a-zäàáâöôüûñéè0-9!\\#\\$\\%\\&\\''\\*\\+\\/\\=\\?\\^_\\`\\{\\|\\}\\~-]+(?:\\.[a-zäàáâöôüûñéè0-9!\\#\\$\\%\\&\\" . '"' . "'\\*\\+\\/\\=\\?\\^_\\`\\{\\|\\}\\~-]+)*@(?:[a-zäàáâöôüûñéè0-9](?:[a-zäàáâöôüûñéè0-9-]*[a-zäàáâöôüûñéè0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"),array(3,'url','^(?:(?:ht|f)tps?\:\/\/)?((([\wÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè\d-]{1,}\.)+[a-z]{2,})|((?:(?:25[0-5]|2[0-4]\d|[01]\d\d|\d?\d)(?:(\.?\d)\.)) {4}))(?:[\w\d]+)?(\/[\w\d\-\.\?\,\'\/\\\+\&\%\$\#\=\~]*)?$'),array(4,'letters','^[A-Za-zÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè\ ]*[A-Za-zÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè]+[A-Za-zÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè\ ]*$'),array(5,'numbers','^[0-9]*$'));
            foreach($arrValues as $arrValue) {
                if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_inputfield_verifications WHERE name="'.$arrValue[1].'"')->EOF) {
                    \Cx\Lib\UpdateUtil::sql('INSERT INTO '.DBPREFIX.'module_mediadir_inputfield_verifications VALUES('.$arrValue[0].',"'.$arrValue[1].'","'.$arrValue[2].'")');
                } else {
                    \Cx\Lib\UpdateUtil::sql('UPDATE '.DBPREFIX.'module_mediadir_inputfield_verifications SET `regex`="'.$arrValue[2].'" WHERE `name`="'.$arrValue[1].'"');
                }
            }

            //mediadir_mail_actions
            $arrValues = array(array(1,'newEntry','admin',0),array(2,'entryAdded','author',1),array(3,'entryConfirmed','author',1),array(4,'entryVoted','author',1),array(5,'entryDeleted','author',1),array(6,'entryEdited','author',1),array(8,'newComment','author',1),array(9,'notificationDisplayduration','admin',0));
            foreach($arrValues as $arrValue) {
                if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_mail_actions WHERE name="'.$arrValue[1].'"')->EOF) {
                    \Cx\Lib\UpdateUtil::sql('INSERT INTO '.DBPREFIX.'module_mediadir_mail_actions VALUES('.$arrValue[0].',"'.$arrValue[1].'","'.$arrValue[2].'",'.$arrValue[3].')');
                }
            }

            //only insert mails if the table is empty
            if(\Cx\Lib\UpdateUtil::sql('SELECT 1 FROM '.DBPREFIX.'module_mediadir_mails')->EOF) {
                \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES
    ('23', '[[URL]] - Eintrag erfolgreich bearbeitet', 'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])

Ihr Eintrag mit dem Titel \"[[TITLE]]\" auf [[URL]] wurde erfolgreich bearbeitet. 

Benutzen Sie folgenden Link um direkt zu Ihrem Eintrag zu gelangen:
[[LINK]]

Freundliche Grüsse
[[URL]]-Team

-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '6', '1', '0'),
('22', '[[URL]] - Eintrag erfolgreich gelöscht', 'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])

Ihr Eintrag mit dem Titel \"[[TITLE]]\" auf [[URL]] wurde erfolgreich gelöscht. 

Freundliche Grüsse
Ihr [[URL]]-Team

-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '5', '1', '0'),
('21', '[[URL]] - Eintrag wurde bewertet', 'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])

Zu Ihrem Eintrag mit dem Titel \"[[TITLE]]\" auf [[URL]] wurde eine Bewertung abgegeben. 

Benutzen Sie folgenden Link um direkt zu Ihrem Eintrag zu gelangen:
[[LINK]]

Freundliche Grüsse
Ihr [[URL]]-Team

-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '4', '1', '0'),
('20', '[[URL]] - Ihr Eintrag wurde aufgeschaltet', 'Guten Tag,

Ihr Eintrag \"[[TITLE]]\" wurde geprüft und ist ab sofort einsehbar.

Benutzen Sie folgenden Link um direkt zu ihrem Eintrag zu gelangen:
[[LINK]]


Freundliche Grüsse
Ihr [[URL]]-Team


-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '3', '1', '0'),
('19', '[[URL]] - Eintrag erfolgteich eingetragen', 'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])

Ihr Eintrag mit dem Titel \"[[TITLE]]\" wurde auf [[URL]] erfolgreich eingetragen. 


Freundliche Grüsse
Ihr [[URL]]-Team

-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '2', '1', '0'),
    ('24', '[[URL]] - Neuer Kommentar hinzugefügt', 'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])

Zu Ihrem Eintrag mit dem Titel \"[[TITLE]]\" auf [[URL]] wurde ein neuer Kommentar hinzugefügt. 

Benutzen Sie folgenden Link um direkt zu Ihrem Eintrag zu gelangen:
[[LINK]]

Freundliche Grüsse
Ihr [[URL]]-Team


-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '8', '1', '0'),
('32', '[[URL]] - Neuer Eintrag zur Prüfung freigegeben', 'Guten Tag,

Auf http://[[URL]] wurde ein neuer Eintrag mit dem Titel \"[[TITLE]]\" erfasst. Bitte prüfen Sie diesen und geben Sie ihn gegebenenfalls frei.


-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '1', '1', '0'),
    ('33', '[[URL]] - Die Anzeigedauer eines Eintrages läuft ab', 'Hallo Admin

Auf [[URL]] läuft in Kürze die Anzeigedauer des Eintrages \"[[TITLE]]\" ab.

Freundliche Grüsse
Ihr [[URL]]-Team

-- 
Diese Nachricht wurde am [[DATE]] automatisch von Contrexx auf http://[[URL]] generiert.', '', '1', '9', '1', '0');");
            }
        }
        catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            // add new options
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_mediadir_settings` (`id`, `name`, `value`) VALUES (50, 'showLatestEntriesInOverview', '1')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_mediadir_settings` (`id`, `name`, `value`) VALUES (51, 'showLatestEntriesInWebdesignTmpl', '1')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_mediadir_settings` (`id`, `name`, `value`) VALUES (52, 'legacyBehavior', '1')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_mediadir_settings` (`id`, `name`, `value`) VALUES (53, 'usePrettyUrls', '0')");

            //following queries for changing the path from images/mediadir into images/MediaDir
            \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_mediadir_categories`
                                     SET `picture` = REPLACE(`picture`, 'images/mediadir', 'images/MediaDir')
                                     WHERE `picture` LIKE ('" . ASCMS_PATH_OFFSET . "/images/mediadir%')");
            \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_mediadir_forms`
                                     SET `picture` = REPLACE(`picture`, 'images/mediadir', 'images/MediaDir')
                                     WHERE `picture` LIKE ('" . ASCMS_PATH_OFFSET . "/images/mediadir%')");
            \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_mediadir_levels`
                                     SET `picture` = REPLACE(`picture`, 'images/mediadir', 'images/MediaDir')
                                     WHERE `picture` LIKE ('" . ASCMS_PATH_OFFSET . "/images/mediadir%')");
            \Cx\Lib\UpdateUtil::sql("UPDATE `" . DBPREFIX . "module_mediadir_rel_entry_inputfields`
                                     SET `value` = REPLACE(`value`, 'images/mediadir', 'images/MediaDir')
                                     WHERE `value` LIKE ('" . ASCMS_PATH_OFFSET . "/images/mediadir%')");

            // implement GDPR
            \Cx\Lib\UpdateUtil::sql('UPDATE `'. DBPREFIX. 'module_mediadir_votes` SET `ip` = MD5(`ip`) WHERE CHAR_LENGTH(`ip`) < 30 AND `ip` != \'\'');
            \Cx\Lib\UpdateUtil::sql('UPDATE `'. DBPREFIX. 'module_mediadir_entries` SET `last_ip` = MD5(`last_ip`) WHERE CHAR_LENGTH(`last_ip`) < 30 AND `last_ip` != \'\'');
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_mediadir_comments', 'ip')) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'. DBPREFIX .'module_mediadir_comments` DROP COLUMN `ip`');
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        //Update script for moving the folder
        $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
        $sourceImagePath = $imagePath . '/mediadir';
        $targetImagePath = $imagePath . '/MediaDir';

        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceImagePath, $targetImagePath
            ));
            return false;
        }
        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'module_mediadir_rel_entry_inputfields' => 'value',
            'module_mediadir_categories'            => 'picture',
            'module_mediadir_levels'                => 'picture',
            'module_mediadir_mails'                 => 'content',
            'module_mediadir_categories_names'      => 'category_description',
            'module_mediadir_comments'              => 'comment',
            'module_mediadir_forms'                 => 'picture',
            'module_mediadir_level_names'           => 'level_description',
        );
        try {
            foreach ($attributes as $table => $attribute) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Medienverzeichnis (MediaDir)'
            ));
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.0')) {
        try {
            // update inputfield_types
            \Cx\Lib\UpdateUtil::sql("TRUNCATE `".DBPREFIX."module_mediadir_inputfield_types`");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('1', 'text', '1', '1', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('2', 'textarea', '1', '1', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('3', 'dropdown', '1', '0', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('4', 'radio', '1', '0', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('5', 'checkbox', '1', '0', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('7', 'file', '1', '1', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('8', 'image', '1', '1', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('11', 'classification', '1', '0', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('12', 'link', '1', '1', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('13', 'linkGroup', '1', '1', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('15', 'googleMap', '1', '0', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('19', 'wysiwyg', '1', '1', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('20', 'mail', '1', '1', '0', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('25', 'country', '1', '0', '1', '0', '')");
            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES ('31', 'range', '1', '0', '1', '0', '')");

            \Cx\Lib\UpdateUtil::sql('DELETE FROM '.DBPREFIX.'module_mediadir_settings WHERE name="settingsNumGalleryPics"');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.3')) {
        try {
            // drop Google Weather
            \Cx\Lib\UpdateUtil::sql('UPDATE `'.DBPREFIX.'module_mediadir_inputfields` SET `type` = 1  WHERE `type` = 21');
            \Cx\Lib\UpdateUtil::sql('DELETE FROM `'.DBPREFIX.'module_mediadir_inputfield_types` WHERE `id` = 21');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}

function tryButDontWorry($sql) {
    try {
        \Cx\Lib\UpdateUtil::sql($sql);
    }
    catch (\Cx\Lib\UpdateException $e) {
        //nothing.
    }
}
function _mediadirInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_categories` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `parent_id` int(7) NOT NULL,
  `order` int(7) NOT NULL,
  `show_subcategories` int(11) NOT NULL,
  `show_entries` int(1) NOT NULL,
  `picture` mediumtext NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories` (`id`, `parent_id`, `order`, `show_subcategories`, `show_entries`, `picture`, `active`) VALUES (\'176\', \'0\', \'0\', \'0\', \'1\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories` (`id`, `parent_id`, `order`, `show_subcategories`, `show_entries`, `picture`, `active`) VALUES (\'177\', \'0\', \'0\', \'0\', \'1\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories` (`id`, `parent_id`, `order`, `show_subcategories`, `show_entries`, `picture`, `active`) VALUES (\'178\', \'0\', \'0\', \'0\', \'1\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories` (`id`, `parent_id`, `order`, `show_subcategories`, `show_entries`, `picture`, `active`) VALUES (\'179\', \'0\', \'0\', \'0\', \'1\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_categories_names` (
  `lang_id` int(1) NOT NULL,
  `category_id` int(7) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_description` mediumtext NOT NULL,
  `category_metadesc` varchar(160) NOT NULL DEFAULT \'\',
  UNIQUE KEY `category` (`lang_id`,`category_id`),
  KEY `lang_id` (`lang_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'1\', \'176\', \'Administration\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'1\', \'177\', \'Entwicklung\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'1\', \'178\', \'Verkauf\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'1\', \'179\', \'Kunden\', \'Die zufriedenen Kunden von MaxMuster AG\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'2\', \'176\', \'Administration\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'2\', \'177\', \'Entwicklung\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'2\', \'178\', \'Verkauf\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_categories_names` (`lang_id`, `category_id`, `category_name`, `category_description`, `category_metadesc`) VALUES (\'2\', \'179\', \'Kunden\', \'Die zufriedenen Kunden von MaxMuster AG\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_comments` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `entry_id` int(7) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `date` varchar(100) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `notification` int(1) NOT NULL DEFAULT \'0\',
  `comment` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_entries` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `order` int(7) NOT NULL DEFAULT \'0\',
  `form_id` int(7) NOT NULL,
  `create_date` int(50) NOT NULL,
  `update_date` int(50) NOT NULL,
  `validate_date` int(50) NOT NULL,
  `added_by` int(10) NOT NULL,
  `updated_by` int(10) NOT NULL,
  `lang_id` int(1) NOT NULL,
  `hits` int(10) NOT NULL,
  `popular_hits` int(10) NOT NULL,
  `popular_date` varchar(20) NOT NULL,
  `last_ip` varchar(50) NOT NULL,
  `ready_to_confirm` int(1) NOT NULL DEFAULT \'0\',
  `confirmed` int(1) NOT NULL,
  `active` int(1) NOT NULL,
  `duration_type` int(1) NOT NULL,
  `duration_start` int(50) NOT NULL,
  `duration_end` int(50) NOT NULL,
  `duration_notification` int(1) NOT NULL,
  `translation_status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lang_id` (`lang_id`),
  KEY `active` (`active`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_entries` (`id`, `order`, `form_id`, `create_date`, `update_date`, `validate_date`, `added_by`, `updated_by`, `lang_id`, `hits`, `popular_hits`, `popular_date`, `last_ip`, `ready_to_confirm`, `confirmed`, `active`, `duration_type`, `duration_start`, `duration_end`, `duration_notification`, `translation_status`) VALUES (\'400\', \'0\', \'23\', \'1348035629\', \'1348035629\', \'1348035629\', \'1\', \'1\', \'1\', \'0\', \'0\', \'1348035629\', \'\', \'1\', \'1\', \'1\', \'1\', \'1348005600\', \'1348005600\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_entries` (`id`, `order`, `form_id`, `create_date`, `update_date`, `validate_date`, `added_by`, `updated_by`, `lang_id`, `hits`, `popular_hits`, `popular_date`, `last_ip`, `ready_to_confirm`, `confirmed`, `active`, `duration_type`, `duration_start`, `duration_end`, `duration_notification`, `translation_status`) VALUES (\'401\', \'0\', \'23\', \'1348035655\', \'1348035655\', \'1348035655\', \'1\', \'1\', \'1\', \'0\', \'0\', \'1348035655\', \'\', \'1\', \'1\', \'1\', \'1\', \'1348005600\', \'1348005600\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_entries` (`id`, `order`, `form_id`, `create_date`, `update_date`, `validate_date`, `added_by`, `updated_by`, `lang_id`, `hits`, `popular_hits`, `popular_date`, `last_ip`, `ready_to_confirm`, `confirmed`, `active`, `duration_type`, `duration_start`, `duration_end`, `duration_notification`, `translation_status`) VALUES (\'402\', \'0\', \'23\', \'1348035686\', \'1348035686\', \'1348035686\', \'1\', \'1\', \'1\', \'0\', \'0\', \'1348035686\', \'\', \'1\', \'1\', \'1\', \'1\', \'1348005600\', \'1348005600\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_entries` (`id`, `order`, `form_id`, `create_date`, `update_date`, `validate_date`, `added_by`, `updated_by`, `lang_id`, `hits`, `popular_hits`, `popular_date`, `last_ip`, `ready_to_confirm`, `confirmed`, `active`, `duration_type`, `duration_start`, `duration_end`, `duration_notification`, `translation_status`) VALUES (\'403\', \'0\', \'24\', \'1348035826\', \'1348050141\', \'1348035826\', \'1\', \'1\', \'1\', \'1\', \'1\', \'1357858800\', \'\', \'1\', \'1\', \'1\', \'1\', \'1348005600\', \'1348005600\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_entries` (`id`, `order`, `form_id`, `create_date`, `update_date`, `validate_date`, `added_by`, `updated_by`, `lang_id`, `hits`, `popular_hits`, `popular_date`, `last_ip`, `ready_to_confirm`, `confirmed`, `active`, `duration_type`, `duration_start`, `duration_end`, `duration_notification`, `translation_status`) VALUES (\'405\', \'0\', \'24\', \'1348036303\', \'1348050158\', \'1348036303\', \'1\', \'1\', \'1\', \'1\', \'1\', \'1357858800\', \'\', \'1\', \'1\', \'1\', \'1\', \'1348005600\', \'1348005600\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_entry_associated_entry` (
  `source_entry_id` int(11) unsigned NOT NULL,
  `target_entry_id` int(11) unsigned NOT NULL,
  `ord` int(11) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`source_entry_id`,`target_entry_id`),
  KEY `ord` (`ord`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_forms` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `order` int(7) NOT NULL,
  `picture` mediumtext NOT NULL,
  `active` int(1) NOT NULL,
  `use_level` int(1) NOT NULL,
  `use_category` int(1) NOT NULL,
  `use_ready_to_confirm` int(1) NOT NULL,
  `use_associated_entries` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `entries_per_page` int(7) NOT NULL DEFAULT \'0\',
  `cmd` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_forms` (`id`, `order`, `picture`, `active`, `use_level`, `use_category`, `use_ready_to_confirm`, `use_associated_entries`, `entries_per_page`, `cmd`) VALUES (\'23\', \'99\', \'/images/MediaDir/uploads/team.jpg\', \'1\', \'1\', \'1\', \'0\', \'0\', \'0\', \'team\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_forms` (`id`, `order`, `picture`, `active`, `use_level`, `use_category`, `use_ready_to_confirm`, `use_associated_entries`, `entries_per_page`, `cmd`) VALUES (\'24\', \'99\', \'/images/MediaDir/uploads/referenzen.jpg\', \'1\', \'1\', \'1\', \'0\', \'0\', \'0\', \'referenzen\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_form_associated_form` (
  `source_form_id` int(11) unsigned NOT NULL,
  `target_form_id` int(11) unsigned NOT NULL,
  `ord` int(11) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`source_form_id`,`target_form_id`),
  KEY `ord` (`ord`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_form_names` (
  `lang_id` int(1) NOT NULL,
  `form_id` int(7) NOT NULL,
  `form_name` varchar(255) NOT NULL,
  `form_description` mediumtext NOT NULL,
  UNIQUE KEY `form` (`lang_id`,`form_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_form_names` (`lang_id`, `form_id`, `form_name`, `form_description`) VALUES (\'1\', \'23\', \'Team\', \'Die Mitarbeiter von MaxMuster AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_form_names` (`lang_id`, `form_id`, `form_name`, `form_description`) VALUES (\'1\', \'24\', \'Referenzen\', \'Die Referenzen von MaxMuster AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_form_names` (`lang_id`, `form_id`, `form_name`, `form_description`) VALUES (\'2\', \'23\', \'Team\', \'Die Mitarbeiter von MaxMuster AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_form_names` (`lang_id`, `form_id`, `form_name`, `form_description`) VALUES (\'2\', \'24\', \'Referenzen\', \'Die Referenzen von MaxMuster AG\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_inputfields` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `form` int(7) NOT NULL,
  `type` int(10) NOT NULL,
  `verification` int(10) NOT NULL,
  `search` int(10) NOT NULL,
  `required` int(10) NOT NULL,
  `order` int(10) NOT NULL,
  `show_in` int(10) NOT NULL,
  `context_type` enum(\'none\',\'title\',\'content\',\'address\',\'zip\',\'city\',\'country\',\'image\',\'slug\',\'keywords\') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'167\', \'23\', \'1\', \'1\', \'0\', \'0\', \'2\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'168\', \'23\', \'1\', \'1\', \'0\', \'0\', \'3\', \'1\', \'title\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'169\', \'23\', \'1\', \'5\', \'0\', \'0\', \'5\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'170\', \'23\', \'2\', \'1\', \'0\', \'0\', \'6\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'171\', \'23\', \'8\', \'1\', \'0\', \'0\', \'7\', \'1\', \'image\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'172\', \'23\', \'1\', \'1\', \'0\', \'0\', \'4\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'173\', \'24\', \'1\', \'1\', \'0\', \'0\', \'2\', \'1\', \'title\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'174\', \'24\', \'1\', \'1\', \'0\', \'0\', \'3\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'175\', \'24\', \'2\', \'1\', \'0\', \'0\', \'8\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'176\', \'24\', \'2\', \'1\', \'0\', \'0\', \'7\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'177\', \'24\', \'1\', \'1\', \'0\', \'0\', \'4\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'178\', \'24\', \'2\', \'1\', \'0\', \'0\', \'6\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'179\', \'24\', \'1\', \'3\', \'0\', \'0\', \'5\', \'1\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'180\', \'24\', \'8\', \'1\', \'0\', \'0\', \'9\', \'1\', \'image\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'181\', \'24\', \'1\', \'1\', \'0\', \'0\', \'10\', \'1\', \'slug\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfields` (`id`, `form`, `type`, `verification`, `search`, `required`, `order`, `show_in`, `context_type`) VALUES (\'182\', \'23\', \'1\', \'1\', \'0\', \'0\', \'8\', \'1\', \'slug\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_inputfield_names` (
  `lang_id` int(10) NOT NULL,
  `form_id` int(7) NOT NULL,
  `field_id` int(10) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_default_value` mediumtext NOT NULL,
  `field_info` mediumtext NOT NULL,
  UNIQUE KEY `field` (`lang_id`,`form_id`,`field_id`),
  KEY `field_id` (`field_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'167\', \'Vorname\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'168\', \'Name\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'169\', \'Bei MaxMuster AG seit\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'170\', \'Freizeitbeschäftigungen\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'171\', \'Bild\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'172\', \'Angestellt als\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'23\', \'182\', \'Slug\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'173\', \'Firma\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'174\', \'Projekt\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'175\', \'Meinung des Kunden\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'176\', \'Projektbeschrieb\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'177\', \'Realisiert im\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'178\', \'Leistungen\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'179\', \'Website\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'180\', \'Logo\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'1\', \'24\', \'181\', \'Slug\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'167\', \'Vorname\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'168\', \'Name\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'169\', \'Bei MaxMuster AG seit\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'170\', \'Freizeitbeschäftigungen\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'171\', \'Bild\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'172\', \'Angestellt als\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'23\', \'182\', \'Slug\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'173\', \'Firma\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'174\', \'Projekt\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'175\', \'Meinung\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'176\', \'Projektbeschrieb\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'177\', \'Realisiert im\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'178\', \'Leistungen\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'179\', \'Website\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'180\', \'Logo\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_names` (`lang_id`, `form_id`, `field_id`, `field_name`, `field_default_value`, `field_info`) VALUES (\'2\', \'24\', \'181\', \'Slug\', \'\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_inputfield_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `active` int(1) NOT NULL,
  `multi_lang` int(1) NOT NULL,
  `exp_search` int(7) NOT NULL,
  `dynamic` int(1) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'1\', \'text\', \'1\', \'1\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'2\', \'textarea\', \'1\', \'1\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'3\', \'dropdown\', \'1\', \'0\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'4\', \'radio\', \'1\', \'0\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'5\', \'checkbox\', \'1\', \'0\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'7\', \'file\', \'1\', \'1\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'8\', \'image\', \'1\', \'1\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'11\', \'classification\', \'1\', \'0\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'12\', \'link\', \'1\', \'1\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'13\', \'linkGroup\', \'1\', \'1\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'15\', \'googleMap\', \'1\', \'0\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'19\', \'wysiwyg\', \'1\', \'1\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'20\', \'mail\', \'1\', \'1\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'25\', \'country\', \'1\', \'0\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_types` (`id`, `name`, `active`, `multi_lang`, `exp_search`, `dynamic`, `comment`) VALUES (\'31\', \'range\', \'1\', \'0\', \'1\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_inputfield_verifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `regex` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_verifications` (`id`, `name`, `regex`) VALUES (\'1\', \'normal\', \'.*\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_verifications` (`id`, `name`, `regex`) VALUES (\'2\', \'e-mail\', \'^[a-zäàáâöôüûñéè0-9!\\\\#\\\\$\\\\%\\\\&\\\\\\\'\\\\*\\\\+\\\\/\\\\=\\\\?\\\\^_\\\\`\\\\{\\\\|\\\\}\\\\~-]+(?:\\\\.[a-zäàáâöôüûñéè0-9!\\\\#\\\\$\\\\%\\\\&\\\\\\\'\\\\*\\\\+\\\\/\\\\=\\\\?\\\\^_\\\\`\\\\{\\\\|\\\\}\\\\~-]+)*@(?:[a-zäàáâöôüûñéè0-9](?:[a-zäàáâöôüûñéè0-9-]*[a-zäàáâöôüûñéè0-9])?\\\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_verifications` (`id`, `name`, `regex`) VALUES (\'3\', \'url\', \'^(?:(?:ht|f)tps?\\\\:\\\\/\\\\/)?((([\\\\wÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè\\\\d-]{1,}\\\\.)+[a-z]{2,})|((?:(?:25[0-5]|2[0-4]\\\\d|[01]\\\\d\\\\d|\\\\d?\\\\d)(?:(\\\\.?\\\\d)\\\\.)) {4}))(?:[\\\\w\\\\d]+)?(\\\\/[\\\\w\\\\d\\\\-\\\\.\\\\?\\\\,\\\\\\\'\\\\/\\\\\\\\\\\\+\\\\&\\\\%\\\\$\\\\#\\\\=\\\\~]*)?$\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_verifications` (`id`, `name`, `regex`) VALUES (\'4\', \'letters\', \'^[A-Za-zÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè\\\\ ]*[A-Za-zÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè]+[A-Za-zÄÀÁÂÖÔÜÛÑÉÈäàáâöôüûñéè\\\\ ]*$\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_inputfield_verifications` (`id`, `name`, `regex`) VALUES (\'5\', \'numbers\', \'^[0-9]*$\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_levels` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `parent_id` int(7) NOT NULL,
  `order` int(7) NOT NULL,
  `show_sublevels` int(11) NOT NULL,
  `show_categories` int(1) NOT NULL,
  `show_entries` int(1) NOT NULL,
  `picture` mediumtext NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_levels` (`id`, `parent_id`, `order`, `show_sublevels`, `show_categories`, `show_entries`, `picture`, `active`) VALUES (\'30\', \'0\', \'0\', \'0\', \'1\', \'1\', \'/images/MediaDir/uploads/team.jpg\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_levels` (`id`, `parent_id`, `order`, `show_sublevels`, `show_categories`, `show_entries`, `picture`, `active`) VALUES (\'31\', \'0\', \'0\', \'0\', \'1\', \'1\', \'/images/MediaDir/uploads/referenzen.jpg\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_level_names` (
  `lang_id` int(1) NOT NULL,
  `level_id` int(7) NOT NULL,
  `level_name` varchar(255) NOT NULL,
  `level_description` mediumtext NOT NULL,
  `level_metadesc` varchar(160) NOT NULL DEFAULT \'\',
  UNIQUE KEY `level` (`lang_id`,`level_id`),
  KEY `lang_id` (`lang_id`),
  KEY `category_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_level_names` (`lang_id`, `level_id`, `level_name`, `level_description`, `level_metadesc`) VALUES (\'1\', \'30\', \'Team\', \'Die Mitarbeiter von MaxMusterAG\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_level_names` (`lang_id`, `level_id`, `level_name`, `level_description`, `level_metadesc`) VALUES (\'1\', \'31\', \'Referenzen\', \'Die Referenzen von MaxMuster AG\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_level_names` (`lang_id`, `level_id`, `level_name`, `level_description`, `level_metadesc`) VALUES (\'2\', \'30\', \'Team\', \'Die Mitarbeiter von MaxMusterAG\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_level_names` (`lang_id`, `level_id`, `level_name`, `level_description`, `level_metadesc`) VALUES (\'2\', \'31\', \'Referenzen\', \'Die Referenzen von MaxMuster AG\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_mails` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `recipients` mediumtext NOT NULL,
  `lang_id` int(1) NOT NULL,
  `action_id` int(1) NOT NULL,
  `is_default` int(1) NOT NULL,
  `active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'19\', \'[[URL]] - Eintrag erfolgreich eingetragen\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])\\r\\n\\r\\nIhr Eintrag mit dem Titel \\\"[[TITLE]]\\\" wurde auf [[URL]] erfolgreich eingetragen. \\r\\n\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr [[URL]]-Team\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'2\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'20\', \'[[URL]] - Ihr Eintrag wurde aufgeschaltet\', \'Guten Tag,\\r\\n\\r\\nIhr Eintrag \\\"[[TITLE]]\\\" wurde geprüft und ist ab sofort einsehbar.\\r\\n\\r\\nBenutzen Sie folgenden Link um direkt zu ihrem Eintrag zu gelangen:\\r\\n[[LINK]]\\r\\n\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr [[URL]]-Team\\r\\n\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'3\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'21\', \'[[URL]] - Eintrag wurde bewertet\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])\\r\\n\\r\\nZu Ihrem Eintrag mit dem Titel \\\"[[TITLE]]\\\" auf [[URL]] wurde eine Bewertung abgegeben. \\r\\n\\r\\nBenutzen Sie folgenden Link um direkt zu Ihrem Eintrag zu gelangen:\\r\\n[[LINK]]\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr [[URL]]-Team\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'4\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'22\', \'[[URL]] - Eintrag erfolgreich gelöscht\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])\\r\\n\\r\\nIhr Eintrag mit dem Titel \\\"[[TITLE]]\\\" auf [[URL]] wurde erfolgreich gelöscht. \\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr [[URL]]-Team\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'5\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'23\', \'[[URL]] - Eintrag erfolgreich bearbeitet\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])\\r\\n\\r\\nIhr Eintrag mit dem Titel \\\"[[TITLE]]\\\" auf [[URL]] wurde erfolgreich bearbeitet. \\r\\n\\r\\nBenutzen Sie folgenden Link um direkt zu Ihrem Eintrag zu gelangen:\\r\\n[[LINK]]\\r\\n\\r\\nFreundliche Grüsse\\r\\n[[URL]]-Team\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'6\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'24\', \'[[URL]] - Neuer Kommentar hinzugefügt\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])\\r\\n\\r\\nZu Ihrem Eintrag mit dem Titel \\\"[[TITLE]]\\\" auf [[URL]] wurde ein neuer Kommentar hinzugefügt. \\r\\n\\r\\nBenutzen Sie folgenden Link um direkt zu Ihrem Eintrag zu gelangen:\\r\\n[[LINK]]\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr [[URL]]-Team\\r\\n\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'8\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'32\', \'[[URL]] - Neuer Eintrag zur Prüfung freigegeben\', \'Guten Tag,\\r\\n\\r\\nAuf http://[[URL]] wurde ein neuer Eintrag mit dem Titel \\\"[[TITLE]]\\\" erfasst. Bitte prüfen Sie diesen und geben Sie ihn gegebenenfalls frei.\\r\\n\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mails` (`id`, `title`, `content`, `recipients`, `lang_id`, `action_id`, `is_default`, `active`) VALUES (\'33\', \'[[URL]] - Die Anzeigedauer eines Eintrages läuft ab\', \'Hallo Admin\\r\\n\\r\\nAuf [[URL]] läuft in Kürze die Anzeigedauer des Eintrages \\\"[[TITLE]]\\\" ab.\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr [[URL]]-Team\\r\\n\\r\\n-- \\r\\nDiese Nachricht wurde am [[DATE]] automatisch von Cloudrexx auf http://[[URL]] generiert.\', \'\', \'1\', \'9\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_mail_actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `default_recipient` enum(\'admin\',\'author\') NOT NULL,
  `need_auth` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'1\', \'newEntry\', \'admin\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'2\', \'entryAdded\', \'author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'3\', \'entryConfirmed\', \'author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'4\', \'entryVoted\', \'author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'5\', \'entryDeleted\', \'author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'6\', \'entryEdited\', \'author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'8\', \'newComment\', \'author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_mail_actions` (`id`, `name`, `default_recipient`, `need_auth`) VALUES (\'9\', \'notificationDisplayduration\', \'admin\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_masks` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `fields` mediumtext NOT NULL,
  `active` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_order_rel_forms_selectors` (
  `selector_id` int(7) NOT NULL,
  `form_id` int(7) NOT NULL,
  `selector_order` int(7) NOT NULL,
  `exp_search` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_order_rel_forms_selectors` (`selector_id`, `form_id`, `selector_order`, `exp_search`) VALUES (\'10\', \'24\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_order_rel_forms_selectors` (`selector_id`, `form_id`, `selector_order`, `exp_search`) VALUES (\'9\', \'24\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_order_rel_forms_selectors` (`selector_id`, `form_id`, `selector_order`, `exp_search`) VALUES (\'9\', \'23\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_order_rel_forms_selectors` (`selector_id`, `form_id`, `selector_order`, `exp_search`) VALUES (\'10\', \'23\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_rel_entry_categories` (
  `entry_id` int(10) NOT NULL,
  `category_id` int(10) NOT NULL,
  KEY `entry_id` (`entry_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_categories` (`entry_id`, `category_id`) VALUES (\'401\', \'177\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_categories` (`entry_id`, `category_id`) VALUES (\'402\', \'176\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_categories` (`entry_id`, `category_id`) VALUES (\'400\', \'178\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_categories` (`entry_id`, `category_id`) VALUES (\'403\', \'179\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_categories` (`entry_id`, `category_id`) VALUES (\'405\', \'179\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (
  `entry_id` int(7) NOT NULL,
  `lang_id` int(7) NOT NULL,
  `form_id` int(7) NOT NULL,
  `field_id` int(7) NOT NULL,
  `value` longtext NOT NULL,
  UNIQUE KEY `entry_id` (`entry_id`,`lang_id`,`form_id`,`field_id`),
  FULLTEXT KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'167\', \'Emily\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'168\', \'Miller\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'169\', \'1999\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'170\', \'Kochen, Ski fahren, meine Familie\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'171\', \'/images/MediaDir/uploads/emily.jpg\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'172\', \'Verkäuferin\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'1\', \'23\', \'182\', \'Miller\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'2\', \'23\', \'167\', \'Emily\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'2\', \'23\', \'168\', \'Miller\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'2\', \'23\', \'169\', \'1999\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'2\', \'23\', \'170\', \'Kochen, Ski fahren, meine Familie\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'2\', \'23\', \'172\', \'Verkäuferin\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'400\', \'2\', \'23\', \'182\', \'Miller\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'167\', \'George\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'168\', \'Smith\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'169\', \'2000\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'170\', \'Programmieren, Unihockey\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'171\', \'/images/MediaDir/uploads/george.jpg\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'172\', \'Entwickler\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'1\', \'23\', \'182\', \'Smith\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'2\', \'23\', \'167\', \'George\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'2\', \'23\', \'168\', \'Smith\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'2\', \'23\', \'169\', \'2000\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'2\', \'23\', \'170\', \'Programmieren, Unihockey\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'2\', \'23\', \'172\', \'Entwickler\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'401\', \'2\', \'23\', \'182\', \'Smith\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'167\', \'Jessica\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'168\', \'Parker\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'169\', \'2012\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'170\', \'Volleyball, Schwimmen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'171\', \'/images/MediaDir/uploads/jessica.jpg\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'172\', \'Sekretärin\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'1\', \'23\', \'182\', \'Parker\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'2\', \'23\', \'167\', \'Jessica\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'2\', \'23\', \'168\', \'Parker\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'2\', \'23\', \'169\', \'2012\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'2\', \'23\', \'170\', \'Volleyball, Schwimmen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'2\', \'23\', \'172\', \'Sekretärin\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'402\', \'2\', \'23\', \'182\', \'Parker\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'173\', \'Cloudrexx AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'174\', \'Visitenkarten und Fotos vom Team\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'175\', \'Die neuen Visitenkarten gestaltet und produziert von der MaxMuster AG haben unsere Erwartungen übertroffen. Wir sind froh, MaxMuster AG für dieses Projekt gewählt zu haben.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'176\', \'Visitenkarten gehören zum guten Ton jeder Unternehmung. Vom Team der Cloudrexx AG wurden professionelle Fotos gemacht, um den Visitenkarten eine persönliche Note zu verleihen.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'177\', \'August 2012\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'178\', \'Qualitativ hochwertiger Druck, Hochglanz Visitenkarten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'179\', \'https://www.cloudrexx.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'180\', \'/images/MediaDir/uploads/contrexx.png\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'1\', \'24\', \'181\', \'Contrexx\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'173\', \'Cloudrexx AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'174\', \'Visitenkarten und Fotos vom Team\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'175\', \'\\\"Die neuen Visitenkarten gestaltet und produziert von der MaxMuster AG haben unsere Erwartungen übertroffen. Wir sind froh, MaxMuster AG für dieses Projekt gewählt zu haben.\\\"\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'176\', \'Visitenkarten gehören zum guten Ton jeder Unternehmung. Vom Team der Cloudrexx AG wurden professionelle Fotos gemacht, um den Visitenkarten eine persönliche Note zu verleihen.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'177\', \'August 2012\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'178\', \'Qualitativ hochwertiger Druck\\r\\nHochglanz Visitenkarten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'179\', \'https://www.cloudrexx.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'403\', \'2\', \'24\', \'181\', \'Contrexx\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'173\', \'Cloudrexx AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'174\', \'Flyer\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'175\', \'Die neuen Flyer sind optisch ein Hingucker und auch die Druckqualität ist sehr gut. MaxMuster AG ist nur zu empfehlen.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'176\', \'Um Cloudrexx den Kunden näher zu bringen und vorzustellen, wurden Flyer gedruckt, welche die Vorzüge der neuen Cloudrexx Version vorstellen. \')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'177\', \'September 2012\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'178\', \'A5 und A4 Flyer\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'179\', \'https://www.cloudrexx.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'180\', \'/images/MediaDir/uploads/cloudrexx.png\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'1\', \'24\', \'181\', \'Cloudrexx\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'173\', \'Cloudrexx AG\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'174\', \'Flyer\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'175\', \'\\\"Die neuen Flyer sind optisch ein Hingucker und auch die Druckqualität ist sehr gut. MaxMuster AG ist nur zu empfehlen.\\\" - Inhaber\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'176\', \'Um Cloudrexx den Kunden näher zu bringen und vorzustellen, wurden Flyer gedruckt, welche die Vorzüge der neuen Cloudrexx Version vorstellen. \')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'177\', \'September 2012\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'178\', \'A5 und A4 Flyer\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'179\', \'https://www.cloudrexx.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_inputfields` (`entry_id`, `lang_id`, `form_id`, `field_id`, `value`) VALUES (\'405\', \'2\', \'24\', \'181\', \'Cloudrexx\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_rel_entry_levels` (
  `entry_id` int(10) NOT NULL,
  `level_id` int(10) NOT NULL,
  KEY `entry_id` (`entry_id`),
  KEY `category_id` (`level_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_levels` (`entry_id`, `level_id`) VALUES (\'402\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_levels` (`entry_id`, `level_id`) VALUES (\'405\', \'31\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_levels` (`entry_id`, `level_id`) VALUES (\'400\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_levels` (`entry_id`, `level_id`) VALUES (\'403\', \'31\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_rel_entry_levels` (`entry_id`, `level_id`) VALUES (\'401\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'1\', \'settingsShowCategoryDescription\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'2\', \'settingsShowCategoryImage\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'3\', \'settingsCategoryOrder\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'4\', \'settingsShowLevels\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'5\', \'settingsShowLevelDescription\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'6\', \'settingsShowLevelImage\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'7\', \'settingsLevelOrder\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'8\', \'settingsConfirmNewEntries\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'9\', \'categorySelectorOrder\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'10\', \'levelSelectorOrder\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'11\', \'settingsConfirmUpdatedEntries\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'12\', \'settingsCountEntries\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'13\', \'settingsThumbSize\', \'300\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'15\', \'settingsEncryptFilenames\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'16\', \'settingsAllowAddEntries\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'17\', \'settingsAllowDelEntries\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'18\', \'settingsAllowEditEntries\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'19\', \'settingsAddEntriesOnlyCommunity\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'20\', \'settingsLatestNumXML\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'21\', \'settingsLatestNumOverview\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'22\', \'settingsLatestNumBackend\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'23\', \'settingsLatestNumFrontend\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'24\', \'settingsPopularNumFrontend\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'25\', \'settingsPopularNumRestore\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'26\', \'settingsLatestNumHeadlines\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'27\', \'settingsGoogleMapStartposition\', \'46.749647513758326,7.6300048828125,8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'28\', \'settingsAllowVotes\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'29\', \'settingsVoteOnlyCommunity\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'30\', \'settingsAllowComments\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'31\', \'settingsCommentOnlyCommunity\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'32\', \'settingsGoogleMapAllowKml\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'33\', \'settingsShowEntriesInAllLang\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'34\', \'settingsPagingNumEntries\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'35\', \'settingsGoogleMapType\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'36\', \'settingsClassificationPoints\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'37\', \'settingsClassificationSearch\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'38\', \'settingsEntryDisplaydurationType\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'39\', \'settingsEntryDisplaydurationValue\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'40\', \'settingsEntryDisplaydurationValueType\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'41\', \'settingsEntryDisplaydurationNotification\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'42\', \'categorySelectorExpSearch\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'43\', \'levelSelectorExpSearch\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'44\', \'settingsTranslationStatus\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'45\', \'settingsReadyToConfirm\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'46\', \'settingsImageFilesize\', \'300\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'47\', \'settingsActiveLanguages\', \'2,1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'48\', \'settingsFrontendUseMultilang\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'49\', \'settingsIndividualEntryOrder\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'50\', \'showLatestEntriesInOverview\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'51\', \'showLatestEntriesInWebdesignTmpl\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'52\', \'legacyBehavior\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings` (`id`, `name`, `value`) VALUES (\'53\', \'usePrettyUrls\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_settings_num_categories` (
  `group_id` int(1) NOT NULL,
  `num_categories` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_categories` (`group_id`, `num_categories`) VALUES (\'3\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_categories` (`group_id`, `num_categories`) VALUES (\'4\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_categories` (`group_id`, `num_categories`) VALUES (\'5\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_settings_num_entries` (
  `group_id` int(1) NOT NULL,
  `num_entries` varchar(10) NOT NULL DEFAULT \'n\'
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_entries` (`group_id`, `num_entries`) VALUES (\'3\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_entries` (`group_id`, `num_entries`) VALUES (\'4\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_entries` (`group_id`, `num_entries`) VALUES (\'5\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_entries` (`group_id`, `num_entries`) VALUES (\'6\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_entries` (`group_id`, `num_entries`) VALUES (\'7\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_settings_num_levels` (
  `group_id` int(1) NOT NULL,
  `num_levels` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_levels` (`group_id`, `num_levels`) VALUES (\'3\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_levels` (`group_id`, `num_levels`) VALUES (\'4\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_num_levels` (`group_id`, `num_levels`) VALUES (\'5\', \'n\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (
  `group_id` int(7) NOT NULL,
  `form_id` int(1) NOT NULL,
  `status_group` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'7\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'6\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'5\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'4\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'3\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'7\', \'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'6\', \'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'5\', \'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'4\', \'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_mediadir_settings_perm_group_forms` (`group_id`, `form_id`, `status_group`) VALUES (\'3\', \'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_mediadir_votes` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `entry_id` int(7) NOT NULL,
  `added_by` varchar(255) NOT NULL,
  `date` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `vote` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
