<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _recommendUpdate()
{
    global $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        /********************************
         * EXTENSION:   Captcha         *
         * ADDED:       Contrexx v3.0.0 *
         ********************************/
        try {
            // migrate content page to version 3.0.1
            $search = array(
            '/(.*)/ms',
            );
            $callback = function($matches) {
                $content = $matches[1];
                if (empty($content)) {
                    return $content;
                }

                if (!preg_match('/<!--\s+BEGIN\s+recommend_captcha\s+-->.*<!--\s+END\s+recommend_captcha\s+-->/ms', $content)) {
                    // migrate captcha stuff
                    $content = preg_replace('/<img[^>]+\{RECOM_CAPTCHA_URL\}.*\{RECOM_CAPTCHA_OFFSET\}[^>]+>/ms', '{RECOM_CAPTCHA_CODE}', $content);

                    // migration for very old versions
                    $content = preg_replace('/(.*)(<tr[^>]*>.*?<td[^>]*>.*?\{RECOM_CAPTCHA_CODE\}.*?<\/td>.*?<\/tr>)/ms', '$1<!-- BEGIN recommend_captcha -->$2<!-- END recommend_captcha -->', $content, -1, $count);

                    // migration for newer versions
                    if (!$count) {
                        $content = preg_replace('/(.*)(<p[^>]*>.*?\{RECOM_CAPTCHA_.*?\}.*?<\/p>)/ms', '$1<!-- BEGIN recommend_captcha -->$2<!-- END recommend_captcha -->', $content);
                    }
                    $content = preg_replace('/(.*)(<p[^>]*><label.*<\/label>)(.*?\{RECOM_CAPTCHA_.*?\}.*?)(<\/p>)/ms', '$1$2{RECOM_CAPTCHA_CODE}$4', $content);
                }

                return $content;
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'recommend'), $search, $callback, array('content'), '3.0.1');
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        try {
            foreach ($pathsToMigrate as $oldPath => $newPath) {
                \Cx\Lib\UpdateUtil::migratePath(
                    '`' . DBPREFIX . 'module_recommend`',
                    '`value`',
                    $oldPath,
                    $newPath
                );
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_recommend',
                array(
                    'id'         => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'value'      => array('type' => 'text', 'after' => 'name'),
                    'lang_id'    => array('type' => 'INT(11)', 'notnull' => true, 'default' => '1', 'after' => 'value')
                ),
                array(
                    'data'       => array('fields' => array('name','lang_id'))
                )
            );
        } catch (Cx\Lib\UpdateException $e) {
            return Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _recommendInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_recommend` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  `lang_id` int(11) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'1\', \'body\', \'<SALUTATION> <RECEIVER_NAME>\\r\\n\\r\\nFolgende Seite wurde ihnen von <SENDER_NAME> (<SENDER_MAIL>) empfohlen:\\r\\n\\r\\n<URL>\\r\\n\\r\\nAnmerkung von <SENDER_NAME>:\\r\\n\\r\\n<COMMENT>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'2\', \'subject\', \'Seitenempfehlung von <SENDER_NAME>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'5\', \'salutation_female\', \'Liebe\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'6\', \'salutation_male\', \'Lieber\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'7\', \'body\', \'<SALUTATION> <RECEIVER_NAME>\\r\\n\\r\\nFolgende Seite wurde ihnen von <SENDER_NAME> (<SENDER_MAIL>) empfohlen:\\r\\n\\r\\n<URL>\\r\\n\\r\\nAnmerkung von <SENDER_NAME>:\\r\\n\\r\\n<COMMENT>\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'8\', \'subject\', \'Seitenempfehlung von <SENDER_NAME>\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'9\', \'salutation_female\', \'Liebe\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_recommend` (`id`, `name`, `value`, `lang_id`) VALUES (\'10\', \'salutation_male\', \'Lieber\', \'2\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
