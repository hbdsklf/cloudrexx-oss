<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _egovUpdate()
{
    global $objDatabase, $_ARRAYLANG, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        // Check required tables..
        $arrTables = $objDatabase->MetaTables('TABLES');
        if (!$arrTables) {
            setUpdateMsg($_ARRAYLANG['TXT_UNABLE_DETERMINE_DATABASE_STRUCTURE']);
            return false;
        }
        // Create new configuration table if missing
        if (!in_array(DBPREFIX."module_egov_configuration", $arrTables)) {
            $query = "
                CREATE TABLE ".DBPREFIX."module_egov_configuration (
                  `name` varchar(255) NOT NULL default '',
                  `value` text NOT NULL,
                  UNIQUE KEY `name` (`name`)
                ) ENGINE=InnoDB;
            ";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        $hasLegacyTable = false;
        try {
            if (\Cx\Lib\UpdateUtil::table_exist(DBPREFIX."module_egov_settings")) {
                $hasLegacyTable = true;
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // Copy original values
        $arrField = array(
            'set_sender_name',
            'set_sender_email',
            'set_recipient_email',
            'set_state_subject',
            'set_state_email',
            'set_calendar_color_1',
            'set_calendar_color_2',
            'set_calendar_color_3',
            'set_calendar_legende_1',
            'set_calendar_legende_2',
            'set_calendar_legende_3',
            'set_calendar_background',
            'set_calendar_border',
            'set_calendar_date_label',
            'set_calendar_date_desc',
            'set_orderentry_subject',
            'set_orderentry_email',
            'set_orderentry_name',
            'set_orderentry_sender',
            'set_orderentry_recipient',
            'set_paypal_email',
            'set_paypal_currency',
            'set_paypal_ipn',
        );
        foreach ($arrField as $fieldname) {
            $query = "
                SELECT 1 FROM ".DBPREFIX."module_egov_configuration
                WHERE name='$fieldname'
            ";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
            if ($objResult->RecordCount() == 1) {
                // The value is already there
                continue;
            }

            // Copy the original value
            if ($hasLegacyTable) {
                $query = "
                    INSERT INTO ".DBPREFIX."module_egov_configuration (name, value)
                    SELECT '$fieldname', `$fieldname`
                      FROM ".DBPREFIX."module_egov_settings
                ";
            } else {
                $query = "
                    INSERT INTO ".DBPREFIX."module_egov_configuration (name, value) VALUES ('$fieldname', '')
                ";
            }
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        try {
            if (\Cx\Lib\UpdateUtil::table_exist(DBPREFIX."module_egov_settings")) {
                \Cx\Lib\UpdateUtil::drop_table(DBPREFIX."module_egov_settings");
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // Add new settings for Yellowpay
        $arrField = array(
            'yellowpay_accepted_payment_methods' => '',
            'yellowpay_authorization' => 'immediate',
            'yellowpay_uid' => 'demo',
            'yellowpay_hashseed' => 'demo',
            'yellowpay_shopid' => '',
            'yellowpay_use_testserver' => '1',
        );

        foreach ($arrField as $fieldname => $defaultvalue) {
            $query = "
                SELECT 1 FROM ".DBPREFIX."module_egov_configuration
                WHERE name='$fieldname'
            ";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
            if ($objResult->RecordCount() == 1) {
                // The value is already there
                continue;
            }

            // Add the new setting with its default value
            $query = "
                INSERT INTO ".DBPREFIX."module_egov_configuration (
                    name, value
                ) VALUES (
                    '$fieldname', '$defaultvalue'
                )
            ";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        // products table
        if (!in_array(DBPREFIX."module_egov_products", $arrTables)) {
            $query = "
                CREATE TABLE `".DBPREFIX."module_egov_products` (
                  `product_id` int(11) NOT NULL auto_increment,
                  `product_autostatus` tinyint(1) NOT NULL default '0',
                  `product_name` varchar(255) NOT NULL default '',
                  `product_desc` text NOT NULL,
                  `product_price` decimal(11,2) NOT NULL default '0.00',
                  `product_per_day` enum('yes','no') NOT NULL default 'no',
                  `product_quantity` tinyint(2) NOT NULL default '0',
                  `product_target_email` varchar(255) NOT NULL default '',
                  `product_target_url` varchar(255) NOT NULL default '',
                  `product_message` text NOT NULL,
                  `product_status` tinyint(1) NOT NULL default '1',
                  `product_electro` tinyint(1) NOT NULL default '0',
                  `product_file` varchar(255) NOT NULL default '',
                  `product_sender_name` varchar(255) NOT NULL default '',
                  `product_sender_email` varchar(255) NOT NULL default '',
                  `product_target_subject` varchar(255) NOT NULL,
                  `product_target_body` text NOT NULL,
                  `product_paypal` tinyint(1) NOT NULL default '0',
                  `product_paypal_sandbox` varchar(255) NOT NULL default '',
                  `product_paypal_currency` varchar(255) NOT NULL default '',
                  `product_orderby` int(11) NOT NULL default '0',
                  PRIMARY KEY  (`product_id`)
                ) TYPE=InnoDB;
            ";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        // Add Yellowpay field to Product table
        $arrProductColumns = $objDatabase->MetaColumns(DBPREFIX.'module_egov_products');
        if ($arrProductColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_egov_products'));
            return false;
        }
        if (!isset($arrProductColumns['YELLOWPAY'])) {
            $query = "
                ALTER TABLE ".DBPREFIX."module_egov_products
                ADD `yellowpay` TINYINT(1) unsigned NOT NULL default '0'
            ";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        // Add quantity limit field to Product table
        if (!isset($arrProductColumns['PRODUCT_QUANTITY_LIMIT'])) {
            $query = "
                ALTER TABLE ".DBPREFIX."module_egov_products
                ADD `product_quantity_limit` TINYINT(2) unsigned NOT NULL default '1'
                AFTER `product_quantity`;
            ";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        // Add alternative payment method name field to Product table
        if (!isset($arrProductColumns['ALTERNATIVE_NAMES'])) {
            $query = "
                ALTER TABLE ".DBPREFIX."module_egov_products
                ADD `alternative_names` TEXT NOT NULL;
            ";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        /********************************
         * EXTENSION:   Timezone        *
         * ADDED:       Contrexx v3.0.0 *
         ********************************/
        try {
            \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_egov_orders` CHANGE `order_date` `order_date` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // Add reservation date to order table
        if (!isset($arrORDERColumns['ORDER_RESERVATION_DATE'])) {
            try {
                \Cx\Lib\UpdateUtil::sql("ALTER TABLE ".DBPREFIX."module_egov_orders ADD `order_reservation_date` DATE NOT NULL DEFAULT '0000-00-00' AFTER `order_values`");
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }

        // set order_reservation_date
        try {
            $dateLabel = '';
            $objResult = \Cx\Lib\UpdateUtil::sql('SELECT value FROM '.DBPREFIX.'module_egov_configuration WHERE name = "set_calendar_date_label" LIMIT 1');
            if ($objResult->RecordCount()) {
                $dateLabel = $objResult->fields['value'];
            }
            if (!empty($dateLabel)) {
                $objResult = \Cx\Lib\UpdateUtil::sql('SELECT order_id, order_values FROM '.DBPREFIX.'module_egov_orders WHERE (order_reservation_date IS NULL OR order_reservation_date = "0000-00-00") AND order_values REGEXP "'.$dateLabel.'::[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+;;"');
                while (!$objResult->EOF) {
                    $reservationDate = null;
                    if (preg_match('/'.preg_quote($dateLabel).'::(\d+)\.(\d+)\.(\d+);;/', $objResult->fields['order_values'], $matches)) {
                        $day = $matches[1];
                        $month = $matches[2];
                        $year = $matches[3];
                        $reservationDate = "$year-$month-$day";
                        \Cx\Lib\UpdateUtil::sql('UPDATE '.DBPREFIX.'module_egov_orders SET order_reservation_date="'.$reservationDate.'" WHERE order_id='.$objResult->fields['order_id']);
                    }
                    $objResult->MoveNext();
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::sql('UPDATE `' . DBPREFIX . 'module_egov_orders` SET `order_ip` = MD5(`order_ip`) WHERE CHAR_LENGTH(`order_ip`) < 30 AND `order_ip` != \'\'');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'product_desc'          => 'module_egov_products',
            'product_message'       => 'module_egov_products',
            'product_target_url'    => 'module_egov_products',
            'product_target_body'   => 'module_egov_products',
            'value'                 => 'module_egov_configuration',
        );
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    return true;
}
function _egovInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_egov_configuration` (
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_background\', \'#FFFFFFF\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_border\', \'#C9C9C9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_color_1\', \'#D5FFDA\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_color_2\', \'#F7FFB4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_color_3\', \'#FFAEAE\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_date_desc\', \'(Das Datum wird durch das Anklicken im Kalender übernommen.)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_date_label\', \'Reservieren für das ausgewählte Datum\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_legende_1\', \'Freie Tage\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_legende_2\', \'Teilweise reserviert\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_calendar_legende_3\', \'Reserviert\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_orderentry_email\', \'Diese Daten wurden eingegeben:\\r\\n\\r\\n[[ORDER_VALUE]]\\r\\n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_orderentry_name\', \'Cloudrexx Demo Webseite\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_orderentry_recipient\', \'info@example.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_orderentry_sender\', \'info@example.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_orderentry_subject\', \'Bestellung/Anfrage für [[PRODUCT_NAME]] eingegangen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_paypal_currency\', \'CHF\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_paypal_email\', \'demo\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_paypal_ipn\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_recipient_email\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_sender_email\', \'info@example.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_sender_name\', \'Cloudrexx Demo\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_state_email\', \'Guten Tag\\r\\n\\r\\nHerzlichen Dank für Ihren Besuch bei der Cloudrexx Demo Webseite.\\r\\nIhre Bestellung/Anfrage wurde bearbeitet. Falls es sich um ein Download Produkt handelt, finden Sie ihre Bestellung im Anhang.\\r\\n\\r\\nIhre Angaben:\\r\\n[[ORDER_VALUE]]\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr Online-Team\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_configuration` (`name`, `value`) VALUES (\'set_state_subject\', \'Bestellung/Anfrage: [[PRODUCT_NAME]]\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_egov_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  `order_ip` varchar(255) NOT NULL DEFAULT \'\',
  `order_product` int(11) NOT NULL DEFAULT \'0\',
  `order_values` text NOT NULL,
  `order_reservation_date` date NOT NULL DEFAULT \'0000-00-00\',
  `order_state` tinyint(4) NOT NULL DEFAULT \'0\',
  `order_quant` tinyint(4) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`order_id`),
  KEY `order_product` (`order_product`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_egov_products` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_autostatus` tinyint(1) NOT NULL DEFAULT \'0\',
  `product_name` varchar(255) NOT NULL DEFAULT \'\',
  `product_desc` text NOT NULL,
  `product_price` decimal(11,2) NOT NULL DEFAULT \'0.00\',
  `product_per_day` enum(\'yes\',\'no\') NOT NULL DEFAULT \'no\',
  `product_quantity` tinyint(2) NOT NULL DEFAULT \'0\',
  `product_quantity_limit` tinyint(2) unsigned NOT NULL DEFAULT \'1\',
  `product_target_email` varchar(255) NOT NULL DEFAULT \'\',
  `product_target_url` varchar(255) NOT NULL DEFAULT \'\',
  `product_message` text NOT NULL,
  `product_status` tinyint(1) NOT NULL DEFAULT \'1\',
  `product_electro` tinyint(1) NOT NULL DEFAULT \'0\',
  `product_file` varchar(255) NOT NULL DEFAULT \'\',
  `product_sender_name` varchar(255) NOT NULL DEFAULT \'\',
  `product_sender_email` varchar(255) NOT NULL DEFAULT \'\',
  `product_target_subject` varchar(255) NOT NULL,
  `product_target_body` text NOT NULL,
  `product_paypal` tinyint(1) NOT NULL DEFAULT \'0\',
  `product_paypal_sandbox` varchar(255) NOT NULL DEFAULT \'\',
  `product_paypal_currency` varchar(255) NOT NULL DEFAULT \'\',
  `product_orderby` int(11) NOT NULL DEFAULT \'0\',
  `yellowpay` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `alternative_names` text NOT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_products` (`product_id`, `product_autostatus`, `product_name`, `product_desc`, `product_price`, `product_per_day`, `product_quantity`, `product_quantity_limit`, `product_target_email`, `product_target_url`, `product_message`, `product_status`, `product_electro`, `product_file`, `product_sender_name`, `product_sender_email`, `product_target_subject`, `product_target_body`, `product_paypal`, `product_paypal_sandbox`, `product_paypal_currency`, `product_orderby`, `yellowpay`, `alternative_names`) VALUES (\'3\', \'1\', \'Produkteschulung Cloudrexx (limitierte Plätze)\', \'Produkteschulung in Thun (Schweiz).<br />\', \'299.00\', \'yes\', \'20\', \'1\', \'info@example.com\', \'\', \'<p>Besten Dank f&uuml;r Ihre Anmeldung!<br />\\r\\nSie erhalten in K&uuml;rze eine E-Mail Nachricht mit den wichtigsten Informationen.</p>\', \'1\', \'0\', \'\', \'Cloudrexx Demo\', \'info@example.com\', \'Bestellung/Anfrage: [[PRODUCT_NAME]]\', \'Guten Tag\\r\\n\\r\\nHerzlichen Dank für Ihren Besuch bei der Cloudrexx Demo Webseite.\\r\\nIhre Bestellung/Anfrage wurde bearbeitet. Falls es sich um ein Download Produkt handelt, finden Sie ihre Bestellung im Anhang.\\r\\n\\r\\nIhre Angaben:\\r\\n[[ORDER_VALUE]]\\r\\n\\r\\nFreundliche Grüsse\\r\\nIhr Online-Team\', \'0\', \'demo\', \'CHF\', \'0\', \'0\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_egov_product_calendar` (
  `calendar_id` int(11) NOT NULL AUTO_INCREMENT,
  `calendar_product` int(11) NOT NULL DEFAULT \'0\',
  `calendar_order` int(11) NOT NULL DEFAULT \'0\',
  `calendar_day` int(2) NOT NULL DEFAULT \'0\',
  `calendar_month` int(2) unsigned zerofill NOT NULL DEFAULT \'00\',
  `calendar_year` int(4) NOT NULL DEFAULT \'0\',
  `calendar_act` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`calendar_id`),
  KEY `calendar_product` (`calendar_product`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_egov_product_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `type` enum(\'text\',\'label\',\'checkbox\',\'checkboxGroup\',\'file\',\'hidden\',\'password\',\'radio\',\'select\',\'textarea\') NOT NULL DEFAULT \'text\',
  `attributes` text NOT NULL,
  `is_required` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `check_type` int(3) NOT NULL DEFAULT \'1\',
  `order_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `product` (`product`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'7\', \'3\', \'Anrede\', \'select\', \'Bitte w&auml;hlen Sie,Frau,Herr,Herr und Frau\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'8\', \'3\', \'Vorname\', \'text\', \'\', \'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'9\', \'3\', \'Nachname\', \'text\', \'\', \'1\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'10\', \'3\', \'Firma\', \'text\', \'\', \'0\', \'1\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'11\', \'3\', \'Zusatz\', \'text\', \'\', \'0\', \'1\', \'4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'12\', \'3\', \'Strasse\', \'text\', \'\', \'0\', \'1\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'13\', \'3\', \'Nummer\', \'text\', \'\', \'0\', \'1\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'14\', \'3\', \'Postleitzahl\', \'text\', \'\', \'0\', \'1\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'15\', \'3\', \'Ort\', \'text\', \'\', \'0\', \'1\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'16\', \'3\', \'Land\', \'text\', \'\', \'0\', \'1\', \'9\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'17\', \'3\', \'E-Mail\', \'text\', \'\', \'1\', \'1\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_egov_product_fields` (`id`, `product`, `name`, `type`, `attributes`, `is_required`, `check_type`, `order_id`) VALUES (\'18\', \'3\', \'Bemerkungen\', \'textarea\', \'\', \'0\', \'1\', \'11\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
