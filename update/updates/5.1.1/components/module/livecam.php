<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _livecamUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_livecam',
                array(
                    'id'                 => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '1', 'primary' => true),
                    'currentImagePath'   => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '/images/Livecam/cam1/current.jpg'),
                    'archivePath'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '/images/Livecam/cam1/archive/'),
                    'thumbnailPath'      => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '/images/Livecam/cam1/thumbs/'),
                    'maxImageWidth'      => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '400'),
                    'thumbMaxSize'       => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '200'),
                    'shadowboxActivate'  => array('type' => 'SET(\'1\',\'0\')', 'notnull' => true, 'default' => '1', 'renamefrom' => 'lightboxActivate'),
                    'showFrom'           => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0'),
                    'showTill'           => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_livecam_settings',
                array(
                    'setid'      => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'setname'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'setvalue'   => array('type' => 'TEXT')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        $query = "SELECT 1 FROM `".DBPREFIX."module_livecam_settings` WHERE `setname` = 'amount_of_cams'";
        $objResult = $objDatabase->SelectLimit($query, 1);
        if ($objResult !== false) {
            if ($objResult->RecordCount() == 0) {
                $query = "INSERT INTO `".DBPREFIX."module_livecam_settings` (`setname`, `setvalue`) VALUES ('amount_of_cams', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }
    }




    /************************************************
    * BUGFIX:   Migrate settings                    *
    * ADDED:    2.1.2                               *
    ************************************************/
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '2.0.0')) {
        $arrFormerSettings = array(
            'currentImageUrl'   => '',
            'archivePath'       => '',
            'thumbnailPath'     => ''
        );

        $query = "SELECT 1 FROM `".DBPREFIX."module_livecam` WHERE `id` = 1";
        $objResult = $objDatabase->SelectLimit($query, 1);
        if ($objResult !== false) {
            if ($objResult->RecordCount() == 0) {
                $query = "SELECT `setname`, `setvalue` FROM `".DBPREFIX."module_livecam_settings` WHERE `setname` IN ('".implode("','", array_keys($arrFormerSettings))."')";
                $objResult = $objDatabase->Execute($query);
                if ($objResult !== false) {
                    while (!$objResult->EOF) {
                        $arrFormerSettings[$objResult->fields['setname']] = $objResult->fields['setvalue'];
                        $objResult->MoveNext();
                    }

                    $query = "INSERT INTO `".DBPREFIX."module_livecam` (`id`, `currentImagePath`, `archivePath`, `thumbnailPath`, `maxImageWidth`, `thumbMaxSize`, `shadowboxActivate`) VALUES
                            ('1', '".addslashes($arrFormerSettings['currentImageUrl'])."', '".addslashes($arrFormerSettings['archivePath'])."', '".addslashes($arrFormerSettings['thumbnailPath'])."', '400', '120', '0')";
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                } else {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        foreach (array_keys($arrFormerSettings) as $setting) {
            $query = "DELETE FROM `".DBPREFIX."module_livecam_settings` WHERE `setname` = '".$setting."'";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        $defaultFrom = mktime(0, 0);
        $defaultTill = mktime(23, 59);
        //set new default settings
        $query = "UPDATE `".DBPREFIX."module_livecam` SET `showFrom`=$defaultFrom, `showTill`=$defaultTill WHERE `showFrom` = '0'";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        /************************************************
        * BUGFIX:   Update content page                 *
        * ADDED:    2.1.3                               *
        ************************************************/
        // both spaces in the search and replace pattern are required in that case
        try {
            \Cx\Lib\UpdateUtil::migrateContentPage('livecam', null, ' {LIVECAM_IMAGE_SHADOWBOX}', ' rel="{LIVECAM_IMAGE_SHADOWBOX}"', '2.1.3');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        // Try migrating the directory
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory(ASCMS_DOCUMENT_ROOT . '/webcam',
                ASCMS_DOCUMENT_ROOT . '/images/Livecam');
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            return false;
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'currentImagePath', 'archivePath', 'thumbnailPath',
        );
        $query = 'SELECT `currentImagePath` FROM `' . DBPREFIX . 'module_livecam`';
        // check if there are livecams from third party websites
        $currentImagePaths = \Cx\Lib\UpdateUtil::sql($query);
        while (!$currentImagePaths->EOF) {
            // if there is a livecam from a third party website, unset currentImagePath
            // from the $attributes array so we do not accidentally break this livecam
            if (preg_match('#^(?:https?:)?//#i', $currentImagePaths->fields['currentImagePath'])) {
                unset($attributes[0]);
                break;
            }
            $currentImagePaths->MoveNext();
        }
        try {
            foreach ($attributes as $attribute) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . 'module_livecam`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            return false;
        }
    }

    return true;
}
function _livecamInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_livecam` (
  `id` int(10) unsigned NOT NULL DEFAULT \'1\',
  `currentImagePath` varchar(255) NOT NULL DEFAULT \'/images/Livecam/cam1/current.jpg\',
  `archivePath` varchar(255) NOT NULL DEFAULT \'/images/Livecam/cam1/archive/\',
  `thumbnailPath` varchar(255) NOT NULL DEFAULT \'/images/Livecam/cam1/thumbs/\',
  `maxImageWidth` int(10) unsigned NOT NULL DEFAULT \'400\',
  `thumbMaxSize` int(10) unsigned NOT NULL DEFAULT \'200\',
  `shadowboxActivate` set(\'1\',\'0\') NOT NULL DEFAULT \'1\',
  `showFrom` int(14) NOT NULL DEFAULT \'0\',
  `showTill` int(14) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_livecam` (`id`, `currentImagePath`, `archivePath`, `thumbnailPath`, `maxImageWidth`, `thumbMaxSize`, `shadowboxActivate`, `showFrom`, `showTill`) VALUES (\'1\', \'http://heimenschwand.ch/webcam/current.jpg\', \'/images/Livecam/cam1/archive\', \'/images/Livecam/cam1/thumbs\', \'400\', \'120\', \'0\', \'1411596037\', \'1411682377\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_livecam_settings` (
  `setid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `setname` varchar(255) NOT NULL DEFAULT \'\',
  `setvalue` text NOT NULL,
  PRIMARY KEY (`setid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_livecam_settings` (`setid`, `setname`, `setvalue`) VALUES (\'1\', \'amount_of_cams\', \'1\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
