<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _crmUpdate() {
    global $objUpdate, $_CONFIG, $_ARRAYLANG;

	try {
        if (   \Cx\Lib\UpdateUtil::table_exist(DBPREFIX . 'module_crm_contacts')
            && $objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.1')
        ) {
            \Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'module_crm_contacts` CONVERT TO CHARACTER SET `utf8`');
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_contacts',
                array(
                    'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'customer_id'            => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'id'),
                    'customer_type'          => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'customer_id'),
                    'customer_name'          => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'customer_type'),
                    'customer_website'       => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'customer_name'),
                    'customer_addedby'       => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'customer_website'),
                    'company_size'           => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'customer_addedby'),
                    'customer_currency'      => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'company_size'),
                    'contact_amount'         => array('type' => 'VARCHAR(256)', 'notnull' => false, 'after' => 'customer_currency'),
                    'contact_familyname'     => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'contact_amount'),
                    'contact_title'          => array('type' => 'VARCHAR(256)', 'notnull' => false, 'after' => 'contact_familyname'),
                    'contact_role'           => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'contact_title'),
                    'contact_customer'       => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'contact_role'),
                    'contact_language'       => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'contact_customer'),
                    'gender'                 => array('type' => 'TINYINT(2)', 'after' => 'contact_language'),
                    'salutation'             => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'gender'),
                    'notes'                  => array('type' => 'text', 'binary' => true, 'notnull' => false, 'after' => 'salutation'),
                    'industry_type'          => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'notes'),
                    'contact_type'           => array('type' => 'TINYINT(2)', 'notnull' => false, 'after' => 'industry_type'),
                    'user_account'           => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'contact_type'),
                    'datasource'             => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'user_account'),
                    'profile_picture'        => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => true, 'default' => '', 'after' => 'datasource'),
                    'status'                 => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'profile_picture'),
                    'added_date'             => array('type' => 'date', 'after' => 'status'),
                    'updated_date'           => array('type' => 'timestamp', 'notnull' => true, 'after' => 'added_date'),
                    'email_delivery'         => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'updated_date')
                ),
                array(
                    'contact_customer'       => array('fields' => array('contact_customer')),
                    'customer_id'            => array('fields' => array('customer_id' => 255)),
                    'customer_name'          => array('fields' => array('customer_name' => 255)),
                    'contact_familyname'     => array('fields' => array('contact_familyname' => 255)),
                    'contact_role'           => array('fields' => array('contact_role' => 255)),
                    'customer_id_2'          => array('fields' => array('customer_id','customer_name','contact_familyname','contact_role','notes'), 'type' => 'FULLTEXT'),
                    'customer_name_fulltext' => array('fields' => array('customer_name'), 'type' => 'FULLTEXT'),
                    'contact_familyname_fulltext'=> array('fields' => array('contact_familyname'), 'type' => 'FULLTEXT'),
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_currency',
                array(
                    'id'                     => array('type' => 'INT(10)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'                   => array('type' => 'VARCHAR(400)', 'after' => 'id'),
                    'active'                 => array('type' => 'INT(1)', 'notnull' => true, 'default' => '1', 'after' => 'name'),
                    'pos'                    => array('type' => 'INT(5)', 'notnull' => true, 'default' => '0', 'after' => 'active'),
                    'default_currency'       => array('type' => 'TINYINT(1)', 'after' => 'pos')
                ),
                array(
                    'name'                   => array('fields' => array('name' => 255)),
                    'fulltext'               => array('fields' => array('name'), 'type' => 'FULLTEXT'),
                )
            );
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.2.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_comment',
                array(
                    'id'                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'customer_id'        => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'id'),
                    'notes_type_id'      => array('type' => 'INT(1)', 'after' => 'customer_id'),
                    'user_id'            => array('type' => 'INT(11)', 'after' => 'notes_type_id'),
                    'date'               => array('type' => 'date', 'after' => 'user_id'),
                    'comment'            => array('type' => 'text', 'notnull' => false, 'after' => 'date'),
                    'added_date'         => array('type' => 'datetime', 'notnull' => false, 'after' => 'comment'),
                    'updated_by'         => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'added_date'),
                    'updated_on'         => array('type' => 'datetime', 'notnull' => false, 'after' => 'updated_by')
                ),
                array(
                    'customer_id'        => array('fields' => array('customer_id')),
                    'comment'            => array('fields' => array('comment'), 'type' => 'FULLTEXT')
                )
            );
        }
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_contact_address',
                array(
                    'id'                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'address'            => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'city'               => array('type' => 'VARCHAR(256)', 'after' => 'address'),
                    'state'              => array('type' => 'VARCHAR(256)', 'after' => 'city'),
                    'zip'                => array('type' => 'VARCHAR(256)', 'after' => 'state'),
                    'country'            => array('type' => 'VARCHAR(256)', 'after' => 'zip'),
                    'Address_Type'       => array('type' => 'TINYINT(4)', 'after' => 'country'),
                    'is_primary'         => array('type' => 'ENUM(\'0\',\'1\')', 'after' => 'Address_Type'),
                    'contact_id'         => array('type' => 'INT(11)', 'after' => 'is_primary')
                ),
                array(
                    'contact_id'         => array('fields' => array('contact_id')),
                    'address'            => array('fields' => array('address' => 255)),
                    'city'               => array('fields' => array('city' => 255)),
                    'state'              => array('fields' => array('state' => 255)),
                    'zip'                => array('fields' => array('zip' => 255)),
                    'country'            => array('fields' => array('country' => 255)),
                    'address_2'          => array('fields' => array('address','city','state','zip','country'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_contact_emails',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'email'          => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'email_type'     => array('type' => 'TINYINT(4)', 'after' => 'email'),
                    'is_primary'     => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => false, 'default' => '0', 'after' => 'email_type'),
                    'contact_id'     => array('type' => 'INT(11)', 'after' => 'is_primary')
                ),
                array(
                    'contact_id'     => array('fields' => array('contact_id')),
                    'email'          => array('fields' => array('email' => 255)),
                    'email_2'        => array('fields' => array('email'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_contact_phone',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'phone'          => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'phone_type'     => array('type' => 'TINYINT(4)', 'after' => 'phone'),
                    'is_primary'     => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => false, 'default' => '0', 'after' => 'phone_type'),
                    'contact_id'     => array('type' => 'INT(11)', 'after' => 'is_primary')
                ),
                array(
                    'contact_id'     => array('fields' => array('contact_id')),
                    'phone'          => array('fields' => array('phone' => 255)),
                    'phone_2'        => array('fields' => array('phone'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_contact_social_network',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'url'            => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'url_profile'    => array('type' => 'TINYINT(4)', 'after' => 'url'),
                    'is_primary'     => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => false, 'default' => '0', 'after' => 'url_profile'),
                    'contact_id'     => array('type' => 'INT(11)', 'after' => 'is_primary')
                ),
                array(
                    'contact_id'     => array('fields' => array('contact_id')),
                    'url'            => array('fields' => array('url' => 255)),
                    'url_2'          => array('fields' => array('url'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_contact_websites',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'url'            => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'url_profile'    => array('type' => 'TINYINT(4)', 'after' => 'url'),
                    'is_primary'     => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => false, 'default' => '0', 'after' => 'url_profile'),
                    'contact_id'     => array('type' => 'INT(11)', 'after' => 'is_primary')
                ),
                array(
                    'contact_id'     => array('fields' => array('contact_id')),
                    'url'            => array('fields' => array('url' => 255)),
                    'url_2'          => array('fields' => array('url'), 'type' => 'FULLTEXT')
                )
            );
        }
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_types',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'label'          => array('type' => 'VARCHAR(250)', 'binary' => true, 'after' => 'id'),
                    'active'         => array('type' => 'INT(1)', 'after' => 'label'),
                    'pos'            => array('type' => 'INT(10)', 'notnull' => true, 'default' => '0', 'after' => 'active'),
                    'default'        => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '0', 'after' => 'pos')
                ),
                array(
                    'label'          => array('fields' => array('label')),
                    'label_2'        => array('fields' => array('label'), 'type' => 'FULLTEXT')
                )
            );
        }
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_industry_type_local',
                array(
                    'entry_id'       => array('type' => 'INT(11)'),
                    'lang_id'        => array('type' => 'INT(11)', 'after' => 'entry_id'),
                    'value'          => array('type' => 'VARCHAR(256)', 'binary' => true, 'after' => 'lang_id')
                ),
                array(
                    'entry_id'       => array('fields' => array('entry_id')),
                    'value'          => array('fields' => array('value' => 255)),
                    'value_2'        => array('fields' => array('value'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_membership_local',
                array(
                    'entry_id'       => array('type' => 'INT(11)'),
                    'lang_id'        => array('type' => 'INT(11)', 'after' => 'entry_id'),
                    'value'          => array('type' => 'VARCHAR(256)', 'binary' => true, 'after' => 'lang_id')
                ),
                array(
                    'entry_id'       => array('fields' => array('entry_id')),
                    'value'          => array('fields' => array('value' => 255)),
                    'value_2'        => array('fields' => array('value'), 'type' => 'FULLTEXT')
                )
            );
        }
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.2.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_notes',
                array(
                    'id'                 => array('type' => 'INT(1)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'               => array('type' => 'VARCHAR(255)', 'after' => 'id'),
                    'status'             => array('type' => 'TINYINT(1)', 'after' => 'name'),
                    'icon'               => array('type' => 'VARCHAR(255)', 'after' => 'status'),
                    'pos'                => array('type' => 'INT(1)', 'after' => 'icon'),
                    'system_defined'     => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '0', 'after' => 'pos')
                ),
                array(
                    'name'               => array('fields' => array('name')),
                    'name_2'             => array('fields' => array('name'), 'type' => 'FULLTEXT')
                )
            );
        }
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_task_types',
                array(
                    'id'                 => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'               => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'status'             => array('type' => 'TINYINT(1)', 'after' => 'name'),
                    'sorting'            => array('type' => 'INT(11)', 'after' => 'status'),
                    'description'        => array('type' => 'text', 'after' => 'sorting'),
                    'icon'               => array('type' => 'VARCHAR(255)', 'after' => 'description'),
                    'system_defined'     => array('type' => 'TINYINT(4)', 'after' => 'icon')
                ),
                array(
                    'name'               => array('fields' => array('name' => 255)),
                    'name_2'             => array('fields' => array('name'), 'type' => 'FULLTEXT')
                )
            );
        }

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_company_size',
                array(
                    'id'           => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'company_size' => array('type' => 'VARCHAR(100)', 'notnull' => true,  'after' => 'id'),
                    'sorting'      => array('type' => 'INT(11)', 'notnull' => true,  'after' => 'company_size'),
                    'status'       => array('type' => 'TINYINT(4)', 'notnull' => true,  'after' => 'sorting'),
                ),
                array(
                    'company_size' => array('fields' => array('company_size'), 'type' => 'FULLTEXT')
                ),
                'InnoDB'
            );
        }
    } catch (\Cx\Lib\UpdateException $e) {
        // we COULD do something else here..
        DBG::trace();
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (1,'1',1,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (2,'2 - 9',2,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (3,'10 - 19',3,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (4,'20 - 49',4,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (5,'50 - 99',5,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (6,'100 - 199',6,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (7,'200 - 749',7,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (8,'750 - 999',8,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (9,'1\'000 - 4\'999',9,1)");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `". DBPREFIX ."module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (10,'> 5\'000',10,1)");

            \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES(18, \'contact_amount_enabled\', \'0\')');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //update module name for email templates
        \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."core_mail_template` SET `section` = 'Crm' WHERE `section` = 'crm'");
        \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."core_text` SET `section` = 'Crm' WHERE `section` = 'crm'");
        //update module name for crm core settings
        \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."core_setting` SET `section` = 'Crm' WHERE `section` = 'crm'");
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //Update script for moving the folder
        $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
        $sourceImagePath = $imagePath . '/crm';
        $targetImagePath = $imagePath . '/Crm';

        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceImagePath, $targetImagePath
            ));
            return false;
        }

        $mediaPath       = ASCMS_DOCUMENT_ROOT . '/media';
        $sourceMediaPath = $mediaPath . '/crm';
        $targetMediaPath = $mediaPath . '/Crm';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceMediaPath, $targetMediaPath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceMediaPath, $targetMediaPath
            ));
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.2.0')) {
        try {
            $result = \Cx\Lib\UpdateUtil::sql('SELECT `id` FROM `'.DBPREFIX.'core_text` WHERE `section` = \'crm\'');
            if ($result->RecordCount() == 0) {
                // migrate mail templates
                installCrmMailTemplates();
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_task',
                array(
                    'id'                 => array('type' => 'INT(2)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'task_id'            => array('type' => 'VARCHAR(10)', 'binary' => true, 'notnull' => true, 'after' => 'id'),
                    'task_title'         => array('type' => 'VARCHAR(255)', 'binary' => true, 'notnull' => true, 'after' => 'task_id'),
                    'task_type_id'       => array('type' => 'INT(2)', 'notnull' => true, 'after' => 'task_title'),
                    'customer_id'        => array('type' => 'INT(2)', 'notnull' => true, 'after' => 'task_type_id'),
                    'due_date'           => array('type' => 'datetime', 'notnull' => true, 'after' => 'customer_id'),
                    'assigned_to'        => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'due_date'),
                    'description'        => array('type' => 'text', 'binary' => true, 'notnull' => true, 'after' => 'assigned_to'),
                    'task_status'        => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1', 'after' => 'description'),
                    'added_by'           => array('type' => 'INT(11)', 'notnull' => true, 'after' => 'task_status'),
                    'added_date_time'    => array('type' => 'datetime', 'notnull' => true, 'after' => 'added_by')
                ),
                array(
                    'customer_id'        => array('fields' => array('customer_id'))
                ),
                'InnoDB'
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'module_crm_contacts'           => 'profile_picture',
            'module_crm_customer_comment'   => 'comment',
            'module_crm_customer_documents' => 'document_name',
            'module_crm_deals'              => 'description',
            'module_crm_notes'              => 'icon',
            'module_crm_task'               => 'description',
            'module_crm_task_types'         => 'icon',
        );
        try {
            foreach ($attributes as $table => $attribute) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'CRM (Crm)'
            ));
            return false;
        }
    }

    // drop url_type
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.1')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_contact_websites',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'url'            => array('type' => 'VARCHAR(256)', 'after' => 'id'),
                    'url_profile'    => array('type' => 'TINYINT(4)', 'after' => 'url'),
                    'is_primary'     => array('type' => 'ENUM(\'0\',\'1\')', 'notnull' => false, 'default' => '0', 'after' => 'url_profile'),
                    'contact_id'     => array('type' => 'INT(11)', 'after' => 'is_primary')
                ),
                array(
                    'contact_id'     => array('fields' => array('contact_id')),
                    'url'            => array('fields' => array('url' => 255)),
                    'url_2'          => array('fields' => array('url'), 'type' => 'FULLTEXT')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    // add default '' to profile_picture
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.1')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_contacts',
                array(
                    'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'customer_id'            => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'id'),
                    'customer_type'          => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'customer_id'),
                    'customer_name'          => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'customer_type'),
                    'customer_website'       => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'customer_name'),
                    'customer_addedby'       => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'customer_website'),
                    'company_size'           => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'customer_addedby'),
                    'customer_currency'      => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'company_size'),
                    'contact_amount'         => array('type' => 'VARCHAR(256)', 'notnull' => false, 'after' => 'customer_currency'),
                    'contact_familyname'     => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'contact_amount'),
                    'contact_title'          => array('type' => 'VARCHAR(256)', 'notnull' => false, 'after' => 'contact_familyname'),
                    'contact_role'           => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => false, 'after' => 'contact_title'),
                    'contact_customer'       => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'contact_role'),
                    'contact_language'       => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'contact_customer'),
                    'gender'                 => array('type' => 'TINYINT(2)', 'after' => 'contact_language'),
                    'salutation'             => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'gender'),
                    'notes'                  => array('type' => 'text', 'binary' => true, 'notnull' => false, 'after' => 'salutation'),
                    'industry_type'          => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'notes'),
                    'contact_type'           => array('type' => 'TINYINT(2)', 'notnull' => false, 'after' => 'industry_type'),
                    'user_account'           => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'contact_type'),
                    'datasource'             => array('type' => 'INT(11)', 'notnull' => false, 'after' => 'user_account'),
                    'profile_picture'        => array('type' => 'VARCHAR(256)', 'binary' => true, 'notnull' => true, 'default' => '', 'after' => 'datasource'),
                    'status'                 => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'profile_picture'),
                    'added_date'             => array('type' => 'date', 'after' => 'status'),
                    'updated_date'           => array('type' => 'timestamp', 'notnull' => true, 'after' => 'added_date'),
                    'email_delivery'         => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '1', 'after' => 'updated_date')
                ),
                array(
                    'contact_customer'       => array('fields' => array('contact_customer')),
                    'customer_id'            => array('fields' => array('customer_id' => 255)),
                    'customer_name'          => array('fields' => array('customer_name' => 255)),
                    'contact_familyname'     => array('fields' => array('contact_familyname' => 255)),
                    'contact_role'           => array('fields' => array('contact_role' => 255)),
                    'customer_id_2'          => array('fields' => array('customer_id','customer_name','contact_familyname','contact_role','notes'), 'type' => 'FULLTEXT'),
                    'customer_name_fulltext' => array('fields' => array('customer_name'), 'type' => 'FULLTEXT'),
                    'contact_familyname_fulltext'=> array('fields' => array('contact_familyname'), 'type' => 'FULLTEXT'),
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.2')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_currency',
                array(
                    'id'                     => array('type' => 'INT(10)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'name'                   => array('type' => 'VARCHAR(400)', 'after' => 'id'),
                    'active'                 => array('type' => 'INT(1)', 'notnull' => true, 'default' => '1', 'after' => 'name'),
                    'pos'                    => array('type' => 'INT(5)', 'notnull' => true, 'default' => '0', 'after' => 'active'),
                    'default_currency'       => array('type' => 'TINYINT(1)', 'after' => 'pos')
                ),
                array(
                    'name'                   => array('fields' => array('name' => 255)),
                    'fulltext'               => array('fields' => array('name'), 'type' => 'FULLTEXT'),
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_crm_customer_types',
                array(
                    'id'             => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'label'          => array('type' => 'VARCHAR(250)', 'binary' => true, 'after' => 'id'),
                    'active'         => array('type' => 'INT(1)', 'after' => 'label'),
                    'pos'            => array('type' => 'INT(10)', 'notnull' => true, 'default' => '0', 'after' => 'active'),
                    'default'        => array('type' => 'TINYINT(2)', 'notnull' => true, 'default' => '0', 'after' => 'pos')
                ),
                array(
                    'label'          => array('fields' => array('label')),
                    'label_2'        => array('fields' => array('label'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::sql('DELETE FROM `'.DBPREFIX.'module_crm_settings` WHERE `setname` = \'allow_pm\'');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}

function installCrmMailTemplates() {
    if (
        !\Cx\Core\MailTemplate\Controller\MailTemplate::store('Crm', array(
            'key' => 'crm_user_account_created',
            'lang_id' => '1',
            'sender' => 'Ihr Firmenname',
            'from' => 'info@example.com',
            'to' => '[CRM_CONTACT_EMAIL]',
            'reply' => 'info@example.com',
            'cc' => '',
            'bcc' => '',
            'subject' => 'Ihr persönlicher Zugang',
            'message' => "Guten Tag,\r\n\r\nNachfolgend erhalten Sie Ihre persönlichen Zugangsdaten zur Website http://www.example.com/\r\n\r\nBenutzername: [CRM_CONTACT_USERNAME]\r\nKennwort: [CRM_CONTACT_PASSWORD]",
            'message_html' => "<div>Guten Tag,<br />\r\n<br />\r\nNachfolgend erhalten Sie Ihre pers&ouml;nlichen Zugangsdaten zur Website <a href=\"http://www.example.com/\">http://www.example.com/</a><br />\r\n<br />\r\nBenutzername: [CRM_CONTACT_USERNAME]<br />\r\nKennwort: [CRM_CONTACT_PASSWORD]</div>",
            'html' => 'true',
            'protected' => 'true',
            'name' => 'Benachrichtigung über Benutzerkonto',
        ))
    ) {
        return false;
    }
    if (
        !\Cx\Core\MailTemplate\Controller\MailTemplate::store('Crm', array(
            'key' => 'crm_task_assigned',
            'lang_id' => '1',
            'sender' => 'Ihr Firmenname',
            'from' => 'info@example.com',
            'to' => '[CRM_ASSIGNED_USER_EMAIL]',
            'reply' => 'info@example.com',
            'cc' => '',
            'bcc' => '',
            'subject' => 'Neue Aufgabe: [CRM_TASK_NAME]',
            'message' => "Der Mitarbeiter [CRM_TASK_CREATED_USER] hat eine neue Aufgabe erstellt und Ihnen zugewiesen: [CRM_TASK_URL]\r\n\r\nBeschreibung: [CRM_TASK_DESCRIPTION_TEXT_VERSION]\r\n\r\nFällig am: [CRM_TASK_DUE_DATE]\r\n",
            'message_html' => "<div style=\"padding:0px; margin:0px; font-family:Tahoma, sans-serif; font-size:14px; width:620px; color: #333;\">\r\n<div style=\"padding: 0px 20px; border:1px solid #e0e0e0; margin-bottom: 10px; width:618px;\">\r\n<h1 style=\"background-color: #e0e0e0;color: #3d4a6b;font-size: 18px;font-weight: normal;padding: 15px 20px;margin-top: 0 !important;margin-bottom: 0 !important;margin-left: -20px !important;margin-right: -20px !important;-webkit-margin-before: 0 !important;-webkit-margin-after: 0 !important;-webkit-margin-start: -20px !important;-webkit-margin-end: -20px !important;\">Neue Aufgabe wurde Ihnen zugewiesen</h1>\r\n\r\n<p style=\"margin-top: 20px;word-wrap: break-word !important;\">Der Mitarbeiter [CRM_TASK_CREATED_USER] hat eine neue Aufgabe erstellt und Ihnen zugewiesen: [CRM_TASK_LINK]</p>\r\n\r\n<p style=\"margin-top: 20px;word-wrap: break-word !important;\">Beschreibung: [CRM_TASK_DESCRIPTION_HTML_VERSION]<br />\r\nF&auml;llig am: [CRM_TASK_DUE_DATE]</p>\r\n</div>\r\n</div>",
            'html' => 'true',
            'protected' => 'true',
            'name' => 'Neue Aufgabe',
        ))
    ) {
        return false;
    }
    if (
        !\Cx\Core\MailTemplate\Controller\MailTemplate::store('Crm', array(
            'key' => 'crm_notify_staff_on_contact_added',
            'lang_id' => '1',
            'sender' => 'Ihr Firmenname',
            'from' => 'info@example.com',
            'to' => '[CRM_ASSIGNED_USER_EMAIL]',
            'reply' => 'info@example.com',
            'cc' => '',
            'bcc' => '',
            'subject' => 'Neuer Kontakt erfasst',
            'message' => "Im CRM wurde ein neuer Kontakt erfasst: [CRM_CONTACT_DETAILS_URL]",
            'message_html' => "<div style=\"padding:0px; margin:0px; font-family:Tahoma, sans-serif; font-size:14px; width:620px; color: #333;\">\r\n<div style=\"padding: 0px 20px; border:1px solid #e0e0e0; margin-bottom: 10px; width:618px;\">\r\n<h1 style=\"background-color: #e0e0e0;color: #3d4a6b;font-size: 18px;font-weight: normal;padding: 15px 20px;margin-top: 0 !important;margin-bottom: 0 !important;margin-left: -20px !important;margin-right: -20px !important;-webkit-margin-before: 0 !important;-webkit-margin-after: 0 !important;-webkit-margin-start: -20px !important;-webkit-margin-end: -20px !important;\">Neuer Kontakt im CRM</h1>\r\n\r\n<p style=\"margin-top: 20px;word-wrap: break-word !important;\">Neuer Kontakt: [CRM_CONTACT_DETAILS_LINK].</p>\r\n</div>\r\n</div>\r\n",
            'html' => 'true',
            'protected' => 'true',
            'name' => 'Benachrichtigung an Mitarbeiter über neue Kontakte',
        ))
    ) {
        return false;
    }

    \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `'.DBPREFIX.'core_setting` (`section`, `name`, `group`, `type`, `value`, `values`, `ord`) VALUES (\'Crm\',\'numof_mailtemplate_per_page_backend\',\'config\',\'text\',\'25\',\'\',1001)');

    return true;
}

function _crmInstall() {

    try {
        $result = \Cx\Lib\UpdateUtil::sql('SELECT `id` FROM `'.DBPREFIX.'core_text` WHERE `section` = \'crm\' OR `section` = \'Crm\'');
        if (!$result || $result->RecordCount() == 0) {
            // add core mail template table because it is possible it doesn't yet exist
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'core_mail_template',
                array(
                    'key'            => array('type' => 'tinytext', 'notnull' => true),
                    'section'        => array('type' => 'tinytext', 'notnull' => true, 'renamefrom' => 'module_id', 'after' => 'key'),
                    'text_id'        => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'renamefrom' => 'text_name_id', 'after' => 'section'),
                    'html'           => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'text_id'),
                    'protected'      => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'html'),
                ),
                null,
                'InnoDB',
                'cx3upgrade'
            );
            Cx\Lib\UpdateUtil::sql("
                ALTER TABLE `".DBPREFIX."core_mail_template`
                ADD PRIMARY KEY (`key` (32), `section` (32))
            ");

            // migrate mail templates
            installCrmMailTemplates();
        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    // crm install sql statements will be added here by pkg manager

	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_company_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_size` varchar(100) NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `company_size` (`company_size`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'1\', \'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'2\', \'2 - 9\', \'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'3\', \'10 - 19\', \'3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'4\', \'20 - 49\', \'4\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'5\', \'50 - 99\', \'5\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'6\', \'100 - 199\', \'6\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'7\', \'200 - 749\', \'7\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'8\', \'750 - 999\', \'8\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'9\', \'1\\\'000 - 4\\\'999\', \'9\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_company_size` (`id`, `company_size`, `sorting`, `status`) VALUES (\'10\', \'> 5\\\'000\', \'10\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` varchar(256) BINARY DEFAULT NULL,
  `customer_type` int(11) DEFAULT NULL,
  `customer_name` varchar(256) BINARY DEFAULT NULL,
  `customer_website` varchar(256) BINARY DEFAULT NULL,
  `customer_addedby` int(11) DEFAULT NULL,
  `company_size` int(11) DEFAULT NULL,
  `customer_currency` int(11) DEFAULT NULL,
  `contact_amount` varchar(256) DEFAULT NULL,
  `contact_familyname` varchar(256) BINARY DEFAULT NULL,
  `contact_title` varchar(256) DEFAULT NULL,
  `contact_role` varchar(256) BINARY DEFAULT NULL,
  `contact_customer` int(11) DEFAULT NULL,
  `contact_language` int(11) DEFAULT NULL,
  `gender` tinyint(2) NOT NULL,
  `salutation` int(11) NOT NULL DEFAULT \'0\',
  `notes` text BINARY,
  `industry_type` int(11) DEFAULT NULL,
  `contact_type` tinyint(2) DEFAULT NULL,
  `user_account` int(11) DEFAULT NULL,
  `datasource` int(11) DEFAULT NULL,
  `profile_picture` varchar(256) BINARY NOT NULL DEFAULT \'\',
  `status` tinyint(2) NOT NULL DEFAULT \'1\',
  `added_date` date NOT NULL,
  `updated_date` timestamp NOT NULL ,
  `email_delivery` tinyint(2) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`),
  KEY `contact_customer` (`contact_customer`),
  KEY `customer_id` (`customer_id`(255)),
  KEY `customer_name` (`customer_name`(255)),
  KEY `contact_familyname` (`contact_familyname`(255)),
  KEY `contact_role` (`contact_role`(255)),
  FULLTEXT KEY `customer_id_2` (`customer_id`,`customer_name`,`contact_familyname`,`contact_role`,`notes`),
  FULLTEXT KEY `customer_name_fulltext` (`customer_name`),
  FULLTEXT KEY `contact_familyname_fulltext` (`contact_familyname`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_contacts` (`id`, `customer_id`, `customer_type`, `customer_name`, `customer_website`, `customer_addedby`, `company_size`, `customer_currency`, `contact_amount`, `contact_familyname`, `contact_title`, `contact_role`, `contact_customer`, `contact_language`, `gender`, `salutation`, `notes`, `industry_type`, `contact_type`, `user_account`, `datasource`, `profile_picture`, `status`, `added_date`, `updated_date`, `email_delivery`) VALUES (\'1\', NULL, \'3\', \'Cloudrexx AG\', NULL, \'1\', NULL, \'80\', NULL, NULL, NULL, NULL, NULL, \'1\', \'0\', \'0\', NULL, NULL, \'1\', NULL, \'1\', \'\', \'1\', \'2018-01-17\', \'2018-01-17 12:24:46\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_contacts` (`id`, `customer_id`, `customer_type`, `customer_name`, `customer_website`, `customer_addedby`, `company_size`, `customer_currency`, `contact_amount`, `contact_familyname`, `contact_title`, `contact_role`, `contact_customer`, `contact_language`, `gender`, `salutation`, `notes`, `industry_type`, `contact_type`, `user_account`, `datasource`, `profile_picture`, `status`, `added_date`, `updated_date`, `email_delivery`) VALUES (\'2\', NULL, NULL, \'Hans\', NULL, \'1\', NULL, NULL, NULL, \'Muster\', NULL, NULL, \'1\', \'1\', \'2\', \'0\', NULL, NULL, \'2\', NULL, \'1\', \'\', \'1\', \'2018-01-17\', \'2018-01-17 12:25:43\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_currency` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(400) NOT NULL,
  `active` int(1) NOT NULL DEFAULT \'1\',
  `pos` int(5) NOT NULL DEFAULT \'0\',
  `default_currency` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255)),
  FULLTEXT KEY `fulltext` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'71\', \'AED-United Arab Emirates Dirham\', \'1\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'72\', \'AMD-Armenian Dram\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'73\', \'ARS-Argentinian Peso\', \'1\', \'2\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'74\', \'AUD-Australian Dollar\', \'1\', \'3\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'75\', \'AZN-Azerbaijani Manat\', \'1\', \'4\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'76\', \'BDT-Bangladeshi Taka\', \'1\', \'5\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'77\', \'BRL-Brazilian Real\', \'1\', \'6\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'78\', \'BYR-Belarusian Ruble\', \'1\', \'7\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'79\', \'CAD-Canadian Dollar\', \'1\', \'8\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'80\', \'CHF-Swiss Franc\', \'1\', \'9\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'81\', \'CLP-Chilean Peso\', \'1\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'82\', \'CNY-Chinese Yuan\', \'1\', \'11\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'83\', \'CZK-Czech Koruna\', \'1\', \'12\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'84\', \'DKK-Danish Krone\', \'1\', \'13\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'85\', \'DZD-Algerian Dinar\', \'1\', \'14\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'86\', \'EUR-Euro\', \'1\', \'15\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'87\', \'GBP-Pound Sterling\', \'1\', \'16\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'88\', \'GEL-Georgian Lari\', \'1\', \'17\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'89\', \'HKD-Hong Kong Dollar\', \'1\', \'18\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'90\', \'HUF-Hungarian Forint\', \'1\', \'19\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'91\', \'IDR-Indonesian Rupiah\', \'1\', \'20\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'92\', \'ILS-Israeli Shekel\', \'1\', \'21\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'93\', \'INR-Indian Rupee\', \'1\', \'22\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'94\', \'JPY-Japanese Yen\', \'1\', \'23\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'95\', \'KRW-South Korean Won\', \'1\', \'24\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'96\', \'KZT-Kazakhstani Tenge\', \'1\', \'25\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'97\', \'LVL-Latvian Lat\', \'1\', \'26\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'98\', \'MAD-Moroccan Dirham\', \'1\', \'27\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'99\', \'MGA-Malagasy Ariary\', \'1\', \'28\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'100\', \'MXN-Mexican Peso\', \'1\', \'29\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'101\', \'MYR-Malaysian Ringgit\', \'1\', \'30\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'102\', \'NOK-Norwegian Krone\', \'1\', \'31\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'103\', \'NZD-New Zealand Dollar\', \'1\', \'32\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'104\', \'PHP-Philippine Peso\', \'1\', \'33\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'105\', \'PLN-Polish Zloty\', \'1\', \'34\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'106\', \'RUB-Rouble\', \'1\', \'35\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'107\', \'SAR-Saudi Riyal\', \'1\', \'36\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'108\', \'SEK-Swedish Krona\', \'1\', \'37\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'109\', \'SGD-Singapore Dollar\', \'1\', \'38\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'110\', \'THB-Thai Baht\', \'1\', \'39\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'111\', \'TRY-Turkish New Lira\', \'1\', \'40\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'112\', \'UAH-Ukraine Hryvnia\', \'1\', \'41\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'113\', \'USD-United States Dollar\', \'1\', \'42\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'114\', \'UZS-Uzbekistani Som\', \'1\', \'43\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'115\', \'VEF-Venezuelan Bolivar\', \'1\', \'44\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'116\', \'VND-Vietnamese Dong\', \'1\', \'45\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_currency` (`id`, `name`, `active`, `pos`, `default_currency`) VALUES (\'117\', \'ZAR-South African Rand\', \'1\', \'46\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `notes_type_id` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `comment` text ,
  `added_date` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  FULLTEXT KEY `comment` (`comment`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_contact_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` varchar(256) NOT NULL,
  `city` varchar(256) NOT NULL,
  `state` varchar(256) NOT NULL,
  `zip` varchar(256) NOT NULL,
  `country` varchar(256) NOT NULL,
  `Address_Type` tinyint(4) NOT NULL,
  `is_primary` enum(\'0\',\'1\') NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `address` (`address`(255)),
  KEY `city` (`city`(255)),
  KEY `state` (`state`(255)),
  KEY `zip` (`zip`(255)),
  KEY `country` (`country`(255)),
  FULLTEXT KEY `address_2` (`address`,`city`,`state`,`zip`,`country`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_contact_address` (`id`, `address`, `city`, `state`, `zip`, `country`, `Address_Type`, `is_primary`, `contact_id`) VALUES (\'1\', \'Burgstrasse 20\', \'Thun\', \'Bern\', \'3600\', \'Switzerland\', \'2\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_contact_address` (`id`, `address`, `city`, `state`, `zip`, `country`, `Address_Type`, `is_primary`, `contact_id`) VALUES (\'2\', \'\', \'\', \'\', \'\', \'Switzerland\', \'2\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_contact_emails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(256) NOT NULL,
  `email_type` tinyint(4) NOT NULL,
  `is_primary` enum(\'0\',\'1\') DEFAULT \'0\',
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `email` (`email`(255)),
  FULLTEXT KEY `email_2` (`email`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_contact_emails` (`id`, `email`, `email_type`, `is_primary`, `contact_id`) VALUES (\'1\', \'info@cloudrexx.com\', \'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_contact_phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(256) NOT NULL,
  `phone_type` tinyint(4) NOT NULL,
  `is_primary` enum(\'0\',\'1\') DEFAULT \'0\',
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `phone` (`phone`(255)),
  FULLTEXT KEY `phone_2` (`phone`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_contact_phone` (`id`, `phone`, `phone_type`, `is_primary`, `contact_id`) VALUES (\'1\', \'+41 33 550 02 55\', \'1\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_contact_social_network` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) NOT NULL,
  `url_profile` tinyint(4) NOT NULL,
  `is_primary` enum(\'0\',\'1\') DEFAULT \'0\',
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `url` (`url`(255)),
  FULLTEXT KEY `url_2` (`url`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_contact_social_network` (`id`, `url`, `url_profile`, `is_primary`, `contact_id`) VALUES (\'1\', \'https://www.facebook.com/cloudrexx/\', \'4\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_contact_websites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) NOT NULL,
  `url_profile` tinyint(4) NOT NULL,
  `is_primary` enum(\'0\',\'1\') DEFAULT \'0\',
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`),
  KEY `url` (`url`(255)),
  FULLTEXT KEY `url_2` (`url`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_contact_websites` (`id`, `url`, `url_profile`, `is_primary`, `contact_id`) VALUES (\'1\', \'https://www.cloudrexx.com\', \'3\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_documents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `document_name` varchar(256) NOT NULL,
  `added_by` int(11) NOT NULL,
  `uploaded_date` datetime NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `contact_id` (`contact_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_membership` (
  `contact_id` int(11) NOT NULL,
  `membership_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_customer_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(250) BINARY NOT NULL,
  `active` int(1) NOT NULL,
  `pos` int(10) NOT NULL DEFAULT \'0\',
  `default` tinyint(2) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `label` (`label`),
  FULLTEXT KEY `label_2` (`label`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'1\', \'Einzelfirma\', \'1\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'2\', \'Kollektivgesellschaft\', \'1\', \'2\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'3\', \'Aktiengesellschaft (AG)\', \'1\', \'3\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'4\', \'Gesellschaft mit beschränkter Haftung (GmbH)\', \'1\', \'4\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'5\', \'Öffentliches Unternehmen\', \'1\', \'5\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'6\', \'Eingetragener Verein\', \'1\', \'6\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'7\', \'Genossenschaft\', \'1\', \'7\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'8\', \'Andere europäische Rechtsform\', \'1\', \'8\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'9\', \'Andere nicht-europäische Rechtsform\', \'1\', \'9\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'10\', \'einfache Gesellschaft (eG)\', \'1\', \'10\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'11\', \'Kommanditgesellschaft\', \'1\', \'11\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'12\', \'Personengesellschaft\', \'1\', \'12\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'13\', \'Nichtregierungsorganisation (NGO)\', \'1\', \'13\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'14\', \'Freiberufler\', \'1\', \'14\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_customer_types` (`id`, `label`, `active`, `pos`, `default`) VALUES (\'15\', \'Privatperson\', \'1\', \'15\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_datasources` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datasource` varchar(256) NOT NULL,
  `status` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_datasources` (`id`, `datasource`, `status`) VALUES (\'1\', \'crm\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_datasources` (`id`, `datasource`, `status`) VALUES (\'2\', \'web form\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_datasources` (`id`, `datasource`, `status`) VALUES (\'3\', \'import\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_deals` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(256) NOT NULL,
  `website` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `customer_contact` int(11) NOT NULL,
  `quoted_price` decimal(10,2) NOT NULL DEFAULT \'0.00\',
  `quote_number` varchar(256) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `due_date` date DEFAULT NULL,
  `stage` int(11) NOT NULL,
  `description` text NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer` (`customer`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_industry_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `sorting` int(11) NOT NULL,
  `status` smallint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'1\', \'0\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'2\', \'0\', \'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'3\', \'0\', \'3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'4\', \'0\', \'4\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'5\', \'0\', \'5\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'6\', \'0\', \'6\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'7\', \'0\', \'7\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'8\', \'0\', \'8\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'9\', \'0\', \'9\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'10\', \'0\', \'10\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'11\', \'0\', \'11\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'12\', \'0\', \'12\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'13\', \'0\', \'13\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'14\', \'0\', \'14\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'15\', \'0\', \'15\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'16\', \'0\', \'16\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'17\', \'0\', \'17\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'18\', \'0\', \'18\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'19\', \'0\', \'19\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'20\', \'0\', \'20\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'21\', \'0\', \'21\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'22\', \'0\', \'22\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'23\', \'0\', \'23\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'24\', \'0\', \'24\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'25\', \'0\', \'25\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_types` (`id`, `parent_id`, `sorting`, `status`) VALUES (\'26\', \'0\', \'26\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_industry_type_local` (
  `entry_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `value` varchar(256) BINARY NOT NULL,
  KEY `entry_id` (`entry_id`),
  KEY `value` (`value`(255)),
  FULLTEXT KEY `value_2` (`value`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'1\', \'1\', \'Kunstgegenstände und Sammlerstücke\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'2\', \'1\', \'Baby\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'3\', \'1\', \'Kosmetik und Düfte\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'4\', \'1\', \'Bücher und Zeitschriften\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'5\', \'1\', \'B2B\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'6\', \'1\', \'Bekleidung, Accessoires und Schuhe\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'7\', \'1\', \'Computer, Zubehör und Dienstleistungen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'8\', \'1\', \'Bildung\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'9\', \'1\', \'Elektronik und Telekommunikation\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'10\', \'1\', \'Unterhaltung und Medien\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'11\', \'1\', \'Finanzdienstleistungen und -produkte\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'12\', \'1\', \'Lebensmittelhandel und Service\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'13\', \'1\', \'Geschenke und Blumen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'14\', \'1\', \'Behörden\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'15\', \'1\', \'Gesundheits- und Pflegeprodukte\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'16\', \'1\', \'Haus und Garten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'17\', \'1\', \'Gemeinnützige Organisationen (NPO)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'18\', \'1\', \'Haustiere und Tiere\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'19\', \'1\', \'Religion und Spiritualität (nicht gemeinnützig)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'20\', \'1\', \'Einzelhandel (nicht anderweitig klassifiziert)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'21\', \'1\', \'Dienstleistungen – Sonstige\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'22\', \'1\', \'Sport- und Outdoor-Artikel\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'23\', \'1\', \'Spielzeug und Hobbys\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'24\', \'1\', \'Reisen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'25\', \'1\', \'Fahrzeugverkauf\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'26\', \'1\', \'Fahrzeugservice und -zubehör\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'1\', \'2\', \'Arts, crafts, and collectibles\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'2\', \'2\', \'Baby\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'3\', \'2\', \'Beauty and fragrances\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'4\', \'2\', \'Books and magazines\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'5\', \'2\', \'Business to business\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'6\', \'2\', \'Clothing, accessories, and shoes\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'7\', \'2\', \'Computers, accessories, and services\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'8\', \'2\', \'Education\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'9\', \'2\', \'Electronics and telecom\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'10\', \'2\', \'Entertainment and media\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'11\', \'2\', \'Financial services and products\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'12\', \'2\', \'Food retail and service\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'13\', \'2\', \'Gifts and flowers\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'14\', \'2\', \'Government\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'15\', \'2\', \'Health and personal care\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'16\', \'2\', \'Home and garden\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'17\', \'2\', \'Nonprofit\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'18\', \'2\', \'Pets and animals\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'19\', \'2\', \'Religion and spirituality (for profit)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'20\', \'2\', \'Retail (not elsewhere classified)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'21\', \'2\', \'Services - other\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'22\', \'2\', \'Sports and outdoors\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'23\', \'2\', \'Toys and hobbies\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'24\', \'2\', \'Travel\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'25\', \'2\', \'Vehicle sales\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_industry_type_local` (`entry_id`, `lang_id`, `value`) VALUES (\'26\', \'2\', \'Vehicle service and accessories\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_memberships` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sorting` int(11) NOT NULL,
  `status` smallint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_memberships` (`id`, `sorting`, `status`) VALUES (\'21\', \'5\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_memberships` (`id`, `sorting`, `status`) VALUES (\'22\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_memberships` (`id`, `sorting`, `status`) VALUES (\'25\', \'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_membership_local` (
  `entry_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `value` varchar(256) BINARY NOT NULL,
  KEY `entry_id` (`entry_id`),
  KEY `value` (`value`(255)),
  FULLTEXT KEY `value_2` (`value`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_membership_local` (`entry_id`, `lang_id`, `value`) VALUES (\'25\', \'2\', \'Lieferanten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_membership_local` (`entry_id`, `lang_id`, `value`) VALUES (\'25\', \'1\', \'Lieferanten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_membership_local` (`entry_id`, `lang_id`, `value`) VALUES (\'22\', \'2\', \'Interessenten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_membership_local` (`entry_id`, `lang_id`, `value`) VALUES (\'22\', \'1\', \'Interessenten\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_membership_local` (`entry_id`, `lang_id`, `value`) VALUES (\'21\', \'2\', \'Reseller\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_membership_local` (`entry_id`, `lang_id`, `value`) VALUES (\'21\', \'1\', \'Reseller\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_notes` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `pos` int(1) NOT NULL,
  `system_defined` tinyint(2) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_notes` (`id`, `name`, `status`, `icon`, `pos`, `system_defined`) VALUES (\'42\', \'Gesprächsnotizen\', \'1\', \'\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_notes` (`id`, `name`, `status`, `icon`, `pos`, `system_defined`) VALUES (\'73\', \'E-Mail Notizen\', \'1\', \'\', \'2\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_notes` (`id`, `name`, `status`, `icon`, `pos`, `system_defined`) VALUES (\'74\', \'Zugangsdaten\', \'1\', \'\', \'4\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_notes` (`id`, `name`, `status`, `icon`, `pos`, `system_defined`) VALUES (\'75\', \'Termin\', \'1\', \'\', \'3\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_settings` (
  `setid` int(7) NOT NULL AUTO_INCREMENT,
  `setname` varchar(255) NOT NULL,
  `setvalue` text NOT NULL,
  PRIMARY KEY (`setid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'11\', \'customer_default_language_backend\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'12\', \'customer_default_language_frontend\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'13\', \'default_user_group\', \'6\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'14\', \'create_user_account\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'15\', \'emp_default_user_group\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'16\', \'user_account_mantatory\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'17\', \'default_country_value\', \'204\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_settings` (`setid`, `setname`, `setvalue`) VALUES (\'18\', \'contact_amount_enabled\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(256) NOT NULL,
  `stage` varchar(256) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `sorting` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'44\', \'Erste Evaluation durch Kunde ausgeführt\', \'30\', \'1\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'45\', \'Richtofferte / Grundofferte\', \'20\', \'1\', \'20\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'46\', \'Kunde favorisiert unser Angebot\', \'50\', \'1\', \'50\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'47\', \'Beste Position, keine Mitbewerber mehr\', \'60\', \'1\', \'60\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'48\', \'Interne Beeinflusser für uns entschieden\', \'70\', \'1\', \'70\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'49\', \'Mündliche Zusage durch Entscheider\', \'80\', \'1\', \'80\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'50\', \'Vertrag unterzeichnet\', \'100\', \'1\', \'100\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'51\', \'Auftragsbestätigung akzeptiert\', \'90\', \'1\', \'90\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'52\', \'Bedarfsaufnahme\', \'10\', \'1\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'53\', \'Gute Gründe für engere Wahl\', \'40\', \'1\', \'40\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_stages` (`id`, `label`, `stage`, `status`, `sorting`) VALUES (\'56\', \'Auftrag nicht erhalten\', \'1\', \'1\', \'110\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_success_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(256) NOT NULL,
  `rate` varchar(256) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `sorting` int(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'2\', \'Erste Evaluation durch Kunde ausgeführt\', \'30\', \'1\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'4\', \'Richtofferte / Grundofferte\', \'20\', \'1\', \'20\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'6\', \'Kunde favorisiert unser Angebot\', \'50\', \'1\', \'50\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'7\', \'Beste Position, keine Mitbewerber mehr\', \'60\', \'1\', \'60\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'8\', \'Interne Beeinflusser für uns entschieden\', \'70\', \'1\', \'70\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'9\', \'Mündliche Zusage durch Entscheider\', \'80\', \'1\', \'80\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'10\', \'Letter or telephone of intend\', \'90\', \'1\', \'90\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'11\', \'Auftragsbestätigung akzeptiert\', \'100\', \'1\', \'100\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'48\', \'Bedarfsaufnahme\', \'10\', \'1\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_success_rate` (`id`, `label`, `rate`, `status`, `sorting`) VALUES (\'49\', \'Gute Gründe für engere Wahl\', \'40\', \'1\', \'40\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_task` (
  `id` int(2) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(10) BINARY NOT NULL,
  `task_title` varchar(255) BINARY NOT NULL,
  `task_type_id` int(2) NOT NULL,
  `customer_id` int(2) NOT NULL,
  `due_date` datetime NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `description` text BINARY NOT NULL,
  `task_status` tinyint(1) NOT NULL DEFAULT \'1\',
  `added_by` int(11) NOT NULL,
  `added_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_crm_task_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `sorting` int(11) NOT NULL,
  `description` text NOT NULL,
  `icon` varchar(255) NOT NULL,
  `system_defined` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`(255)),
  FULLTEXT KEY `name_2` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'8\', \'Phone call\', \'1\', \'1\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'9\', \'Demo\', \'1\', \'2\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'10\', \'E-mail\', \'1\', \'3\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'11\', \'Fax\', \'1\', \'4\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'12\', \'Execution control\', \'1\', \'5\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'13\', \'Lunch\', \'1\', \'6\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'14\', \'Appoinment\', \'1\', \'7\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'15\', \'Note\', \'1\', \'8\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'16\', \'Delivery\', \'1\', \'9\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'17\', \'Social networks\', \'1\', \'10\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'18\', \'Expression of gratitude\', \'1\', \'11\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'52\', \'Unterlagen zusenden\', \'1\', \'0\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'53\', \'Rückruf\', \'1\', \'0\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'54\', \'Nachfassen\', \'1\', \'0\', \'\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_crm_task_types` (`id`, `name`, `status`, `sorting`, `description`, `icon`, `system_defined`) VALUES (\'55\', \'Treffen\', \'1\', \'0\', \'\', \'\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
