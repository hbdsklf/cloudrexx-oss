<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _knowledgeUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        if (!in_array('knowledge_phase_1', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            try{
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_knowledge_article_content',
                    array(
                        'id'       => array('type' => 'INT(10)',  'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                        'article'  => array('type' => 'INT(10)',  'notnull' => true, 'default' => 0, 'unsigned' => true),
                        'lang'     => array('type' => 'INT(10)',  'notnull' => true, 'default' => 0, 'unsigned' => true),
                        'question' => array('type' => 'TEXT',     'notnull' => true, 'default' => 0),
                        'answer'   => array('type' => 'TEXT',     'notnull' => true, 'default' => 0),
                    ),
                    array( # indexes
                        'module_knowledge_article_content_lang'    => array( 'fields'=>array('lang')),
                        'module_knowledge_article_content_article' => array( 'fields'=>array('article')),
                        'content' => array('fields' => array('question','answer'), 'type' => 'FULLTEXT'),
                    )
                );
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_knowledge_articles',
                    array(
                        'id'           => array('type' => 'INT(10)',    'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                        'category'     => array('type' => 'INT(10)',    'notnull' => true, 'default' => 0, 'unsigned' => true),
                        'active'       => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => 1),
                        'hits'         => array('type' => 'INT',        'notnull' => true, 'default' => 0),
                        'votes'        => array('type' => 'INT',        'notnull' => true, 'default' => 0),
                        'votevalue'    => array('type' => 'INT',        'notnull' => true, 'default' => 0),
                        'sort'         => array('type' => 'INT',        'notnull' => true, 'default' => 0),
                        'date_created' => array('type' => 'INT(14)',    'notnull' => true, 'default' => 0),
                        'date_updated' => array('type' => 'INT(14)',    'notnull' => true, 'default' => 0),
                    )
                );
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_knowledge_categories',
                    array(
                        'id'           => array('type' => 'INT(10)',    'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                        'active'       => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => 1, 'unsigned' => true),
                        'parent'       => array('type' => 'INT(10)',    'notnull' => true, 'default' => 0, 'unsigned' => true),
                        'sort'         => array('type' => 'INT(10)',    'notnull' => true, 'default' => 1, 'unsigned' => true),
                    ),
                    array( # indexes
                        'module_knowledge_categories_sort'   => array( 'fields'=>array('sort')),
                        'module_knowledge_categories_parent' => array( 'fields'=>array('parent'))
                    )
                );
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_knowledge_categories_content',
                    array(
                        'id'           => array('type' => 'INT(10)',    'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                        'category'     => array('type' => 'INT(10)', 'notnull' => true, 'default' => 0, 'unsigned' => true),
                        'name'         => array('type' => 'VARCHAR(255)',    'notnull' => true, 'default' => ''),
                        'lang'         => array('type' => 'INT(11)',    'notnull' => true, 'default' => 1),
                    )
                );
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_knowledge_settings',
                    array(
                        'id'    => array('type' => 'INT(10)',          'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                        'name'  => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                        'value' => array('type' => 'TEXT',         'notnull' => true, 'default' => 0),
                    ),
                    array( # indexes
                        'module_knowledge_settings_name'   => array( 'fields'=>array('name'))
                    )
                );
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX . 'module_knowledge_tags',
                    array(
                        'id'    => array('type' => 'INT(10)',          'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                        'name'  => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                        'lang'  => array('type' => 'INT(10)',          'notnull' => true, 'default' => 1, 'unsigned' => true),
                    ),
                    array( # indexes
                        'module_knowledge_tags_name'   => array( 'fields'=>array('name'))
                    )
                );

                if (strpos(\Cx\Lib\UpdateUtil::sql('SHOW CREATE TABLE `'.DBPREFIX.'module_knowledge_tags_articles`')->fields['Create Table'], 'UNIQUE KEY') === false) {
                    \Cx\Lib\UpdateUtil::table(
                        DBPREFIX . 'module_knowledge_tags_articles',
                        array(
                            'id'      => array('type' => 'INT(10)',  'notnull' => true, 'primary' => true, 'auto_increment' => true, 'unsigned' => true),
                            'article' => array('type' => 'INT(10)',  'notnull' => true, 'default' => 0, 'unsigned' => true),
                            'tag'     => array('type' => 'INT(10)',  'notnull' => true, 'default' => 0, 'unsigned' => true),
                        ),
                        array( # indexes
                            'module_knowledge_tags_articles_tag'     => array( 'fields'=>array('tag')),
                            'module_knowledge_tags_articles_article' => array( 'fields'=>array('article')),
                        )
                    );
                }

            } catch (\Cx\Lib\UpdateException $e) {
                // we COULD do something else here..
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }

            $_SESSION['contrexx_update']['update']['done'][] = 'knowledge_phase_1';
            return 'timeout';
        }


        if (!in_array('knowledge_phase_2', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            $arrSettings = array(
                array(
                    'name'  => 'max_subcategories',
                    'value' => '5'
                ),
                array(
                    'name'  => 'column_number',
                    'value' => '2'
                ),
                array(
                    'name'  => 'max_rating',
                    'value' => '8'
                ),
                array(
                    'name'  => 'best_rated_sidebar_template',
                    'value' => '<h2>Bestbewertete Artikel</h2>\r\n<div class="clearfix">\r\n<ul class="knowledge_sidebar">\r\n<!-- BEGIN article -->\r\n<li><a href="[[URL]]">[[ARTICLE]]</a></li>\r\n<!-- END article -->\r\n</ul>\r\n</div>'
                ),
                array(
                    'name'  => 'best_rated_sidebar_length',
                    'value' => '82'
                ),
                array(
                    'name'  => 'best_rated_sidebar_amount',
                    'value' => '5'
                ),
                array(
                    'name'  => 'tag_cloud_sidebar_template',
                    'value' => '[[CLOUD]] <br style="clear: both;" />'
                ),
                array(
                    'name'  => 'most_read_sidebar_template',
                    'value' => '<h2>Bestbewertete Artikel 2</h2>\r\n<div class="clearfix">\r\n<ul class="knowledge_sidebar">\r\n<!-- BEGIN article -->\r\n<li><a href="[[URL]]">[[ARTICLE]]</a></li>\r\n<!-- END article -->\r\n</ul>\r\n</div>'
                ),
                array(
                    'name'  => 'most_read_sidebar_length',
                    'value' => '79'
                ),
                array(
                    'name'  => 'most_read_sidebar_amount',
                    'value' => '5'
                ),
                array(
                    'name'  => 'best_rated_siderbar_template',
                    'value' => ''
                ),
                array(
                    'name'  => 'most_read_amount',
                    'value' => '5'
                ),
                array(
                    'name'  => 'best_rated_amount',
                    'value' => '5'
                ),
                array(
                    'name'  => 'show_all_lang',
                    'value' => '0'
                )
            );
            foreach ($arrSettings as $arrSetting) {
                $query = "SELECT 1 FROM `".DBPREFIX."module_knowledge_settings` WHERE `name` = '".$arrSetting['name']."'";
                $objResult = $objDatabase->SelectLimit($query, 1);
                if ($objResult !== false) {
                    if ($objResult->RecordCount() == 0) {
                        $query = "INSERT INTO `".DBPREFIX."module_knowledge_settings` (`name`, `value`) VALUES ('".$arrSetting['name']."', '".$arrSetting['value']."')";
                        if ($objDatabase->Execute($query) === false) {
                            return _databaseError($query, $objDatabase->ErrorMsg());
                        }
                    }
                } else {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'knowledge_phase_2';
            return 'timeout';
        }


        if (!in_array('knowledge_phase_3', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            /*******************************************
            * EXTENSION:    Duplicate entries clean up *
            * ADDED:        Contrexx v3.0.2            *
            *******************************************/
            try {
                \Cx\Lib\UpdateUtil::table(
                    DBPREFIX.'module_knowledge_tags_articles',
                    array(
                        'article'    => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                        'tag'        => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'article')
                    ),
                    array(
                        'article'    => array('fields' => array('article', 'tag'), 'type' => 'UNIQUE', 'force' => true)
                    ),
                    'InnoDB'
                );
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'knowledge_phase_3';
            return 'timeout';
        }

        if (!in_array('knowledge_phase_4', ContrexxUpdate::_getSessionArray($_SESSION['contrexx_update']['update']['done']))) {
            // migrate path to images and media
            $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
            $attributes = array(
                'answer'  =>  'module_knowledge_article_content',
                'value' =>  'module_knowledge_settings',
            );
            try {
                foreach ($attributes as $attribute => $table) {
                    foreach ($pathsToMigrate as $oldPath => $newPath) {
                        \Cx\Lib\UpdateUtil::migratePath(
                            '`' . DBPREFIX . $table . '`',
                            '`' . $attribute . '`',
                            $oldPath,
                            $newPath
                        );
                    }
                }
            } catch (\Cx\Lib\Update_DatabaseException $e) {
                \DBG::log($e->getMessage());
                return false;
            }
            $_SESSION['contrexx_update']['update']['done'][] = 'knowledge_phase_4';
        }
    }

    return true;
}
function _knowledgeInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(10) unsigned NOT NULL DEFAULT \'0\',
  `active` tinyint(1) NOT NULL DEFAULT \'1\',
  `hits` int(11) NOT NULL DEFAULT \'0\',
  `votes` int(11) NOT NULL DEFAULT \'0\',
  `votevalue` int(11) NOT NULL DEFAULT \'0\',
  `sort` int(11) NOT NULL DEFAULT \'0\',
  `date_created` int(14) NOT NULL DEFAULT \'0\',
  `date_updated` int(14) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_articles` (`id`, `category`, `active`, `hits`, `votes`, `votevalue`, `sort`, `date_created`, `date_updated`) VALUES (\'1\', \'1\', \'1\', \'20\', \'2\', \'14\', \'0\', \'1292236792\', \'1292847652\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_articles` (`id`, `category`, `active`, `hits`, `votes`, `votevalue`, `sort`, `date_created`, `date_updated`) VALUES (\'7\', \'2\', \'1\', \'50\', \'2\', \'15\', \'0\', \'1292405974\', \'1292849754\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_article_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `article` int(10) unsigned NOT NULL DEFAULT \'0\',
  `lang` int(10) unsigned NOT NULL DEFAULT \'0\',
  `question` text NOT NULL,
  `answer` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_knowledge_article_content_lang` (`lang`),
  KEY `module_knowledge_article_content_article` (`article`),
  FULLTEXT KEY `content` (`question`,`answer`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_article_content` (`id`, `article`, `lang`, `question`, `answer`) VALUES (\'184\', \'1\', \'1\', \'Was ist Cloudrexx?\', \'Cloudrexx ist ein modernes, einzigartiges und modulares Web Content Management System (WCMS) f&uuml;r die komplette Verwaltung einer Webseite. Zudem kann Cloudrexx auch f&uuml;r andere&nbsp;Informationsangebote wie Intranet, Extranet, eShop, Portal und weiteres eingesetzt werden. Das Cloudrexx basiert auf neuster PHP und MySQL Technologie und besticht in der einfachen Bedienung.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_article_content` (`id`, `article`, `lang`, `question`, `answer`) VALUES (\'185\', \'1\', \'2\', \'What is Cloudrexx?\', \'Cloudrexx is a powerful Open Source Web Content Management System (WCMS) which will assist you in the creation, administration and maintenance of contents for the internet or intranet. It\\\'s a completely web-based system with an easy-to-use WYSIWYG editor and intuitive user interface. Cloudrexx is very flexible, by adding modules you can customize the CMS according to your individual demands and it requires no technical knowledge or previous training. Visit us online at <a rel=\\\"nofollow\\\" title=\\\"https://www.cloudrexx.com/\\\" class=\\\"external\\\" href=\\\"https://www.cloudrexx.com/\\\">https://www.cloudrexx.com/</a> to learn more.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_article_content` (`id`, `article`, `lang`, `question`, `answer`) VALUES (\'187\', \'7\', \'1\', \'Wie erstelle ich einen neuen Eintrag?\', \'<h3>1. Im Backend einloggen</h3> <p>Melden Sie sich zuerst im Backend Ihrer Website an. Die URL&nbsp;zum Backend lautet jeweils <a href=\\\"http://www.DOMAINNAME.TLD/cadmin\\\">http://www.DOMAINNAME.TLD/cadmin</a></p> <h3>2. Neuen Artikel erstellen</h3> <p>Navigieren Sie nach &quot;Module/Wissensdatenbank/Artikel&quot; und klicken Sie dort auf die Schaltfl&auml;che &quot;Hinzuf&uuml;gen&quot;.</p> <h3>3. Kategorie festlegen</h3> <p>W&auml;hlen Sie die Kategorie, unter welcher der Artikel erscheinen soll.</p> <img height=\\\"23\\\" width=\\\"412\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_3_de.jpg\\\" alt=\\\"\\\" /><br /> <br /> <h3>4. Frage festlegen</h3> <p>Definieren Sie die Frage, welche beantwortet werden soll.</p> <img height=\\\"47\\\" border=\\\"0\\\" width=\\\"475\\\" alt=\\\"\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_4_de.jpg\\\" /><br /> <br /> <h3>5. Stichworte festlegen</h3> <p>W&auml;hlen Sie vorhandene Stichworte aus oder f&uuml;gen Sie neue hinzu.</p> <img height=\\\"21\\\" width=\\\"378\\\" alt=\\\"\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_5_de.jpg\\\" /><br /> <br /> <h3>6. Antwort schreiben</h3> <p>Schreiben Sie die Antwort im Editor.</p> <img height=\\\"102\\\" width=\\\"425\\\" alt=\\\"\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_6_de.jpg\\\" /><br /> <br /> <h3>7. Eintrag speichern</h3> <p>Klicken Sie nun auf die Schaltfl&auml;che &quot;Speichern&quot;.</p>\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_article_content` (`id`, `article`, `lang`, `question`, `answer`) VALUES (\'188\', \'7\', \'2\', \'How do I create a new entry?\', \'<h3>1. Backend login</h3> <p>First login to your backend. The url is always named on <a href=\\\"http://www.DOMAINNAME.TLD/cadmin\\\">http://www.DOMAINNAME.TLD/cadmin</a></p> <h3>2. Create a new entry</h3> <p>Navigate to &quot;Module/Knowledge base/Articles&quot; and click on the button &quot;Add&quot;.</p> <h3>3. Set the category</h3> <p>Choose the category in which the entry should appear.</p> <img height=\\\"23\\\" width=\\\"408\\\" alt=\\\"\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_3_en.jpg\\\" /><br /> <br /> <h3>4. Set the question</h3> <p>Define the question that has to be answered.</p> <img height=\\\"49\\\" border=\\\"0\\\" width=\\\"412\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_4_en.jpg\\\" alt=\\\"\\\" /><br /> <br /> <h3>5. Set the keywords</h3> <p>Choose some existing keywords or add new ones.</p> <img height=\\\"22\\\" width=\\\"348\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_5_en.jpg\\\" alt=\\\"\\\" /><br /> <br /> <h3>6. Write the answer</h3> <p>Type in the answer into the editor.</p> <img height=\\\"104\\\" width=\\\"424\\\" src=\\\"images/content/knowledgebase/wie_erstelle_ich_einen_neuen_eintrag_schritt_6_en.jpg\\\" alt=\\\"\\\" /><br /> <br /> <h3>7. Save the entry</h3> <p>Now click on the button &quot;Save&quot;.</p>\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `parent` int(10) unsigned NOT NULL DEFAULT \'0\',
  `sort` int(10) unsigned NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`),
  KEY `module_knowledge_categories_sort` (`sort`),
  KEY `module_knowledge_categories_parent` (`parent`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_categories` (`id`, `active`, `parent`, `sort`) VALUES (\'1\', \'1\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_categories` (`id`, `active`, `parent`, `sort`) VALUES (\'2\', \'1\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_categories_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `lang` int(11) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_categories_content` (`id`, `category`, `name`, `lang`) VALUES (\'25\', \'1\', \'Über Cloudrexx\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_categories_content` (`id`, `category`, `name`, `lang`) VALUES (\'26\', \'1\', \'About Cloudrexx\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_categories_content` (`id`, `category`, `name`, `lang`) VALUES (\'31\', \'2\', \'Anleitungen zur Wissensdatenbank\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_categories_content` (`id`, `category`, `name`, `lang`) VALUES (\'32\', \'2\', \'Instructions for the knowledgebase\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module_knowledge_settings_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'1\', \'max_subcategories\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'2\', \'column_number\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'3\', \'max_rating\', \'8\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'6\', \'best_rated_sidebar_template\', \'<div class=\\\"clearfix\\\">\\r\\n<ul class=\\\"knowledge_sidebar\\\">\\r\\n<!-- BEGIN article -->\\r\\n<li><a href=\\\"[[URL]]\\\">[[ARTICLE]]</a></li>\\r\\n<!-- END article -->\\r\\n</ul>\\r\\n</div>\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'7\', \'best_rated_sidebar_length\', \'82\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'8\', \'best_rated_sidebar_amount\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'9\', \'tag_cloud_sidebar_template\', \'[[CLOUD]] <br style=\\\"clear: both;\\\" />\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'10\', \'most_read_sidebar_template\', \'<div class=\\\"clearfix\\\">\\r\\n<ul class=\\\"knowledge_sidebar\\\">\\r\\n<!-- BEGIN article -->\\r\\n<li><a href=\\\"[[URL]]\\\">[[ARTICLE]]</a></li>\\r\\n<!-- END article -->\\r\\n</ul>\\r\\n</div>\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'12\', \'most_read_sidebar_length\', \'79\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'13\', \'most_read_sidebar_amount\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'14\', \'best_rated_siderbar_template\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'15\', \'most_read_amount\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_settings` (`id`, `name`, `value`) VALUES (\'16\', \'best_rated_amount\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `lang` int(10) unsigned NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`),
  KEY `module_knowledge_tags_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'1\', \'cloudrexx\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'2\', \'thun\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'5\', \'cloudrexx\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'7\', \'thun\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'12\', \'neu\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'15\', \'wcms\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'20\', \'eintrag erstellen\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'21\', \'neuer eintrag\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'22\', \'new\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'23\', \'new entry\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'24\', \'create entry\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags` (`id`, `name`, `lang`) VALUES (\'28\', \'wcms\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_knowledge_tags_articles` (
  `article` int(10) unsigned NOT NULL DEFAULT \'0\',
  `tag` int(10) unsigned NOT NULL DEFAULT \'0\',
  UNIQUE KEY `article` (`article`,`tag`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'1\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'1\', \'7\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'1\', \'15\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'1\', \'28\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'7\', \'12\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'7\', \'20\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'7\', \'21\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'7\', \'22\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'7\', \'23\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_knowledge_tags_articles` (`article`, `tag`) VALUES (\'7\', \'24\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
