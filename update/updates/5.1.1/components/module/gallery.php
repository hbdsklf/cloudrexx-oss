<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _galleryUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_gallery_categories',
                array(
                    'id'                     => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'pid'                    => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'sorting'                => array('type' => 'INT(6)',  'notnull' => true, 'default' => '0'),
                    'status'                 => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '1'),
                    'comment'                => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0'),
                    'voting'                 => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0'),
                    'backendProtected'       => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'backend_access_id'      => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'frontendProtected'      => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'frontend_access_id'     => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0')
                )
            );
            \Cx\Lib\UpdateUtil::table(

                DBPREFIX.'module_gallery_pictures',
                array(
                    'id'                     => array('type' => 'INT(11)',          'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'catid'                  => array('type' => 'INT(11)',          'notnull' => true, 'default' => '0'),
                    'validated'              => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0'),
                    'status'                 => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '1'),
                    'catimg'                 => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0'),
                    'sorting'                => array('type' => 'INT(6) UNSIGNED',  'notnull' => true, 'default' => '999'),
                    'size_show'              => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '1'),
                    'path'                   => array('type' => 'TEXT',             'notnull' => true),
                    'link'                   => array('type' => 'TEXT',             'notnull' => true),
                    'lastedit'               => array('type' => 'INT(14)',          'notnull' => true, 'default' => '0'),
                    'size_type'              => array('type' =>"SET('abs', 'proz')",'notnull' => true, 'default' => 'proz'),
                    'size_proz'              => array('type' => "INT(3)",           'notnull' => true, 'default' => '0'),
                    'size_abs_h'             => array('type' => 'INT(11)',          'notnull' => true, 'default' => '0'),
                    'size_abs_w'             => array('type' => 'INT(11)',          'notnull' => true, 'default' => '0'),
                    'quality'                => array('type' => 'TINYINT(3)',       'notnull' => true, 'default' => '0')
                ),
                array(
                    'galleryPicturesIndex' => array('type' => 'FULLTEXT', 'fields' => array('path'))
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        $arrSettings = array(
            '1'  => array( 'name' => 'max_images_upload',        'value' => '10'),
            '2'  => array( 'name' => 'standard_quality',         'value' => '95'),
            '3'  => array( 'name' => 'standard_size_proz',       'value' => '25'),
            '4'  => array( 'name' => 'standard_width_abs',       'value' => '140'),
            '6'  => array( 'name' => 'standard_height_abs',      'value' => '0'),
            '7'  => array( 'name' => 'standard_size_type',       'value' => 'abs'),
            '8'  => array( 'name' => 'validation_show_limit',    'value' => '10'),
            '9'  => array( 'name' => 'validation_standard_type', 'value' => 'all'),
            '11' => array( 'name' => 'show_names',               'value' => 'off'),
            '12' => array( 'name' => 'quality',                  'value' => '95'),
            '13' => array( 'name' => 'show_comments',            'value' => 'off'),
            '14' => array( 'name' => 'show_voting',              'value' => 'off'),
            '15' => array( 'name' => 'enable_popups',            'value' => 'on'),
            '16' => array( 'name' => 'image_width',              'value' => '1200'),
            '17' => array( 'name' => 'paging',                   'value' => '30'),
            '18' => array( 'name' => 'show_latest',              'value' => 'on'),
            '19' => array( 'name' => 'show_random',              'value' => 'on'),
            '20' => array( 'name' => 'header_type',              'value' => 'hierarchy'),
            '21' => array( 'name' => 'show_ext',                 'value' => 'off'),
            '22' => array( 'name' => 'show_file_name',           'value' => 'on'),
            '23' => array( 'name' => 'slide_show',               'value' => 'slideshow'),
            '24' => array( 'name' => 'slide_show_seconds',       'value' => '3')
        );

        foreach ($arrSettings as $id => $arrSetting) {
            $query = "SELECT 1 FROM `".DBPREFIX."module_gallery_settings` WHERE `name`= '".$arrSetting['name']."'" ;
            if (($objRS = $objDatabase->Execute($query)) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
            if($objRS->RecordCount() == 0){
                $query = "
                    INSERT INTO `".DBPREFIX."module_gallery_settings` (`id`, `name`, `value`)
                    VALUES (".$id.", '".$arrSetting['name']."', '".$arrSetting['value']."')
                    ON DUPLICATE KEY UPDATE `id` = `id`
                ";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }

        /**************************************************************************
         * EXTENSION:   cleanup: delete translations, comments and                *
         *              votes of inexisting pictures                              *
         * ADDED:       Contrexx v3.0.1                                           *
         **************************************************************************/
        try {
            \Cx\Lib\UpdateUtil::sql('
                DELETE `language_pics`
                FROM `'.DBPREFIX.'module_gallery_language_pics` as `language_pics` LEFT JOIN `'.DBPREFIX.'module_gallery_pictures` as `pictures` ON `pictures`.`id` = `language_pics`.`picture_id`
                WHERE `pictures`.`id` IS NULL
            ');

            \Cx\Lib\UpdateUtil::sql('
                DELETE `comments`
                FROM `'.DBPREFIX.'module_gallery_comments` as `comments` LEFT JOIN `'.DBPREFIX.'module_gallery_pictures` as `pictures` ON `pictures`.`id` = `comments`.`picid`
                WHERE `pictures`.`id` IS NULL
            ');

            \Cx\Lib\UpdateUtil::sql('
                DELETE `votes`
                FROM `'.DBPREFIX.'module_gallery_votes` as `votes` LEFT JOIN `'.DBPREFIX.'module_gallery_pictures` as `pictures` ON `pictures`.`id` = `votes`.`picid`
                WHERE `pictures`.`id` IS NULL
            ');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    // remove the script tag at the beginning of the gallery page
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.3')) {
        try {
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array('module' => 'gallery'), '/^\s*(<script[^>]+>.+?Shadowbox.+?<\/script>)+/sm', '', array('content'), '3.0.3');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_gallery_comments',
                array(
                    'id'         => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'picid'      => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'id'),
                    'date'       => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'picid'),
                    'name'       => array('type' => 'VARCHAR(50)', 'notnull' => true, 'default' => '', 'after' => 'date'),
                    'email'      => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'name'),
                    'www'        => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'email'),
                    'comment'    => array('type' => 'text', 'after' => 'www')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_gallery_votes',
                array(
                    'id'         => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'picid'      => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'id'),
                    'date'       => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'picid'),
                    'md5'        => array('type' => 'VARCHAR(32)', 'notnull' => true, 'default' => '', 'after' => 'date'),
                    'mark'       => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'md5')
                )
            );

            \Cx\Lib\UpdateUtil::sql("INSERT INTO `".DBPREFIX."module_gallery_settings` (`id`, `name`, `value`) VALUES (25,'show_image_size','off')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //Update script for moving the folder
        $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
        $sourceImagePath = $imagePath . '/gallery';
        $targetImagePath = $imagePath . '/Gallery';

        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceImagePath, $targetImagePath
            ));
            return false;
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'path', 'link',
        );
        try {
            foreach ($attributes as $attribute) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . 'module_gallery_pictures`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Bildergalerie (Gallery)'
            ));
            return false;
        }
    }

    return true;
}
function _galleryInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT \'0\',
  `sorting` int(6) NOT NULL DEFAULT \'0\',
  `status` set(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `comment` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `voting` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `backendProtected` int(11) NOT NULL DEFAULT \'0\',
  `backend_access_id` int(11) NOT NULL DEFAULT \'0\',
  `frontendProtected` int(11) NOT NULL DEFAULT \'0\',
  `frontend_access_id` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_categories` (`id`, `pid`, `sorting`, `status`, `comment`, `voting`, `backendProtected`, `backend_access_id`, `frontendProtected`, `frontend_access_id`) VALUES (\'1\', \'0\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picid` int(10) unsigned NOT NULL DEFAULT \'0\',
  `date` int(14) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(50) NOT NULL DEFAULT \'\',
  `email` varchar(250) NOT NULL DEFAULT \'\',
  `www` varchar(250) NOT NULL DEFAULT \'\',
  `comment` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_language` (
  `gallery_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` set(\'name\',\'desc\') NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  PRIMARY KEY (`gallery_id`,`lang_id`,`name`),
  FULLTEXT KEY `galleryindex` (`value`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language` (`gallery_id`, `lang_id`, `name`, `value`) VALUES (\'1\', \'1\', \'name\', \'Contrexx Software\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language` (`gallery_id`, `lang_id`, `name`, `value`) VALUES (\'1\', \'1\', \'desc\', \'Die Contrexx Editionen\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language` (`gallery_id`, `lang_id`, `name`, `value`) VALUES (\'1\', \'2\', \'name\', \'Contrexx Software\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language` (`gallery_id`, `lang_id`, `name`, `value`) VALUES (\'1\', \'2\', \'desc\', \'The Contrexx Editions\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_language_pics` (
  `picture_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `desc` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`picture_id`,`lang_id`),
  FULLTEXT KEY `galleryindex` (`name`,`desc`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language_pics` (`picture_id`, `lang_id`, `name`, `desc`) VALUES (\'1\', \'1\', \'Contrexx Free\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language_pics` (`picture_id`, `lang_id`, `name`, `desc`) VALUES (\'1\', \'2\', \'Contrexx Free\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language_pics` (`picture_id`, `lang_id`, `name`, `desc`) VALUES (\'2\', \'1\', \'Contrexx Non-Profit\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language_pics` (`picture_id`, `lang_id`, `name`, `desc`) VALUES (\'2\', \'2\', \'Contrexx Non-Profit\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language_pics` (`picture_id`, `lang_id`, `name`, `desc`) VALUES (\'3\', \'1\', \'Contrexx Business\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_language_pics` (`picture_id`, `lang_id`, `name`, `desc`) VALUES (\'3\', \'2\', \'Contrexx Business\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT \'0\',
  `validated` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `status` set(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `catimg` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `sorting` int(6) unsigned NOT NULL DEFAULT \'999\',
  `size_show` set(\'0\',\'1\') NOT NULL DEFAULT \'1\',
  `path` text NOT NULL,
  `link` text NOT NULL,
  `lastedit` int(14) NOT NULL DEFAULT \'0\',
  `size_type` set(\'abs\',\'proz\') NOT NULL DEFAULT \'proz\',
  `size_proz` int(3) NOT NULL DEFAULT \'0\',
  `size_abs_h` int(11) NOT NULL DEFAULT \'0\',
  `size_abs_w` int(11) NOT NULL DEFAULT \'0\',
  `quality` tinyint(3) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `galleryPicturesIndex` (`path`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_pictures` (`id`, `catid`, `validated`, `status`, `catimg`, `sorting`, `size_show`, `path`, `link`, `lastedit`, `size_type`, `size_proz`, `size_abs_h`, `size_abs_w`, `quality`) VALUES (\'1\', \'1\', \'1\', \'1\', \'0\', \'3\', \'1\', \''.DBPREFIX.'free.png\', \'\', \'1358242130\', \'abs\', \'25\', \'274\', \'274\', \'95\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_pictures` (`id`, `catid`, `validated`, `status`, `catimg`, `sorting`, `size_show`, `path`, `link`, `lastedit`, `size_type`, `size_proz`, `size_abs_h`, `size_abs_w`, `quality`) VALUES (\'2\', \'1\', \'1\', \'1\', \'0\', \'2\', \'1\', \''.DBPREFIX.'nonprofit.png\', \'\', \'1358242112\', \'abs\', \'25\', \'274\', \'274\', \'95\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_pictures` (`id`, `catid`, `validated`, `status`, `catimg`, `sorting`, `size_show`, `path`, `link`, `lastedit`, `size_type`, `size_proz`, `size_abs_h`, `size_abs_w`, `quality`) VALUES (\'3\', \'1\', \'1\', \'1\', \'1\', \'1\', \'1\', \''.DBPREFIX.'business.png\', \'\', \'1358242094\', \'abs\', \'25\', \'274\', \'274\', \'95\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'1\', \'max_images_upload\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'2\', \'standard_quality\', \'95\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'3\', \'standard_size_proz\', \'25\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'4\', \'standard_width_abs\', \'274\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'6\', \'standard_height_abs\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'7\', \'standard_size_type\', \'abs\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'8\', \'validation_show_limit\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'9\', \'validation_standard_type\', \'all\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'11\', \'show_names\', \'on\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'12\', \'quality\', \'95\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'13\', \'show_comments\', \'off\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'14\', \'show_voting\', \'off\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'15\', \'enable_popups\', \'on\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'16\', \'image_width\', \'4000\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'17\', \'paging\', \'30\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'18\', \'show_latest\', \'on\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'19\', \'show_random\', \'on\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'20\', \'header_type\', \'hierarchy\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'21\', \'show_ext\', \'on\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'22\', \'show_file_name\', \'off\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'23\', \'slide_show\', \'slideshow\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'24\', \'slide_show_seconds\', \'3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_gallery_settings` (`id`, `name`, `value`) VALUES (\'25\', \'show_image_size\', \'off\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_gallery_votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picid` int(10) unsigned NOT NULL DEFAULT \'0\',
  `date` int(14) unsigned NOT NULL DEFAULT \'0\',
  `md5` varchar(32) NOT NULL DEFAULT \'\',
  `mark` int(2) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
