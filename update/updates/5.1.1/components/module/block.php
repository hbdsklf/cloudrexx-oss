<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _blockUpdate()
{
    global $_ARRAYLANG, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try{
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_block_targeting_option',
                array(
                    'block_id'       => array('type' => 'INT(11)', 'notnull' => true, 'primary' => true),
                    'filter'         => array('type' => 'ENUM(\'include\',\'exclude\')', 'notnull' => true, 'default' => 'include'),
                    'type'           => array('type' => 'ENUM(\'country\')', 'notnull' => true, 'default' => 'country', 'primary' => true),
                    'value'          => array('type' => 'text', 'notnull' => true)
                ),
                array(),
                'InnoDb'
            );

            \Cx\Lib\UpdateUtil::sql('INSERT IGNORE INTO `'.DBPREFIX.'module_block_settings` (`id`, `name`, `value`) VALUES(2, \'markParsedBlock\', \'0\')');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        try {
            foreach ($pathsToMigrate as $oldPath => $newPath) {
                \Cx\Lib\UpdateUtil::migratePath(
                    '`' . DBPREFIX . 'module_block_rel_lang_content`',
                    '`content`',
                    $oldPath,
                    $newPath
                );
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Inhaltscontainer (Block)'
            ));
            return false;
        }
    }

    return true;
}

function _blockInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_block_blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `start` int(10) NOT NULL DEFAULT \'0\',
  `end` int(10) NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `random` int(1) NOT NULL DEFAULT \'0\',
  `random_2` int(1) NOT NULL DEFAULT \'0\',
  `random_3` int(1) NOT NULL DEFAULT \'0\',
  `random_4` int(1) NOT NULL DEFAULT \'0\',
  `global` int(1) NOT NULL DEFAULT \'0\',
  `category` int(1) NOT NULL DEFAULT \'0\',
  `direct` int(1) NOT NULL DEFAULT \'0\',
  `active` int(1) NOT NULL DEFAULT \'0\',
  `order` int(1) NOT NULL DEFAULT \'0\',
  `cat` int(10) NOT NULL DEFAULT \'0\',
  `wysiwyg_editor` int(1) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_blocks` (`id`, `start`, `end`, `name`, `random`, `random_2`, `random_3`, `random_4`, `global`, `category`, `direct`, `active`, `order`, `cat`, `wysiwyg_editor`) VALUES (\'29\', \'0\', \'0\', \'Slide 1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'1\', \'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_blocks` (`id`, `start`, `end`, `name`, `random`, `random_2`, `random_3`, `random_4`, `global`, `category`, `direct`, `active`, `order`, `cat`, `wysiwyg_editor`) VALUES (\'30\', \'0\', \'0\', \'Slide 2\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'2\', \'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_block_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent` int(10) NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `seperator` varchar(255) NOT NULL DEFAULT \'\',
  `order` int(10) NOT NULL DEFAULT \'0\',
  `status` tinyint(1) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_categories` (`id`, `parent`, `name`, `seperator`, `order`, `status`) VALUES (\'2\', \'0\', \'Startseite\', \'\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_categories` (`id`, `parent`, `name`, `seperator`, `order`, `status`) VALUES (\'3\', \'0\', \'Sidebar\', \'\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_block_rel_lang_content` (
  `block_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `content` mediumtext NOT NULL,
  `active` int(1) NOT NULL DEFAULT \'0\',
  UNIQUE KEY `id_lang` (`block_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_rel_lang_content` (`block_id`, `lang_id`, `content`, `active`) VALUES (\'29\', \'1\', \'<img alt=\\\"Slider 1\\\" height=\\\"500\\\" src=\\\"images/content/slideshow/slide_1.jpg\\\" width=\\\"1200\\\" />\\r\\n<h1>Be<span style=\\\"color: #0A85C8;\\\">Successful.</span></h1>\\r\\n\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_rel_lang_content` (`block_id`, `lang_id`, `content`, `active`) VALUES (\'29\', \'2\', \'<img alt=\\\"Slider 1\\\" height=\\\"500\\\" src=\\\"images/content/slideshow/slide_1.jpg\\\" width=\\\"1200\\\" />\\r\\n<h1>Be<font color=\\\"#0A85C8\\\">Smart.</font></h1>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_rel_lang_content` (`block_id`, `lang_id`, `content`, `active`) VALUES (\'30\', \'1\', \'<img alt=\\\"Slider 2\\\" height=\\\"500\\\" src=\\\"images/content/slideshow/slide_2.jpg\\\" width=\\\"1200\\\" />\\r\\n<h1>Make<span style=\\\"color: #0A85C8;\\\">Solutions.</span></h1>\\r\\n\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_rel_lang_content` (`block_id`, `lang_id`, `content`, `active`) VALUES (\'30\', \'2\', \'<img alt=\\\"Slider 2\\\" height=\\\"500\\\" src=\\\"images/content/slideshow/slide_2.jpg\\\" width=\\\"1200\\\" />\\r\\n<h1>Be<font color=\\\"#0A85C8\\\">Sucessful.</font></h1>\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_block_rel_pages` (
  `block_id` int(7) NOT NULL DEFAULT \'0\',
  `page_id` int(7) NOT NULL DEFAULT \'0\',
  `placeholder` enum(\'global\',\'direct\',\'category\') NOT NULL DEFAULT \'global\',
  PRIMARY KEY (`block_id`,`page_id`,`placeholder`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_block_settings` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `value` varchar(100) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_settings` (`id`, `name`, `value`) VALUES (\'1\', \'blockGlobalSeperator\', \'<br /><br />\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_block_settings` (`id`, `name`, `value`) VALUES (\'2\', \'markParsedBlock\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_block_targeting_option` (
  `block_id` int(11) NOT NULL,
  `filter` enum(\'include\',\'exclude\') NOT NULL DEFAULT \'include\',
  `type` enum(\'country\') NOT NULL DEFAULT \'country\',
  `value` text NOT NULL,
  PRIMARY KEY (`block_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}