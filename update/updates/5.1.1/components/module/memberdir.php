<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _memberdirUpdate() {
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG, $_CORELANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_memberdir_directories', array(
                    'dirid' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'parentdir' => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'active' => array('type' => 'SET(\'1\',\'0\')', 'notnull' => true, 'default' => '1'),
                    'name' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'description' => array('type' => 'TEXT'),
                    'displaymode' => array('type' => 'SET(\'0\',\'1\',\'2\')', 'notnull' => true, 'default' => '0'),
                    'sort' => array('type' => 'INT(11)', 'notnull' => true, 'default' => '1'),
                    'pic1' => array('type' => 'SET(\'1\',\'0\')', 'notnull' => true, 'default' => '0'),
                    'pic2' => array('type' => 'SET(\'1\',\'0\')', 'notnull' => true, 'default' => '0'),
                    'lang_id' => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '1')
                ), array(
                    'memberdir_dir' => array('fields' => array('name', 'description'), 'type' => 'FULLTEXT')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_memberdir_name', array(
                    'field' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'dirid' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'name' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'active' => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => ''),
                    'lang_id' => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '1')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_memberdir_settings', array(
                    'setid' => array('type' => 'INT(4)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'setname' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'setvalue' => array('type' => 'TEXT'),
                    'lang_id' => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '1')
                )
            );
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_memberdir_values', array(
                    'id' => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'dirid' => array('type' => 'INT(14)', 'notnull' => true, 'default' => '0'),
                    'pic1' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'pic2' => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    '0' => array('type' => 'SMALLINT(5)', 'notnull' => true, 'unsigned' => true, 'default' => '0'),
                    '1' => array('type' => 'TEXT'),
                    '2' => array('type' => 'TEXT'),
                    '3' => array('type' => 'TEXT'),
                    '4' => array('type' => 'TEXT'),
                    '5' => array('type' => 'TEXT'),
                    '6' => array('type' => 'TEXT'),
                    '7' => array('type' => 'TEXT'),
                    '8' => array('type' => 'TEXT'),
                    '9' => array('type' => 'TEXT'),
                    '10' => array('type' => 'TEXT'),
                    '11' => array('type' => 'TEXT'),
                    '12' => array('type' => 'TEXT'),
                    '13' => array('type' => 'TEXT'),
                    '14' => array('type' => 'TEXT'),
                    '15' => array('type' => 'TEXT'),
                    '16' => array('type' => 'TEXT'),
                    '17' => array('type' => 'TEXT'),
                    '18' => array('type' => 'TEXT'),
                    'lang_id' => array('type' => 'INT(2)', 'unsigned' => true, 'notnull' => true, 'default' => '1')
                )
            );

            $arrSettings = array(
                'default_listing' => array('1', '1'),
                'max_height' => array('400', '1'),
                'max_width' => array('500', '1')
            );

            foreach ($arrSettings as $key => $arrSetting) {
                if (!\Cx\Lib\UpdateUtil::sql("SELECT 1 FROM `" . DBPREFIX . "module_memberdir_settings` WHERE `setname` = '" . $key . "'")->RecordCount()) {
                    \Cx\Lib\UpdateUtil::sql("INSERT INTO `" . DBPREFIX . "module_memberdir_settings`
                        SET `setname`    = '" . $key . "',
                            `setvalue`   = '" . $arrSetting[0] . "',
                            `lang_id`    = '" . $arrSetting[1] . "'
                    ");
                }
            }
        } catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        if (
            file_exists(ASCMS_MEDIA_PATH . '/memberdir') &&
            !\Cx\Lib\FileSystem\FileSystem::makeWritable(ASCMS_MEDIA_PATH . '/memberdir')
        ) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_MEDIA_PATH . '/memberdir/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //Update script for moving the folder
        $mediaPath       = ASCMS_DOCUMENT_ROOT . '/media';
        $sourceMediaPath = $mediaPath . '/memberdir';
        $targetMediaPath = $mediaPath . '/MemberDir';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceMediaPath, $targetMediaPath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceMediaPath, $targetMediaPath
            ));
            return false;
        }
    }

    return true;
}
function _memberdirInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_memberdir_directories` (
  `dirid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parentdir` int(11) NOT NULL DEFAULT \'0\',
  `active` set(\'1\',\'0\') NOT NULL DEFAULT \'1\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `description` text NOT NULL,
  `displaymode` set(\'0\',\'1\',\'2\') NOT NULL DEFAULT \'0\',
  `sort` int(11) NOT NULL DEFAULT \'1\',
  `pic1` set(\'1\',\'0\') NOT NULL DEFAULT \'0\',
  `pic2` set(\'1\',\'0\') NOT NULL DEFAULT \'0\',
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`dirid`),
  FULLTEXT KEY `memberdir_dir` (`name`,`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_directories` (`dirid`, `parentdir`, `active`, `name`, `description`, `displaymode`, `sort`, `pic1`, `pic2`, `lang_id`) VALUES (\'1\', \'0\', \'1\', \'Verein ABC\', \'Mitglieder des Vereins ABC\', \'0\', \'0\', \'0\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_directories` (`dirid`, `parentdir`, `active`, `name`, `description`, `displaymode`, `sort`, `pic1`, `pic2`, `lang_id`) VALUES (\'2\', \'0\', \'1\', \'Verein XYZ\', \'Mitglieder des Vereins XYZ\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_directories` (`dirid`, `parentdir`, `active`, `name`, `description`, `displaymode`, `sort`, `pic1`, `pic2`, `lang_id`) VALUES (\'3\', \'1\', \'1\', \'Vorstand\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_directories` (`dirid`, `parentdir`, `active`, `name`, `description`, `displaymode`, `sort`, `pic1`, `pic2`, `lang_id`) VALUES (\'4\', \'1\', \'1\', \'Leitung\', \'Leitung\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_memberdir_name` (
  `field` int(10) unsigned NOT NULL DEFAULT \'0\',
  `dirid` int(10) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `active` set(\'0\',\'1\') NOT NULL DEFAULT \'\',
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'1\'
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'1\', \'1\', \'Name\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'2\', \'1\', \'Vorname\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'3\', \'1\', \'Firma\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'4\', \'1\', \'Telefonnummer\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'5\', \'1\', \'Mobilnummer\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'6\', \'1\', \'Adresse\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'7\', \'1\', \'PLZ\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'8\', \'1\', \'Ort\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'9\', \'1\', \'E-Mail\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'10\', \'1\', \'Fax\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'11\', \'1\', \'Internet\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'12\', \'1\', \'Geburtsdatum\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'13\', \'1\', \'Beschreibung\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'14\', \'1\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'15\', \'1\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'16\', \'1\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'17\', \'1\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'18\', \'1\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'1\', \'2\', \'Name\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'2\', \'2\', \'Vorname\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'3\', \'2\', \'Firma\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'4\', \'2\', \'Telefonnummer\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'5\', \'2\', \'Mobilnummer\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'6\', \'2\', \'Adresse\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'7\', \'2\', \'PLZ\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'8\', \'2\', \'Ort\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'9\', \'2\', \'E-Mail\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'10\', \'2\', \'Fax\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'11\', \'2\', \'Internet\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'12\', \'2\', \'Geburtsdatum\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'13\', \'2\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'14\', \'2\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'15\', \'2\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'16\', \'2\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'17\', \'2\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'18\', \'2\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'1\', \'3\', \'Name\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'2\', \'3\', \'Vorname\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'3\', \'3\', \'Firma\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'4\', \'3\', \'Telefonnummer\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'5\', \'3\', \'Mobilnummer\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'6\', \'3\', \'Adresse\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'7\', \'3\', \'PLZ\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'8\', \'3\', \'Ort\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'9\', \'3\', \'E-Mail\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'10\', \'3\', \'Fax\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'11\', \'3\', \'Internet\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'12\', \'3\', \'Geburtsdatum\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'13\', \'3\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'14\', \'3\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'15\', \'3\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'16\', \'3\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'17\', \'3\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'18\', \'3\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'1\', \'4\', \'Name\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'2\', \'4\', \'Vorname\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'3\', \'4\', \'Firma\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'4\', \'4\', \'Telefonnummer\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'5\', \'4\', \'Mobilnummer\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'6\', \'4\', \'Adresse\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'7\', \'4\', \'PLZ\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'8\', \'4\', \'Ort\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'9\', \'4\', \'E-Mail\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'10\', \'4\', \'Fax\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'11\', \'4\', \'Internet\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'12\', \'4\', \'Geburtsdatum\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'13\', \'4\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'14\', \'4\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'15\', \'4\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'16\', \'4\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'17\', \'4\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_name` (`field`, `dirid`, `name`, `active`, `lang_id`) VALUES (\'18\', \'4\', \'\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_memberdir_settings` (
  `setid` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `setname` varchar(255) NOT NULL DEFAULT \'\',
  `setvalue` text NOT NULL,
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`setid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_settings` (`setid`, `setname`, `setvalue`, `lang_id`) VALUES (\'1\', \'default_listing\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_settings` (`setid`, `setname`, `setvalue`, `lang_id`) VALUES (\'3\', \'max_height\', \'400\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_settings` (`setid`, `setname`, `setvalue`, `lang_id`) VALUES (\'4\', \'max_width\', \'500\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_memberdir_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dirid` int(14) NOT NULL DEFAULT \'0\',
  `pic1` varchar(255) NOT NULL DEFAULT \'\',
  `pic2` varchar(255) NOT NULL DEFAULT \'\',
  `0` smallint(5) unsigned NOT NULL DEFAULT \'0\',
  `1` text NOT NULL,
  `2` text NOT NULL,
  `3` text NOT NULL,
  `4` text NOT NULL,
  `5` text NOT NULL,
  `6` text NOT NULL,
  `7` text NOT NULL,
  `8` text NOT NULL,
  `9` text NOT NULL,
  `10` text NOT NULL,
  `11` text NOT NULL,
  `12` text NOT NULL,
  `13` text NOT NULL,
  `14` text NOT NULL,
  `15` text NOT NULL,
  `16` text NOT NULL,
  `17` text NOT NULL,
  `18` text NOT NULL,
  `lang_id` int(2) unsigned NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_values` (`id`, `dirid`, `pic1`, `pic2`, `0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15`, `16`, `17`, `18`, `lang_id`) VALUES (\'15\', \'1\', \'none\', \'none\', \'0\', \'Muster\', \'Hans\', \'\', \'\', \'\', \'Musterstrasse 12\', \'00000\', \'Musterhausen\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_values` (`id`, `dirid`, `pic1`, `pic2`, `0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15`, `16`, `17`, `18`, `lang_id`) VALUES (\'16\', \'1\', \'none\', \'none\', \'0\', \'Musterfrau\', \'Sabine\', \'\', \'\', \'\', \'Teststrasse 123\', \'12345\', \'Testerhausen\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_memberdir_values` (`id`, `dirid`, `pic1`, `pic2`, `0`, `1`, `2`, `3`, `4`, `5`, `6`, `7`, `8`, `9`, `10`, `11`, `12`, `13`, `14`, `15`, `16`, `17`, `18`, `lang_id`) VALUES (\'17\', \'1\', \'none\', \'none\', \'0\', \'Mustermann\', \'Peter\', \'\', \'\', \'\', \'Musterweg 321\', \'32112\', \'Musterhausen\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'\', \'1\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
