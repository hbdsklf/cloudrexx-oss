<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _downloadsUpdate()
{
    global $objDatabase, $_ARRAYLANG, $_CORELANG, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        /************************************************
        * EXTENSION:    Initial creation of the         *
        *               database tables                 *
        * ADDED:        Contrexx v2.1.0                 *
        ************************************************/
        $arrTables  = $objDatabase->MetaTables('TABLES');
        if (!sizeof($arrTables)) {
            setUpdateMsg($_ARRAYLANG['TXT_UNABLE_DETERMINE_DATABASE_STRUCTURE']);
            return false;
        }

        $tables = array(
            DBPREFIX.'module_downloads_category' => "CREATE TABLE `".DBPREFIX."module_downloads_category` (
                 `id` int(11) unsigned NOT NULL auto_increment,
                 `parent_id` int(11) unsigned NOT NULL default '0',
                 `is_active` tinyint(1) unsigned NOT NULL default '1',
                 `visibility` tinyint(1) unsigned NOT NULL default '1',
                 `owner_id` int(5) unsigned NOT NULL default '0',
                 `order` int(3) unsigned NOT NULL default '0',
                 `deletable_by_owner` tinyint(1) unsigned NOT NULL default '1',
                 `modify_access_by_owner` tinyint(1) unsigned NOT NULL default '1',
                 `read_access_id` int(11) unsigned NOT NULL default '0',
                 `add_subcategories_access_id` int(11) unsigned NOT NULL default '0',
                 `manage_subcategories_access_id` int(11) unsigned NOT NULL default '0',
                 `add_files_access_id` int(11) unsigned NOT NULL default '0',
                 `manage_files_access_id` int(11) unsigned NOT NULL default '0',
                 `image` varchar(255) NOT NULL default '',
                  PRIMARY KEY (`id`),
                  KEY `is_active` (`is_active`),
                  KEY `visibility` (`visibility`)
                ) ENGINE=InnoDB",
            #################################################################################
            DBPREFIX.'module_downloads_category_locale' => "CREATE TABLE `".DBPREFIX."module_downloads_category_locale` (
                 `lang_id` int(11) unsigned NOT NULL default '0',
                 `category_id` int(11) unsigned NOT NULL default '0',
                 `name` varchar(255) NOT NULL default '',
                 `description` text NOT NULL,
                  PRIMARY KEY (`lang_id`,`category_id`),
                  FULLTEXT KEY `name` (`name`),
                  FULLTEXT KEY `description` (`description`)
                ) ENGINE=InnoDB",
            #################################################################################
            DBPREFIX.'module_downloads_download_locale' => "CREATE TABLE `".DBPREFIX."module_downloads_download_locale` (
                 `lang_id` int(11) unsigned NOT NULL default '0',
                 `download_id` int(11) unsigned NOT NULL default '0',
                 `name` varchar(255) NOT NULL default '',
                 `description` text NOT NULL,
                  PRIMARY KEY (`lang_id`,`download_id`),
                  FULLTEXT KEY `name` (`name`),
                  FULLTEXT KEY `description` (`description`)
                ) ENGINE=InnoDB",
            #################################################################################
            DBPREFIX.'module_downloads_rel_download_category' => "CREATE TABLE `".DBPREFIX."module_downloads_rel_download_category` (
                 `download_id` int(10) unsigned NOT NULL default '0',
                 `category_id` int(10) unsigned NOT NULL default '0',
                 `order` int(3) unsigned NOT NULL default '0',
                  PRIMARY KEY (`download_id`,`category_id`)
                ) ENGINE=InnoDB",
            #################################################################################
            DBPREFIX.'module_downloads_rel_download_download' => "CREATE TABLE `".DBPREFIX."module_downloads_rel_download_download` (
                 `id1` int(10) unsigned NOT NULL default '0',
                 `id2` int(10) unsigned NOT NULL default '0',
                  PRIMARY KEY (`id1`,`id2`)
                ) ENGINE=InnoDB",
            #################################################################################
            DBPREFIX.'module_downloads_settings' => "CREATE TABLE `".DBPREFIX."module_downloads_settings` (
                 `id` int(11) NOT NULL auto_increment,
                 `name` varchar(32) NOT NULL default '',
                 `value` varchar(255) NOT NULL default '',
                  PRIMARY KEY (`id`)
                ) ENGINE=InnoDB"
       );

        foreach ($tables as $name => $query) {
            #print_r($arrTables);
            if (in_array($name, $arrTables)) {
                continue;
            }
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        try{
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_downloads_download',
                array(
                    'id'                 => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'type'               => array('type' => 'ENUM(\'file\',\'url\')', 'notnull' => true, 'default' => 'file'),
                    'mime_type'          => array('type' => 'ENUM(\'image\',\'document\',\'pdf\',\'media\',\'archive\',\'application\',\'link\')', 'notnull' => true, 'default' => 'image'),
                    'source'             => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'source_name'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'icon'               => array('type' => 'ENUM(\'_blank\',\'avi\',\'bmp\',\'css\',\'doc\',\'dot\',\'exe\',\'fla\',\'gif\',\'htm\',\'html\',\'inc\',\'jpg\',\'js\',\'mp3\',\'nfo\',\'pdf\',\'php\',\'png\',\'pps\',\'ppt\',\'rar\',\'swf\',\'txt\',\'wma\',\'xls\',\'zip\')', 'notnull' => true, 'default' => '_blank'),
                    'size'               => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'image'              => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'owner_id'           => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'access_id'          => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'license'            => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'version'            => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => ''),
                    'author'             => array('type' => 'VARCHAR(100)', 'notnull' => true, 'default' => ''),
                    'website'            => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => ''),
                    'ctime'              => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'mtime'              => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'is_active'          => array('type' => 'TINYINT(3)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'visibility'         => array('type' => 'TINYINT(1)', 'unsigned' => true, 'notnull' => true, 'default' => '1'),
                    'order'              => array('type' => 'INT(3)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'views'              => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'download_count'     => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'expiration'         => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0'),
                    'validity'           => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0')
                ),
                array(
                    'is_active'          => array('fields' => array('is_active')),
                    'visibility'         => array('fields' => array('visibility'))
                )
            );
        }
        catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }




        /************************************************
        * EXTENSION:    Initial adding of the           *
        *               settings values                 *
        * ADDED:        Contrexx v2.1.0                 *
        ************************************************/
        $arrSettings = array(
            'overview_cols_count'           => '2',
            'overview_max_subcats'          => '5',
            'use_attr_size'                 => '1',
            'use_attr_license'              => '1',
            'use_attr_version'              => '1',
            'use_attr_author'               => '1',
            'use_attr_website'              => '1',
            'most_viewed_file_count'        => '5',
            'most_downloaded_file_count'    => '5',
            'most_popular_file_count'       => '5',
            'newest_file_count'             => '5',
            'updated_file_count'            => '5',
            'new_file_time_limit'           => '604800',
            'updated_file_time_limit'       => '604800',
            'associate_user_to_groups'      => ''
        );

        foreach ($arrSettings as $name => $value) {
            $query = "SELECT 1 FROM `".DBPREFIX."module_downloads_settings` WHERE `name` = '".$name."'";
            $objResult = $objDatabase->SelectLimit($query, 1);
            if ($objResult) {
                if ($objResult->RecordCount() == 0) {
                    $query = "INSERT INTO `".DBPREFIX."module_downloads_settings` (`name`, `value`) VALUES ('".$name."', '".$value."')";
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }




        /************************************************
        * BUGFIX:   Set write access to the upload dir  *
        ************************************************/
        if (file_exists(ASCMS_DOWNLOADS_IMAGES_PATH)) {
            if (\Cx\Lib\FileSystem\FileSystem::makeWritable(ASCMS_DOWNLOADS_IMAGES_PATH)) {
                if ($mediaDir = @opendir(ASCMS_DOWNLOADS_IMAGES_PATH)) {
                    while($file = readdir($mediaDir)) {
                        if ($file != '.' && $file != '..') {
                            if (!\Cx\Lib\FileSystem\FileSystem::makeWritable(ASCMS_DOWNLOADS_IMAGES_PATH.'/'.$file)) {
                                setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_FILE'], ASCMS_DOWNLOADS_IMAGES_PATH.'/'.$file, $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
                                return false;
                            }
                        }
                    }
                } else {
                    setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_DOWNLOADS_IMAGES_PATH.'/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
                    return false;
                }
            } else {
                setUpdateMsg(sprintf($_ARRAYLANG['TXT_SET_WRITE_PERMISSON_TO_DIR_AND_CONTENT'], ASCMS_DOWNLOADS_IMAGES_PATH.'/', $_CORELANG['TXT_UPDATE_TRY_AGAIN']), 'msg');
                return false;
            }
        }




        /************************************************
        * EXTENSION:    Groups                          *
        * ADDED:        Contrexx v2.1.2                 *
        ************************************************/
        try{
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_downloads_group',
                array(
                    'id'         => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'is_active'  => array('type' => 'TINYINT(1)', 'notnull' => true, 'default' => '1'),
                    'type'       => array('type' => 'ENUM(\'file\',\'url\')', 'notnull' => true, 'default' => 'file'),
                    'info_page'  => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_downloads_group_locale',
                array(
                    'lang_id'    => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'group_id'   => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'name'       => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '')
                ),
                array(
                    'name'       => array('fields' => array('name'), 'type' => 'FULLTEXT')
                )
            );

            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_downloads_rel_group_category',
                array(
                    'group_id'       => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'category_id'    => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true)
                )
            );
        }
        catch (\Cx\Lib\UpdateException $e) {
            // we COULD do something else here..
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }



    /*******************************************************
    * EXTENSION:    Localisation of download source fields *
    * ADDED:        Contrexx v3.0.0                        *
    ********************************************************/
    try {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
            $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_downloads_download_locale');
            if ($arrColumns === false) {
                setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_downloads_download_locale'));
                return false;
            }

            if (!isset($arrColumns['SOURCE']) && !isset($arrColumns['SOURCE_NAME'])) {
                \Cx\Lib\UpdateUtil::sql('
                    ALTER TABLE `'.DBPREFIX.'module_downloads_download_locale`
                    ADD `source` VARCHAR(255) NULL DEFAULT NULL AFTER `name`,
                    ADD `source_name` VARCHAR(255) NULL DEFAULT NULL AFTER `source`,
                    ADD `metakeys` TEXT NOT NULL AFTER `description`
                ');
                \Cx\Lib\UpdateUtil::sql('
                    UPDATE `'.DBPREFIX.'module_downloads_download` AS download INNER JOIN `'.DBPREFIX.'module_downloads_download_locale` AS download_locale ON download.id = download_locale.download_id
                    SET download_locale.source = download.source, download_locale.source_name = download.source_name
                ');
                \Cx\Lib\UpdateUtil::sql('
                    ALTER TABLE `'.DBPREFIX.'module_downloads_download`
                    DROP COLUMN `source`,
                    DROP COLUMN `source_name`
                ');
            }
        }

        /**********************************************************
        * Migrate downloads file type                             *
        **********************************************************/
        if (   $objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')
            && !isset($arrColumns['file_type'])
        ) {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX .'module_downloads_download_locale',
                array(
                    'lang_id'     => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                    'download_id' => array('type' => 'INT(11)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'lang_id', 'primary' => 'true'),
                    'name'        => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'download_id'),
                    'source'      => array('type' => 'VARCHAR(1024)', 'notnull' => false, 'after' => 'name'),
                    'source_name' => array('type' => 'VARCHAR(1024)', 'notnull' => false, 'after' => 'source'),
                    'file_type'   => array('type' => 'VARCHAR(10)', 'notnull' => false, 'default' => null, 'after' => 'source_name'),
                    'description' => array('type' => 'TEXT', 'notnull' => true, 'after' => 'file_type'),
                    'metakeys'    => array('type' => 'TEXT', 'notnull' => true, 'after' => 'description'),
                ),
                array(
                    'name'        => array('fields' => array('name'), 'type' => 'FULLTEXT'),
                    'description' => array('fields' => array('description'), 'type' => 'FULLTEXT'),
                )
            );
            \Cx\Lib\UpdateUtil::sql('
                UPDATE
                    `'. DBPREFIX .'module_downloads_download_locale` AS downloadlocale
                LEFT JOIN
                    `'. DBPREFIX .'module_downloads_download` AS download
                ON (download.`id` = downloadlocale.`download_id`)
                SET downloadlocale.`file_type` = download.`icon`
            ');
            \Cx\Lib\UpdateUtil::sql('
                ALTER TABLE `'. DBPREFIX .'module_downloads_download` DROP `icon`
            ');

            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegex(array(), '/images\/downloads\//', 'images/Downloads/', array('content', 'target'), '5.0.0');
        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }




    /**********************************************************
    * EXTENSION:    Increase length of download source fields *
    * ADDED:        Contrexx v3.1.0                           *
    **********************************************************/
    try {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0')) {

            \Cx\Lib\UpdateUtil::sql('
                ALTER TABLE `'.DBPREFIX.'module_downloads_download_locale`
                CHANGE `source` `source` VARCHAR(1024) NULL DEFAULT NULL,
                CHANGE `source_name` `source_name` VARCHAR(1024) NULL DEFAULT NULL
            ');

        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }


    /**********************************************************
    * EXTENSION:    Add access ids of "editing all downloads" *
    *               to groups which had access to "administer"*
    * ADDED:        Contrexx v3.1.1                           *
    **********************************************************/
    try {
        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.1.0.2')) {
            $result = \Cx\Lib\UpdateUtil::sql("SELECT `group_id` FROM `" . DBPREFIX . "access_group_static_ids` WHERE access_id = 142 GROUP BY `group_id`");
            if ($result !== false) {
                while (!$result->EOF) {
                    \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `" . DBPREFIX . "access_group_static_ids` (`access_id`, `group_id`)
                                                VALUES (143, " . intval($result->fields['group_id']) . ")");
                    $result->MoveNext();
                }
            }
        }
    } catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try{
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (16,'use_attr_metakeys','1')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (17,'downloads_sorting_order','newestToOldest')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (18,'categories_sorting_order','alphabetic')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (19,'list_downloads_current_lang','0')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (20,'integrate_into_search_component','0')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (21,'global_search_linking','detail')");

            \Cx\Lib\UpdateUtil::sql("ALTER TABLE `".DBPREFIX."module_downloads_download` CHANGE `version` `version` VARCHAR(255) NOT NULL DEFAULT ''");

            //following queries for changing the path from images/downloads into images/Downloads
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_downloads_download`
                                     SET `image` = REPLACE(`image`, 'images/downloads', 'images/Downloads')
                                     WHERE `image` LIKE ('".ASCMS_PATH_OFFSET."/images/downloads%')");
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_downloads_category`
                                     SET `image` = REPLACE(`image`, 'images/downloads', 'images/Downloads')
                                     WHERE `image` LIKE ('".ASCMS_PATH_OFFSET."/images/downloads%')");
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_downloads_download_locale`
                                     SET `source` = REPLACE(`source`, 'images/downloads', 'images/Downloads')
                                     WHERE `source` LIKE ('".ASCMS_PATH_OFFSET."/images/downloads%')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        //Update script for moving the folder
        $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
        $sourceImagePath = $imagePath . '/downloads';
        $targetImagePath = $imagePath . '/Downloads';

        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceImagePath, $targetImagePath
            ));
            return false;
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'module_downloads_category'         => 'image',
            'module_downloads_category_locale'  => 'description',
            'module_downloads_download'         => 'image',
            'module_downloads_download_locale'  => 'source',
            'module_downloads_download_locale'  => 'description',
        );
        try {
            foreach ($attributes as $table => $attribute) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Digital Asset Management (Downloads)'
            ));
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.2')) {
        try{
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (22,'auto_file_naming','off')");
            \Cx\Lib\UpdateUtil::sql("INSERT IGNORE INTO `".DBPREFIX."module_downloads_settings` (`id`, `name`, `value`) VALUES (23,'pretty_regex_pattern','')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _downloadsInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `visibility` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `owner_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `order` int(3) unsigned NOT NULL DEFAULT \'0\',
  `deletable_by_owner` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `modify_access_by_owner` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `read_access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `add_subcategories_access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `manage_subcategories_access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `add_files_access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `manage_files_access_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `image` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`),
  KEY `visibility` (`visibility`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category` (`id`, `parent_id`, `is_active`, `visibility`, `owner_id`, `order`, `deletable_by_owner`, `modify_access_by_owner`, `read_access_id`, `add_subcategories_access_id`, `manage_subcategories_access_id`, `add_files_access_id`, `manage_files_access_id`, `image`) VALUES (\'10\', \'0\', \'1\', \'1\', \'1\', \'3\', \'1\', \'1\', \'61\', \'62\', \'63\', \'64\', \'65\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category` (`id`, `parent_id`, `is_active`, `visibility`, `owner_id`, `order`, `deletable_by_owner`, `modify_access_by_owner`, `read_access_id`, `add_subcategories_access_id`, `manage_subcategories_access_id`, `add_files_access_id`, `manage_files_access_id`, `image`) VALUES (\'12\', \'10\', \'1\', \'1\', \'1\', \'2\', \'1\', \'1\', \'53\', \'70\', \'71\', \'72\', \'73\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category` (`id`, `parent_id`, `is_active`, `visibility`, `owner_id`, `order`, `deletable_by_owner`, `modify_access_by_owner`, `read_access_id`, `add_subcategories_access_id`, `manage_subcategories_access_id`, `add_files_access_id`, `manage_files_access_id`, `image`) VALUES (\'13\', \'10\', \'1\', \'1\', \'1\', \'1\', \'1\', \'1\', \'54\', \'74\', \'75\', \'76\', \'77\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category` (`id`, `parent_id`, `is_active`, `visibility`, `owner_id`, `order`, `deletable_by_owner`, `modify_access_by_owner`, `read_access_id`, `add_subcategories_access_id`, `manage_subcategories_access_id`, `add_files_access_id`, `manage_files_access_id`, `image`) VALUES (\'14\', \'0\', \'1\', \'1\', \'1\', \'1\', \'1\', \'1\', \'0\', \'53\', \'54\', \'55\', \'56\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_category_locale` (
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `category_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `description` text NOT NULL,
  PRIMARY KEY (`lang_id`,`category_id`),
  FULLTEXT KEY `name` (`name`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'1\', \'10\', \'Community\', \'Dateiaustausch der Mitglieder\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'1\', \'12\', \'Tools\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'1\', \'13\', \'Bilder\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'1\', \'14\', \'Cloudrexx\', \'Cloudrexx Software\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'2\', \'10\', \'Community\', \'Data Exchange for members\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'2\', \'12\', \'Tools\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'2\', \'13\', \'Bilder\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_category_locale` (`lang_id`, `category_id`, `name`, `description`) VALUES (\'2\', \'14\', \'Cloudrexx\', \'Cloudrexx Software\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_download` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` enum(\'file\',\'url\') NOT NULL DEFAULT \'file\',
  `mime_type` enum(\'image\',\'document\',\'pdf\',\'media\',\'archive\',\'application\',\'link\') NOT NULL DEFAULT \'image\',
  `size` int(10) unsigned NOT NULL DEFAULT \'0\',
  `image` varchar(255) NOT NULL DEFAULT \'\',
  `owner_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `access_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `license` varchar(255) NOT NULL DEFAULT \'\',
  `version` varchar(255) NOT NULL DEFAULT \'\',
  `author` varchar(100) NOT NULL DEFAULT \'\',
  `website` varchar(255) NOT NULL DEFAULT \'\',
  `ctime` int(14) unsigned NOT NULL DEFAULT \'0\',
  `mtime` int(14) unsigned NOT NULL DEFAULT \'0\',
  `is_active` tinyint(3) unsigned NOT NULL DEFAULT \'0\',
  `visibility` tinyint(1) unsigned NOT NULL DEFAULT \'1\',
  `order` int(3) unsigned NOT NULL DEFAULT \'0\',
  `views` int(10) unsigned NOT NULL DEFAULT \'0\',
  `download_count` int(10) unsigned NOT NULL DEFAULT \'0\',
  `expiration` int(14) unsigned NOT NULL DEFAULT \'0\',
  `validity` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `is_active` (`is_active`),
  KEY `visibility` (`visibility`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_download` (`id`, `type`, `mime_type`, `size`, `image`, `owner_id`, `access_id`, `license`, `version`, `author`, `website`, `ctime`, `mtime`, `is_active`, `visibility`, `order`, `views`, `download_count`, `expiration`, `validity`) VALUES (\'1\', \'url\', \'application\', \'0\', \'/images/content/cloudrexx.png\', \'1\', \'0\', \'AGPL / Cloudrexx EULA\', \'\', \'Cloudrexx AG\', \'https://www.cloudrexx.com\', \'1292505824\', \'1529591567\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_download` (`id`, `type`, `mime_type`, `size`, `image`, `owner_id`, `access_id`, `license`, `version`, `author`, `website`, `ctime`, `mtime`, `is_active`, `visibility`, `order`, `views`, `download_count`, `expiration`, `validity`) VALUES (\'2\', \'url\', \'application\', \'0\', \'/images/content/cloudrexx.png\', \'1\', \'0\', \'AGPL / Cloudrexx EULA\', \'\', \'Cloudrexx AG\', \'https://www.cloudrexx.com\', \'1292506378\', \'1529591673\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_download_locale` (
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `download_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `source` varchar(1024) DEFAULT NULL,
  `source_name` varchar(1024) DEFAULT NULL,
  `file_type` varchar(10) DEFAULT NULL,
  `description` text NOT NULL,
  `metakeys` text NOT NULL,
  PRIMARY KEY (`lang_id`,`download_id`),
  FULLTEXT KEY `name` (`name`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_download_locale` (`lang_id`, `download_id`, `name`, `source`, `source_name`, `file_type`, `description`, `metakeys`) VALUES (\'1\', \'1\', \'Cloudrexx Download\', \'https://www.cloudrexx.com/latest_release_full\', \'www.cloudrexx.com\', \'HTML\', \'Aktuelles Cloudrexx Download Release\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_download_locale` (`lang_id`, `download_id`, `name`, `source`, `source_name`, `file_type`, `description`, `metakeys`) VALUES (\'1\', \'2\', \'Cloudrexx Update\', \'https://www.cloudrexx.com/latest_release_update\', \'www.cloudrexx.com\', \'HTML\', \'Aktuelles Cloudrexx Update Paket\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_download_locale` (`lang_id`, `download_id`, `name`, `source`, `source_name`, `file_type`, `description`, `metakeys`) VALUES (\'2\', \'1\', \'Cloudrexx Download\', \'https://www.cloudrexx.com/latest_release_full\', \'www.cloudrexx.com\', \'HTML\', \'Latest Cloudrexx Download release\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_download_locale` (`lang_id`, `download_id`, `name`, `source`, `source_name`, `file_type`, `description`, `metakeys`) VALUES (\'2\', \'2\', \'Cloudrexx Update\', \'https://www.cloudrexx.com/latest_release_update\', \'www.cloudrexx.com\', \'HTML\', \'Latest Cloudrexx update package\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `is_active` tinyint(1) NOT NULL DEFAULT \'1\',
  `type` enum(\'file\',\'url\') NOT NULL DEFAULT \'file\',
  `info_page` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_group_locale` (
  `lang_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `group_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`lang_id`,`group_id`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_rel_download_category` (
  `download_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `category_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `order` int(3) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`download_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_rel_download_category` (`download_id`, `category_id`, `order`) VALUES (\'1\', \'14\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_rel_download_category` (`download_id`, `category_id`, `order`) VALUES (\'2\', \'14\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_rel_download_download` (
  `id1` int(10) unsigned NOT NULL DEFAULT \'0\',
  `id2` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id1`,`id2`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_rel_download_download` (`id1`, `id2`) VALUES (\'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_rel_group_category` (
  `group_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `category_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`group_id`,`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_downloads_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL DEFAULT \'\',
  `value` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'1\', \'overview_cols_count\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'2\', \'overview_max_subcats\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'3\', \'use_attr_size\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'4\', \'use_attr_license\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'5\', \'use_attr_version\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'6\', \'use_attr_author\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'7\', \'use_attr_website\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'8\', \'most_viewed_file_count\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'9\', \'most_downloaded_file_count\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'10\', \'most_popular_file_count\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'11\', \'newest_file_count\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'12\', \'updated_file_count\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'13\', \'new_file_time_limit\', \'604800\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'14\', \'updated_file_time_limit\', \'604800\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'15\', \'associate_user_to_groups\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'16\', \'use_attr_metakeys\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'17\', \'downloads_sorting_order\', \'newestToOldest\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'18\', \'categories_sorting_order\', \'alphabetic\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'19\', \'list_downloads_current_lang\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'20\', \'integrate_into_search_component\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'21\', \'global_search_linking\', \'detail\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'22\', \'auto_file_naming\', \'off\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_downloads_settings` (`id`, `name`, `value`) VALUES (\'23\', \'pretty_regex_pattern\', \'\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
