<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _forumUpdate()
{
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        $arrSettings = array(
                            '9' => array(
                                'name' 	=> 'banned_words',
                                'value' => 'penis enlargement,free porn,(?i:buy\\\\s*?(?:cheap\\\\s*?)?viagra)'),
                            '10' => array(
                                'name' 	=> 'wysiwyg_editor',
                                'value' => '1'),
                            '11' => array(
                                'name' 	=> 'tag_count',
                                'value' => '10'),
                            '12' => array(
                                'name' 	=> 'latest_post_per_thread',
                                'value' => '1'),
                            '13' => array(
                                'name' 	=> 'allowed_extensions',
                                'value' => '7z,aiff,asf,avi,bmp,csv,doc,fla,flv,gif,gz,gzip, jpeg,jpg,mid,mov,mp3,mp4,mpc,mpeg,mpg,ods,odt,pdf, png,ppt,pxd,qt,ram,rar,rm,rmi,rmvb,rtf,sdc,sitd,swf, sxc,sxw,tar,tgz,tif,tiff,txt,vsd,wav,wma,wmv,xls,xml ,zip')
                            );

        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_forum_postings',
                array(
                    'id'                 => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'category_id'        => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'id'),
                    'thread_id'          => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'category_id'),
                    'prev_post_id'       => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'thread_id'),
                    'user_id'            => array('type' => 'INT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'prev_post_id'),
                    'time_created'       => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'user_id'),
                    'time_edited'        => array('type' => 'INT(14)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'time_created'),
                    'is_locked'          => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'time_edited'),
                    'is_sticky'          => array('type' => 'SET(\'0\',\'1\')', 'notnull' => true, 'default' => '0', 'after' => 'is_locked'),
                    'rating'             => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0', 'after' => 'is_sticky'),
                    'views'              => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'rating'),
                    'icon'               => array('type' => 'SMALLINT(5)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'after' => 'views'),
                    'keywords'           => array('type' => 'text', 'after' => 'icon'),
                    'subject'            => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'keywords'),
                    'content'            => array('type' => 'text', 'after' => 'subject'),
                    'attachment'         => array('type' => 'VARCHAR(250)', 'notnull' => true, 'default' => '', 'after' => 'content')
                ),
                array(
                    'category_id'        => array('fields' => array('category_id','thread_id','prev_post_id','user_id')),
                    'fulltext'           => array('fields' => array('keywords','subject','content'), 'type' => 'FULLTEXT')
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        foreach ($arrSettings as $id => $arrSetting) {
            $query = "SELECT 1 FROM `".DBPREFIX."module_forum_settings` WHERE `name`= '".$arrSetting['name']."'" ;
            if (($objRS = $objDatabase->Execute($query)) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
            if($objRS->RecordCount() == 0){
                $query = "INSERT INTO `".DBPREFIX."module_forum_settings`
                                 (`id`, `name`, `value`)
                          VALUES (".$id.", '".$arrSetting['name']."', '".addslashes($arrSetting['value'])."')" ;
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }


        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_forum_rating',
                array(
                    'id'         => array('type' => 'INT(11)', 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'user_id'    => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'post_id'    => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0'),
                    'time'       => array('type' => 'INT(11)', 'notnull' => true, 'default' => '0')
                ),
                array(
                    'user_id'    => array('fields' => array('user_id','post_id'))
                )
            );
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        /**********************************
         * EXTENSION:   Content Migration *
         * ADDED:       Contrexx v3.0.0   *
         **********************************/
        try {
            // migrate content page to version 3.0.1
            $search = array(
            '/(.*)/ms',
            );
            $callback = function($matches) {
                $content = $matches[1];
                if (empty($content)) {
                    return $content;
                }

                // replace message textarea with {FORUM_MESSAGE_INPUT}
                $content = preg_replace('/<textarea[^>]+name\s*=\s*[\'"]message[\'"][^>]*>.*?\{FORUM_MESSAGE\}.*?<\/textarea>/ms', '{FORUM_MESSAGE_INPUT}', $content);

                if (!preg_match('/<!--\s+BEGIN\s+captcha\s+-->.*<!--\s+END\s+captcha\s+-->/ms', $content)) {
                    // migration for versions < 2.0

                    // add missing template block captcha
                    $content = preg_replace('/(.*)(<tr[^>]*>.*?<td[^>]*>.*?\{FORUM_CAPTCHA_IMAGE_URL\}.*?<\/td>.*?<\/tr>)/ms', '$1<!-- BEGIN captcha -->$2<!-- END captcha -->', $content);
                }
                    
                // add missing placeholder {FORUM_JAVASCRIPT_SCROLLTO}
                if (strpos($content, '{FORUM_JAVASCRIPT_SCROLLTO}') === false) {
                    $content = '{FORUM_JAVASCRIPT_SCROLLTO}'.$content;
                }

                // hide deprecated marckup buttons
                $content = preg_replace('/(<!--\s+)?(<input[^>]+onclick\s*=\s*[\'"]\s*addText\([^>]+>)(?:\s+-->)?/ms', '<!-- $2 -->', $content);

                // replace image with {FORUM_CAPTCHA_CODE}
                $content = preg_replace('/(<!--\s+BEGIN\s+captcha\s+-->.*)<img[^>]+\{FORUM_CAPTCHA_IMAGE_URL\}[^>]+>(?:<br\s*\/?>)?(.*<!--\s+END\s+captcha\s+-->)/ms', '$1{FORUM_CAPTCHA_CODE}$2', $content);

                // replace text "Captcha-Code" with {TXT_FORUM_CAPTCHA}
                $content = preg_replace('/(<!--\s+BEGIN\s+captcha\s+-->.*)Captcha-Code:?(.*<!--\s+END\s+captcha\s+-->)/ms', '$1{TXT_FORUM_CAPTCHA}$2', $content);

                // remove <input type="text" name="captcha" id="captcha" />
                $content = preg_replace('/(<!--\s+BEGIN\s+captcha\s+-->.*)<input[^>]+name\s*=\s*[\'"]captcha[\'"][^>]*>(.*<!--\s+END\s+captcha\s+-->)/ms', '$1$2', $content);

                // remove <input type="hidden" name="offset" value="[[FORUM_CAPTCHA_OFFSET]]" />
                $content = preg_replace('/(<!--\s+BEGIN\s+captcha\s+-->.*)<input[^>]+name\s*=\s*[\'"]offset[\'"][^>]*>(.*<!--\s+END\s+captcha\s+-->)/ms', '$1$2', $content);

                // add missing block threadActions
                if (!preg_match('/<!--\s+BEGIN\s+threadActions\s+-->.*<!--\s+END\s+threadActions\s+-->/ms', $content)) {
                    $threadActionHtml = <<<FORUM
<!-- BEGIN threadActions --><br />
    <span style="color: rgb(255, 0, 0);">{TXT_THREAD_ACTION_ERROR}&nbsp;</span><br />
    <span style="color: #006900;">{TXT_THREAD_ACTION_SUCCESS}&nbsp;</span> <!-- BEGIN moveForm -->
    <form action="index.php?section=forum&amp;cmd=thread&amp;action=move&amp;id={FORUM_THREAD_ID}" method="POST" name="frmThreadMove">
        <select name="moveToThread" size="32" style="width:225px;"> {FORUM_THREADS} </select><br />
        <input type="submit" value="{TXT_FORUM_THREAD_ACTION_MOVE}" />&nbsp;</form>
    <!-- END moveForm --><!-- END threadActions -->
FORUM;

                    $content = preg_replace('/(<!--\s+END\s+addPost\s+-->)/ms', '$1'.$threadActionHtml, $content);
                }

                return $content;
            };

            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'forum'), $search, $callback, array('content'), '3.0.1');
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        $mediaPath       = ASCMS_DOCUMENT_ROOT . '/media';
        $sourceMediaPath = $mediaPath . '/forum';
        $targetMediaPath = $mediaPath . '/Forum';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceMediaPath, $targetMediaPath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceMediaPath, $targetMediaPath
            ));
            return false;
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        try {
            foreach ($pathsToMigrate as $oldPath => $newPath) {
                \Cx\Lib\UpdateUtil::migratePath(
                    '`' . DBPREFIX . 'module_forum_categories_lang`',
                    '`description`',
                    $oldPath,
                    $newPath
                );
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Forum (Forum)'
            ));
            return false;
        }

    }

    return true;
}
function _forumInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_access` (
  `category_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `group_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `read` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `write` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `edit` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `delete` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `move` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `close` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `sticky` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`category_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'1\', \'0\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'1\', \'3\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'1\', \'4\', \'1\', \'1\', \'1\', \'1\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'1\', \'5\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'6\', \'0\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'6\', \'3\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'6\', \'4\', \'1\', \'1\', \'1\', \'1\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'6\', \'5\', \'1\', \'1\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'9\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'9\', \'3\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'9\', \'4\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_access` (`category_id`, `group_id`, `read`, `write`, `edit`, `delete`, `move`, `close`, `sticky`) VALUES (\'9\', \'5\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_categories` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `order_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `status` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories` (`id`, `parent_id`, `order_id`, `status`) VALUES (\'1\', \'0\', \'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories` (`id`, `parent_id`, `order_id`, `status`) VALUES (\'6\', \'1\', \'99\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories` (`id`, `parent_id`, `order_id`, `status`) VALUES (\'8\', \'0\', \'99\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories` (`id`, `parent_id`, `order_id`, `status`) VALUES (\'9\', \'8\', \'99\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_categories_lang` (
  `category_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `description` text NOT NULL,
  PRIMARY KEY (`category_id`,`lang_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories_lang` (`category_id`, `lang_id`, `name`, `description`) VALUES (\'1\', \'1\', \'Beispielskategorie\', \'Diese Kategorie soll als Beispiel dienen.\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories_lang` (`category_id`, `lang_id`, `name`, `description`) VALUES (\'1\', \'2\', \'Beispielskategorie\', \'Sample Category\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories_lang` (`category_id`, `lang_id`, `name`, `description`) VALUES (\'6\', \'1\', \'Forenregeln\', \'Beispielforum\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories_lang` (`category_id`, `lang_id`, `name`, `description`) VALUES (\'8\', \'2\', \'Rules\', \'Rules\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_categories_lang` (`category_id`, `lang_id`, `name`, `description`) VALUES (\'9\', \'2\', \'Readme - Sample Rules\', \'here you should read the sample rules of this board before starting topics\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_notification` (
  `category_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `thread_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `user_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `is_notified` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`category_id`,`thread_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_postings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `thread_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `prev_post_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `user_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `time_created` int(14) unsigned NOT NULL DEFAULT \'0\',
  `time_edited` int(14) unsigned NOT NULL DEFAULT \'0\',
  `is_locked` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `is_sticky` set(\'0\',\'1\') NOT NULL DEFAULT \'0\',
  `rating` int(11) NOT NULL DEFAULT \'0\',
  `views` int(10) unsigned NOT NULL DEFAULT \'0\',
  `icon` smallint(5) unsigned NOT NULL DEFAULT \'0\',
  `keywords` text NOT NULL,
  `subject` varchar(250) NOT NULL DEFAULT \'\',
  `content` text NOT NULL,
  `attachment` varchar(250) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`,`thread_id`,`prev_post_id`,`user_id`),
  FULLTEXT KEY `fulltext` (`keywords`,`subject`,`content`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_postings` (`id`, `category_id`, `thread_id`, `prev_post_id`, `user_id`, `time_created`, `time_edited`, `is_locked`, `is_sticky`, `rating`, `views`, `icon`, `keywords`, `subject`, `content`, `attachment`) VALUES (\'4\', \'6\', \'1\', \'0\', \'1\', \'1292234985\', \'1292235345\', \'\', \'\', \'0\', \'15\', \'1\', \'foren regeln, regeln,\', \'Forenregeln\', \'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\\r\\n\', \'\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_postings` (`id`, `category_id`, `thread_id`, `prev_post_id`, `user_id`, `time_created`, `time_edited`, `is_locked`, `is_sticky`, `rating`, `views`, `icon`, `keywords`, `subject`, `content`, `attachment`) VALUES (\'5\', \'6\', \'1\', \'4\', \'1\', \'1292947019\', \'0\', \'\', \'\', \'0\', \'2\', \'1\', \'board rules\', \'Board Rules\', \'here you should read the sample rules of this board.\\r\\n\', \'\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT \'0\',
  `post_id` int(11) NOT NULL DEFAULT \'0\',
  `time` int(11) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_settings` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT \'\',
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'1\', \'thread_paging\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'2\', \'posting_paging\', \'10\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'3\', \'latest_entries_count\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'4\', \'block_template\', \'<div id=\\\"forum\\\">    \\r\\n         <div class=\\\"div_board\\\">\\r\\n	         <div class=\\\"div_title\\\">[[TXT_FORUM_LATEST_ENTRIES]]</div>\\r\\n		<table cellspacing=\\\"0\\\" cellpadding=\\\"0\\\">\\r\\n			<tr class=\\\"row3\\\">\\r\\n				<th width=\\\"65%\\\" style=\\\"text-align: left;\\\">[[TXT_FORUM_THREAD]]</th>\\r\\n				<th width=\\\"15%\\\" style=\\\"text-align: left;\\\">[[TXT_FORUM_OVERVIEW_FORUM]]</th>		\\r\\n				<th width=\\\"15%\\\" style=\\\"text-align: left;\\\">[[TXT_FORUM_THREAD_STRATER]]</th>		\\r\\n				<th width=\\\"1%\\\" style=\\\"text-align: left;\\\">[[TXT_FORUM_POST_COUNT]]</th>		\\r\\n				<th width=\\\"4%\\\" style=\\\"text-align: left;\\\">[[TXT_FORUM_THREAD_CREATE_DATE]]</th>\\r\\n			</tr>\\r\\n			<!-- BEGIN latestPosts -->\\r\\n			<tr class=\\\"row_[[FORUM_ROWCLASS]]\\\">\\r\\n				<td>[[FORUM_THREAD]]</td>\\r\\n				<td>[[FORUM_FORUM_NAME]]</td>\\r\\n				<td>[[FORUM_THREAD_STARTER]]</td>\\r\\n				<td>[[FORUM_POST_COUNT]]</td>\\r\\n				<td>[[FORUM_THREAD_CREATE_DATE]]</td>\\r\\n			</tr>	\\r\\n			<!-- END latestPosts -->	\\r\\n		</table>\\r\\n	</div>\\r\\n</div>\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'5\', \'notification_template\', \'[[FORUM_USERNAME]],\\r\\n\\r\\nEs wurde ein neuer Beitrag im Thema \\\\\\\"[[FORUM_THREAD_SUBJECT]]\\\\\\\", gestartet \\r\\nvon \\\\\\\"[[FORUM_THREAD_STARTER]]\\\\\\\", geschrieben.\\r\\n\\r\\nDer neue Beitrag umfasst folgenden Inhalt:\\r\\n\\r\\n-----------------NACHRICHT START-----------------\\r\\n-----Betreff-----\\r\\n[[FORUM_LATEST_SUBJECT]]\\r\\n\\r\\n----Nachricht----\\r\\n[[FORUM_LATEST_MESSAGE]]\\r\\n-----------------NACHRICHT ENDE------------------\\r\\n\\r\\nUm den ganzen Diskussionsverlauf zu sehen oder zur Abmeldung dieser \\r\\nBenachrichtigung, besuchen Sie folgenden Link:\\r\\n[[FORUM_THREAD_URL]]\\r\\n\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'6\', \'notification_subject\', \'Neuer Beitrag in \\\\\\\"[[FORUM_THREAD_SUBJECT]]\\\\\\\"\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'7\', \'notification_from_email\', \'noreply@example.com\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'8\', \'notification_from_name\', \'nobody\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'9\', \'banned_words\', \'penis enlargement,free porn,(?i:buy\\\\\\\\s*?(?:cheap\\\\\\\\s*?)?viagra)\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'10\', \'wysiwyg_editor\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'11\', \'tag_count\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'12\', \'latest_post_per_thread\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_settings` (`id`, `name`, `value`) VALUES (\'13\', \'allowed_extensions\', \'7z,aiff,asf,avi,bmp,csv,doc,fla,flv,gif,gz,gzip,jpeg,jpg,mid,mov,mp3,mp4,mpc,mpeg,mpg,ods,odt,pdf,png,ppt,pxd,qt,ram,rar,rm,rmi,rmvb,rtf,sdc,sitd,swf,sxc,sxw,tar,tgz,tif,tiff,txt,vsd,wav,wma,wmv,xls,xml,zip\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_forum_statistics` (
  `category_id` int(5) unsigned NOT NULL DEFAULT \'0\',
  `thread_count` int(10) unsigned NOT NULL DEFAULT \'0\',
  `post_count` int(10) unsigned NOT NULL DEFAULT \'0\',
  `last_post_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_statistics` (`category_id`, `thread_count`, `post_count`, `last_post_id`) VALUES (\'6\', \'1\', \'2\', \'5\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_forum_statistics` (`category_id`, `thread_count`, `post_count`, `last_post_id`) VALUES (\'9\', \'0\', \'0\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}