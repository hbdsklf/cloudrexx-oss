<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _guestbookUpdate()
{
    global $objDatabase, $_ARRAYLANG, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {

        $arrGuestbookColumns = $objDatabase->MetaColumns(DBPREFIX.'module_guestbook');
        if ($arrGuestbookColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_guestbook'));
            return false;
        }

        if (isset($arrGuestbookColumns['NICKNAME']) and !isset($arrGuestbookColumns['NAME'])) {
            $query = "ALTER TABLE ".DBPREFIX."module_guestbook
                      CHANGE `nickname` `name` varchar(255) NOT NULL default ''";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        if (!isset($arrGuestbookColumns['FORENAME'])) {
            $query = "ALTER TABLE ".DBPREFIX."module_guestbook
                      ADD `forename` varchar(255) NOT NULL default '' AFTER `name`";
            $objResult = $objDatabase->Execute($query);
            if (!$objResult) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        // this addidional structure update/check is required due that the full version's structure isn't as it should be
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX . 'module_guestbook',
                array(
                    'id'        => array('type' => 'INT(6)', 'unsigned' => true, 'auto_increment' => true, 'primary' => true),
                    'status'    => array('type' => 'TINYINT(1)', 'unsigned' => true, 'default' => 0),
                    'name'      => array('type' => 'VARCHAR(255)'),
                    'forename'  => array('type' => 'VARCHAR(255)'),
                    'gender'    => array('type' => 'CHAR(1)', 'notnull' => true, 'default' => ''),
                    'url'       => array('type' => 'TINYTEXT'),
                    'email'     => array('type' => 'TINYTEXT'),
                    'comment'   => array('type' => 'TEXT'),
                    'location'  => array('type' => 'TINYTEXT'),
                    'lang_id'   => array('type' => 'TINYINT(2)', 'default' => '1'),
                    'datetime'  => array('type' => 'DATETIME', 'default' => '0000-00-00 00:00:00')            ),
                array(
                    'comment'   => array('fields' => array('comment'), 'type' => 'FULLTEXT')
                )
            );
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        /********************************
         * EXTENSION:   Timezone        *
         * ADDED:       Contrexx v3.0.0 *
         ********************************/
        try {
            \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_guestbook` CHANGE `datetime` `datetime` TIMESTAMP NOT NULL DEFAULT "0000-00-00 00:00:00"');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }


        /********************************
         * EXTENSION:   Captcha         *
         * ADDED:       Contrexx v3.0.0 *
         ********************************/
        try {
            // switch to source mode for guestbook content page
            \Cx\Lib\UpdateUtil::setSourceModeOnContentPage(array('module' => 'guestbook', 'cmd' => 'post'), '3.0.1');

            // migrate content page to version 3.0.1
            $search = array(
            '/(.*)/ms',
            );
            $callback = function($matches) {
                $content = $matches[1];
                if (empty($content)) {
                    return $content;
                }

                $content = str_replace(array('nickname', 'NICKNAME'), array('name', 'NAME'), $content);

                if (!preg_match('/<!--\s+BEGIN\s+guestbookForm\s+-->.*<!--\s+END\s+guestbookForm\s+-->/ms', $content)) {
                    $content = '<!-- BEGIN guestbookForm -->'.$content.'<!-- END guestbookForm -->';
                }
                if (!preg_match('/<!--\s+BEGIN\s+guestbookStatus\s+-->.*<!--\s+END\s+guestbookStatus\s+-->/ms', $content)) {
                    $content .= <<<STATUS_HTML
<!-- BEGIN guestbookStatus -->
{GUESTBOOK_STATUS}<br /><br />
<a href="index.php?section=guestbook">Zurück zum Gästebuch</a>
<!-- END guestbookStatus -->
STATUS_HTML;
                }

                if (!preg_match('/<!--\s+BEGIN\s+guestbook_captcha\s+-->.*<!--\s+END\s+guestbook_captcha\s+-->/ms', $content)) {
                    // migrate captcha stuff
                    $newCaptchaCode = <<<CAPTCHA_HTML
<!-- BEGIN guestbook_captcha -->
<p><label for="coreCaptchaCode">{TXT_GUESTBOOK_CAPTCHA}</label>{GUESTBOOK_CAPTCHA_CODE}</p>
<!-- END guestbook_captcha -->
CAPTCHA_HTML;
                    $content = preg_replace('/<[^>]+\{IMAGE_URL\}.*\{CAPTCHA_OFFSET\}[^>]+>/ms', $newCaptchaCode, $content);
                }

                // this adds the missing placeholders [[FEMALE_CHECKED]], [[MALE_CHECKED]]
                $pattern = '/(<input[^>]+name=[\'"]malefemale[\'"])([^>]*>)/ms';
                if (preg_match_all($pattern, $content, $match)) {
                    foreach ($match[0] as $idx => $input) {
                        // check if "checked"-placeholder is missing inputfield
                        if (!preg_match('/\{(FE)?MALE_CHECKED\}/ms', $input)) {
                            if (preg_match('/value\s*=\s*[\'"]F[\'"]/', $input)) {
                                $content = str_replace($input, $match[1][$idx].' {FEMALE_CHECKED} '.$match[2][$idx], $content);
                            } elseif (preg_match('/value\s*=\s*[\'"]M[\'"]/', $input)) {
                                $content = str_replace($input, $match[1][$idx].' {MALE_CHECKED} '.$match[2][$idx], $content);
                            }
                        }
                    }
                }

                return $content;
            };
            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'guestbook', 'cmd' => 'post'), $search, $callback, array('content'), '3.0.1');
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _guestbookInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_guestbook` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `forename` varchar(255) NOT NULL DEFAULT \'\',
  `gender` char(1) NOT NULL DEFAULT \'\',
  `url` tinytext NOT NULL,
  `email` tinytext NOT NULL,
  `comment` text NOT NULL,
  `location` tinytext NOT NULL,
  `lang_id` tinyint(2) NOT NULL DEFAULT \'1\',
  `datetime` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `comment` (`comment`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_guestbook` (`id`, `status`, `name`, `forename`, `gender`, `url`, `email`, `comment`, `location`, `lang_id`, `datetime`) VALUES (\'1\', \'1\', \'CLOUDREXX\', \'\', \'M\', \'https://www.cloudrexx.com/\', \'nospam@example.com\', \'This is a sample entry.\\r\\n\\r\\nsincerely yours\\r\\nCLOUDREXX AG\', \'Schweiz\', \'2\', \'2010-12-13 09:00:10\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_guestbook_settings` (
  `name` varchar(50) NOT NULL DEFAULT \'\',
  `value` varchar(250) NOT NULL DEFAULT \'\',
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_guestbook_settings` (`name`, `value`) VALUES (\'guestbook_send_notification_email\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_guestbook_settings` (`name`, `value`) VALUES (\'guestbook_activate_submitted_entries\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_guestbook_settings` (`name`, `value`) VALUES (\'guestbook_replace_at\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_guestbook_settings` (`name`, `value`) VALUES (\'guestbook_only_lang_entries\', \'0\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
