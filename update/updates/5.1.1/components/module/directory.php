<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

function _directoryUpdate() {
    global $objDatabase, $objUpdate, $_CONFIG, $_ARRAYLANG;

    /// 2.0

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_directory_dir');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_directory_dir'));
            return false;
        }

        $arrNewCols = array(
                    'LONGITUDE' => array(
                            'type'      => 'DECIMAL( 18, 15 )',
                            'default'   => '0',
                            'after'     => 'premium',
                        ),
                    'LATITUDE'  => array(
                            'type'      => 'DECIMAL( 18, 15 )',
                            'default'   => '0',
                            'after'     => 'longitude',
                        ),
                    'ZOOM'      => array(
                            'type'      => 'DECIMAL( 18, 15 )',
                            'default'   => '1',
                            'after'     => 'latitude',
                        ),
                    'COUNTRY'   => array(
                            'type'      => 'VARCHAR( 255 )',
                            'default'   => '',
                            'after'     => 'city',
                    ));
        foreach ($arrNewCols as $col => $arrAttr) {
            if (!isset($arrColumns[$col])) {
                $query = "ALTER TABLE `".DBPREFIX."module_directory_dir` ADD `".strtolower($col)."` ".$arrAttr['type']." NOT NULL DEFAULT '".$arrAttr['default']."' AFTER `".$arrAttr['after']."`";

                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }

        $inputColumns = '(`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`)';

        $arrInputs = array(
            69 => "INSERT INTO `".DBPREFIX."module_directory_inputfields` ".$inputColumns." VALUES (69, 13, 'googlemap', 'TXT_DIR_F_GOOGLEMAP', 1, 1, 0, 0, 6, 0, 0)",
            70 => "INSERT INTO `".DBPREFIX."module_directory_inputfields` ".$inputColumns." VALUES (70, 3, 'country', 'TXT_DIR_F_COUNTRY', 1, 1, 1, 0, 1, 0, 0)",
        );

        foreach ($arrInputs as $id => $queryInputs) {
            $query = "SELECT id FROM ".DBPREFIX."module_directory_inputfields WHERE id=".$id;
            $objCheck = $objDatabase->SelectLimit($query, 1);
            if ($objCheck !== false) {
                if ($objCheck->RecordCount() == 0) {
                    if ($objDatabase->Execute($queryInputs) === false) {
                        return _databaseError($queryInputs, $objDatabase->ErrorMsg());
                    }
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        $query = "SELECT setid FROM ".DBPREFIX."module_directory_settings WHERE setname='country'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =    "INSERT INTO `".DBPREFIX."module_directory_settings` ( `setid` , `setname` , `setvalue` ,  `settyp` )
                            VALUES (NULL, 'country', ',Schweiz,Deutschland,Österreich', 0)";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "ALTER TABLE `".DBPREFIX."module_directory_dir` CHANGE `spez_field_21` `spez_field_21` VARCHAR( 255 ) NOT NULL DEFAULT ''";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "ALTER TABLE `".DBPREFIX."module_directory_dir` CHANGE `spez_field_22` `spez_field_22` VARCHAR( 255 ) NOT NULL DEFAULT ''";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "SELECT setid FROM ".DBPREFIX."module_directory_settings WHERE setname='pagingLimit'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =    "INSERT INTO `".DBPREFIX."module_directory_settings` ( `setid` , `setname` , `setvalue` ,  `settyp` )
                            VALUES (NULL, 'pagingLimit', '20', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "SELECT setid FROM ".DBPREFIX."module_directory_settings WHERE setname='googlemap_start_location'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =    "INSERT INTO `".DBPREFIX."module_directory_settings` ( `setid` , `setname` , `setvalue` ,  `settyp` )
                            VALUES (NULL, 'googlemap_start_location', '46:8:1', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }


        /// 2.1

        $query = "SELECT setid FROM ".DBPREFIX."module_directory_settings WHERE setname='youtubeWidth'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =    "INSERT INTO `".DBPREFIX."module_directory_settings` ( `setid` , `setname` , `setvalue` ,  `settyp` )
                             VALUES (NULL , 'youtubeWidth', '400', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "SELECT setid FROM ".DBPREFIX."module_directory_settings WHERE setname='youtubeHeight'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =    "INSERT INTO `".DBPREFIX."module_directory_settings` ( `setid` , `setname` , `setvalue` ,  `settyp` )
                             VALUES (NULL , 'youtubeHeight', '300', '1')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $query = "SELECT id FROM ".DBPREFIX."module_directory_inputfields WHERE name='youtube'";
        $objCheck = $objDatabase->SelectLimit($query, 1);
        if ($objCheck !== false) {
            if ($objCheck->RecordCount() == 0) {
                $query =    "INSERT INTO `".DBPREFIX."module_directory_inputfields` (`id` ,`typ` ,`name` ,`title` ,`active` ,`active_backend` ,`is_required` ,`read_only` ,`sort` ,`exp_search` ,`is_search`)
                             VALUES (NULL , '1', 'youtube', 'TXT_DIRECTORY_YOUTUBE', '0', '0', '0', '0', '0', '0', '0')";
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        } else {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }

        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_directory_dir');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_directory_dir'));
            return false;
        }

        if (!array_key_exists("YOUTUBE", $arrColumns)) {
            $query = "ALTER TABLE `".DBPREFIX."module_directory_dir` ADD `youtube` MEDIUMTEXT NOT NULL;";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        $query   = "ALTER TABLE `".DBPREFIX."module_directory_dir`
                    CHANGE `logo` `logo` VARCHAR(50) NULL,
                    CHANGE `map` `map` VARCHAR(255) NULL,
                    CHANGE `lokal` `lokal` VARCHAR(255) NULL,
                    CHANGE `spez_field_11` `spez_field_11` VARCHAR(255) NULL,
                    CHANGE `spez_field_12` `spez_field_12` VARCHAR(255) NULL,
                    CHANGE `spez_field_13` `spez_field_13` VARCHAR(255) NULL,
                    CHANGE `spez_field_14` `spez_field_14` VARCHAR(255) NULL,
                    CHANGE `spez_field_15` `spez_field_15` VARCHAR(255) NULL,
                    CHANGE `spez_field_16` `spez_field_16` VARCHAR(255) NULL,
                    CHANGE `spez_field_17` `spez_field_17` VARCHAR(255) NULL,
                    CHANGE `spez_field_18` `spez_field_18` VARCHAR(255) NULL,
                    CHANGE `spez_field_19` `spez_field_19` VARCHAR(255) NULL,
                    CHANGE `spez_field_20` `spez_field_20` VARCHAR(255) NULL;";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }


        //delete obsolete table  contrexx_module_directory_access
        try {
            \Cx\Lib\UpdateUtil::drop_table(DBPREFIX.'module_directory_access');
        } catch (\Cx\Lib\UpdateException $e) {
            DBG::trace();
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        /********************************
         * EXTENSION:   Fulltext key    *
         * ADDED:       Contrexx v3.0.0 *
         ********************************/
        try {
            $objResult = \Cx\Lib\UpdateUtil::sql('SHOW KEYS FROM `'.DBPREFIX.'module_directory_categories` WHERE  `Key_name` = "directoryindex" and (`Column_name`= "name" OR `Column_name` = "description")');
            if ($objResult && ($objResult->RecordCount() == 0)) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_directory_categories` ADD FULLTEXT KEY `directoryindex` (`name`, `description`)');
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }



        /**********************************
         * EXTENSION:   Content Migration *
         * ADDED:       Contrexx v3.0.0   *
         **********************************/
        try {
            // migrate content page to version 3.0.1
            $search = array(
            '/(.*)/ms',
            );
            $callback = function($matches) {
                $content = $matches[1];
                if (empty($content)) {
                    return $content;
                }

                // add missing placeholder {DIRECTORY_GOOGLEMAP_JAVASCRIPT_BLOCK}
                if (strpos($content, '{DIRECTORY_GOOGLEMAP_JAVASCRIPT_BLOCK}') === false) {
                    $content .= "\n{DIRECTORY_GOOGLEMAP_JAVASCRIPT_BLOCK}";
                }

                // move placeholder {DIRECTORY_JAVASCRIPT} to the end of the content page
                $content = str_replace('{DIRECTORY_JAVASCRIPT}', '', $content);
                $content .= "\n{DIRECTORY_JAVASCRIPT}";

                return $content;
            };

            \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'directory'), $search, $callback, array('content'), '3.0.1');
        }
        catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_directory_dir', 'ip')) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_directory_dir` DROP COLUMN `ip`');
            }
            if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX.'module_directory_dir', 'provider')) {
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `'.DBPREFIX.'module_directory_dir` DROP COLUMN `provider`');
            }
            \Cx\Lib\UpdateUtil::sql('UPDATE `' . DBPREFIX. 'module_directory_dir` SET `lastip` = MD5(`lastip`) WHERE CHAR_LENGTH(`lastip`) < 30 AND `lastip` != \'\'');
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.1')) {
        try {
            // fetch index infos
            $structure = \Cx\Lib\UpdateUtil::sql('SHOW INDEX FROM `'.DBPREFIX.'module_directory_dir`');
            if ($structure === false) {
                setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_directory_dir'));
                return false;
            }

            $indexIsOk = false;
            $indexExists = false;
            while (!$structure->EOF) {
                // fetch unique keys
                if (
                    empty($structure->fields['Key_name']) ||
                    $structure->fields['Key_name'] != 'title'
                ) {
                    $structure->MoveNext();
                    continue;
                }

                // drop obsolete index
                \Cx\Lib\UpdateUtil::sql('ALTER TABLE `' . DBPREFIX . 'module_directory_dir` DROP INDEX `title`');
                break;
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }


    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        //Update script for moving the folder
        $mediaPath       = ASCMS_DOCUMENT_ROOT . '/media';
        $sourceMediaPath = $mediaPath . '/directory';
        $targetMediaPath = $mediaPath . '/Directory';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceMediaPath, $targetMediaPath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceMediaPath, $targetMediaPath
            ));
            return false;
        }
        // update script for dropped table
        try {
            if (\Cx\Lib\UpdateUtil::table_exist(DBPREFIX.'module_directory_settings_google')) {
                \Cx\Lib\UpdateUtil::drop_table(DBPREFIX.'module_directory_settings_google');
            }
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }
    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        $attributes = array(
            'attachment'    => 'module_directory_dir',
            'logo'          => 'module_directory_dir',
            'map'           => 'module_directory_dir',
            'lokal'         => 'module_directory_dir',
            'spez_field_1'  => 'module_directory_dir',
            'spez_field_2'  => 'module_directory_dir',
            'spez_field_3'  => 'module_directory_dir',
            'spez_field_4'  => 'module_directory_dir',
            'spez_field_5'  => 'module_directory_dir',
            'spez_field_6'  => 'module_directory_dir',
            'spez_field_7'  => 'module_directory_dir',
            'spez_field_8'  => 'module_directory_dir',
            'spez_field_9'  => 'module_directory_dir',
            'spez_field_10' => 'module_directory_dir',
            'spez_field_11' => 'module_directory_dir',
            'spez_field_12' => 'module_directory_dir',
            'spez_field_13' => 'module_directory_dir',
            'spez_field_14' => 'module_directory_dir',
            'spez_field_15' => 'module_directory_dir',
            'spez_field_16' => 'module_directory_dir',
            'spez_field_17' => 'module_directory_dir',
            'spez_field_18' => 'module_directory_dir',
            'spez_field_19' => 'module_directory_dir',
            'spez_field_20' => 'module_directory_dir',
            'spez_field_25' => 'module_directory_dir',
            'spez_field_26' => 'module_directory_dir',
            'spez_field_27' => 'module_directory_dir',
            'spez_field_28' => 'module_directory_dir',
            'spez_field_29' => 'module_directory_dir',
            'content'       => 'module_directory_mail',
            'description'   => 'module_directory_categories',
            'description'   => 'module_directory_levels',
        );
        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table . '`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Verzeichnis (Directory)'
            ));
            return false;
        }
    }

    return true;
}
function _directoryInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_categories` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` int(6) unsigned NOT NULL DEFAULT \'0\',
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `description` varchar(250) NOT NULL DEFAULT \'\',
  `displayorder` smallint(6) unsigned NOT NULL DEFAULT \'1000\',
  `metadesc` varchar(250) NOT NULL DEFAULT \'\',
  `metakeys` varchar(250) NOT NULL DEFAULT \'\',
  `showentries` int(1) NOT NULL DEFAULT \'1\',
  `status` int(1) NOT NULL DEFAULT \'1\',
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `parentid` (`parentid`),
  KEY `displayorder` (`displayorder`),
  KEY `status` (`status`),
  FULLTEXT KEY `directoryindex` (`name`,`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_dir` (
  `id` int(7) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT \'\',
  `attachment` varchar(255) NOT NULL DEFAULT \'\',
  `rss_file` varchar(255) NOT NULL DEFAULT \'\',
  `rss_link` varchar(255) NOT NULL DEFAULT \'\',
  `link` varchar(255) NOT NULL DEFAULT \'\',
  `date` varchar(14) DEFAULT NULL,
  `description` mediumtext NOT NULL,
  `platform` varchar(40) NOT NULL DEFAULT \'\',
  `language` varchar(40) NOT NULL DEFAULT \'\',
  `relatedlinks` varchar(255) NOT NULL DEFAULT \'\',
  `hits` int(9) NOT NULL DEFAULT \'0\',
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  `addedby` varchar(50) NOT NULL DEFAULT \'\',
  `validatedate` varchar(14) NOT NULL DEFAULT \'\',
  `lastip` varchar(50) NOT NULL DEFAULT \'\',
  `popular_date` varchar(30) NOT NULL DEFAULT \'\',
  `popular_hits` int(7) NOT NULL DEFAULT \'0\',
  `xml_refresh` varchar(15) NOT NULL DEFAULT \'\',
  `canton` varchar(50) NOT NULL DEFAULT \'\',
  `searchkeys` varchar(255) NOT NULL DEFAULT \'\',
  `company_name` varchar(100) NOT NULL DEFAULT \'\',
  `street` varchar(255) NOT NULL DEFAULT \'\',
  `zip` varchar(5) NOT NULL DEFAULT \'\',
  `city` varchar(50) NOT NULL DEFAULT \'\',
  `country` varchar(255) NOT NULL DEFAULT \'\',
  `phone` varchar(20) NOT NULL DEFAULT \'\',
  `contact` varchar(100) NOT NULL DEFAULT \'\',
  `information` varchar(100) NOT NULL DEFAULT \'\',
  `fax` varchar(20) NOT NULL DEFAULT \'\',
  `mobile` varchar(20) NOT NULL DEFAULT \'\',
  `mail` varchar(50) NOT NULL DEFAULT \'\',
  `homepage` varchar(50) NOT NULL DEFAULT \'\',
  `industry` varchar(100) NOT NULL DEFAULT \'\',
  `legalform` varchar(50) NOT NULL DEFAULT \'\',
  `conversion` varchar(50) NOT NULL DEFAULT \'\',
  `employee` varchar(255) NOT NULL DEFAULT \'\',
  `foundation` varchar(10) NOT NULL DEFAULT \'\',
  `mwst` varchar(50) NOT NULL DEFAULT \'\',
  `opening` varchar(255) NOT NULL DEFAULT \'\',
  `holidays` varchar(255) NOT NULL DEFAULT \'\',
  `places` varchar(255) NOT NULL DEFAULT \'\',
  `logo` varchar(50) DEFAULT NULL,
  `team` varchar(255) NOT NULL DEFAULT \'\',
  `portfolio` varchar(255) NOT NULL DEFAULT \'\',
  `offers` varchar(255) NOT NULL DEFAULT \'\',
  `concept` varchar(255) NOT NULL DEFAULT \'\',
  `map` varchar(255) DEFAULT NULL,
  `lokal` varchar(255) DEFAULT NULL,
  `spezial` int(4) NOT NULL DEFAULT \'0\',
  `premium` int(1) NOT NULL DEFAULT \'0\',
  `longitude` decimal(18,15) NOT NULL DEFAULT \'0.000000000000000\',
  `latitude` decimal(18,15) NOT NULL DEFAULT \'0.000000000000000\',
  `zoom` decimal(18,15) NOT NULL DEFAULT \'1.000000000000000\',
  `spez_field_1` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_2` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_3` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_4` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_5` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_6` mediumtext NOT NULL,
  `spez_field_7` mediumtext NOT NULL,
  `spez_field_8` mediumtext NOT NULL,
  `spez_field_9` mediumtext NOT NULL,
  `spez_field_10` mediumtext NOT NULL,
  `spez_field_11` varchar(255) DEFAULT NULL,
  `spez_field_12` varchar(255) DEFAULT NULL,
  `spez_field_13` varchar(255) DEFAULT NULL,
  `spez_field_14` varchar(255) DEFAULT NULL,
  `spez_field_15` varchar(255) DEFAULT NULL,
  `spez_field_21` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_22` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_16` varchar(255) DEFAULT NULL,
  `spez_field_17` varchar(255) DEFAULT NULL,
  `spez_field_18` varchar(255) DEFAULT NULL,
  `spez_field_19` varchar(255) DEFAULT NULL,
  `spez_field_20` varchar(255) DEFAULT NULL,
  `spez_field_23` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_24` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_25` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_26` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_27` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_28` varchar(255) NOT NULL DEFAULT \'\',
  `spez_field_29` varchar(255) NOT NULL DEFAULT \'\',
  `youtube` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `date` (`date`),
  KEY `temphitsout` (`hits`),
  KEY `status` (`status`),
  FULLTEXT KEY `name` (`title`,`description`),
  FULLTEXT KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_inputfields` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `typ` int(2) NOT NULL DEFAULT \'0\',
  `name` varchar(255) NOT NULL DEFAULT \'\',
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `active` int(1) NOT NULL DEFAULT \'0\',
  `active_backend` int(1) NOT NULL DEFAULT \'0\',
  `is_required` int(11) NOT NULL DEFAULT \'0\',
  `read_only` int(1) NOT NULL DEFAULT \'0\',
  `sort` int(5) NOT NULL DEFAULT \'0\',
  `exp_search` int(1) NOT NULL DEFAULT \'0\',
  `is_search` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'1\', \'1\', \'title\', \'TXT_DIR_F_TITLE\', \'1\', \'1\', \'1\', \'0\', \'1\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'2\', \'2\', \'description\', \'TXT_DIR_F_DESCRIPTION\', \'1\', \'1\', \'1\', \'0\', \'2\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'3\', \'3\', \'platform\', \'TXT_DIR_F_PLATFORM\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'4\', \'3\', \'language\', \'TXT_DIR_F_LANG\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'5\', \'1\', \'addedby\', \'TXT_DIR_F_ADDED_BY\', \'1\', \'1\', \'1\', \'0\', \'6\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'6\', \'1\', \'relatedlinks\', \'TXT_DIR_F_RELATED_LINKS\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'7\', \'3\', \'canton\', \'TXT_DIR_F_CANTON\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'8\', \'2\', \'searchkeys\', \'TXT_DIR_F_SEARCH_KEYS\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'9\', \'1\', \'company_name\', \'TXT_DIR_F_CO_NAME\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'10\', \'1\', \'street\', \'TXT_DIR_F_STREET\', \'1\', \'1\', \'0\', \'0\', \'2\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'11\', \'1\', \'zip\', \'TXT_DIR_F_PLZ\', \'1\', \'1\', \'0\', \'0\', \'3\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'12\', \'1\', \'phone\', \'TXT_DIR_F_PHONE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'13\', \'1\', \'contact\', \'TXT_DIR_F_PERSON\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'14\', \'1\', \'city\', \'TXT_DIR_CITY\', \'1\', \'1\', \'0\', \'0\', \'4\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'15\', \'1\', \'information\', \'TXT_INFOZEILE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'16\', \'1\', \'fax\', \'TXT_TELEFAX\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'17\', \'1\', \'mobile\', \'TXT_MOBILE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'18\', \'1\', \'mail\', \'TXT_DIR_F_EMAIL\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'19\', \'1\', \'homepage\', \'TXT_HOMEPAGE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'20\', \'1\', \'industry\', \'TXT_BRANCHE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'21\', \'1\', \'legalform\', \'TXT_RECHTSFORM\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'22\', \'2\', \'conversion\', \'TXT_UMSATZ\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'23\', \'2\', \'employee\', \'TXT_MITARBEITER\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'24\', \'1\', \'foundation\', \'TXT_GRUENDUNGSJAHR\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'25\', \'1\', \'mwst\', \'TXT_MWST_NR\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'26\', \'2\', \'opening\', \'TXT_OEFFNUNGSZEITEN\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'27\', \'2\', \'holidays\', \'TXT_BETRIEBSFERIEN\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'28\', \'2\', \'places\', \'TXT_SUCHORTE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'29\', \'4\', \'logo\', \'TXT_LOGO\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'30\', \'2\', \'team\', \'TXT_TEAM\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'32\', \'2\', \'portfolio\', \'TXT_REFERENZEN\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'33\', \'2\', \'offers\', \'TXT_ANGEBOTE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'34\', \'2\', \'concept\', \'TXT_KONZEPT\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'35\', \'4\', \'map\', \'TXT_MAP\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'36\', \'4\', \'lokal\', \'TXT_LOKAL\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'37\', \'5\', \'spez_field_1\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'38\', \'5\', \'spez_field_2\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'39\', \'5\', \'spez_field_3\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'40\', \'5\', \'spez_field_4\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'41\', \'5\', \'spez_field_5\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'42\', \'6\', \'spez_field_6\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'43\', \'6\', \'spez_field_7\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'44\', \'6\', \'spez_field_8\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'45\', \'6\', \'spez_field_9\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'46\', \'6\', \'spez_field_10\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'47\', \'7\', \'spez_field_11\', \'Screenshot\', \'1\', \'1\', \'1\', \'0\', \'5\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'48\', \'7\', \'spez_field_12\', \'Grafik\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'49\', \'7\', \'spez_field_13\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'50\', \'7\', \'spez_field_14\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'51\', \'7\', \'spez_field_15\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'52\', \'8\', \'spez_field_21\', \'Land\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'53\', \'8\', \'spez_field_22\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'54\', \'7\', \'spez_field_18\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'55\', \'7\', \'spez_field_19\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'56\', \'7\', \'spez_field_20\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'57\', \'9\', \'spez_field_23\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'58\', \'9\', \'spez_field_24\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'1\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'59\', \'10\', \'spez_field_25\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'60\', \'10\', \'spez_field_26\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'61\', \'10\', \'spez_field_27\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'62\', \'10\', \'spez_field_28\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'63\', \'10\', \'spez_field_29\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'64\', \'7\', \'spez_field_16\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'65\', \'7\', \'spez_field_17\', \'\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'66\', \'1\', \'link\', \'TXT_DIRECTORY_LINK\', \'1\', \'1\', \'1\', \'0\', \'3\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'67\', \'11\', \'attachment\', \'TXT_DIRECTORY_ATTACHMENT\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'68\', \'12\', \'rss_link\', \'TXT_DIRECTORY_RSS_FEED\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'69\', \'13\', \'googlemap\', \'TXT_DIR_F_GOOGLEMAP\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'70\', \'3\', \'country\', \'TXT_DIR_F_COUNTRY\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_inputfields` (`id`, `typ`, `name`, `title`, `active`, `active_backend`, `is_required`, `read_only`, `sort`, `exp_search`, `is_search`) VALUES (\'71\', \'1\', \'youtube\', \'TXT_DIRECTORY_YOUTUBE\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_levels` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `parentid` int(7) NOT NULL DEFAULT \'0\',
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `description` varchar(255) NOT NULL DEFAULT \'\',
  `metadesc` varchar(100) NOT NULL DEFAULT \'\',
  `metakeys` varchar(100) NOT NULL DEFAULT \'\',
  `displayorder` int(7) NOT NULL DEFAULT \'0\',
  `showlevels` int(1) NOT NULL DEFAULT \'0\',
  `showcategories` int(1) NOT NULL DEFAULT \'0\',
  `onlyentries` int(1) NOT NULL DEFAULT \'0\',
  `status` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  KEY `displayorder` (`displayorder`),
  KEY `parentid` (`parentid`),
  KEY `name` (`name`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_mail` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `content` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_mail` (`id`, `title`, `content`) VALUES (\'1\', \'[[URL]] - Eintrag aufgeschaltet\', \'Hallo [[FIRSTNAME]] [[LASTNAME]] ([[USERNAME]])\\r\\n\\r\\nDein Eintrag mit dem Titel \\\"[[TITLE]]\\\" wurde auf [[URL]] erfolgreich aufgeschaltet. \\r\\n\\r\\nBenutze folgenden Link um direkt zu Deinem Eintrag zu gelangen:\\r\\n[[LINK]]\\r\\n\\r\\nMit freundlichen Grüssen\\r\\n[[URL]] - Team\\r\\n\\r\\n[[DATE]]\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_mail` (`id`, `title`, `content`) VALUES (\'2\', \'[[URL]] - Neuer Eintrag\', \'Hallo Admin\\r\\n\\r\\nAuf [[URL]] wurde ein Eintrag aufgeschaltet oder editiert. Bitte überprüfen Sie diesen und Bestätigen Sie ihn falls nötig.\\r\\n\\r\\nEintrag Details:\\r\\n\\r\\nTitel: [[TITLE]]\\r\\nBenutzername: [[USERNAME]]\\r\\nVorname: [[FIRSTNAME]]\\r\\nNachname:[[LASTNAME]]\\r\\nLink: [[LINK]]\\r\\n\\r\\nAutomatisch generierte Nachricht\\r\\n[[DATE]]\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_rel_dir_cat` (
  `dir_id` int(7) NOT NULL DEFAULT \'0\',
  `cat_id` int(7) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`dir_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_rel_dir_level` (
  `dir_id` int(7) NOT NULL DEFAULT \'0\',
  `level_id` int(7) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`dir_id`,`level_id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_settings` (
  `setid` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `setname` varchar(250) NOT NULL DEFAULT \'\',
  `setvalue` text NOT NULL,
  `settyp` int(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`setid`),
  KEY `setname` (`setname`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'1\', \'levels\', \'0\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'5\', \'xmlLimit\', \'10\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'6\', \'platform\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'7\', \'language\', \',Deutsch,English,Italian,French\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'10\', \'latest_content\', \'3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'11\', \'latest_xml\', \'10\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'12\', \'entryStatus\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'13\', \'description\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'14\', \'populardays\', \'7\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'16\', \'canton\', \',Aargau,Appenzell-Ausserrhoden,Appenzell-Innerrhoden,Basel-Land,\\r\\nBasel-Stadt,Bern,Freiburg,Genf,Glarus,Graubünden,Jura,Luzern,\\r\\nNeuenburg,Nidwalden,Obwalden,St. Gallen,Schaffhausen,Schwyz,\\r\\nSolothurn,Thurgau,Tessin,Uri,Waadt,Wallis,Zug,Zürich\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'17\', \'refreshfeeds\', \'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'22\', \'mark_new_entrees\', \'7\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'23\', \'showConfirm\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'26\', \'addFeed\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'27\', \'addFeed_only_community\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'28\', \'editFeed\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'29\', \'editFeed_status\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'30\', \'adminMail\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'31\', \'indexview\', \'0\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'32\', \'spez_field_21\', \',Germany,\\r\\nSwitzerland,\\r\\nAustria,\\r\\nLiechtenstein,\\r\\nUnited States,\\r\\nAlbania,\\r\\nAlgeria,\\r\\nAndorra,\\r\\nAngola,\\r\\nAnguilla,\\r\\nAntigua and Barbuda,\\r\\nArgentina,\\r\\nArmenia,\\r\\nAruba,\\r\\nAustralia,\\r\\nAzerbaijan Republic,\\r\\nBahamas,\\r\\nBahrain,\\r\\nBarbados,\\r\\nBelgium,\\r\\nBelize,\\r\\nBenin,\\r\\nBermuda,\\r\\nBhutan,\\r\\nBolivia,\\r\\nBosnia and Herzegovina,\\r\\nBotswana,\\r\\nBrazil,\\r\\nBritish Virgin Islands,\\r\\nBrunei,\\r\\nBulgaria,\\r\\nBurkina Faso,\\r\\nBurundi,\\r\\nCambodia,\\r\\nCanada,\\r\\nCape Verde,\\r\\nCayman Islands,\\r\\nChad,\\r\\nChile,\\r\\nChina Worldwide,\\r\\nColombia,\\r\\nComoros,\\r\\nCook Islands,\\r\\nCosta Rica,\\r\\nCroatia,\\r\\nCyprus,\\r\\nCzech Republic,\\r\\nDemocratic Republic of the Congo,\\r\\nDenmark,\\r\\nDjibouti,\\r\\nDominica,\\r\\nDominican Republic,\\r\\nEcuador,\\r\\nEl Salvador,\\r\\nEritrea,\\r\\nEstonia,\\r\\nEthiopia,\\r\\nFalkland Islands,\\r\\nFaroe Islands,\\r\\nFederated States of Micronesia,\\r\\nFiji,\\r\\nFinland,\\r\\nFrance,\\r\\nFrench Guiana,\\r\\nFrench Polynesia,\\r\\nGabon Republic,\\r\\nGambia,\\r\\nGibraltar,\\r\\nGreece,\\r\\nGreenland,\\r\\nGrenada,\\r\\nGuadeloupe,\\r\\nGuatemala,\\r\\nGuinea,\\r\\nGuinea Bissau,\\r\\nGuyana,\\r\\nHonduras,\\r\\nHong Kong,\\r\\nHungary,\\r\\nIceland,\\r\\nIndia,\\r\\nIndonesia,\\r\\nIreland,\\r\\nIsrael,\\r\\nItaly,\\r\\nJamaica,\\r\\nJapan,\\r\\nJordan,\\r\\nKazakhstan,\\r\\nKenya,\\r\\nKiribati,\\r\\nKuwait,\\r\\nKyrgyzstan,\\r\\nLaos,\\r\\nLatvia,\\r\\nLesotho,\\r\\nLithuania,\\r\\nLuxembourg,\\r\\nMadagascar,\\r\\nMalawi,\\r\\nMalaysia,\\r\\nMaldives,\\r\\nMali,\\r\\nMalta,\\r\\nMarshall Islands,\\r\\nMartinique,\\r\\nMauritania,\\r\\nMauritius,\\r\\nMayotte,\\r\\nMexico,\\r\\nMongolia,\\r\\nMontserrat,\\r\\nMorocco,\\r\\nMozambique,\\r\\nNamibia,\\r\\nNauru,\\r\\nNepal,\\r\\nNetherlands,\\r\\nNetherlands Antilles,\\r\\nNew Caledonia,\\r\\nNew Zealand,\\r\\nNicaragua,\\r\\nNiger,\\r\\nNiue,\\r\\nNorfolk Island,\\r\\nNorway,\\r\\nOman,\\r\\nPalau,\\r\\nPanama,\\r\\nPapua New Guinea,\\r\\nPeru,\\r\\nPhilippines,\\r\\nPitcairn Islands,\\r\\nPoland,\\r\\nPortugal,\\r\\nQatar,\\r\\nRepublic of the Congo,\\r\\nReunion,\\r\\nRomania,\\r\\nRussia,\\r\\nRwanda,\\r\\nSaint Vincent and the Grenadines,\\r\\nSamoa,\\r\\nSan Marino,\\r\\nSão Tomé and Príncipe,\\r\\nSaudi Arabia,\\r\\nSenegal,\\r\\nSeychelles,\\r\\nSierra Leone,\\r\\nSingapore,\\r\\nSlovakia,\\r\\nSlovenia,\\r\\nSolomon Islands,\\r\\nSomalia,\\r\\nSouth Africa,\\r\\nSouth Korea,\\r\\nSpain,\\r\\nSri Lanka,\\r\\nSt. Helena,\\r\\nSt. Kitts and Nevis,\\r\\nSt. Lucia,\\r\\nSt. Pierre and Miquelon,\\r\\nSuriname,\\r\\nSvalbard and Jan Mayen Islands,\\r\\nSwaziland,\\r\\nSweden,\\r\\nTaiwan,\\r\\nTajikistan,\\r\\nTanzania,\\r\\nThailand,\\r\\nTogo,\\r\\nTonga,\\r\\nTrinidad and Tobago,\\r\\nTunisia,\\r\\nTurkey,\\r\\nTurkmenistan,\\r\\nTurks and Caicos Islands,\\r\\nTuvalu,\\r\\nUganda,\\r\\nUkraine,\\r\\nUnited Arab Emirates,\\r\\nUnited Kingdom,\\r\\nUruguay,\\r\\nVanuatu,\\r\\nVatican City State,\\r\\nVenezuela,\\r\\nVietnam,\\r\\nWallis and Futuna Islands,\\r\\nYemen,\\r\\nZambia\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'33\', \'spez_field_22\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'34\', \'thumbSize\', \'120\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'35\', \'sortOrder\', \'0\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'36\', \'spez_field_23\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'37\', \'spez_field_24\', \'\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'38\', \'encodeFilename\', \'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'39\', \'country\', \',Schweiz,Deutschland,Österreich,Weltweit\', \'0\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'40\', \'pagingLimit\', \'4\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'41\', \'youtubeWidth\', \'20\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'42\', \'youtubeHeight\', \'300\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'43\', \'youtubeWidth\', \'400\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_directory_settings` (`setid`, `setname`, `setvalue`, `settyp`) VALUES (\'44\', \'youtubeHeight\', \'300\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_directory_vote` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `feed_id` int(7) NOT NULL DEFAULT \'0\',
  `vote` int(2) NOT NULL DEFAULT \'0\',
  `count` int(7) NOT NULL DEFAULT \'0\',
  `client` varchar(255) NOT NULL DEFAULT \'\',
  `time` varchar(20) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}
