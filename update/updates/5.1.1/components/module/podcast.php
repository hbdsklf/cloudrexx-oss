<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _podcastUpdate() {
    global $objDatabase, $_ARRAYLANG, $objUpdate, $_CONFIG;

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '3.0.0')) {
        //move podcast images directory
        $path = ASCMS_DOCUMENT_ROOT . '/images';
        $oldImagesPath = '/content/podcast';
        $newImagesPath = '/podcast';

        if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '1.2.1')) {
            if (   !file_exists($path . $newImagesPath)
                && file_exists($path . $oldImagesPath)
            ) {
                \Cx\Lib\FileSystem\FileSystem::makeWritable($path . $oldImagesPath);
                if (!\Cx\Lib\FileSystem\FileSystem::copy_folder($path . $oldImagesPath, $path . $newImagesPath)) {
                    setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'], $path . $oldImagesPath, $path . $newImagesPath));
                    return false;
                }
            }
            \Cx\Lib\FileSystem\FileSystem::makeWritable($path . $newImagesPath);
            \Cx\Lib\FileSystem\FileSystem::makeWritable($path . $newImagesPath . '/youtube_thumbnails');

            //change thumbnail paths
            $query = "UPDATE `" . DBPREFIX . "module_podcast_medium` SET `thumbnail` = REPLACE(`thumbnail`, '/images/content/podcast/', '/images/podcast/')";
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        //set new default settings
        $query = "UPDATE `" . DBPREFIX . "module_podcast_settings` SET `setvalue` = '50' WHERE `setname` = 'thumb_max_size' AND `setvalue` = ''";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }
        $query = "UPDATE `" . DBPREFIX . "module_podcast_settings` SET `setvalue` = '85' WHERE `setname` = 'thumb_max_size_homecontent' AND `setvalue` = ''";
        if ($objDatabase->Execute($query) === false) {
            return _databaseError($query, $objDatabase->ErrorMsg());
        }



        // only update if installed version is at least a version 2.0.0
        // older versions < 2.0 have a complete other structure of the content page and must therefore completely be reinstalled
        if (!$objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '2.0.0')) {
            try {
                // migrate content page to version 3.0.1
                $search = array(
                '/(.*)/ms',
                );
                $callback = function($matches) {
                    $content = $matches[1];
                    if (empty($content)) {
                        return $content;
                    }

                    // add missing placeholder {PODCAST_JAVASCRIPT}
                    if (strpos($content, '{PODCAST_JAVASCRIPT}') === false) {
                        $content .= "\n{PODCAST_JAVASCRIPT}";
                    }

                    // add missing placeholder {PODCAST_PAGING}
                    if (strpos($content, '{PODCAST_PAGING}') === false) {
                        $content = preg_replace('/(\s+)(<!--\s+END\s+podcast_media\s+-->)/ms', '$1$2$1<div class="noMedium">$1    {PODCAST_PAGING}$1</div>', $content);
                    }

                    return $content;
                };

                \Cx\Lib\UpdateUtil::migrateContentPageUsingRegexCallback(array('module' => 'podcast'), $search, $callback, array('content'), '3.0.1');
            } catch (\Cx\Lib\UpdateException $e) {
                return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
            }
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.0.0')) {
        try {
            \Cx\Lib\UpdateUtil::table(
                DBPREFIX.'module_podcast_template',
                array(
                    'id'             => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'auto_increment' => true, 'primary' => true),
                    'description'    => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'id'),
                    'template'       => array('type' => 'text', 'after' => 'description'),
                    'extensions'     => array('type' => 'VARCHAR(255)', 'notnull' => true, 'default' => '', 'after' => 'template')
                ),
                array(
                    'description'    => array('fields' => array('description'), 'type' => 'UNIQUE')
                )
            );

    // TODO: ask user to confirm this change
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_podcast_template` SET `template` = '<iframe width=\"[[MEDIUM_WIDTH]]\" height=\"[[MEDIUM_HEIGHT]]\" src=\"[[MEDIUM_URL]]\" frameborder=\"0\" allowfullscreen></iframe>' WHERE `description` = 'YouTube Video'");

            // Update the thumbnail path from images/podcast into images/Podcast
            \Cx\Lib\UpdateUtil::sql("UPDATE `".DBPREFIX."module_podcast_medium`
                                     SET `thumbnail` = REPLACE(`thumbnail`, 'images/podcast', 'images/Podcast')
                                     WHERE `thumbnail` LIKE ('".ASCMS_PATH_OFFSET."/images/podcast%')");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }

        //Update script for moving the folder
        $imagePath       = ASCMS_DOCUMENT_ROOT . '/images';
        $sourceImagePath = $imagePath . '/podcast';
        $targetImagePath = $imagePath . '/Podcast';

        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceImagePath, $targetImagePath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceImagePath, $targetImagePath
            ));
            return false;
        }

        $mediaPath       = ASCMS_DOCUMENT_ROOT . '/media';
        $sourceMediaPath = $mediaPath . '/podcast';
        $targetMediaPath = $mediaPath . '/Podcast';
        try {
            \Cx\Lib\UpdateUtil::migrateOldDirectory($sourceMediaPath, $targetMediaPath);
        } catch (\Exception $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MOVE_DIRECTORY'],
                $sourceMediaPath, $targetMediaPath
            ));
            return false;
        }

        // migrate path to images and media
        $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
        $attributes = array(
            'source'    => 'module_podcast_medium',
            'thumbnail' => 'module_podcast_medium',
            'setvalue'  => 'module_podcast_settings',
            'template'  => 'module_podcast_template',
        );
        try {
            foreach ($attributes as $attribute => $table) {
                foreach ($pathsToMigrate as $oldPath => $newPath) {
                    \Cx\Lib\UpdateUtil::migratePath(
                        '`' . DBPREFIX . $table .'`',
                        '`' . $attribute . '`',
                        $oldPath,
                        $newPath
                    );
                }
            }
        } catch (\Cx\Lib\Update_DatabaseException $e) {
            \DBG::log($e->getMessage());
            setUpdateMsg(sprintf(
                $_ARRAYLANG['TXT_UNABLE_TO_MIGRATE_MEDIA_PATH'],
                'Podcast (Podcast)'
            ));
            return false;
        }
    }

    if ($objUpdate->_isNewerVersion($_CONFIG['coreCmsVersion'], '5.1.1')) {
        try {
            \Cx\Lib\UpdateUtil::sql("
                UPDATE `".DBPREFIX."module_podcast_template`
                SET `template` = replace(
                    `template`,
                    '/modules/podcast/lib/',
                    '/modules/Podcast/View/Script/'
                )
                WHERE `template` LIKE '%/modules/podcast/lib/%'
            ");
        } catch (\Cx\Lib\UpdateException $e) {
            return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
        }
    }

    return true;
}
function _podcastInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_podcast_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `description` varchar(255) NOT NULL DEFAULT \'\',
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `podcastindex` (`title`,`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_category` (`id`, `title`, `description`, `status`) VALUES (\'1\', \'Cloudrexx\', \'Cloudrexx Video\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_podcast_medium` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT \'\',
  `youtube_id` varchar(25) NOT NULL DEFAULT \'\',
  `author` varchar(255) NOT NULL DEFAULT \'\',
  `description` text NOT NULL,
  `source` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL DEFAULT \'\',
  `template_id` int(11) unsigned NOT NULL DEFAULT \'0\',
  `width` int(10) unsigned NOT NULL DEFAULT \'0\',
  `height` int(10) unsigned NOT NULL DEFAULT \'0\',
  `playlenght` int(10) unsigned NOT NULL DEFAULT \'0\',
  `size` int(10) unsigned NOT NULL DEFAULT \'0\',
  `views` int(10) unsigned NOT NULL DEFAULT \'0\',
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  `date_added` int(14) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `podcastindex` (`title`,`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_medium` (`id`, `title`, `youtube_id`, `author`, `description`, `source`, `thumbnail`, `template_id`, `width`, `height`, `playlenght`, `size`, `views`, `status`, `date_added`) VALUES (\'1\', \'Websites kopieren mit Cloudrexx\', \'4fTlBeSjtWA\', \'Cloudrexx AG\', \'Mit der neuen Webseite Kopierfunktion ist es möglich, vorkonfigurierte Webseiten zu kopieren. Loggen Sie sich mit Ihrem Kundenkonto auf my.cloudrexx.com ein und klicken Sie auf “Übersicht”. Dort finden Sie die neue Funktion.\', \'//youtube.com/embed/4fTlBeSjtWA\', \'/images/Podcast/youtube_thumbnails/youtube_4fTlBeSjtWA.jpg\', \'101\', \'425\', \'350\', \'0\', \'0\', \'1\', \'1\', \'1516187698\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_medium` (`id`, `title`, `youtube_id`, `author`, `description`, `source`, `thumbnail`, `template_id`, `width`, `height`, `playlenght`, `size`, `views`, `status`, `date_added`) VALUES (\'2\', \'Webdesign mit Cloudrexx einfach gemacht\', \'XbixGuJWNKI\', \'Cloudrexx AG\', \'Der Template Editor ist die neueste Innovation in unserer Cloud-CXM-Lösung Cloudrexx. Das neue Feature vereinfacht die Bearbeitung von Templates ohne Bearbeitung des Codes.\', \'//youtube.com/embed/XbixGuJWNKI\', \'/images/Podcast/youtube_thumbnails/youtube_XbixGuJWNKI.jpg\', \'101\', \'425\', \'350\', \'0\', \'0\', \'1\', \'1\', \'1516187725\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_medium` (`id`, `title`, `youtube_id`, `author`, `description`, `source`, `thumbnail`, `template_id`, `width`, `height`, `playlenght`, `size`, `views`, `status`, `date_added`) VALUES (\'3\', \'Wie Sie Ihr erstes Cloudrexx-Projekt starten\', \'DlE8M8gofXA\', \'Cloudrexx AG\', \'Ein Cloudrexx Projekt zu starten ist ganz einfach. Wir zeigen in 65 Sekunden wie&#39;s geht.\', \'//youtube.com/embed/DlE8M8gofXA\', \'/images/Podcast/youtube_thumbnails/youtube_DlE8M8gofXA.jpg\', \'101\', \'425\', \'350\', \'0\', \'0\', \'1\', \'1\', \'1516187740\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_medium` (`id`, `title`, `youtube_id`, `author`, `description`, `source`, `thumbnail`, `template_id`, `width`, `height`, `playlenght`, `size`, `views`, `status`, `date_added`) VALUES (\'4\', \'So werden Sie Cloudrexx Partner\', \'vbUqYlkcDcM\', \'Cloudrexx AG\', \'Das Cloudrexx Partnerprogramm ist mehrstufig und bietet viel Flexibilität. Wir zeigen Ihnen hier, wie Sie Partner werden können.\', \'//youtube.com/embed/vbUqYlkcDcM\', \'/images/Podcast/youtube_thumbnails/youtube_vbUqYlkcDcM.jpg\', \'101\', \'425\', \'350\', \'0\', \'0\', \'1\', \'1\', \'1516187758\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_medium` (`id`, `title`, `youtube_id`, `author`, `description`, `source`, `thumbnail`, `template_id`, `width`, `height`, `playlenght`, `size`, `views`, `status`, `date_added`) VALUES (\'5\', \'Warum Cloudrexx?\', \'OI5VqEDkJD4\', \'Cloudrexx AG\', \'In unserem ersten Cloudrexx-Erklärvideo erläutern wir, welche Argumente dafür sprechen, dass Webagenturen Webprojekte mit einem Cloud-Tool wie Cloudrexx umsetzen.\', \'//youtube.com/embed/OI5VqEDkJD4\', \'/images/Podcast/youtube_thumbnails/youtube_OI5VqEDkJD4.jpg\', \'101\', \'425\', \'350\', \'0\', \'0\', \'1\', \'1\', \'1516187779\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_podcast_rel_category_lang` (
  `category_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `lang_id` int(10) unsigned NOT NULL DEFAULT \'0\'
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_category_lang` (`category_id`, `lang_id`) VALUES (\'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_category_lang` (`category_id`, `lang_id`) VALUES (\'1\', \'2\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_podcast_rel_medium_category` (
  `medium_id` int(10) unsigned NOT NULL DEFAULT \'0\',
  `category_id` int(10) unsigned NOT NULL DEFAULT \'0\'
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_medium_category` (`medium_id`, `category_id`) VALUES (\'1\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_medium_category` (`medium_id`, `category_id`) VALUES (\'2\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_medium_category` (`medium_id`, `category_id`) VALUES (\'3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_medium_category` (`medium_id`, `category_id`) VALUES (\'4\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_rel_medium_category` (`medium_id`, `category_id`) VALUES (\'5\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_podcast_settings` (
  `setid` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `setname` varchar(250) NOT NULL DEFAULT \'\',
  `setvalue` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`setid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'3\', \'default_width\', \'320\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'4\', \'default_height\', \'240\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'5\', \'feed_title\', \'Cloudrexx Demo-Seite Neuste Videos\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'6\', \'feed_description\', \'Neuste Videos\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'7\', \'feed_image\', \'\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'8\', \'latest_media_count\', \'4\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'9\', \'latest_media_categories\', \'1,4,2,5,3\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'10\', \'thumb_max_size\', \'140\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'11\', \'thumb_max_size_homecontent\', \'90\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_settings` (`setid`, `setname`, `setvalue`, `status`) VALUES (\'12\', \'auto_validate\', \'0\', \'1\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_podcast_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL DEFAULT \'\',
  `template` text NOT NULL,
  `extensions` varchar(255) NOT NULL DEFAULT \'\',
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'50\', \'Video für Windows (Windows Media Player Plug-in)\', \'<object id=\\\"podcastPlayer\\\" classid=\\\"clsid:6BF52A52-394A-11d3-B153-00C04F79FAA6\\\" standby=\\\"Loading Windows Media Player components...\\\" type=\\\"application/x-oleobject\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<embed type=\\\"application/x-mplayer2\\\" name=\\\"podcastPlayer\\\" showstatusbar=\\\"1\\\" src=\\\"[[MEDIUM_URL]]\\\" autostart=\\\"1\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]+70\\\" />\\r\\n<param name=\\\"URL\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"BufferingTime\\\" value=\\\"60\\\" />\\r\\n<param name=\\\"AllowChangeDisplaySize\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"AutoStart\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"EnableContextMenu\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"stretchToFit\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"ShowControls\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"ShowTracker\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"uiMode\\\" value=\\\"full\\\" />\\r\\n</object>\', \'avi, wmv\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'51\', \'RealMedia (RealMedia Player Plug-in)\', \'<object id=\\\"podcastPlayer1\\\" classid=\\\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\">\\r\\n<param name=\\\"controls\\\" value=\\\"all\\\">\\r\\n<param name=\\\"autostart\\\" value=\\\"true\\\">\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" autostart=\\\"true\\\" type=\\\"video/x-pn-realvideo\\\" console=\\\"video1\\\" controls=\\\"All\\\" nojava=\\\"true\\\"></embed>\\r\\n</object>\', \'ram, rpm\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'52\', \'QuickTime Film (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/quicktime\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/quicktime\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'mov, qt, mqv\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'53\', \'CAF-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-caf\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-caf\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'caf\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'54\', \'AAC-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-aac\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-aac\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'aac, adts\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'55\', \'AMR-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/AMR\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/AMR\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'amr\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'56\', \'GSM-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-gsm\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-gsm\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'gsm\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'57\', \'QUALCOMM PureVoice Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/vnd.qcelp\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/vnd.qcelp\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'qcp\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'58\', \'MIDI (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-midi\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-midi\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'mid, midi, smf, kar\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'59\', \'uLaw/AU-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/basic\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/basic\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'au, snd, ulw\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'60\', \'AIFF-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-aiff\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-aiff\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'aiff, aif, aifc, cdda\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'61\', \'WAVE-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-wav\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-wav\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'wav, bwf\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'62\', \'Video für Windows (AVI) (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/x-msvideo\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/x-msvideo\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'avi, vfw\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'63\', \'AutoDesk Animator (FLC) (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/flc\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/flc\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'flc, fli, cel\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'64\', \'Digitales Video (DV) (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/x-dv\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/x-dv\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'dv, dif\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'65\', \'SDP-Stream-Beschreibung (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"application/x-sdp\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"application/x-sdp\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'sdp\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'66\', \'RTSP-Stream-Beschreibung (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"application/x-rtsp\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"application/x-rtsp\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'rtsp, rts\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'67\', \'MP3-Wiedergabeliste (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-mpegurl\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-mpegurl\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'m3u, m3url\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'68\', \'MPEG-Medien (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/x-mpeg\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/x-mpeg\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'mpeg, mpg, m1s, m1v, m1a, m75, m15, mp2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'69\', \'3GPP-Medien (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/3gpp\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/3gpp\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'3gp, 3gpp\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'70\', \'3GPP2-Medien (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/3gpp2\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/3gpp2\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'3g2, 3gp2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'71\', \'SD-Video (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/sd-video\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/sd-video\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'sdv\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'72\', \'AMC-Medien (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"application/x-mpeg\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"application/x-mpeg\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'amc\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'73\', \'MPEG-4-Medien (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/mp4\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/mp4\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'mp4\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'74\', \'AAC-Audiodatei (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-m4a\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-m4a\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'m4a\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'75\', \'AAC-Audio (geschützt) (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-m4p\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-m4p\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'m4p\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'76\', \'ACC-Audiobuch (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-m4b\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-m4b\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'m4b\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'77\', \'Video (geschützt) (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"video/x-m4v\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"video/x-m4v\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'m4v\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'78\', \'MP3-Audio (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-mpeg\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-mpeg\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'mp3, swa\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'79\', \'Sound Designer II (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-sd2\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"audio/x-sd2\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'sd2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'80\', \'BMP-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-bmp\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-bmp\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'bmp, dib\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'81\', \'MacPaint Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-macpaint\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-macpaint\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'pntg, pnt, mac\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'82\', \'PICT-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-pict\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-pict\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'pict, pic, pct\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'83\', \'PNG-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-png\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-png\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'png\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'84\', \'QuickTime Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-quicktime\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-quicktime\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'qtif, qti\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'85\', \'SGI-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-sgi\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-sgi\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'sgi, rgb\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'86\', \'TGA-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-targa\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-targa\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'targa, tga\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'87\', \'TIFF-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-tiff\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-tiff\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'tif, tiff\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'88\', \'Photoshop-Bild (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/x-photoshop\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/x-photoshop\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'psd\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'89\', \'JPEG2000 image (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"image/jp2\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"image/jp2\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'jp2\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'90\', \'SMIL 1.0 (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"application/smil\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"application/smil\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'smi, sml, smil\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'91\', \'Flash-Medien (QuckTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"application/x-shockwave-flash\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"application/x-shockwave-flash\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'swf\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'92\', \'QuickTime HTML (QHTML) (QuickTime Plug-in)\', \'<object classid=\\\"clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B\\\" codebase=\\\"http://www.apple.com/qtactivex/qtplugin.cab\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"autoplay\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"controller\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"target\\\" value=\\\"myself\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"text/x-html-insertion\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" type=\\\"text/x-html-insertion\\\" pluginspage=\\\"http://www.apple.com/quicktime/download/\\\" autoplay=\\\"true\\\" controller=\\\"true\\\" target=\\\"myself\\\" />\\r\\n</object>\', \'qht, qhtm\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'93\', \'MP3-Audio (RealPlayer Player)\', \'<object id=\\\"videoplayer1\\\" classid=\\\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"controls\\\" value=\\\"all\\\" />\\r\\n<param name=\\\"autostart\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-mpeg\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" autostart=\\\"true\\\" type=\\\"audio/x-mpeg\\\" console=\\\"video1\\\" controls=\\\"All\\\" nojava=\\\"true\\\"></embed>\\r\\n</object>\', \'mp3\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'94\', \'MP3-Wiedergabeliste (RealPlayer Plug-in)\', \'<object id=\\\"videoplayer1\\\" classid=\\\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"controls\\\" value=\\\"all\\\" />\\r\\n<param name=\\\"autostart\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-mpegurl\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" autostart=\\\"true\\\" type=\\\"audio/x-mpegurl\\\" console=\\\"video1\\\" controls=\\\"All\\\" nojava=\\\"true\\\"></embed>\\r\\n</object>\', \'m3u, m3url\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'95\', \'WAVE-Audio (RealPlayer Plug-in)\', \'<object id=\\\"videoplayer1\\\" classid=\\\"clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\">\\r\\n<param name=\\\"src\\\" value=\\\"[[MEDIUM_URL]]\\\" />\\r\\n<param name=\\\"controls\\\" value=\\\"all\\\" />\\r\\n<param name=\\\"autostart\\\" value=\\\"true\\\" />\\r\\n<param name=\\\"type\\\" value=\\\"audio/x-wav\\\" />\\r\\n<embed src=\\\"[[MEDIUM_URL]]\\\" width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" autostart=\\\"true\\\" type=\\\"audio/x-wav\\\" console=\\\"video1\\\" controls=\\\"All\\\" nojava=\\\"true\\\"></embed>\\r\\n</object>\', \'wav\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'100\', \'Flash-Video (Flash Video File)\', \'<object\\r\\n  type=\\\"application/x-shockwave-flash\\\"\\r\\n  data=\\\"[[ASCMS_PATH_OFFSET]]/modules/podcast/lib/FlowPlayer.swf\\\" \\r\\n	width=\\\"[[MEDIUM_WIDTH]]\\\"\\r\\n  height=\\\"[[MEDIUM_HEIGHT]]\\\"\\r\\n  id=\\\"FlowPlayer\\\">\\r\\n    <param name=\\\"movie\\\" value=\\\"[[ASCMS_PATH_OFFSET]]/modules/podcast/lib/FlowPlayer.swf\\\" />\\r\\n    <param name=\\\"quality\\\" value=\\\"high\\\" />\\r\\n    <param name=\\\"scale\\\" value=\\\"noScale\\\" />\\r\\n    <param name=\\\"allowfullscreen\\\" value=\\\"true\\\" />\\r\\n    <param name=\\\"allowScriptAccess\\\" value=\\\"always\\\" />\\r\\n    <param name=\\\"allownetworking\\\" value=\\\"all\\\" />\\r\\n    <param name=\\\"flashvars\\\" value=\\\"config={\\r\\n      autoPlay:true,\\r\\n      loop: false,\\r\\n      autoRewind: true,\\r\\n      videoFile: \\\'[[MEDIUM_URL]]\\\',\\r\\n      fullScreenScriptURL:\\\'[[ASCMS_PATH_OFFSET]]/modules/podcast/lib/fullscreen.js\\\',\\r\\n      initialScale:\\\'orig\\\'}\\\" />\\r\\n</object>\', \'flv\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_podcast_template` (`id`, `description`, `template`, `extensions`) VALUES (\'101\', \'YouTube Video\', \'<iframe width=\\\"[[MEDIUM_WIDTH]]\\\" height=\\\"[[MEDIUM_HEIGHT]]\\\" src=\\\"[[MEDIUM_URL]]\\\" frameborder=\\\"0\\\" allowfullscreen></iframe>\', \'swf\')');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}