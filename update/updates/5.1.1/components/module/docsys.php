<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */


function _docsysUpdate()
{
    global $objDatabase, $_ARRAYLANG;

    try{
        \Cx\Lib\UpdateUtil::table(
            DBPREFIX.'module_docsys_entry_category',
            array(
                'entry'      => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true),
                'category'   => array('type' => 'INT(10)', 'unsigned' => true, 'notnull' => true, 'default' => '0', 'primary' => true)
            )
        );

        if (\Cx\Lib\UpdateUtil::column_exist(DBPREFIX . 'module_docsys', 'catid')) {
            $query = "SELECT `id`, `catid` FROM `".DBPREFIX."module_docsys`";
            $objResult = $objDatabase->Execute($query);
            if ($objResult !== false) {
                while (!$objResult->EOF) {
                    $query = "SELECT 1 FROM `".DBPREFIX."module_docsys_entry_category` WHERE `entry` = ".$objResult->fields['id']." AND `category` = ".$objResult->fields['catid'];
                    $objCheck = $objDatabase->SelectLimit($query, 1);
                    if ($objCheck !== false) {
                        if ($objCheck->RecordCount() == 0) {
                            $query = "INSERT INTO `".DBPREFIX."module_docsys_entry_category` (`entry`, `category`) VALUES ('".$objResult->fields['id']."', '".$objResult->fields['catid']."')";
                            if ($objDatabase->Execute($query) === false) {
                                return _databaseError($query, $objDatabase->ErrorMsg());
                            }
                        }
                    } else {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }

                    $objResult->MoveNext();
                }
            } else {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }

        // Fix some fuckup that UpdatUtil can't do.. make sure that "id" is unique before attempting
        // to make it a primary key
        $duplicateIDs_sql = "SELECT COUNT(*) as c, id FROM ".DBPREFIX."module_docsys GROUP BY id HAVING c > 1";
        $duplicateIDs = $objDatabase->Execute($duplicateIDs_sql);
        if ($duplicateIDs === false) {
            return _databaseError($duplicateIDs_sql, $objDatabase->ErrorMsg());
        }
        $fix_queries = array();
        while (!$duplicateIDs->EOF) {
            $id    = $duplicateIDs->fields['id'];
            $entries_sql = "SELECT * FROM ".DBPREFIX."module_docsys WHERE id = $id";
            $entries     = $objDatabase->Execute($entries_sql);
            if ($entries === false) {
                return _databaseError($entries_sql, $objDatabase->ErrorMsg());
            }
            // NOW: put them all in an array, DELETE them and then re-INSERT them
            // without id. the auto_increment will take care of the rest. The first one we
            // re-insert can keep it's id.
            $entries_sql = "SELECT * FROM ".DBPREFIX."module_docsys WHERE id = $id";
            $entries     = $objDatabase->Execute($entries_sql);
            if ($entries === false) {
                return _databaseError($entries_sql, $objDatabase->ErrorMsg());
            }
            $is_first = true;
            $fix_queries[] = "DELETE FROM ".DBPREFIX."module_docsys WHERE id = $id";
            while (!$entries->EOF) {
                $pairs = array();
                foreach ($entries->fields as $k => $v) {
                    // only first may keep it's id
                    if ($k == 'id' and !$is_first) {
                            continue;
                    }
                    $pairs[] = "$k = '" . addslashes($v) . "'";
                }
                $fix_queries[] = "INSERT INTO ".DBPREFIX."module_docsys SET ".join(', ', $pairs);

                $is_first = false;
                $entries->MoveNext();
            }
            $duplicateIDs->MoveNext();
        }

        // Now run all of these queries. basically DELETE, INSERT,INSERT, DELETE,INSERT...
        foreach ($fix_queries as $insert_query) {
            if ($objDatabase->Execute($insert_query) === false) {
                return _databaseError($insert_query, $objDatabase->ErrorMsg());
            }
        }




        // alter column startdate from date to int
        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_docsys');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_docsys'));
            return false;
        }
        if (isset($arrColumns['STARTDATE'])) {
            if ($arrColumns['STARTDATE']->type == 'date') {
                if (!isset($arrColumns['STARTDATE_NEW'])) {
                    $query = 'ALTER TABLE `'.DBPREFIX.'module_docsys` ADD `startdate_new` INT(14) UNSIGNED NOT NULL DEFAULT \'0\' AFTER `startdate`';
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }

                $query = 'UPDATE `'.DBPREFIX.'module_docsys` SET `startdate_new` = UNIX_TIMESTAMP(`startdate`) WHERE `startdate` != \'0000-00-00\'';
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }

                $query = 'ALTER TABLE `'.DBPREFIX.'module_docsys` DROP `startdate`';
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }
        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_docsys');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_docsys'));
            return false;
        }
        if (!isset($arrColumns['STARTDATE'])) {
            $query = 'ALTER TABLE `'.DBPREFIX.'module_docsys` CHANGE `startdate_new` `startdate` INT(14) UNSIGNED NOT NULL DEFAULT \'0\'';
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        // alter column enddate from date to int
        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_docsys');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_docsys'));
            return false;
        }
        if (isset($arrColumns['ENDDATE'])) {
            if ($arrColumns['ENDDATE']->type == 'date') {
                if (!isset($arrColumns['ENDDATE_NEW'])) {
                    $query = 'ALTER TABLE `'.DBPREFIX.'module_docsys` ADD `enddate_new` INT(14) UNSIGNED NOT NULL DEFAULT \'0\' AFTER `enddate`';
                    if ($objDatabase->Execute($query) === false) {
                        return _databaseError($query, $objDatabase->ErrorMsg());
                    }
                }

                $query = 'UPDATE `'.DBPREFIX.'module_docsys` SET `enddate_new` = UNIX_TIMESTAMP(`enddate`) WHERE `enddate` != \'0000-00-00\'';
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }

                $query = 'ALTER TABLE `'.DBPREFIX.'module_docsys` DROP `enddate`';
                if ($objDatabase->Execute($query) === false) {
                    return _databaseError($query, $objDatabase->ErrorMsg());
                }
            }
        }
        $arrColumns = $objDatabase->MetaColumns(DBPREFIX.'module_docsys');
        if ($arrColumns === false) {
            setUpdateMsg(sprintf($_ARRAYLANG['TXT_UNABLE_GETTING_DATABASE_TABLE_STRUCTURE'], DBPREFIX.'module_docsys'));
            return false;
        }
        if (!isset($arrColumns['ENDDATE'])) {
            $query = 'ALTER TABLE `'.DBPREFIX.'module_docsys` CHANGE `enddate_new` `enddate` INT(14) UNSIGNED NOT NULL DEFAULT \'0\'';
            if ($objDatabase->Execute($query) === false) {
                return _databaseError($query, $objDatabase->ErrorMsg());
            }
        }


        \Cx\Lib\UpdateUtil::table(
            DBPREFIX . 'module_docsys',
            array(
                'id'        => array('type' => 'INT(6)', 'unsigned' => true, 'auto_increment' => true, 'primary' => true),
                'date'      => array('type' => 'INT(14)', 'notnull' => false),
                'title'     => array('type' => 'VARCHAR(250)'),
                'author'    => array('type' => 'VARCHAR(150)'),
                'text'      => array('type' => 'MEDIUMTEXT', 'notnull' => true),
                'source'    => array('type' => 'VARCHAR(250)'),
                'url1'      => array('type' => 'VARCHAR(250)'),
                'url2'      => array('type' => 'VARCHAR(250)'),
                'lang'      => array('type' => 'INT(2)', 'unsigned' => true, 'default' => '0'),
                'userid'    => array('type' => 'INT(6)', 'unsigned' => true, 'default' => '0'),
                'startdate' => array('type' => 'INT(14)', 'unsigned' => true, 'default' => '0'),
                'enddate'   => array('type' => 'INT(14)', 'unsigned' => true, 'default' => '0'),
                'status'    => array('type' => 'TINYINT(4)', 'default' => '1'),
                'changelog' => array('type' => 'INT(14)', 'default' => '0')
            ),
            array(
                'newsindex' => array('fields' => array('title', 'text'), 'type' => 'FULLTEXT')
            )
        );
    }
    catch (\Cx\Lib\UpdateException $e) {
        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
    }

    // migrate path to images and media
    $pathsToMigrate = \Cx\Lib\UpdateUtil::getMigrationPaths();
    try {
        foreach ($pathsToMigrate as $oldPath => $newPath) {
            \Cx\Lib\UpdateUtil::migratePath(
                '`' . DBPREFIX . 'module_docsys`',
                '`text`',
                $oldPath,
                $newPath
            );
        }
    } catch (\Cx\Lib\Update_DatabaseException $e) {
        \DBG::log($e->getMessage());
        return false;
    }

    return true;
}
function _docsysInstall() {
	global $_DBCONFIG;
	try {
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_docsys` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(14) DEFAULT NULL,
  `title` varchar(250) NOT NULL DEFAULT \'\',
  `author` varchar(150) NOT NULL DEFAULT \'\',
  `text` mediumtext NOT NULL,
  `source` varchar(250) NOT NULL DEFAULT \'\',
  `url1` varchar(250) NOT NULL DEFAULT \'\',
  `url2` varchar(250) NOT NULL DEFAULT \'\',
  `lang` int(2) unsigned NOT NULL DEFAULT \'0\',
  `userid` int(6) unsigned NOT NULL DEFAULT \'0\',
  `startdate` int(14) unsigned NOT NULL DEFAULT \'0\',
  `enddate` int(14) unsigned NOT NULL DEFAULT \'0\',
  `status` tinyint(4) NOT NULL DEFAULT \'1\',
  `changelog` int(14) NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `newsindex` (`title`,`text`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_docsys` (`id`, `date`, `title`, `author`, `text`, `source`, `url1`, `url2`, `lang`, `userid`, `startdate`, `enddate`, `status`, `changelog`) VALUES (\'1\', \'1292236792\', \'Google Sitemaps\', \'system\', \'<div><strong><font size=\\\"2\\\">Nutzen und Funktion der Google Sitemaps</font></strong></div>\\r\\n<div>&nbsp;</div>\\r\\n<div><font size=\\\"2\\\">Google Sitemaps sind ein neuer Dienst, welcher von der Suchmaschine Google angeboten. Der Service ist momentan noch in der BETA-Phase, hat sich im Bereich der Suchmaschinen Optimierung bereits etabliert.</font></div>\\r\\n<div>&nbsp;</div>\\r\\n<div><font size=\\\"2\\\">Das Sitemapprotokoll dient dazu, Suchmaschinen die URLs auf Ihren Websites zu melden, die zum Durchsuchen verf&uuml;gbar sind. In ihrer einfachsten Form ist eine Sitemap, die das Sitemapprotokoll verwendet, eine XML-Datei, in der URLs f&uuml;r eine Website aufgelistet werden. Beachten Sie jedoch, dass das Sitemapprotokoll nur eine Erg&auml;nzung, keinen Ersatz f&uuml;r die crawlerbasierten Verfahren darstellt, die von Suchmaschinen bereits zur Erkundung von URLs genutzt werden. Indem Sie eine Sitemap (oder mehrere) bei einer Suchmaschine einreichen, tragen Sie dazu bei, dass die Crawler der Suchmaschine bessere Ergebnisse beim Durchsuchen Ihrer Website erzielen.</font></div>\\r\\n<div>&nbsp;</div>\\r\\n<div><font size=\\\"2\\\">Cloudrexx erstellt beim &Auml;ndern und Erstellen Ihres Seiteninhaltes automatisch ein Sitemap-File f&uuml;r Sie. Damit dieses File von Google auch gefunden wird, m&uuml;ssen Sie sich jedoch manuell auf der Google-Seite f&uuml;r den Dienst registrieren. Dies k&ouml;nnen Sie auf folgender Seite machen: </font><a href=\\\"https://www.google.com/webmasters/sitemaps/login?hl=de\\\"><font size=\\\"2\\\">https://www.google.com/webmasters/sitemaps/login?hl=de</font></a></div>\\r\\n<div>&nbsp;</div>\\r\\n<div>&nbsp;</div>\\r\\n<div><font size=\\\"2\\\">Sie k&ouml;nnen die automatische Erstellung der Google-Sitemap in folgendem Menu aktivieren:</font></div>\\r\\n<ul>\\r\\n    <li>Administration &gt; Grundeinstellung &gt; System &gt; Subsysteme</li>\\r\\n</ul>\\r\\n<div><font size=\\\"2\\\">Sobald Sie den Menupunkt auf aktiviert gesetzt haben, wird bei jeder &Auml;nderung Ihres Seiteninhalts automatisch eine Sitemap generiert. Diese befindet sich im Hauptverzeichnis Ihres Webservers und tr&auml;gt den Namen <strong>sitemap.xml</strong>.</font></div>\\r\\n<div>&nbsp;</div>\\r\\n<div>&nbsp;</div>\\r\\n<div><font size=\\\"2\\\">Beispiel: </font><a href=\\\"http://www.beispiel.ch/\\\"><font size=\\\"2\\\">http://www.beispiel.ch</font></a></div>\\r\\n<div><font size=\\\"2\\\">Pfad zur Sitemap-Datei: </font><a href=\\\"http://www.beispiel.ch/sitemap.xml\\\"><font size=\\\"2\\\">http://www.beispiel.ch/sitemap.xml</font></a></div>\\r\\n<div>&nbsp;</div>\\r\\n<div><font size=\\\"2\\\">Beachten Sie bitte, dass zur fehlerfreien Erstellung mindestens die Berechtigungen 666 auf die Datei sitemap.xml gesetzt sein m&uuml;ssen. Ansonsten kann die Datei nicht geschrieben werden. Ausserdem muss unbedingt eine Datei mit dem Namen sitemap.xml vorhanden sein, da wir aus Sicherheitsgr&uuml;nden im Hauptverzeichnis keine neuen Dateien erzeugen k&ouml;nnen.</font></div>\\r\\n<div>&nbsp;</div>\', \'\', \'\', \'\', \'1\', \'1\', \'0\', \'0\', \'1\', \'1235730488\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_docsys_categories` (
  `catid` int(2) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT \'\',
  `lang` int(2) unsigned NOT NULL DEFAULT \'1\',
  `sort_style` enum(\'alpha\',\'date\',\'date_alpha\') NOT NULL DEFAULT \'alpha\',
  PRIMARY KEY (`catid`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_docsys_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'1\', \'Anleitungen\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_docsys_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'2\', \'Produkt Infos\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('INSERT INTO `'.DBPREFIX.'module_docsys_categories` (`catid`, `name`, `lang`, `sort_style`) VALUES (\'3\', \'Beschreibungen\', \'1\', \'alpha\')');
		\Cx\Lib\UpdateUtil::sql('CREATE TABLE `'.DBPREFIX.'module_docsys_entry_category` (
  `entry` int(10) unsigned NOT NULL DEFAULT \'0\',
  `category` int(10) unsigned NOT NULL DEFAULT \'0\',
  PRIMARY KEY (`entry`,`category`)
) ENGINE=InnoDB DEFAULT CHARACTER SET ' . $_DBCONFIG['charset'] . ' COLLATE ' . $_DBCONFIG['collation'] . ' COMMENT=\'cx3upgrade\'');
	} catch (\Cx\Lib\UpdateException $e) {
                        return \Cx\Lib\UpdateUtil::DefaultActionHandler($e);
                        }
}