--------------------------------------------------------------------------------
Cloudrexx 5.1.1 Download Update für ab Version 1.2.0 von Contrexx/Cloudrexx
Release Datum: 26.03.2021
https://www.cloudrexx.com
--------------------------------------------------------------------------------
Inhaltsverzeichnis:

1__________Vorwort
2__________Update Vorgang
2.1________Vorbereitungen (WICHTIG)
2.2________Update durchführen
2.2.1______Dateien entpacken
2.2.2______PHP-Version wechseln
2.2.3______Update starten
2.2.4______Update wieder entfernen
3__________Konfiguration der neuen Version
4__________SUPPORT


--------------------------------------------------------------------------------
1) Vorwort
--------------------------------------------------------------------------------
    Diese Anleitung hilft Ihnen ein bestehendes Contrexx/Cloudrexx ab der
    Version 1.2.0 auf die Version 5.1.1 von Cloudrexx zu aktualisieren.

--------------------------------------------------------------------------------
2) Update Vorgang
--------------------------------------------------------------------------------
2.1) Vorbereitungen (WICHTIG)
--------------------------------------------------------------------------------
    Sie können in den Grundeinstellungen der Administrationskonsole den
    "Seitenstatus" auf "Deaktiviert" setzten, damit während des Updates Ihre
    Besucher nicht auf eine fehlfunktionierende Webseite treffen.

    WICHTIG:
    Erstellen Sie unbedingt ein Backup der Datenbank und des Webverzeichnisses, 
    da das Update-System selber keine Backup-Funktionalität zur Verfügung
    stellt.
    Falls das Update unerwartet abgebrochen wird oder auf Grund eines Fehlers
    nicht zu Ende geführt werden kann, befindet sich die Website-Installation
    anschliessend in einem unbekannten Zustand, welcher sich nur mit
    entsprechendem Know-How und enormen Aufwand reparieren lässt.
    Die Einspielung eines Backups ist in diesem Fall zielführender.

--------------------------------------------------------------------------------
2.2) Update durchführen
--------------------------------------------------------------------------------
2.2.1) Dateien entpacken
--------------------------------------------------------------------------------
    Platzieren Sie das Verzeichnis "update", welches sich im Verzeichnis
    "CMS_FILES" dieses Update-Paketes befindet in das Hauptverzeichnis Ihrer
    Contrexx/Cloudrexx Installation.

--------------------------------------------------------------------------------
2.2.2) PHP-Version wechseln
--------------------------------------------------------------------------------
    Wechseln Sie die eingesetzte PHP-Version auf PHP 7.3 oder höher (max 7.4).
    Beachten Sie dabei folgendes:
    - Die PHP-Erweiterung "intl" muss installiert sein.
    - Als Webserver wird ausschliesslich Apache 2 unterstützt.
    - Als Betriebssystem werden ausschliesslich Linux-Derivate unterstützt.

--------------------------------------------------------------------------------
2.2.3) Update starten
--------------------------------------------------------------------------------
    Rufen Sie nun mit Ihrem Internet Browser die URL
    http://www.IHRE-WEB-ADRESSE.com/update/index.php auf und folgen Sie den
    Instruktionen.
    Abhängig vom Hosting, der Ausgangsversion und von der Grösse der Webseite
    kann das Update zwischen 10 Minuten und mehreren Stunden dauern. Während
    dieser Zeit muss das Browser-Fenster durchgehend geöffnet bleiben.
    
    ACHTUNG:
    Falls das Browser-Fenster nach Initierung des Update-Vorgangs geschlossen
    wird, bevor das Update erfolgreich zu Ende geführt wurde, so befindet sich
    die Website-Installation in einem unbekannten Zustand, welcher sich nur mit
    entsprechendem Know-How und enormen Aufwand reparieren lässt.
    Die Einspielung eines Backups ist in diesem Fall zielführender.

--------------------------------------------------------------------------------
2.2.4) Update wieder entfernen
--------------------------------------------------------------------------------
    Löschen Sie nach erfolgreichem Update-Vorgang das Verzeichnis "/update"
    wieder von Ihrer Website-Installation.

    WICHTIG:
    Falls der Update-Vorgang nicht erfolgreich abgeschlossen werden konnte
    und Sie wünschen Unterstützung durch einen Techniker, so belassen Sie das
    Verzeichnis "/update" auf dem Webspace, damit eine Analyse durchgeführt
    werden kann.

--------------------------------------------------------------------------------
3) Konfiguration der neuen Version
--------------------------------------------------------------------------------
    Nach der Durchführung des Updatevorgangs ist es notwendig, dass die neue
    Version korrekt eingerichtet wird, damit die Website wieder voll
    funktionsfähig ist.
    Eine Aufstellung, inkl. Anleitung der durchzuführenden Anpassungen befindet
    sich unter der folgenden Adresse:

    https://wiki.cloudrexx.com/Version_5.1.1

--------------------------------------------------------------------------------
4) SUPPORT
--------------------------------------------------------------------------------
    Bei Fragen und Problemen stehen Ihnen unter der folgenden Adresse
    eine Auswahl an Hilfeleistungen zur Verf�gung:

    https://support.cloudrexx.com
