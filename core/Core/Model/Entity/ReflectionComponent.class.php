<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Represents an abstraction of a component
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_core
 * @version     3.1.0
 */

namespace Cx\Core\Core\Model\Entity;

/**
 * ReflectionComponentException
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_core
 * @version     3.1.0
 */
class ReflectionComponentException extends \Exception {}

/**
 * Represents an abstraction of a component
 *
 * @copyright   Cloudrexx AG
 * @author      Michael Ritter <michael.ritter@comvation.com>
 * @package     cloudrexx
 * @subpackage  core_core
 * @version     3.1.0
 */
class ReflectionComponent {
    /**
     * List of all available component types
     * @todo Wouldn't it be better to move this to Component class?
     * @var array List of component types
     */
    protected static $componentTypes = array('core', 'core_module', 'module', 'lib');

    /**
     * Name of the component this instance is an abstraction of
     * @var string Component name
     */
    protected $componentName = null;

    /**
     * Type of the component this instance is an abstraction of
     * @var string Component type
     */
    protected $componentType = null;

    /**
     * Fully qualified filename for/of the package file
     * @var string ZIP package filename
     */
    protected $packageFile = null;

    /**
     * Database object
     *
     * @var object
     */
    protected $db = null;

    /**
     * Two different ways to instanciate this are supported:
     * 1. Supply an instance of \Cx\Core\Core\Model\Entity\Component
     * 2. Supply a install package zip filename
     * 3. Supply a component name and type
     * @param mixed $arg1 Either an instance of \Cx\Core\Core\Model\Entity\Component or the name of a component
     * @param string|null $arg2 (only if a component name was supplied as $arg1) Component type (one of core_module, module, core, lib)
     * @throws ReflectionComponentException
     * @throws \BadMethodCallException
     */
    public function __construct($arg1, $arg2 = null) {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $this->db = $cx->getDb()->getAdoDb();

        if (is_a($arg1, 'Cx\Core\Core\Model\Entity\SystemComponent')) {
            $this->componentName = $arg1->getName();
            $this->componentType = $arg1->getType();
            return;
        }
        $arg1Parts = explode('.', $arg1);
        if (file_exists($arg1) && end($arg1Parts) == 'zip') {
            // clean up tmp dir
            \Cx\Lib\FileSystem\FileSystem::delete_folder(ASCMS_APP_CACHE_FOLDER, true);

            // Uncompress package using PCLZip
            $file = new \PclZip($arg1);
            $list = $file->extract(PCLZIP_OPT_PATH, ASCMS_APP_CACHE_FOLDER);

            // Check for meta.yml, if none: throw Exception
            if (!file_exists(ASCMS_APP_CACHE_FOLDER . '/meta.yml')) {
                throw new ReflectionComponentException('This ain\'t no package file: "' . $arg1 . '"');
            }

            // Read meta info
            $metaTypes = array('core'=>'core', 'core_module'=>'system', 'module'=>'application', 'lib'=>'other');
            $yaml = new \Symfony\Component\Yaml\Yaml();
            $content = file_get_contents(ASCMS_APP_CACHE_FOLDER . '/meta.yml');
            $meta = $yaml->parse($content);
            $type = array_key_exists($meta['DlcInfo']['type'], $metaTypes) ? $meta['DlcInfo']['type'] : 'lib';

            // initialize ReflectionComponent
            $this->packageFile = $arg1;
            $this->componentName = $meta['DlcInfo']['name'];
            $this->componentType = $type;
            return;
        } else if (is_string($arg1) && $arg2 && in_array($arg2, self::$componentTypes)) {
            $this->componentName = $arg1;
            $this->componentType = $arg2;

            // look for the valid component name or legacy
            if (!$this->isValidComponentName($this->componentName) && !$this->isValid()) {
                throw new \BadMethodCallException("Provided component name \"{$this->componentName}\" is invalid. Component name must be written in CamelCase notation.");
            }

            return;
        }
        throw new \BadMethodCallException('Pass a component or zip package filename or specify a component name and type');
    }

    /**
     * Check if the provided string is a valid component name
     * @param  string component name
     * @return boolean True if sring $name is a valid component name
     */
    public function isValidComponentName($name) {
        return preg_match('/^([A-Z][a-z0-9]*)+$/', $name);
    }

    /**
     * Returns the components name
     * @return string Component name
     */
    public function getName() {
        return $this->componentName;
    }

    /**
     * Returns the components type
     * @return string Component type
     */
    public function getType() {
        return $this->componentType;
    }

    /**
     * Tells wheter this component is customized or not
     * @return boolean True if customized (and customizings are active)
     */
    protected function isCustomized() {
        $basepath = ASCMS_DOCUMENT_ROOT . SystemComponent::getPathForType($this->componentType);
        $componentPath = $basepath . '/' . $this->componentName;
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $classLoader = $cx->getClassLoader();
        return $classLoader->getFilePath($componentPath) != $componentPath;
    }

    /**
     * Returns wheter this component exists or not in the system
     * Note : It not depends the component type
     *
     * @param boolean $allowCustomizing (optional) Set to false if you want to ignore customizings
     * @return boolean True if it exists, false otherwise
     */
    public function exists($allowCustomizing = true) {
        foreach (self::$componentTypes as $componentType) {
            $basepath      = ASCMS_DOCUMENT_ROOT . \Cx\Core\Core\Model\Entity\SystemComponent::getPathForType($componentType);
            $componentPath = $basepath . '/' . $this->componentName;

            if (!$allowCustomizing) {
                if (file_exists($componentPath)) {
                    return true;
                }
            }
            $cx = \Cx\Core\Core\Controller\Cx::instanciate();
            $classLoader = $cx->getClassLoader();
            if ($classLoader->getFilePath($componentPath)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns wheter this component installed or not
     *
     * @return boolean True if it exists, false otherwise
     */
    public function isInstalled() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();

        $query = '
            SELECT
                `id`
            FROM
                `' . DBPREFIX . 'component`
            WHERE
                `name` = \'' . $this->componentName . '\'
        ';
        $result = $cx->getDb()->getAdoDb()->query($query);
        if ($result && $result->RecordCount()) {
            return true;
        }

        $query = '
            SELECT
                `id`
            FROM
                `' . DBPREFIX . 'modules`
            WHERE
                `name` = \'' . $this->componentName . '\'
        ';
        $result = $cx->getDb()->getAdoDb()->query($query);

        if ($result && $result->RecordCount()) {
            return true;
        }

        return false;
    }

    /**
     * Returns wheter this component is valid or not. A valid component will work as expected
     * @return boolean True if valid, false otherwise
     */
    public function isValid() {
        // file system
        if (!$this->exists()) {
            return false;
        }

        // DB: entry in components or modules
        // DB: entry in backend areas
        // DB: existing page if necessary

        // what else?

        return true;
    }

    /**
     * Tells wheter this is a legacy component or not
     * @return boolean True if its a legacy one, false otherwise
     */
    public function isLegacy() {
        if (!$this->exists()) {
            return false;
        }
        if (file_exists($this->getDirectory() . '/Controller/')) {
            return false;
        }
        return true;
    }

    /**
     * Returns the absolute path to this component's location in the file system
     * @param boolean $allowCustomizing (optional) Set to false if you want to ignore customizings
     * @param boolean $forceCustomized (optional) If true, the directory in customizing folder is returned, default false
     * @return string Path for this component
     */
    public function getDirectory($allowCustomizing = true, $forceCustomized = false) {
        $docRoot = ASCMS_DOCUMENT_ROOT;
        if ($forceCustomized) {
            $allowCustomizing = false;
            $docRoot = ASCMS_CUSTOMIZING_PATH;
        }
        $basepath = $docRoot.SystemComponent::getPathForType($this->componentType);
        $componentPath = $basepath . '/' . $this->componentName;
        if (!$allowCustomizing) {
            return $componentPath;
        }
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $classLoader = $cx->getClassLoader();
        return $classLoader->getFilePath($componentPath);
    }

    /**
     * Return namespace of the component
     *
     * @return string
     */
    public function getNameSpace() {
        return SystemComponent::getBaseNamespaceForType($this->componentType) . '\\' . $this->componentName;
    }

    /**
     * Installs this component from a zip file (if available)
     * @todo DB stuff (structure and data)
     * @todo check dependency versions
     * @todo activate templates
     */
    public function install() {
        // Check (not already installed (different version), all dependencies installed)
        if (!$this->packageFile) {
            throw new SystemComponentException('Package file not available');
        }
        if (!file_exists(ASCMS_APP_CACHE_FOLDER . '/meta.yml')) {
            throw new ReflectionComponentException('Invalid package file');
        }
        if ($this->exists()) {
            throw new SystemComponentException('Component is already installed');
        }

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $websitePath = $cx->getWebsiteDocumentRootPath();

        // Read meta file
        $yaml = new \Symfony\Component\Yaml\Yaml();
        $content = file_get_contents(ASCMS_APP_CACHE_FOLDER . '/meta.yml');
        $meta = $yaml->parse($content);

        // Check dependencies
        echo "Checking  dependencies ... ";
        foreach ($meta['DlcInfo']['dependencies'] as $dependencyInfo) {
            $dependency = new static($dependencyInfo['name'], $dependencyInfo['type']);
            if (!$dependency->exists()) {
                throw new SystemComponentException('Dependency "' . $dependency->getName() . '" not met');
            }
        }
        echo "Done \n";

        // Copy ZIP contents
        echo "Copying files to installation ... ";
        $filesystem = new \Cx\Lib\FileSystem\FileSystem();
        $filesystem->copyDir(
            ASCMS_APP_CACHE_FOLDER . '/DLC_FILES',
            ASCMS_APP_CACHE_FOLDER_WEB_PATH . '/DLC_FILES',
            '',
            $websitePath,
            '',
            '',
            true
        );
        echo "Done \n";

        // Activate (if type is system or application)
        // TODO: templates need to be activated too!
        if ($this->componentType != 'core' && $this->componentType != 'core_module' && $this->componentType != 'module') {
            return;
        }

        // Copy ZIP contents (also copy meta.yml into component folder if type is system or application)
        try {
            $objFile = new \Cx\Lib\FileSystem\File(ASCMS_APP_CACHE_FOLDER . '/meta.yml');
            $objFile->copy($this->getDirectory(false) . '/meta.yml');
        } catch (\Cx\Lib\FileSystem\FileSystemException $e) {
            \DBG::msg($e->getMessage());
        }

        echo "Importing component data (structure & data) ... ";
        if (!file_exists($this->getDirectory(false)."/Model/Yaml")) {
            $this->importStructureFromSql();
        } else {
            $this->createTablesFromYaml();
        }
        $this->importDataFromSql();
        echo "Done \n";

        // Activate this component
        echo "Activating component ... ";
        $this->activate();
        echo "Done \n";
    }

    /**
     * Import table's from the yml files
     */
    protected function createTablesFromYaml()
    {
        $ymlDirectory = $this->getDirectory(false).'/Model/Yaml';

        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $em  = $cx->getDb()->getEntityManager();

        $classes = array();
        foreach (glob($ymlDirectory.'/*.yml') as $yml) {
            $ymlArray  = \Symfony\Component\Yaml\Yaml::parse($yml);
            $classes[] = $em->getClassMetadata(key($ymlArray));
        }

        $scm = new \Doctrine\ORM\Tools\SchemaTool($em);
        $scm->createSchema($classes);
    }

    /**
     * Imports table structure to database from sql dump file
     */
    protected function importStructureFromSql()
    {
        $sqlDump = ASCMS_APP_CACHE_FOLDER . '/DLC_FILES'. SystemComponent::getPathForType($this->componentType) . '/' . $this->componentName . '/Data/Structure.sql';

        $fp = @fopen ($sqlDump, "r");
        if ($fp !== false) {
            while (!feof($fp)) {
                $buffer = fgets($fp);
                if ((substr($buffer,0,1) != "#") && (substr($buffer,0,2) != "--")) {
                    $sqlQuery .= $buffer;
                    if (preg_match("/;[ \t\r\n]*$/", $buffer)) {
                        $sqlQuery = preg_replace('#contrexx_#', DBPREFIX, $sqlQuery, 1);
                        $result = $this->db->Execute($sqlQuery);
                        if ($result === false) {
                            throw new SystemComponentException($sqlQuery .' ('. $this->db->ErrorMsg() .')');
                        }
                        $sqlQuery = '';
                    }
                }
            }
        } else {
            throw new SystemComponentException('File not found : '. $sqlDump);
        }
    }

    /**
     * import component data's to database from sql dump file
     */
    protected function importDataFromSql()
    {
        $sqlDump = ASCMS_APP_CACHE_FOLDER . '/DLC_FILES'. SystemComponent::getPathForType($this->componentType) . '/' . $this->componentName . '/Data/Data.sql';

        if (!file_exists($sqlDump)) {
            return;
        }

        $pattern = '/\s+INTO\s+`?([a-z\\d_]+)`?/i';

        $moduleId = 0;
        $fp = @fopen ($sqlDump, "r");
        if ($fp !== false) {
            while (!feof($fp)) {
                $buffer = fgets($fp);
                if ((substr($buffer,0,1) != "#") && (substr($buffer,0,2) != "--")) {
                    $sqlQuery .= $buffer;
                    if (preg_match("/;[ \t\r\n]*$/", $buffer)) {
                        $sqlQuery = preg_replace('#contrexx_#', DBPREFIX, $sqlQuery, 1);
                        $matches = null;
                        preg_match($pattern, $sqlQuery , $matches , 0);
                        $table = isset($matches[1]) ? $matches[1] : '';

                        switch ($table) {
                             case DBPREFIX.'modules':
                                 $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                 $newModuleId = $this->db->GetOne('SELECT MAX(`id`)+1 FROM `'. DBPREFIX .'modules`');
                                 $replacements = array('id' => $newModuleId);
                                 $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                 break;
                             case DBPREFIX.'component':
                                 $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                 $replacements = array('id' => NULL);
                                 $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                 break;
                             case DBPREFIX.'backend_areas':
                                 $data = $this->getColumnsAndDataFromSql($sqlQuery);
                                 $replacements = array('module_id' => $newModuleId);
                                 $sqlQuery = $this->repalceDataInQuery($table, $data, $replacements);
                                 break;
                             default :
                                 break;
                        }

                        $result = $this->db->Execute($sqlQuery);
                        if ($result === false) {
                            throw new SystemComponentException($sqlQuery .' ('. $this->db->ErrorMsg() .')');
                        }
                        $sqlQuery = '';
                    }
                }
            }
        }
    }

    /**
     * replace data in the existing data by the given replacements
     * and return the sql query
     *
     * @param string $table        Table name
     * @param array  $columns      Columns array
     * @param array  $data         Data array
     * @param array  $replacements Replacement data array
     */
    protected function repalceDataInQuery($table, $data, $replacements)
    {
        $data = array_intersect_key($replacements + $data, $data);

        $sql  = 'INSERT INTO `'.$table.'` ';
        $sql .= "SET \n";

        $firstCol = true;
        foreach($data as $column => $data) {
            $value = is_null($data) ? "NULL" : (is_string($data) ? "'$data'" : $data);

            $sql .= '    '.($firstCol ? '' : ',') ."`$column` = $value\n";
            $firstCol = false;
        }

        return $sql;
    }

    /**
     * parse the mysql query and return the columns and data from the given query.
     *
     * @param string $sqlQuery Mysql query
     *
     * @return array
     */
    public function getColumnsAndDataFromSql($sqlQuery)
    {
        $columnAndData = null;
        preg_match_all('/\((.+?)\)/', $sqlQuery, $columnAndData);
        $columnsString = $columnAndData[1][0];
        $dataString    = $columnAndData[1][1];

        $columns = null;
        preg_match_all('/\`(.*?)\`/', $columnsString, $columns);
        $data = null;
        preg_match_all('/\'(.*?)\'/', $dataString, $data);

        return array_combine($columns[1], $data[1]);
    }

    /**
     * Get the component related tables
     *
     * @return array  component related tables
     */
    protected function getComponentTables() {
        global $_DBCONFIG;

        // load tables
        $tblSyntax = DBPREFIX . $this->componentType . '_' . strtolower($this->componentName);
        $objResult = $this->db->query('SHOW TABLES LIKE "'. $tblSyntax .'_%"');

        $componentTables = array();
        while (!$objResult->EOF) {
            $componentTables[] = $objResult->fields['Tables_in_'. $_DBCONFIG['database'] .' ('. $tblSyntax .'_%)'];
            $objResult->MoveNext();
        }

        return $componentTables;
    }

    /**
     * Removes this component
     *
     * This might not work perfectly for legacy components, since there could
     * be files outside the component's directory!
     * Be sure there is no other component relying on this one!
     * @todo    Apparently unused; drop?
     */
    public function remove() {
        // remove from db
        $this->removeFromDb();

        // if there are no files, quit
        if (!$this->exists()) {
            return;
        }

        // remove from fs
        \Cx\Lib\FileSystem\FileSystem::delete_folder($this->getDirectory(), true);
    }

    /**
     * Activate this component
     *
     * Adds all missing database records, updates where necessary.
     * Recreates all proxies on success.
     * Fails if the component folder doesn't exist.
     * When adding custom components, new module and access IDs are created.
     * Note that most of the methods called within contain "custom" in their
     * names.  These also apply to standard components, but only do standard
     * stuff, and return standard values for them.
     * Standard component IDs are defined by the installer, and should be
     * preserved whenever possible.
     * @return  void
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     * @todo    Backend navigation entry from meta.yml (rxqcmv1)
     * @todo    Pages from meta.yml (rxqcmv1)
     */
    public function activate()
    {
        if (!$this->exists()) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'No folder found'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        $moduleId = $this->addCustomModule();
        $this->addCustomComponent($moduleId);
        $this->addCustomBackendArea($moduleId);
        if ($this->componentType !== 'module') {
            return;
        }
        $this->addCustomPages($moduleId);
        $workbenchComponent = new self('Workbench', 'core_module');
        if (!$workbenchComponent->exists()) {
            return; // The code below MUST NOT run on production
        }
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $em = $cx->getDb()->getEntityManager();
        $proxyFolderPath = $em->getConfiguration()->getProxyDir();
        $metadata = $em->getMetadataFactory()->getAllMetadata();
        $em->getProxyFactory()->generateProxyClasses(
            $metadata, $proxyFolderPath
        );
    }

    /**
     * Add or update a module table record, and return the module ID
     *
     * Existing entries will only have their status and flags reset to defaults.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function addCustomModule()
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $databaseConnection = $cx->getDb()->getAdoDb();
        $moduleId = $this->getNextCustomModuleId();
        $distributor = 'Cloudrexx AG';
        $workbenchComponent = new self('Workbench', 'core_module');
        if ($workbenchComponent->exists()) {
            $workbench = new \Cx\Core_Modules\Workbench\Controller\Workbench();
            $distributor = $workbench->getConfigEntry('distributor');
        }
        $description = 'TXT_' . strtoupper($this->componentType)
            . '_' . strtoupper($this->componentName) . '_DESCRIPTION';
        $isRequired = (int) $this->componentType === 'core';
        $isCore = (int) $this->componentType === 'core'
            || $this->componentType === 'core_module';
        $result = $databaseConnection->query('
            INSERT INTO `' . DBPREFIX . 'modules` (
                `id`, `name`, `distributor`, `description_variable`,
                `status`, `is_required`, `is_core`,
                `is_active`, `is_licensed`
            ) VALUES (
                ?, ?, ?, ?,
                ?, ?, ?,
                ?, ?
            )
            ON DUPLICATE KEY UPDATE
                `status`=?, `is_required`=?, `is_core`=?,
                `is_active`=?, `is_licensed`=?',
            [
                $moduleId, $this->componentName, $distributor, $description,
                'y', $isRequired, $isCore,
                1, 1,
                'y', $isRequired, $isCore,
                1, 1
            ]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to add or update module table entry'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        return $moduleId;
    }

    /**
     * Return the next available ID for a custom module
     *
     * For existing modules, returns their current ID.
     * This may be taken from the modules table, or, if not present there,
     * from the backend areas table.
     * If neither can be found, returns the next available ID greater than
     * or equal to 900.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomModuleId()
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $databaseConnection = $cx->getDb()->getAdoDb();
        $result = $databaseConnection->query('
            SELECT `id`
            FROM `' . DBPREFIX . 'modules`
            WHERE `name`=?',
            [$this->componentName]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query current module ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF) {
            return $result->fields['id'];
        }
        $moduleId = $this->getModuleIdFromBackendArea();
        if ($moduleId) {
            return $moduleId;
        }
        $moduleIdPrevious = 899;
        $result = $databaseConnection->query('
            SELECT MAX(`id`) `id`
            FROM `' . DBPREFIX . 'modules`
            WHERE `id`>?',
            [$moduleIdPrevious]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query available module ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['id']) {
            $moduleIdPrevious = $result->fields['id'];
        }
        return $moduleIdPrevious + 1;
    }

    /**
     * Return the module ID for this component from the backend areas table
     *
     * Returns null if there's no such entry.
     * Mind that {@see deactivate()} won't remove that record, thus allowing
     * to (re-)activate components using the same module ID.
     * Otherwise, standard components would be installed using a custom ID
     * (900 and up).
     * @return  int|null
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getModuleIdFromBackendArea()
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $databaseConnection = $cx->getDb()->getAdoDb();
        $result = $databaseConnection->query('
            SELECT `module_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `uri` LIKE CONCAT("%cmd=", ?, "&%")
            OR `uri` LIKE CONCAT("%cmd=", ?)',
            [
                $this->componentName,
                $this->componentName
            ]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query current module ID from backend area'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if ($result->EOF) {
            return null;
        }
        return $result->fields['module_id'];
    }

    /**
     * Add a custom component table entry
     *
     * This is a noop for legacy components.
     * Doesn't touch existing records.
     * Note:  As the component ID column is AUTO_INCREMENT, and there's no
     * setId() method available on SystemComponent, the ID is forcibly
     * updated to the $moduleId after creation.
     * That SHOULD not be an issue, since there are at most as many records
     * for components as in the module table (legacy).
     * However, any orphan records will inevitably lead to a conflict
     * at some point.
     * @param   int         $moduleId
     * @return  void
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function addCustomComponent($moduleId)
    {
        if ($this->isLegacy()) {
            return;
        }
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $em = $cx->getDb()->getEntityManager();
        $componentRepo = $em->getRepository(
            'Cx\\Core\\Core\\Model\\Entity\\SystemComponent'
        );
        if ($componentRepo->findOneBy(array(
            'name' => $this->componentName,
            'type' => $this->componentType,
        ))) {
            return;
        }
        $component = new SystemComponent();
        $component->setName($this->componentName);
        $component->setType($this->componentType);
        $em->persist($component);
        $em->flush();
        try {
            $em->createQueryBuilder()
                ->update('Cx\\Core\\Core\\Model\\Entity\\SystemComponent', 'c')
                ->set('c.id', $moduleId)
                ->where('c.type=:type')
                ->andWhere('c.name=:name')
                ->getQuery()->execute([
                    'type' => $this->componentType,
                    'name' => $this->componentName
                ]);
        } catch (\Exception $e) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to update ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
    }

    /**
     * Add a backend area table record for any component
     *
     * Searches for the component by its "uri" column.
     * For existing records, updates the module ID only.
     * Adds default values for new ones.
     * @param   int     $moduleId
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function addCustomBackendArea($moduleId)
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $databaseConnection = $cx->getDb()->getAdoDb();
        $result = $databaseConnection->query('
            SELECT `area_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `uri` LIKE CONCAT("%cmd=", ?, "&%")
            OR `uri` LIKE CONCAT("%cmd=", ?)',
            [
                $this->componentName,
                $this->componentName
            ]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query current backend area'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF) {
            $result = $databaseConnection->query('
                UPDATE `'.DBPREFIX.'backend_areas`
                SET `module_id`=?
                WHERE `area_id`=?',
                [
                    $moduleId,
                    $result->fields['area_id']
                ]
            );
            return;
        }
        $parentAreaId = 0;
        if ($this->componentType === 'module') {
            $parentAreaId = 2;
        }
        $ordinal = $this->getNextCustomOrdinal($parentAreaId);
        $accessId = $this->getNextCustomAccessId();
        $areaName = 'TXT_' . strtoupper($this->componentType)
            . '_' . strtoupper($this->componentName);
        $uri = 'index.php?cmd=' . $this->componentName;
        $databaseConnection->query('
            INSERT INTO `'.DBPREFIX.'backend_areas` (
                `parent_area_id`, `type`, `scope`, `area_name`,
                `is_active`, `uri`, `target`,
                `module_id`, `order_id`, `access_id`
            ) VALUES (
                ?, ?, ?, ?,
                ?, ?, ?,
                ?, ?, ?
            )',
            [
                $parentAreaId, 'navigation', 'backend', $areaName,
                (int) ($parentAreaId === 2), $uri, '_self',
                $moduleId, $ordinal, $accessId
            ]
        );
    }

    /**
     * Return the next available Access ID for a custom component
     *
     * Returns an integer greater than or equal to 900.
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomAccessId()
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $databaseConnection = $cx->getDb()->getAdoDb();
        $accessIdPrevious = 899;
        $result = $databaseConnection->query('
            SELECT MAX(`access_id`) `access_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `access_id`>?',
            [$accessIdPrevious]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query backend area access ID'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['access_id']) {
            $accessIdPrevious = $result->fields['access_id'];
        }
        return $accessIdPrevious + 1;
    }

    /**
     * Return the next ordinal for the given parent backend area ID
     *
     * Increases the previously highest ordinal by one, or returns 1 if none.
     * @param   int     $parentAreaId
     * @return  int
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function getNextCustomOrdinal($parentAreaId)
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $databaseConnection = $cx->getDb()->getAdoDb();
        $ordinalPrevious = 0;
        $result = $databaseConnection->query('
            SELECT MAX(`order_id`) `order_id`
            FROM `' . DBPREFIX . 'backend_areas`
            WHERE `parent_area_id`=?',
            [$parentAreaId]
        );
        if (!$result) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query backend area ordinal'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if (!$result->EOF && $result->fields['order_id']) {
            $ordinalPrevious = $result->fields['order_id'];
        }
        return $ordinalPrevious + 1;
    }

    /**
     * Add default Pages for a component
     *
     * Only applies to modules; for other types, this is a noop.
     * Does nothing if any page exists for the module already.
     * If no content is found in the module repository, creates a
     * default application page.
     * @see     loadPagesFromModuleRepository()
     * @param   int     $moduleId
     * @return  void
     */
    protected function addCustomPages($moduleId)
    {
        if ($this->componentType !== 'module') {
            return;
        }
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $em = $cx->getDb()->getEntityManager();
        $pageRepo = $em->getRepository(
            '\Cx\Core\ContentManager\Model\Entity\Page'
        );
        if ($pageRepo->findBy(array(
            'module' => $this->componentName,
            'type' => \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION,
        ))) {
            return;
        }
        if ($this->loadPagesFromModuleRepository($moduleId)) {
            return;
        }
        $nodeRepo = $em->getRepository(
            '\\Cx\\Core\\ContentManager\\Model\\Entity\\Node'
        );
        $newnode = new \Cx\Core\ContentManager\Model\Entity\Node();
        $newnode->setParent($nodeRepo->getRoot());
        $em->persist($newnode);
        $em->flush();
        $nodeRepo->moveDown($newnode, true);
        foreach (\FWLanguage::getActiveFrontendLanguages() as $lang) {
            $type = \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK;
            if ($lang['is_default'] === 'true' || !$lang['fallback']) {
                $type = \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION;
            }
            $page = $pageRepo->createPage(
                $newnode,
                $lang['id'],
                $this->componentName,
                $type,
                $this->componentName,
                '',
                false,
                '{APPLICATION_DATA}'
            );
            $em->persist($page);
        }
        $em->flush();
    }

    /**
     * Creates application Pages from the module repository
     *
     * Returns false if there are no pages in the repository for that module,
     * true otherwise.
     * @param   int     $moduleId
     * @return  bool
     * @throws  \Cx\Core\Core\Controller\ComponentException     on failure
     */
    protected function loadPagesFromModuleRepository($moduleId)
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $em = $cx->getDb()->getEntityManager();
        $nodeRepo = $em->getRepository('\\Cx\\Core\\ContentManager\\Model\\Entity\\Node');
        $pageRepo = $em->getRepository('\\Cx\\Core\\ContentManager\\Model\\Entity\\Page');
        $objResult = $cx->getDb()->getAdoDb()->query('
            SELECT `id`, `moduleid`, `content`, `title`,
                `cmd`, `expertmode`, `parid`, `displaystatus`,
                `username`, `displayorder`
            FROM `' . DBPREFIX . 'module_repository`
            WHERE `moduleid`=' . $moduleId . '
            ORDER BY `parid` ASC'
        );
        if (!$objResult) {
            throw new \Cx\Core\Core\Controller\ComponentException(
                'Failed to query module repository'
                . ' for component "' . $this->componentType
                . '/' . $this->componentName . '"'
            );
        }
        if ($objResult->EOF) {
            return false;
        }
        $paridarray = array();
        while (!$objResult->EOF) {
            $root = false;
            if (isset($paridarray[$objResult->fields['parid']])) {
                $parcat = $paridarray[$objResult->fields['parid']];
            } else {
                $root = true;
                $parcat = $nodeRepo->getRoot();
            }
            $newnode = new \Cx\Core\ContentManager\Model\Entity\Node();
            $newnode->setParent($parcat);
            $em->persist($newnode);
            $em->flush();
            $nodeRepo->moveDown($newnode, true);
            $paridarray[$objResult->fields['id']] = $newnode;
            foreach (\FWLanguage::getActiveFrontendLanguages() as $lang) {
                if ($lang['is_default'] === 'true' || !$lang['fallback']) {
                    $page = $pageRepo->createPage(
                        $newnode,
                        $lang['id'],
                        $objResult->fields['title'],
                        \Cx\Core\ContentManager\Model\Entity\Page::TYPE_APPLICATION,
                        $this->componentName,
                        $objResult->fields['cmd'],
                        !$root && $objResult->fields['displaystatus'],
                        $objResult->fields['content']
                    );
                } else {
                    $page = $pageRepo->createPage(
                        $newnode,
                        $lang['id'],
                        $objResult->fields['title'],
                        \Cx\Core\ContentManager\Model\Entity\Page::TYPE_FALLBACK,
                        $this->componentName,
                        $objResult->fields['cmd'],
                        !$root && $objResult->fields['displaystatus'],
                        ''
                    );
                }
                $em->persist($page);
            }
            $em->flush();
            $objResult->MoveNext();
        }
        return true;
    }

    /**
     * Deactivate this component
     *
     * Removes Pages, as well as the component and module table entries.
     * Doesn't touch the backend area with the access IDs.
     */
    public function deactivate()
    {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();
        $adoDb = $cx->getDb()->getAdoDb();
        $adoDb->execute('
            DELETE FROM `' . DBPREFIX . 'modules`
            WHERE `name`=?',
            [$this->componentName]
        );
        $em = $cx->getDb()->getEntityManager();
        $pageRepo = $em->getRepository(
            'Cx\\Core\\ContentManager\\Model\\Entity\\Page'
        );
        $pages = $pageRepo->findBy(array(
            'module' => $this->componentName,
        ));
        foreach ($pages as $page) {
            $em->remove($page);
        }
        $componentRepo = $em->getRepository(
            'Cx\\Core\\Core\\Model\\Entity\\SystemComponent'
        );
        $component = $componentRepo->findOneBy(
            array(
                'name' => $this->getName(),
                'type' => $this->getType(),
            )
        );
        if ($component) {
            $em->remove($component->getSystemComponent());
        }
        $em->flush();
    }

    /**
     * This completely removes this component from DB
     * @todo Test removing components tables (including doctrine schema)
     * @internal    Currently unused in this class; see {@see remove()}
     *              The only current usage is in Workbench's
     *              ReflectionComponent::internalRelocate()
     */
    protected function removeFromDb() {
        $cx = \Cx\Core\Core\Controller\Cx::instanciate();

        // component
        $em = $cx->getDb()->getEntityManager();
        $componentRepo = $em->getRepository('Cx\\Core\\Core\\Model\\Entity\\SystemComponent');
        $systemComponent = $componentRepo->findOneBy(array(
            'type' => $this->componentType,
            'name' => $this->componentName,
        ));
        if ($systemComponent) {
            $em->remove($systemComponent->getSystemComponent());
            $em->flush();
        }

        // modules (legacy)
        $adoDb = $cx->getDb()->getAdoDb();
        $query = '
            SELECT
                `id`
            FROM
                `' . DBPREFIX . 'modules`
            WHERE
                `name` = \'' . $this->componentName . '\'
        ';
        $res = $adoDb->execute($query);
        $moduleId = $res->fields['id'];

        if (!empty($moduleId)) {
            $query = '
                DELETE FROM
                    `' . DBPREFIX . 'modules`
                WHERE
                    `id` = \'' . $moduleId . '\'
            ';
            $adoDb->execute($query);

            // backend_areas
            $query = '
                DELETE FROM
                    `' . DBPREFIX . 'backend_areas`
                WHERE
                    `module_id` = \'' . $moduleId . '\'
            ';
            $adoDb->execute($query);
        }

        // module tables (LIKE DBPREFIX . strtolower($moduleName)%)
        $query = '
            SHOW TABLES
            LIKE
                \'' . DBPREFIX . 'module_' . strtolower($this->componentName) . '%\'
        ';
        $result = $adoDb->execute($query);
        while (!$result->EOF) {
            $query = '
                DROP TABLE
                    `' . current($result->fields) . '`
            ';
            $adoDb->execute($query);

            $result->MoveNext();
        }


        $query = 'DELETE FROM `'. DBPREFIX .'core_mail_template` WHERE `section` = "'. $this->componentName .'"';
        $adoDb->execute($query);

        $query = 'DELETE FROM `'. DBPREFIX .'core_text` WHERE `section` = "'. $this->componentName .'"';
        $adoDb->execute($query);

        $query = 'DELETE FROM `'. DBPREFIX .'core_setting` WHERE `section` = "'. $this->componentName .'"';
        $adoDb->execute($query);

        $query = 'DELETE FROM `'. DBPREFIX .'settings` WHERE `setname` LIKE "'. $this->componentName .'%"';
        $adoDb->execute($query);

        // pages
        $this->deactivate();
    }
}
