<?php

/**
 * Cloudrexx
 *
 * @link      http://www.cloudrexx.com
 * @copyright Cloudrexx AG 2007-2015
 *
 * According to our dual licensing model, this program can be used either
 * under the terms of the GNU Affero General Public License, version 3,
 * or under a proprietary license.
 *
 * The texts of the GNU Affero General Public License with an additional
 * permission and of our proprietary license can be found at and
 * in the LICENSE file you have received along with this program.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * "Cloudrexx" is a registered trademark of Cloudrexx AG.
 * The licensing of the program under the AGPLv3 does not imply a
 * trademark license. Therefore any rights, title and interest in
 * our trademarks remain entirely with us.
 */

/**
 * Class ThumbnailGenerator
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 * @package     cloudrexx
 * @subpackage  coremodule_mediabrowser
 */

namespace Cx\Core\MediaSource\Model\Entity;

use Cx\Core\Core\Controller\Cx;
use Cx\Model\Base\EntityBase;

/**
 * Class ThumbnailGenerator
 *
 * @copyright   Cloudrexx AG
 * @author      Robin Glauser <robin.glauser@comvation.com>
 */
class ThumbnailGenerator extends EntityBase
{
    /**
     * List with thumbnails
     * @var array
     */
    protected $thumbnails;

    /**
     * @var Cx
     */
    protected $cx;

    /**
     * @var MediaSourceManager
     */
    protected $mediaSourceManager;

    /**
     * ThumbnailGenerator constructor.
     *
     * @param $cx Cx
     * @param $mediaSourceManager MediaSourceManager
     */
    public function __construct($cx,$mediaSourceManager) {
        $this->cx = $cx;
        $this->mediaSourceManager = $mediaSourceManager;
    }

    /**
     * Create the thumbnails for an image file
     *
     * Returns an array of thumbnail image file names, without any folder path.
     * For SVG images, returns an array with the original file name.
     * Example:
     * <code>
     * <?php
     * $im = new ImageManager() // Reuse the instance for efficiency
     * \Cx\Core_Modules\MediaBrowser\Model\FileSystem::createThumbnail(
     *      'files/',
     *      'Django,
     *      'jpg',
     *      $im
     * );
     * ?>
     * </code>
     * @param   string          $path           Virtual or absolute path to the file
     * @param   string          $fileNamePlain  Base file name, without extension
     * @param   string          $fileExtension  File extension
     * @param   \ImageManager   $imageManager
     * @param   bool            $generateThumbnailByRatio
     * @param   bool            $force
     * @return  array                           Thumbnail file names
     */
    public function createThumbnail(
        $path, $fileNamePlain, $fileExtension, \ImageManager $imageManager,
        $generateThumbnailByRatio = false, $force = false
    ) {
        if (preg_match('/^svg$/i', $fileExtension)) {
            // SVG images are their own thumbnail
            return [$fileNamePlain . '.' . $fileExtension];
        }
        $thumbnails = array();
        foreach (
            $this->getThumbnails() as $thumbnail
        ) {
            if ($force) {
                \Cx\Lib\FileSystem\FileSystem::delete_file(
                    MediaSourceManager::getAbsolutePath($path) . '/'
                    . $fileNamePlain . $thumbnail['value'] . '.'
                    . strtolower($fileExtension)
                );
            } elseif (\Cx\Lib\FileSystem\FileSystem::exists(
                MediaSourceManager::getAbsolutePath($path) . '/'
                . $fileNamePlain . $thumbnail['value'] . '.' . strtolower($fileExtension)
            )
            ) {
                $thumbnails[] = $fileNamePlain . $thumbnail['value'] . '.'
                    . strtolower($fileExtension);
                continue;
            }
            if ($imageManager->_createThumb(
                MediaSourceManager::getAbsolutePath($path) . '/',
                '',
                $fileNamePlain . '.' . $fileExtension,
                $thumbnail['size'],
                $thumbnail['quality'],
                $fileNamePlain . $thumbnail['value'] . '.' . strtolower($fileExtension),
                $generateThumbnailByRatio
            )
            ) {
                $thumbnails[] = $fileNamePlain . $thumbnail['value'] . '.'
                    . strtolower($fileExtension);
                continue;
            }
        }
        return $thumbnails;
    }

    /**
     * Create thumbnail images for the given original path
     *
     * Returns the result of calling createThumbnail().
     * @param   string  $filePath
     * @param   bool    $force      Defaults to false
     * @return  array
     */
    public function createThumbnailFromPath($filePath, $force = false) {
        if (
            (
                strpos($filePath, '/') === 0 &&
                strpos($filePath, $this->cx->getWebsitePath()) !== 0
            ) ||
            !file_exists($filePath)
        ) {
            $filePath = $this->cx->getWebsitePath() . $filePath;
        }
        $fileInfo = pathinfo($filePath);
        return $this->createThumbnail(
            $fileInfo['dirname'],
            preg_replace('/\.thumb_[a-z]+/i', '', $fileInfo['filename']),
            $fileInfo['extension'], new \ImageManager(), false, $force
        );
    }

    /**
     * Loads thumbnails from database
     */
    protected function loadThumbnails() {
        $pdo              = \Cx\Core\Core\Controller\Cx::instanciate()->getDb()
            ->getPdoConnection();
        $sth              = $pdo->query(
            'SELECT id, name, size, 100 as quality,
             CONCAT(".thumb_",name) as value FROM  `'
            . DBPREFIX
            . 'settings_thumbnail`'
        );
        $this->thumbnails = $sth->fetchAll();
    }

    /**
     * Get Thumbnails from database
     * @return array
     */
    public function getThumbnails() {
        if (!$this->thumbnails) {
            $this->loadThumbnails();
        }
        return $this->thumbnails;
    }


    /**
     * Return the path of thumbnail images for all known sizes
     *
     * For SVG images, returns the original path for all sizes.
     * @param   string  $path       Folder path
     * @param   string  $filename   File name
     * @param   bool    $create     Create missing thumbnails if true
     * @param   bool    $rebuild    If $create is true, recreate existing ones
     * @return array thumbnail name array
     */
    public function getThumbnailsFromFile($path, $filename, $create = false, $rebuild = false)
    {
        $isVector = (bool) preg_match('/\.svg$/i', $filename);
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $filename  = pathinfo($filename, PATHINFO_FILENAME);
        $this->getThumbnails();
        $websitepath   = \Cx\Core\Core\Controller\Cx::instanciate()->getWebsitePath();
        $thumbnails    = array();
        foreach ($this->thumbnails as $thumbnail) {
            $thumbnails[$thumbnail['size']] = preg_replace(
                '/\.' . $extension . '$/i',
                ($isVector
                    ? '.' . $extension
                    : $thumbnail['value'] . '.' . strtolower($extension)
                ),
                \Cx\Core\Core\Controller\Cx::instanciate()
                    ->getWebsiteOffsetPath()
                    . str_replace(
                        $websitepath, '',
                        rtrim($path, '/') . '/' . $filename . '.' . $extension
                    )
            );
        }
        if ($create && file_exists($websitepath . str_replace($websitepath, '', rtrim($path, '/')) . '/' . $filename . '.' . $extension)) {
            $this->createThumbnailFromPath(rtrim($path, '/') . '/' . $filename . '.' . $extension, $rebuild);
        }
        return $thumbnails;
    }

    /**
     * Return the thumbnail file path of the smallest size
     *
     * Returns the original path for SVG images.
     * @param   string  $file       File path
     * @return  string              Thumbnail file path
     */
    public function getThumbnailFilename($file) {
        // legacy fallback for older calls.
        if (preg_match('/\.thumb$/', $file)) {
            return $file;
        }

        // backup original file path
        $originalFile = $file;

        // if path of $file is absolete, then do verify it.
        // if its invalid, do move it into the websites' data repository
        if (
            strpos($file, '/') === 0 &&
            !MediaSourceManager::isSubdirectory(
                $this->cx->getCodeBasePath(),
                $file
            ) &&
            !MediaSourceManager::isSubdirectory(
                $this->cx->getWebsitePath(),
                $file
            )
        ) {
            $file = $this->cx->getWebsitePath() . $file;
        }

        if (!file_exists($file) && !file_exists($this->cx->getWebsitePath().'/'.ltrim($file,'/')) ) {
            return $originalFile.'.thumb';
        }

        if (!file_exists($file)){
            $file = $this->cx->getWebsitePath().$file;
        }
        if (preg_match('/\.svg$/i', $file)) {
            return $file; // SVG images are their own thumbnail
        }
        // Add missing thumbnails within the website data repository
        if (file_exists($file)
            && MediaSourceManager::isSubdirectory(
                $this->cx->getWebsitePath(),
                $file
            )
        ) {
            $this->createThumbnailFromPath($file);
        }

        // fetch file infos
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $filename  = pathinfo($file, PATHINFO_FILENAME);
        $webpath   = pathinfo($originalFile, PATHINFO_DIRNAME);

        // fetch thumbnail formats
        $this->getThumbnails();

        // use first thumbnail format
        // TODO: there should be an option that lets the admin set which
        // format is the default (CLX-836)
        $thumbnailType = $this->thumbnails[0];

        // generate & return thumbnail filename
        return  preg_replace(
            '/\.' . $extension . '$/i',
            $thumbnailType['value'] . '.' . strtolower($extension),
           $webpath .'/'. $filename . '.' . $extension
        );
    }
}
